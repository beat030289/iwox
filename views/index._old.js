/* System Modules */
global.express = require('express');
global.session = require('express-session');
global.bodyParser = require('body-parser');
global.dateFormat = require('dateformat');
global.ejs = require('ejs');
global.fs = require('fs-extra');
global.os = require("os");
var url = require('url');
var mysql = require('mysql');
global.fileupload = require('express-fileupload');
global.pagination = require('pagination');
global.uniqid = require('uniqid');
/* Table Definition */
global.iw_user='iw_tns_user';
global.iw_setting='iw_tns_setting';
global.iw_webmodules='iw_mas_webmodules';
global.iw_servicetype='iw_mas_servicetype';
global.iw_extratype='iw_mas_extratype';
global.iw_zeit='iw_mas_zeit';
global.iw_category='iw_mas_category';
global.iw_customer='iw_tns_customer';
global.iw_branch='iw_tns_branch';
global.iw_authorization='iw_tns_authorization';
global.iw_localapproval='iw_tns_localapproval';
global.iw_room='iw_tns_room';
global.iw_service='iw_tns_service';
global.iw_serviceextra='iw_tns_service_extra';
global.iw_serviceservicetype='iw_tns_service_servicetype';
global.iw_girls='iw_tns_girls';
global.iw_girls_idproof='iw_tns_girls_idproof';
global.iw_girls_permit='iw_tns_girls_permit';
global.iw_girls_gallery='iw_tns_girls_gallery';
global.iw_girls_service='iw_tns_girls_service';
global.iw_girls_extra='iw_tns_girls_extra';
global.iw_girls_pin='iw_tns_girls_pin';
global.iw_permitperson='iw_tns_permit_person';
global.iw_hair='iw_tns_hair';
global.iw_eyes='iw_tns_eyes';
global.iw_privatepart='iw_tns_privatepart';
global.iw_civilstatus='iw_tns_civilstatus';
global.iw_nationality='iw_tns_nationality';
global.iw_bust='iw_tns_bust';
global.iw_shoes='iw_tns_shoes';
global.iw_cloth='iw_tns_cloth';
global.iw_vouchers='iw_tns_vouchers';
global.iw_pin='iw_tns_pin';
global.iw_workplanner='iw_tns_workplanner';

/*Image link Name*/
global.project_file_link = "assets/images/";
global.project_file_link_font = "images/";
global.file_link_localapproval = project_file_link+"localapproval/";
global.file_link_room = project_file_link+"rooms/";
global.file_link_branch = project_file_link+"branch/";
global.file_link_girls = project_file_link+"girls/";
global.file_link_gallery = project_file_link+"gallery/";
/*Image link Name front*/
global.file_link_branch_fornt = project_file_link_font+"branch/";
global.file_link_rooms_fornt = project_file_link_font+"rooms/";
global.file_link_localapproval_fornt = project_file_link_font+"localapproval/";
global.file_link_girls_fornt = project_file_link_font+"girls/";
global.file_link_gallery_fornt = project_file_link_font+"gallery/";
/* User defined Modules */
global.nodemailer = require('nodemailer');
global.sql = require('./libraries/sqlfunctions');
global.dataTable = require('./libraries/dataTable');
global.utls = require('./utils/utils');
//var slashes = require('slashes');
global.rad2deg = require('rad2deg');
global.deg2rad = require('deg2rad');
global.datetime = require('node-datetime');
var Cryptr = require('cryptr');
global.cryptr = new Cryptr('lifo@123#');
global.md5 = require('md5');
global.app = express();
global.site_url = os.hostname();
global.randomstring = require("randomstring");
// global.vhost = require('vhost');
var fss = require('fs');
// var cors = require('cors')
// app.use(cors());
global.nDate = new Date().toLocaleString('en-US', {
  timeZone: 'Europe/Zurich'
});
/* Mail configration */
var transporter = nodemailer.createTransport({
		host: 'smtp.gmail.com',
		port: 465,
		secure: true, 
		auth: {
			user: 'qlicswiss@gmail.com',
			pass: 'Swiss2019!'
		}
	});
var sslPath = '/etc/letsencrypt/live/qlic.ch/';

/* Mysql connection */
//console.log("site_url"+site_url);
if(site_url=='ip-172-31-22-11' || site_url=='Lifo-10' || site_url=='lifo10' || site_url=='lifo-server'  || site_url=='USER10'){
	//global.image_url = "https://qlicswiss.s3.eu-central-1.amazonaws.com/";	
	global.con = mysql.createConnection({
	  host: "192.168.1.134",
	  user: "root",
	  password: "",
	  port:3306,
	  database: "iwox",
	  charset: "utf8_general_ci"
	});
	//console.log(con);
} else{
	//global.image_url = "https://qlicswiss.s3.eu-central-1.amazonaws.com/";	
	global.con = mysql.createConnection({
	   host: "3.127.12.202",
      user: "root",
      database: "iwox",
      password: "iwox@123",
	});
}
// price module
global.numeral = require('numeral');
numeral.register('locale', 'fr-ch', {
	delimiters: {
		thousands: '\'',
		decimal: '.'
	},
	abbreviations: {
		thousand: 'k',
		million: 'm',
		billion: 'b',
		trillion: 't'
	},
	ordinal : function (number) {
		return number === 1 ? 'er' : 'e';
	},
	currency: {
		symbol: 'CHF'
	}
});
numeral.locale('fr-ch');
numeral().format('0,0');
const app_exp = require("express")();
app_exp.set("view engine", "pug");
app_exp.use(require("body-parser").urlencoded({extended: false}));
/** Initialize session **/
app.use(session({
    secret: 'iwoxApp',
	// store: sessionStore, // connect-mongo session store
    proxy: true,
    resave: true,
    saveUninitialized: true
}));
/** Initialize file upload **/
app.use(fileupload());
/** Request Header Setting **/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// set the view engine to ejs
app.use(express.static(__dirname + '/assets'));
//app.use('/admin', express.static('/assets/admin_assets'));
app.set('view engine', 'ejs');
app.use(function(req, res, next){
//global.siteadmin_url=req.protocol + '://qlic.ch/';
global.site_redirect_url=req.protocol + '://' + req.get('host')+'/';
global.site_redirect_urlv=req.protocol + '://' + req.get('host');
global.image_url = req.protocol + '://' + req.get('host')+'/';	
console.log(site_redirect_url);
//console.log("siteadmin_url"+siteadmin_url);
	// global.host=req.get('host');
	// host_sp=host.split('.');
	// global.domain=host_sp[0];
	// if(domain=='qlic')
	// {
	// 	next();
	// }
	// else
	// {
	// 	res.status(404);
	//   // respond with html page
	//   if (req.accepts('html')) {
	//     res.render('pages/404', { siteadmin_url: siteadmin_url});
	//     return;
	//   }
	//   // respond with json
	//   if (req.accepts('json')) {
	//     res.send({ error: 'Not found' });
	//     return;
	//   }
	//   // default to plain-text. send()
	//   res.type('txt').send('Not found');
	// }
	next();
});
/*Router call function */

var apiroutes = require('./routes/route');

app.use(apiroutes);
/** Check not empty **/
function checkNotEmpty(val){
	if(val!='' && val!=0 && typeof(val)!='undefined' && typeof(val)!='null')
		return true;
	else return false;
}
app.locals.checkNotEmpty = function(val) {
	if(val!='' && val!=0 && typeof(val)!='undefined' && typeof(val)!='null')
		return true;
	else return false;
}
/** Encrypt **/
app.locals.encrypt = function(id){
	const encryptedString = cryptr.encrypt(id);
	return encryptedString;	
}
/** Decrypt **/
app.locals.decrypt = function(id){
	const decryptedString = cryptr.decrypt(id);
	return decryptedString;	
}
//app.listen(5005);
console.log(site_url);
if(site_url=='ip-172-31-22-11' || site_url=='Lifo-10' || site_url=='lifo10' || site_url=='lifo-server'  || site_url=='USER10'){
	server = app.listen(5005);
	console.log(5005);
} else{
	server = app.listen(80);
	console.log(80);
	/*const fs = require('fs');
    var options = {
         key: fs.readFileSync(sslPath + 'privkey.pem'),
        cert: fs.readFileSync(sslPath + 'fullchain.pem')
    };
     // key: fs.readFileSync("./ssl/private.key"),
     //    cert: fs.readFileSync("./ssl/certificate.crt"),
    console.log('live port: 80')
    var server = require('https').createServer(options, app).listen(443, function() {
        console.log('listening on *:443');
        //console.log(server);
    });
    var http = require('http');
    http.createServer(function (req, res) {
    res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
    res.end();
    }).listen(80);*/
}

global.io = require("socket.io")(server);

io.on('connection', (socket) => {
	socket.on('qlic_api', function(data){
		console.log(data);
		socketmobileapi.ajaxMobileApi(data,function(delivery,ret){
			socket.emit(delivery, ret);
		});
	});
   
    socket.on('disconnect', function() {
        //console.log('user ' + app.locals.session_id + ' disconnected');
        //console.log(socket.id);
    });
});

/** Check not empty **/
function checkNotEmpty(val) {
    if (val != '' && val != 0 && typeof(val) != 'undefined' && typeof(val) != 'null')
        return true;
    else return false;
}
/** Encrypt **/
app.locals.encrypt = function(id){
	const encryptedString = cryptr.encrypt(id);
	return encryptedString;	
}
/** Decrypt **/
app.locals.decrypt = function(id){
	try{
	const decryptedString = cryptr.decrypt(id);
	return decryptedString;	
	}
	catch(ex){
		return '';
	}
}


