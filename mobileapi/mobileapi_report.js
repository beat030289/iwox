module.exports = {
        mobileApi: function(reqData, callback) {
            var process = reqData.process;
            //console.log(process);
            switch (process) {

               /*** get girls ***/
                case 'serviceGirlsData':
                
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                    var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    const getGirlsListData = async function(req, res) {
                        let sqlgirls = 'SELECT w.girls_id,(SELECT g.profilename FROM '+iw_girls+' g WHERE g.id=w.girls_id ) AS girlsname FROM '+iw_workplanner+' w WHERE w.branch_id = "'+branch+'" AND w.workdate = "'+today+'"  GROUP BY w.girls_id';
                       let girls = await myQuery(sqlgirls);
                         console.log(girls);
                        res = {
                            status: 'success',
                            girlsList: girls.result
                        };
                        callback('serviceGirlsData_res', res);
                    }
                    getGirlsListData();
                    break;
                    /*** get service Girlist Report ***/
                case 'serviceGirlistReport':
               
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    girlsId = (utls.checkNotEmpty(reqData.girlsid)) ? reqData.girlsid : '';
                    rdate = (utls.checkNotEmpty(reqData.gdate)) ? reqData.gdate : '';
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                    var query1="";
                    var query2="";
                    if(girlsId!="" && girlsId!=undefined && girlsId!=null)
                    {
                        query1='SELECT t.eserviceamount,t.eservicename,t.egirlsamount,t.ebranchamount,t.serviceamount,t.extraamount,t.girlsamount,t.girlsamount,t.branchamount,t.voucherprice,t.servicetime,t.starttime,t.booking_date,t.totalamount AS amount,t.customer,t.customer_type,TIME_FORMAT(t.starttime,"%H:%i") AS correcttime,t.roomname,(SELECT b.title FROM '+iw_branch+' b WHERE b.id=t.branch_id ) AS branchname,(SELECT GROUP_CONCAT(DISTINCT e.extra) FROM  iw_extrabooking e WHERE e.booking_id=t.id ORDER BY  e.booking_id ASC) AS extradata,(SELECT s.price FROM  '+iw_service+' s WHERE s.id=t.service_id ) AS serviceamount1 FROM iw_transaction t WHERE t.girls_id="'+girlsId+'" AND t.booking_date="'+rdate+'" AND t.status!="2"';
                        query2='SELECT l.booking_id,l.price,l.paymentfor,DATE_FORMAT(l.created_date, "%d.%m.%Y (%h:%i)") AS created_date FROM iw_tns_paymentlog l WHERE l.girls_id="'+girlsId+'" AND l.booking_date="'+rdate+'" AND l.status!="1" AND l.type="2"';
                    }
                    else
                    {
                        query1='SELECT t.eserviceamount,t.eservicename,t.egirlsamount,t.ebranchamount,t.serviceamount,t.extraamount,t.girlsamount,t.branchamount,t.voucherprice,t.servicetime,t.starttime,t.booking_date,t.totalamount AS amount,t.customer,t.customer_type,TIME_FORMAT(t.starttime,"%H:%i") AS correcttime,t.roomname,(SELECT b.title FROM '+iw_branch+' b WHERE b.id=t.branch_id ) AS branchname,(SELECT GROUP_CONCAT(DISTINCT e.extra) FROM  iw_extrabooking e WHERE e.booking_id=t.id ORDER BY  e.booking_id ASC) AS extradata,(SELECT s.price FROM  '+iw_service+' s WHERE s.id=t.service_id ) AS serviceamount1 FROM iw_transaction t WHERE t.booking_date="'+rdate+'" AND t.status!="2"';
                        query2='SELECT l.booking_id,l.price,l.paymentfor,DATE_FORMAT(l.created_date, "%d.%m.%Y (%h:%i)") AS created_date FROM iw_tns_paymentlog l WHERE l.booking_date="'+rdate+'" AND l.status!="1" AND l.type="2"';
                    }
                    var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    const getGirlsReportSlip = async function(req, res) {
                        let sqlReportSlip =query1;
                        let sqlpaymentlist =query2;
                        let reportSlip = await myQuery(sqlReportSlip);
                        let paymentSlip = await myQuery(sqlpaymentlist);
                       
                        res = {
                            status: 'success',
                            reportSlip: reportSlip.result,
                            paymentSlip: paymentSlip.result
                        };
                        callback('serviceGirlistReport_res', res);
                    }
                    getGirlsReportSlip();
                    break;
                    /*** get service Girlist Report download***/
                case 'serviceGirlistReportdownload':
                console.log("tttttttttttttttttttttttttttttttttttttttttt");
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    girlsId = (utls.checkNotEmpty(reqData.girlsid)) ? reqData.girlsid : '';
                    rdate = (utls.checkNotEmpty(reqData.gdate)) ? reqData.gdate : '';
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                    var printdate = dateFormat(vvDate, "dd.mm.yyyy");
                    var query1="";
                    if(girlsId!="" && girlsId!=undefined && girlsId!=null)
                    {
                        query1='SELECT t.eserviceamount,t.eservicename,t.egirlsamount,t.ebranchamount,t.branchamount,t.girlsamount,t.serviceamount,t.extraamount,t.voucherprice,t.servicetime,t.girlsname AS gname,t.starttime,t.booking_date,t.totalamount AS amount,t.customer,t.customer_type,TIME_FORMAT(t.starttime,"%H:%i") AS correcttime,t.roomname,(SELECT b.title FROM '+iw_branch+' b WHERE b.id=t.branch_id ) AS branchname,(SELECT GROUP_CONCAT(DISTINCT e.extra) FROM  iw_extrabooking e WHERE e.booking_id=t.id ORDER BY  e.booking_id ASC) AS extradata,(SELECT s.price FROM  '+iw_service+' s WHERE s.id=t.service_id ) AS serviceamount1 FROM iw_transaction t WHERE t.girls_id="'+girlsId+'" AND t.booking_date="'+rdate+'" AND t.status!="2"';
                        query2='SELECT l.booking_id,l.price,l.paymentfor,DATE_FORMAT(l.created_date, "%d.%m.%Y (%h:%i)") AS created_date FROM iw_tns_paymentlog l WHERE l.girls_id="'+girlsId+'" AND l.booking_date="'+rdate+'" AND l.status!="1" AND l.type="2"';
                    }
                    else
                    {
                        query1='SELECT t.eserviceamount,t.eservicename,t.egirlsamount,t.ebranchamount,t.branchamount,t.girlsamount,t.serviceamount,t.extraamount,t.voucherprice,t.servicetime,"Alle" AS gname,t.starttime,t.booking_date,t.totalamount AS amount,t.customer,t.customer_type,TIME_FORMAT(t.starttime,"%H:%i") AS correcttime,t.roomname,(SELECT b.title FROM '+iw_branch+' b WHERE b.id=t.branch_id ) AS branchname,(SELECT GROUP_CONCAT(DISTINCT e.extra) FROM  iw_extrabooking e WHERE e.booking_id=t.id ORDER BY  e.booking_id ASC) AS extradata,(SELECT s.price FROM  '+iw_service+' s WHERE s.id=t.service_id ) AS serviceamount1 FROM iw_transaction t WHERE t.booking_date="'+rdate+'" AND t.status!="2"';
                        query2='SELECT l.booking_id,l.price,l.paymentfor,DATE_FORMAT(l.created_date, "%d.%m.%Y (%h:%i)") AS created_date FROM iw_tns_paymentlog l WHERE l.booking_date="'+rdate+'" AND l.status!="1" AND l.type="2"';
                    }
                    var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var finame = dateFormat(vvDate, "yyyy_mm_dd_HH_MM_ss");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    const getGirlsReportSlipdownload = async function(req, res) {
                        let sqlReportSlip =query1;
                        let sqlpaymentlist =query2;
                        
                        let sqlReportcount ='SELECT filenumber AS tid FROM iw_tns_download WHERE  date_created="'+today+'" AND branch_id="'+branch+'"';
                       let reportSlip = await myQuery(sqlReportSlip);
                       let paymentSlip = await myQuery(sqlpaymentlist);
                       let scount = await myQuery(sqlReportcount);
                       var ttcount =scount.result;
                       var newnumber="";
                       if(reportSlip.result!="")
                       {
                            if(ttcount=="")
                           {
                                newnumber="1";
                                let sqlIndata ='INSERT INTO iw_tns_download (`date_created`,`filenumber`,`branch_id`) VALUES ("' + today + '","1","' + branch + '")';
                                let sqlIndatalist ='INSERT INTO iw_tns_downloadlist (`date_created`,`filenumber`,`branch_id`,`girls_id`) VALUES ("' + today + '","v1","' + branch + '","' + girlsId + '")';
                                let downloadinsert = await myQuery(sqlIndata);
                                let downloadinsertlist = await myQuery(sqlIndatalist);
                           }
                           else
                           {
                                newnumber = parseInt(ttcount[0].tid)+parseInt(1);
                                let sqlupdata = 'UPDATE iw_tns_download  SET filenumber="'+newnumber+'" WHERE date_created="'+today+'" AND branch_id="'+branch+'"';
                                let sqlIndatalist ='INSERT INTO iw_tns_downloadlist (`date_created`,`filenumber`,`branch_id`,`girls_id`) VALUES ("' + today + '","v'+newnumber+'","' + branch + '","' + girlsId + '")';
                                let downloadupdate = await myQuery(sqlupdata);
                                let downloadinsertlist = await myQuery(sqlIndatalist);
                           }
                       }
                       console.log(reportSlip);
                       
                        res = {
                            status: 'success',
                            newnumber: newnumber,
                            fname: finame,
                            printdate: printdate,
                            reportSlip: reportSlip.result,
                            paymentSlip: paymentSlip.result
                        };
                        callback('serviceGirlistReportdownload_res', res);
                    }
                    getGirlsReportSlipdownload();
                    break;
                 


            }
        }
    }
    /** Check not empty **/
function checkNotEmpty(val) {
    if (val != '' && val != 0 && typeof(val) != 'undefined' && typeof(val) != 'null')
        return true;
    else return false;
}
/** Addslashes function **/
function addslashes(str) {
    if (str != '' && typeof(str) != "undefined" && typeof(str) != "null")
        return slashes.add(str);
    else return str;
}
/** Stripslashes function **/
function stripslashes(str) {
    if (str != '' && typeof(str) != "undefined" && typeof(str) != "null")
        return slashes.strip(str);
    else return str;
}