module.exports = {
        mobileApi: function(reqData, callback) {
            var process = reqData.process;
            //console.log(process);
            switch (process) {
                /*** get report Details ***/
               case 'gerdailyReport':
                
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                   var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    var currenttoday = dateFormat(vvDate, "dd.mm.yyyy");
                    var fname = dateFormat(vvDate, "dd_mm_yyyy_HH_MM_ss");
                    const dailyreport = async function(req, res) {
                        try
                        {
                           /*let sqldailylist ='SELECT (SELECT(SUM(CASE WHEN ((p4.paymenttype = "Wir1" || p4.paymenttype = "Wir2") AND p4.type = "1") THEN p4.price ELSE 0 END)-SUM(CASE WHEN ((p4.paymenttype = "Wir1" || p4.paymenttype = "Wir2") AND p4.type = "2") THEN p4.price ELSE 0 END)) FROM iw_tns_paymentlog p4 WHERE p4.girls_id=w.girls_id AND p4.booking_date="'+today+'" AND p4.status=0) AS wiramount,(SELECT (SUM(CASE WHEN (p3.paymenttype = "Reka" AND p3.type = "1") THEN p3.price ELSE 0 END)-SUM(CASE WHEN (p3.paymenttype = "Reka" AND p3.type = "2") THEN p3.price ELSE 0 END)) FROM iw_tns_paymentlog p3 WHERE p3.girls_id=w.girls_id AND p3.booking_date="'+today+'" AND p3.status=0)AS reakamount,(SELECT (SUM(CASE WHEN ((p2.paymenttype = "POST" || p2.paymenttype = "Visa" || p2.paymenttype = "Mastercard" || p2.paymenttype = "American" || p2.paymenttype = "DinersClub" || p2.paymenttype = "Twint") AND p2.type = "1") THEN p2.price ELSE 0 END)-SUM(CASE WHEN ((p2.paymenttype = "POST" || p2.paymenttype = "Visa" || p2.paymenttype = "Mastercard" || p2.paymenttype = "American" || p2.paymenttype = "DinersClub" || p2.paymenttype = "Twint") AND p2.type = "2") THEN p2.price ELSE 0 END)) FROM iw_tns_paymentlog p2 WHERE p2.girls_id=w.girls_id AND p2.booking_date="'+today+'" AND p2.status=0) AS kkamount,(SELECT (SUM(CASE WHEN (p1.paymenttype = "EuroCash" AND p1.type = "1") THEN p1.price ELSE 0 END)-SUM(CASE WHEN (p1.paymenttype = "EuroCash" AND p1.type = "2") THEN p1.price ELSE 0 END)) FROM iw_tns_paymentlog p1 WHERE p1.girls_id=w.girls_id AND p1.booking_date="'+today+'" AND p1.status=0) AS euroamount,w.workdate,(SELECT g.profilename FROM iw_tns_girls g WHERE g.id=w.girls_id) AS girlsname,w.girls_id,w.workdate,COALESCE(SUM(t.extraamount),0) AS extraamount,COALESCE((SUM(t.voucherprice)+SUM(t.pointprice)+SUM(t.rubbleprice)),0) AS iwoxprice,(SELECT COUNT(tt.service_id) FROM iw_transaction tt WHERE tt.booking_date="'+today+'" AND w.girls_id=tt.girls_id) AS servicecount,COALESCE(SUM(t.serviceamount),0) AS totalamount,COALESCE((SUM(t.totalamount)/2),0) AS girlsrealamount,COALESCE(SUM(t.girlsamount),0) AS girlsamount,COALESCE(SUM(t.branchamount),0) AS branchamount FROM  iw_tns_workplanner w LEFT JOIN iw_transaction t ON w.girls_id = t.girls_id AND t.booking_date="'+today+'" WHERE w.workdate="'+today+'" AND w.branch_id="'+branch+'"  GROUP BY w.girls_id';*/
                           let sqldailylist ='SELECT (SELECT(SUM(CASE WHEN ((p4.paymenttype = "Wir1" || p4.paymenttype = "Wir2") AND p4.type = "1") THEN p4.price ELSE 0 END)-SUM(CASE WHEN ((p4.paymenttype = "Wir1" || p4.paymenttype = "Wir2") AND p4.type = "2") THEN p4.price ELSE 0 END)) FROM iw_tns_paymentlog p4 WHERE p4.girls_id=w.girls_id AND p4.booking_date="'+today+'" AND p4.status=0) AS wiramount,(SELECT (SUM(CASE WHEN (p3.paymenttype = "Reka" AND p3.type = "1") THEN p3.price ELSE 0 END)-SUM(CASE WHEN (p3.paymenttype = "Reka" AND p3.type = "2") THEN p3.price ELSE 0 END)) FROM iw_tns_paymentlog p3 WHERE p3.girls_id=w.girls_id AND p3.booking_date="'+today+'" AND p3.status=0)AS reakamount,(SELECT (SUM(CASE WHEN ((p2.paymenttype = "POST" || p2.paymenttype = "Visa" || p2.paymenttype = "Mastercard" || p2.paymenttype = "American" || p2.paymenttype = "DinersClub" || p2.paymenttype = "Twint") AND p2.type = "1") THEN p2.price ELSE 0 END)-SUM(CASE WHEN ((p2.paymenttype = "POST" || p2.paymenttype = "Visa" || p2.paymenttype = "Mastercard" || p2.paymenttype = "American" || p2.paymenttype = "DinersClub" || p2.paymenttype = "Twint") AND p2.type = "2") THEN p2.price ELSE 0 END)) FROM iw_tns_paymentlog p2 WHERE p2.girls_id=w.girls_id AND p2.booking_date="'+today+'" AND p2.status=0) AS kkamount,(SELECT (SUM(CASE WHEN (p1.paymenttype = "EuroCash" AND p1.type = "1") THEN p1.price ELSE 0 END)-SUM(CASE WHEN (p1.paymenttype = "EuroCash" AND p1.type = "2") THEN p1.price ELSE 0 END)) FROM iw_tns_paymentlog p1 WHERE p1.girls_id=w.girls_id AND p1.booking_date="'+today+'" AND p1.status=0) AS euroamount,w.workdate,(SELECT g.profilename FROM iw_tns_girls g WHERE g.id=w.girls_id) AS girlsname,w.girls_id,w.workdate,COALESCE(SUM(t.extraamount),0) AS extraamount,COALESCE((SUM(t.voucherprice)+SUM(t.pointprice)+SUM(t.rubbleprice)),0) AS iwoxprice,(SELECT COUNT(tt.service_id) FROM iw_transaction tt WHERE tt.booking_date="'+today+'" AND w.girls_id=tt.girls_id) AS servicecount,COALESCE((SUM(t.serviceamount)+SUM(t.eserviceamount)),0) AS totalamount,COALESCE((SUM(t.totalamount)/2),0) AS girlsrealamount,COALESCE((SUM(t.girlsamount)+SUM(t.egirlsamount)),0) AS girlsamount,COALESCE((SUM(t.branchamount)+SUM(t.ebranchamount)),0) AS branchamount FROM  iw_tns_workplanner w LEFT JOIN iw_transaction t ON w.girls_id = t.girls_id AND t.booking_date="'+today+'" WHERE w.workdate="'+today+'" AND w.branch_id="'+branch+'"  GROUP BY w.girls_id';
                            
                            let sqlguestinfo ='SELECT * FROM iw_tns_guestinfo WHERE created_date="'+today+'" AND branch_id="'+branch+'"';
                            let sqlrevenue ='SELECT * FROM iw_tns_revenue WHERE created_date="'+today+'" AND branch_id="'+branch+'"';
                            let sqlexpenditure ='SELECT * FROM iw_tns_expenditure WHERE created_date="'+today+'" AND branch_id="'+branch+'"';
                             let sqlbranch ='SELECT title FROM iw_tns_branch WHERE id="'+branch+'"';

                            let getdaily = await myQuery(sqldailylist);
                            let getguestinfo = await myQuery(sqlguestinfo);
                            let getrevenue = await myQuery(sqlrevenue);
                            let getExpenditure = await myQuery(sqlexpenditure);
                            let getbranch = await myQuery(sqlbranch);
                            res = {
                            status: 'success',
                            cdate: currenttoday,
                            dailyData: getdaily.result,
                            guestInfo: getguestinfo.result,
                            revenue: getrevenue.result,
                            expenditure: getExpenditure.result,
                            branchname: getbranch.result,
                            fname: fname
                          
                        };
                        callback('gerdailyReport_res', res);
                        

                        }catch(e)
                        {
                            console.log(e);
                        }
                       
                    }
                    dailyreport();
                    break;
                    case 'revenueSubmit':
                     userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    rname = (utls.checkNotEmpty(reqData.rname)) ? reqData.rname : '';
                    ramount = (utls.checkNotEmpty(reqData.ramount)) ? reqData.ramount : '';
                    rcomment = (utls.checkNotEmpty(reqData.rcomment)) ? reqData.rcomment : '';
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                    var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    var currenttoday = dateFormat(vvDate, "dd.mm.yyyy");
                    const chutSubmit = async function(req, res) {
                        let sqlrevenuedel ='DELETE FROM iw_tns_revenue WHERE created_date="'+today+'" AND branch_id="'+branch+'"';
                            let rSubmit = await myQuery(sqlrevenuedel);
                            if(rSubmit)
                            {
                                if (Array.isArray(reqData.rname)) {
                                var totallength =rname.length;
                                rname.forEach(function(rdata,index){
                                    const uSubmit = async function(req, res) {
                                        try
                                        {
                                            let sqlrevenue ='INSERT INTO iw_tns_revenue (`name`,`amount`, `comments`, `created_date`, `branch_id`) VALUES ("' + rdata + '","' + ramount[index] + '","' + rcomment[index] + '","' + today + '","' + branch + '")';
                                            console.log(sqlrevenue);
                                            let rSubmit = await myQuery(sqlrevenue);
                                           
                                        }catch(e)
                                        {
                                            console.log();
                                        }
                                   
                                    }
                                    uSubmit();
                                    if(totallength == (parseInt(index)+parseInt(1)))
                                    {
                                        res = {
                                        status: 'success',
                                        };
                                        callback('revenueSubmit_res', res);  
                                    }

                                });
                            }
                            else
                            {
                                const uSubmit = async function(req, res) {
                                    try
                                    {
                                        let sqlrevenue ='INSERT INTO iw_tns_revenue (`name`,`amount`, `comments`, `created_date`, `branch_id`) VALUES ("' + rname + '","' + ramount + '","' + rcomment + '","' + today + '","' + branch + '")';
                                        let rSubmit = await myQuery(sqlrevenue);
                                       
                                    }catch(e)
                                    {
                                        console.log();
                                    }
                                    res = {
                                        status: 'success',
                                        };
                                        callback('revenueSubmit_res', res);
                                }
                                uSubmit();
                            }
                       }
                    }
                    chutSubmit();
                    break;
                    case 'expenditureSubmit':
                     userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    ename = (utls.checkNotEmpty(reqData.ename)) ? reqData.ename : '';
                    eamount = (utls.checkNotEmpty(reqData.eamount)) ? reqData.eamount : '';
                    ecomment = (utls.checkNotEmpty(reqData.ecomment)) ? reqData.ecomment : '';
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                    var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    var currenttoday = dateFormat(vvDate, "dd.mm.yyyy");
                    const chuSubmit = async function(req, res) {
                        let sqlexpendituredel ='DELETE FROM iw_tns_expenditure WHERE created_date="'+today+'" AND branch_id="'+branch+'"';
                            let eSubmit = await myQuery(sqlexpendituredel);
                           if(eSubmit)
                           {
                                if (Array.isArray(reqData.ename)) {
                                var totallength =ename.length;
                                ename.forEach(function(edata,index){
                                    const uSubmit = async function(req, res) {
                                        try
                                        {
                                            let sqlexpenditure ='INSERT INTO iw_tns_expenditure (`name`,`amount`, `comments`, `created_date`, `branch_id`) VALUES ("' + edata + '","' + eamount[index] + '","' + ecomment[index] + '","' + today + '","' + branch + '")';
                                           let eSubmit = await myQuery(sqlexpenditure);
                                           
                                        }catch(e)
                                        {
                                            console.log();
                                        }
                                   
                                    }
                                    uSubmit();
                                    if(totallength == (parseInt(index)+parseInt(1)))
                                    {
                                        res = {
                                        status: 'success',
                                        };
                                        callback('expenditureSubmit_res', res);  
                                    }

                                });
                            }
                            else
                            {
                                const uSubmit = async function(req, res) {
                                    try
                                    {
                                        let sqlexpenditure ='INSERT INTO iw_tns_expenditure (`name`,`amount`, `comments`, `created_date`, `branch_id`) VALUES ("' + ename + '","' + eamount + '","' + ecomment + '","' + today + '","' + branch + '")';
                                        console.log(sqlrevenue);
                                        let eSubmit = await myQuery(sqlexpenditure);
                                       
                                    }catch(e)
                                    {
                                        console.log();
                                    }
                                     res = {
                                        status: 'success',
                                        };
                                        callback('expenditureSubmit_res', res);  
                               
                                }
                                uSubmit();
                            }
                        }
                    }
                    chuSubmit();
                    break;
                    case 'allSubmit':
                     userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    notopen = (utls.checkNotEmpty(reqData.notopen)) ? reqData.notopen : '';
                    price = (utls.checkNotEmpty(reqData.price)) ? reqData.price : '';
                    selection = (utls.checkNotEmpty(reqData.selection)) ? reqData.selection : '';
                    busy = (utls.checkNotEmpty(reqData.busy)) ? reqData.busy : '';
                    club = (utls.checkNotEmpty(reqData.club)) ? reqData.club : '';
                    others = (utls.checkNotEmpty(reqData.others)) ? reqData.others : '';
                    total = (utls.checkNotEmpty(reqData.total)) ? reqData.total : '';
                    comments = (utls.checkNotEmpty(reqData.bcomments)) ? reqData.bcomments : '';
                    
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                    var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    var currenttoday = dateFormat(vvDate, "dd.mm.yyyy");
                    const uSubmit = async function(req, res) {
                            try
                            {
                                let sqlchek ='SELECT id FROM iw_tns_guestinfo WHERE created_date="'+today+'" AND branch_id="'+branch+'"';
                                let olddatacheck = await myQuery(sqlchek);
                                if(olddatacheck.result=="")
                                {
                                    let sqldataint ='INSERT INTO iw_tns_guestinfo (`created_date`,`branch_id`, `notopen`, `price`, `selection`, `busy`, `club`, `others`, `total`, `comments`) VALUES ("' + today + '","' + branch + '","' + notopen + '","' + price + '","' + selection + '","' + busy + '","' + club + '","' + others + '","' + total + '","' + comments + '")';
                                    let getinfo = await myQuery(sqldataint);
                                }
                                else
                                {
                                    let sqldel ='DELETE FROM iw_tns_guestinfo WHERE created_date="'+today+'" AND branch_id="'+branch+'"';
                                    let infodel = await myQuery(sqldel);
                                    if(infodel!="")
                                    {
                                        let sqldataint ='INSERT INTO iw_tns_guestinfo (`created_date`,`branch_id`, `notopen`, `price`, `selection`, `busy`, `club`, `others`, `total`, `comments`) VALUES ("' + today + '","' + branch + '","' + notopen + '","' + price + '","' + selection + '","' + busy + '","' + club + '","' + others + '","' + total + '","' + comments + '")';
                                        let getinfo = await myQuery(sqldataint);
                                    }

                                }
                                
                               
                            }catch(e)
                            {
                                console.log();
                            }
                            res = {
                                status: 'success',
                                };
                            callback('allSubmit_res', res);  
                       
                    }
                    uSubmit();
                    break;
                    case 'finaltotal':
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                   var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    var currenttoday = dateFormat(vvDate, "dd.mm.yyyy");
                    
                    const fdata = async function(req, res) {
                        try
                        {
                            /*let sqlfdata ='SELECT COALESCE((SUM(t.voucherprice)+SUM(t.pointprice)+SUM(t.rubbleprice)),0) AS iwoxprice,COALESCE(SUM(t.extraamount),0) AS extraamount,COALESCE(SUM(t.serviceamount),0) AS totalamount,COALESCE(SUM(t.branchamount),0) AS branchamount,(SELECT(SUM(CASE WHEN ((p4.paymenttype = "Wir1" || p4.paymenttype = "Wir2") AND p4.type = "1") THEN p4.price ELSE 0 END)-SUM(CASE WHEN ((p4.paymenttype = "Wir1" || p4.paymenttype = "Wir2") AND p4.type = "2") THEN p4.price ELSE 0 END)) FROM iw_tns_paymentlog p4 WHERE p4.booking_id=t.id AND p4.booking_date="'+today+'" AND p4.status=0) AS wiramount,(SELECT (SUM(CASE WHEN (p3.paymenttype = "Reka" AND p3.type = "1") THEN p3.price ELSE 0 END)-SUM(CASE WHEN (p3.paymenttype = "Reka" AND p3.type = "2") THEN p3.price ELSE 0 END)) FROM iw_tns_paymentlog p3 WHERE p3.booking_id=t.id AND p3.booking_date="'+today+'" AND p3.status=0)AS reakamount,(SELECT (SUM(CASE WHEN ((p2.paymenttype = "POST" || p2.paymenttype = "Visa" || p2.paymenttype = "Mastercard" || p2.paymenttype = "American" || p2.paymenttype = "DinersClub" || p2.paymenttype = "Twint") AND p2.type = "1") THEN p2.price ELSE 0 END)-SUM(CASE WHEN ((p2.paymenttype = "POST" || p2.paymenttype = "Visa" || p2.paymenttype = "Mastercard" || p2.paymenttype = "American" || p2.paymenttype = "DinersClub" || p2.paymenttype = "Twint") AND p2.type = "2") THEN p2.price ELSE 0 END)) FROM iw_tns_paymentlog p2 WHERE p2.booking_id=t.id AND p2.booking_date="'+today+'" AND p2.status=0) AS kkamount,(SELECT (SUM(CASE WHEN (p1.paymenttype = "EuroCash" AND p1.type = "1") THEN p1.price ELSE 0 END)-SUM(CASE WHEN (p1.paymenttype = "EuroCash" AND p1.type = "2") THEN p1.price ELSE 0 END)) FROM iw_tns_paymentlog p1 WHERE p1.booking_id=t.id AND p1.booking_date="'+today+'" AND p1.status=0) AS euroamount FROM iw_transaction t WHERE t.booking_date="'+today+'" AND t.branch_id="'+branch+'"';*/
                            let sqlfdata ='SELECT COALESCE((SUM(t.voucherprice)+SUM(t.pointprice)+SUM(t.rubbleprice)),0) AS iwoxprice,COALESCE(SUM(t.extraamount),0) AS extraamount,COALESCE((SUM(t.serviceamount)+SUM(t.eserviceamount)),0) AS totalamount,COALESCE((SUM(t.branchamount)+SUM(t.ebranchamount)),0) AS branchamount,(SELECT(SUM(CASE WHEN ((p4.paymenttype = "Wir1" || p4.paymenttype = "Wir2") AND p4.type = "1") THEN p4.price ELSE 0 END)-SUM(CASE WHEN ((p4.paymenttype = "Wir1" || p4.paymenttype = "Wir2") AND p4.type = "2") THEN p4.price ELSE 0 END)) FROM iw_tns_paymentlog p4 WHERE p4.booking_id=t.id AND p4.booking_date="'+today+'" AND p4.status=0) AS wiramount,(SELECT (SUM(CASE WHEN (p3.paymenttype = "Reka" AND p3.type = "1") THEN p3.price ELSE 0 END)-SUM(CASE WHEN (p3.paymenttype = "Reka" AND p3.type = "2") THEN p3.price ELSE 0 END)) FROM iw_tns_paymentlog p3 WHERE p3.booking_id=t.id AND p3.booking_date="'+today+'" AND p3.status=0)AS reakamount,(SELECT (SUM(CASE WHEN ((p2.paymenttype = "POST" || p2.paymenttype = "Visa" || p2.paymenttype = "Mastercard" || p2.paymenttype = "American" || p2.paymenttype = "DinersClub" || p2.paymenttype = "Twint") AND p2.type = "1") THEN p2.price ELSE 0 END)-SUM(CASE WHEN ((p2.paymenttype = "POST" || p2.paymenttype = "Visa" || p2.paymenttype = "Mastercard" || p2.paymenttype = "American" || p2.paymenttype = "DinersClub" || p2.paymenttype = "Twint") AND p2.type = "2") THEN p2.price ELSE 0 END)) FROM iw_tns_paymentlog p2 WHERE p2.booking_id=t.id AND p2.booking_date="'+today+'" AND p2.status=0) AS kkamount,(SELECT (SUM(CASE WHEN (p1.paymenttype = "EuroCash" AND p1.type = "1") THEN p1.price ELSE 0 END)-SUM(CASE WHEN (p1.paymenttype = "EuroCash" AND p1.type = "2") THEN p1.price ELSE 0 END)) FROM iw_tns_paymentlog p1 WHERE p1.booking_id=t.id AND p1.booking_date="'+today+'" AND p1.status=0) AS euroamount FROM iw_transaction t WHERE t.booking_date="'+today+'" AND t.branch_id="'+branch+'"';
                            //console.log(sqlfdata);
                           let sqlrevenuetotal ='SELECT COALESCE(SUM(amount),0) AS ramount FROM iw_tns_revenue WHERE created_date="'+today+'" AND branch_id="'+branch+'"';
                            let sqlexpendituretotal ='SELECT COALESCE(SUM(amount),0) AS eamount FROM iw_tns_expenditure WHERE created_date="'+today+'" AND branch_id="'+branch+'"';

                            let getfdata = await myQuery(sqlfdata);
                            let getrevenuetotal = await myQuery(sqlrevenuetotal);
                            let getExpendituretotal = await myQuery(sqlexpendituretotal);
                        res = {
                            status: 'success',
                            cdate: currenttoday,
                            fdata: getfdata.result,
                            revenuetotal: getrevenuetotal.result,
                            expendituretotal: getExpendituretotal.result,
                        };
                        callback('finaltotal_res', res);
                        }catch(e)
                        {
                            console.log(e);
                        }
                       
                    }
                    fdata();
                    break;
                    /*** printget report Details ***/
               case 'printgerdailyReport':
                
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                   var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    var currenttoday = dateFormat(vvDate, "dd.mm.yyyy");
                    var fname = dateFormat(vvDate, "dd_mm_yyyy_HH_MM_ss");
                    const printdailyreport = async function(req, res) {
                        try
                        {
                           /*let sqldailylist ='SELECT (SELECT(SUM(CASE WHEN ((p4.paymenttype = "Wir1" || p4.paymenttype = "Wir2") AND p4.type = "1") THEN p4.price ELSE 0 END)-SUM(CASE WHEN ((p4.paymenttype = "Wir1" || p4.paymenttype = "Wir2") AND p4.type = "2") THEN p4.price ELSE 0 END)) FROM iw_tns_paymentlog p4 WHERE p4.girls_id=w.girls_id AND p4.booking_date="'+today+'" AND p4.status=0) AS wiramount,(SELECT (SUM(CASE WHEN (p3.paymenttype = "Reka" AND p3.type = "1") THEN p3.price ELSE 0 END)-SUM(CASE WHEN (p3.paymenttype = "Reka" AND p3.type = "2") THEN p3.price ELSE 0 END)) FROM iw_tns_paymentlog p3 WHERE p3.girls_id=w.girls_id AND p3.booking_date="'+today+'" AND p3.status=0)AS reakamount,(SELECT (SUM(CASE WHEN ((p2.paymenttype = "POST" || p2.paymenttype = "Visa" || p2.paymenttype = "Mastercard" || p2.paymenttype = "American" || p2.paymenttype = "DinersClub" || p2.paymenttype = "Twint") AND p2.type = "1") THEN p2.price ELSE 0 END)-SUM(CASE WHEN ((p2.paymenttype = "POST" || p2.paymenttype = "Visa" || p2.paymenttype = "Mastercard" || p2.paymenttype = "American" || p2.paymenttype = "DinersClub" || p2.paymenttype = "Twint") AND p2.type = "2") THEN p2.price ELSE 0 END)) FROM iw_tns_paymentlog p2 WHERE p2.girls_id=w.girls_id AND p2.booking_date="'+today+'" AND p2.status=0) AS kkamount,(SELECT (SUM(CASE WHEN (p1.paymenttype = "EuroCash" AND p1.type = "1") THEN p1.price ELSE 0 END)-SUM(CASE WHEN (p1.paymenttype = "EuroCash" AND p1.type = "2") THEN p1.price ELSE 0 END)) FROM iw_tns_paymentlog p1 WHERE p1.girls_id=w.girls_id AND p1.booking_date="'+today+'" AND p1.status=0) AS euroamount,w.workdate,(SELECT g.profilename FROM iw_tns_girls g WHERE g.id=w.girls_id) AS girlsname,w.girls_id,w.workdate,COALESCE(SUM(t.extraamount),0) AS extraamount,COALESCE((SUM(t.voucherprice)+SUM(t.pointprice)+SUM(t.rubbleprice)),0) AS iwoxprice,(SELECT COUNT(tt.service_id) FROM iw_transaction tt WHERE tt.booking_date="'+today+'" AND w.girls_id=tt.girls_id) AS servicecount,COALESCE(SUM(t.serviceamount),0) AS totalamount,COALESCE((SUM(t.totalamount)/2),0) AS girlsrealamount,COALESCE(SUM(t.girlsamount),0) AS girlsamount,COALESCE(SUM(t.branchamount),0) AS branchamount FROM  iw_tns_workplanner w LEFT JOIN iw_transaction t ON w.girls_id = t.girls_id AND t.booking_date="'+today+'" WHERE w.workdate="'+today+'" AND w.branch_id="'+branch+'"  GROUP BY w.girls_id';*/
                           let sqldailylist ='SELECT (SELECT(SUM(CASE WHEN ((p4.paymenttype = "Wir1" || p4.paymenttype = "Wir2") AND p4.type = "1") THEN p4.price ELSE 0 END)-SUM(CASE WHEN ((p4.paymenttype = "Wir1" || p4.paymenttype = "Wir2") AND p4.type = "2") THEN p4.price ELSE 0 END)) FROM iw_tns_paymentlog p4 WHERE p4.girls_id=w.girls_id AND p4.booking_date="'+today+'" AND p4.status=0) AS wiramount,(SELECT (SUM(CASE WHEN (p3.paymenttype = "Reka" AND p3.type = "1") THEN p3.price ELSE 0 END)-SUM(CASE WHEN (p3.paymenttype = "Reka" AND p3.type = "2") THEN p3.price ELSE 0 END)) FROM iw_tns_paymentlog p3 WHERE p3.girls_id=w.girls_id AND p3.booking_date="'+today+'" AND p3.status=0)AS reakamount,(SELECT (SUM(CASE WHEN ((p2.paymenttype = "POST" || p2.paymenttype = "Visa" || p2.paymenttype = "Mastercard" || p2.paymenttype = "American" || p2.paymenttype = "DinersClub" || p2.paymenttype = "Twint") AND p2.type = "1") THEN p2.price ELSE 0 END)-SUM(CASE WHEN ((p2.paymenttype = "POST" || p2.paymenttype = "Visa" || p2.paymenttype = "Mastercard" || p2.paymenttype = "American" || p2.paymenttype = "DinersClub" || p2.paymenttype = "Twint") AND p2.type = "2") THEN p2.price ELSE 0 END)) FROM iw_tns_paymentlog p2 WHERE p2.girls_id=w.girls_id AND p2.booking_date="'+today+'" AND p2.status=0) AS kkamount,(SELECT (SUM(CASE WHEN (p1.paymenttype = "EuroCash" AND p1.type = "1") THEN p1.price ELSE 0 END)-SUM(CASE WHEN (p1.paymenttype = "EuroCash" AND p1.type = "2") THEN p1.price ELSE 0 END)) FROM iw_tns_paymentlog p1 WHERE p1.girls_id=w.girls_id AND p1.booking_date="'+today+'" AND p1.status=0) AS euroamount,w.workdate,(SELECT g.profilename FROM iw_tns_girls g WHERE g.id=w.girls_id) AS girlsname,w.girls_id,w.workdate,COALESCE(SUM(t.extraamount),0) AS extraamount,COALESCE((SUM(t.voucherprice)+SUM(t.pointprice)+SUM(t.rubbleprice)),0) AS iwoxprice,(SELECT COUNT(tt.service_id) FROM iw_transaction tt WHERE tt.booking_date="'+today+'" AND w.girls_id=tt.girls_id) AS servicecount,COALESCE((SUM(t.serviceamount)+SUM(t.eserviceamount)),0) AS totalamount,COALESCE((SUM(t.totalamount)/2),0) AS girlsrealamount,COALESCE((SUM(t.girlsamount)+SUM(t.egirlsamount)),0) AS girlsamount,COALESCE((SUM(t.branchamount)+SUM(t.ebranchamount)),0) AS branchamount FROM  iw_tns_workplanner w LEFT JOIN iw_transaction t ON w.girls_id = t.girls_id AND t.booking_date="'+today+'" WHERE w.workdate="'+today+'" AND w.branch_id="'+branch+'"  GROUP BY w.girls_id';
                            let sqlguestinfo ='SELECT * FROM iw_tns_guestinfo WHERE created_date="'+today+'" AND branch_id="'+branch+'"';
                            let sqlrevenue ='SELECT * FROM iw_tns_revenue WHERE created_date="'+today+'" AND branch_id="'+branch+'"';
                            let sqlexpenditure ='SELECT * FROM iw_tns_expenditure WHERE created_date="'+today+'" AND branch_id="'+branch+'"';
                            let sqlbranch ='SELECT title FROM iw_tns_branch WHERE id="'+branch+'"';

                            let getdaily = await myQuery(sqldailylist);
                            let getguestinfo = await myQuery(sqlguestinfo);
                            let getrevenue = await myQuery(sqlrevenue);
                            let getExpenditure = await myQuery(sqlexpenditure);
                            let getbranch = await myQuery(sqlbranch);
                            res = {
                            status: 'success',
                            cdate: currenttoday,
                            dailyData: getdaily.result,
                            guestInfo: getguestinfo.result,
                            revenue: getrevenue.result,
                            expenditure: getExpenditure.result,
                            branchname: getbranch.result,
                            fname: fname
                          
                        };
                        callback('printgerdailyReport_res', res);
                        }catch(e)
                        {
                            console.log(e);
                        }
                       
                    }
                    printdailyreport();
                    break;
                    case 'get_notification':
                
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                   var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    var currenttoday = dateFormat(vvDate, "dd.mm.yyyy");
                     const getList = async function(req, res) {
                        try
                        {
                            let sqlNoteList ='SELECT n.id,n.title,n.description,DATE_FORMAT(n.created_date, "%d.%m.%Y") AS created_date FROM iw_tns_notification n WHERE n.view_status=0 AND n.delete_status=0 AND n.id IN (SELECT nb.note_id FROM iw_tns_notification_branch nb WHERE nb.type="branch" AND nb.branch_id="'+branch+'")';
                            let getNoteList = await myQuery(sqlNoteList);
                            res = {
                            status: 'success',
                            noteList:getNoteList.result
                            };
                            console.log(getNoteList.result);
                        callback('get_notification_res', res);
                        }catch(e)
                        {
                            console.log();
                        }
                       
                    }
                    getList();
                    break;
                    case 'get_noteDelete':
                
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    noteid = (utls.checkNotEmpty(reqData.noteid)) ? reqData.noteid : '';
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                   var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    var currenttoday = dateFormat(vvDate, "dd.mm.yyyy");
                     const getListDel = async function(req, res) {
                        try
                        {
                            let sqlNoteListDelete ='UPDATE iw_tns_notification SET delete_status="1" WHERE id="'+noteid+'"';
                            let getNoteListDelete = await myQuery(sqlNoteListDelete);
                            res = {
                            status: 'success',
                            
                            };
                           
                        callback('get_noteDelete_res', res);
                        }catch(e)
                        {
                            console.log();
                        }
                       
                    }
                    getListDel();
                    break;
                    case 'get_notification_details':
                
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    noteid = (utls.checkNotEmpty(reqData.noteid)) ? reqData.noteid : '';
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                   var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    var currenttoday = dateFormat(vvDate, "dd.mm.yyyy");
                     const getnoteDetails = async function(req, res) {
                        try
                        {
                            let sqlNote ='SELECT * FROM iw_tns_notification WHERE id="'+noteid+'"';
                            let sqlNoteImage ='SELECT image FROM iw_tns_notification_gallery WHERE note_id="'+noteid+'"';
                            let getNote = await myQuery(sqlNote);
                            let getNoteImage = await myQuery(sqlNoteImage);
                            res = {
                            status: 'success',
                            noteData: getNote.result,
                            noteimage: getNoteImage.result
                            
                            };
                            console.log(getNote.result);
                            console.log(getNoteImage.result);
                        callback('get_notification_details_res', res);
                        }catch(e)
                        {
                            console.log();
                        }
                       
                    }
                    getnoteDetails();
                    break;
                    case 'get_playIdSubmit':
                
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    playId = (utls.checkNotEmpty(reqData.playId)) ? reqData.playId : '';
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                   var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    var currenttoday = dateFormat(vvDate, "dd.mm.yyyy");
                     const playidCall = async function(req, res) {
                        try
                        {
                            let sqlplayid ='UPDATE iw_tns_branch SET onsignalid="'+playId+'" WHERE id="'+branch+'"';
                            
                            let getPlayId = await myQuery(sqlplayid);
                            res = {
                            status: 'success',
                            };
                            
                        callback('get_playIdSubmit_res', res);
                        }catch(e)
                        {
                            console.log();
                        }
                       
                    }
                    playidCall();
                    break;
               }
        }
    }

    
    /** Check not empty **/
function checkNotEmpty(val) {
    if (val != '' && val != 0 && typeof(val) != 'undefined' && typeof(val) != 'null')
        return true;
    else return false;
}
/** Addslashes function **/
function addslashes(str) {
    if (str != '' && typeof(str) != "undefined" && typeof(str) != "null")
        return slashes.add(str);
    else return str;
}
/** Stripslashes function **/
function stripslashes(str) {
    if (str != '' && typeof(str) != "undefined" && typeof(str) != "null")
        return slashes.strip(str);
    else return str;
}
// time added function
function timestrToSec(timestr) {
    var parts = timestr.split(":");
    return (parts[0] * 3600) +
        (parts[1] * 60) +
        (+parts[2]);
}

function pad(num) {
    if (num < 10) {
        return "0" + num;
    } else {
        return "" + num;
    }
}

function formatTime(seconds) {
    return [pad(Math.floor(seconds / 3600)),
        pad(Math.floor(seconds / 60) % 60),
        pad(seconds % 60),
    ].join(":");
}