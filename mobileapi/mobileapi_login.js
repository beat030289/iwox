module.exports = {
        mobileApi: function(reqData, callback) {
            var process = reqData.process;
            //console.log(process);
            switch (process) {

                case 'loginSubmit':
                    /*** Login cheking ***/
                    tabid = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                   // tabpin = (utls.checkNotEmpty(reqData.password)) ? reqData.password : '';
                    tabpin = reqData.password;
                    console.log("tabid"+tabid);
                    console.log("tabpin"+tabpin);
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                    var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    sql.runQuery('SELECT g.id,g.tabid,g.profilename as fullname,(SELECT wg.branch_id FROM iw_tns_workplanner wg WHERE wg.workdate="'+today+'" AND wg.girls_id =g.id GROUP BY wg.girls_id) AS branch FROM ' + iw_girls + ' g WHERE g.tabid="' + tabid + '" AND g.pin="' + tabpin + '"', function(err, result) {
                        if (err)
                        {
                            console.log(err);
                            res = {
                                status: 'error',
                                message: 'Ungültige ID / Pin'
                            };
                            callback('loginSubmit_res', res);
                        }
                        else {
                            console.log(result)
                            if (result == "") {
                                sql.runQuery('SELECT u.id,u.tabid,u.branch,concat(u.first_name," ",u.last_name)as fullname FROM ' + iw_user + ' u WHERE u.tabid="' + tabid + '" AND u.pin="' + tabpin + '"', function(usererr, userresult) {
                                    if (usererr) {
                                        res = {
                                            status: 'error',
                                            message: 'Ungültige ID / Pin'
                                        };
                                        callback('loginSubmit_res', res);
                                    } else {
                                        if (userresult == "") {
                                            res = {
                                                status: 'error',
                                                message: 'Ungültige ID / Pin'
                                            };
                                            callback('loginSubmit_res', res)
                                        } else {
                                            res = {
                                                status: 'success',
                                                message: 'Successfully',
                                                details: userresult,
                                                type: 'user',
                                            };
                                            callback('loginSubmit_res', res);
                                        }

                                    }

                                });
                            } 
                            else {
                                console.log(result);
                                console.log("branchid"+result[0].branch);
                                if(result[0].branch=="" || result[0].branch==null || result[0].branch==undefined) {
                                    res = {
                                        status: 'success',
                                        message: 'Successfully',
                                        details: result,
                                        type: 'girl',
                                    };
                                    callback('loginSubmit_res', res);
                                }
                                else
                                {
                                   res = {
                                        status: 'success',
                                        message: 'Successfully',
                                        details: result,
                                        type: 'girl',
                                    };
                                    callback('loginSubmit_res', res); 
                                }
                                
                            }
                        }
                    });
                    break;
                    /*** get profile Details ***/
                case 'getProfile':
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    query1="";
                    if(type=="user")
                    {
                        query1='SELECT id,first_name,last_name,email,mobile FROM ' + iw_user + ' WHERE tabid="' + userId + '" '
                    }
                    else
                    {
                        query1='SELECT id,first_name,last_name,email,mobile FROM ' + iw_girls + ' WHERE tabid="' + userId + '" '
                    }
                    sql.runQuery(query1, function(err_cc, result_cc) {
                        if (err_cc) {
                            console.log(err_cc);
                            res = {
                                status: 'error',
                                profile: []
                            };

                            callback('getProfile_res', res);
                        } else {
                            console.log(result_cc);
                            res = {
                                status: 'success',
                                profile: result_cc
                            };

                            callback('getProfile_res', res);

                        }
                    });

                    break;
                    /*** Edit profile Details ***/
                case 'editProfile':
                    firstName = (utls.checkNotEmpty(reqData.firstName)) ? reqData.firstName : '';
                    lastName = (utls.checkNotEmpty(reqData.lastName)) ? reqData.lastName : '';
                    email = (utls.checkNotEmpty(reqData.email)) ? reqData.email : '';
                    mobile = (utls.checkNotEmpty(reqData.mobile)) ? reqData.mobile : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    //address = (utls.checkNotEmpty(reqData.address)) ? reqData.address : '';
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    if(type=="user")
                    {
                            sql.runQuery('SELECT id FROM ' + iw_user + ' WHERE  tabid ="' + userId + '" ', function(errQry, result) {
                            if (errQry) {
                                console.log(errQry);
                                res = {
                                    status: 'error',
                                    message: 'Ungültiger Prozess'

                                };
                                callback('editProfile_res', res);
                            } else {
                                sql.runQuery('UPDATE ' + iw_user + ' SET first_name="' + firstName + '",last_name="' + lastName + '",email="' + email + '",mobile="' + mobile + '" WHERE tabid="' + userId + '"', function(err_u, result_u) {
                                    if (err_u) {
                                        res = {
                                            status: 'error',
                                            message: 'Ungültiger Prozess'

                                        };
                                        callback('editProfile_res', res);
                                    } else {
                                        res = {
                                            status: 'success',
                                             message: 'Erfolgreich aktualisiert'
                                        };
                                        callback('editProfile_res', res);
                                    }
                                });
                            }
                        });
                    }
                    else
                    {
                            sql.runQuery('SELECT id FROM ' + iw_girls + ' WHERE  tabid ="' + userId + '" ', function(errQry, result) {
                            if (errQry) {
                                console.log(errQry);
                                res = {
                                    status: 'error',
                                    message: 'Ungültiger Prozess'

                                };
                                callback('editProfile_res', res);
                            } else {
                                sql.runQuery('UPDATE ' + iw_girls + ' SET first_name="' + firstName + '",last_name="' + lastName + '",email="' + email + '",mobile="' + mobile + '" WHERE tabid="' + userId + '"', function(err_u, result_u) {
                                    if (err_u) {
                                        res = {
                                            status: 'error',
                                            message: 'Ungültiger Prozess'

                                        };
                                        callback('editProfile_res', res);
                                    } else {
                                        res = {
                                            status: 'success',
                                             message: 'Erfolgreich aktualisiert'
                                        };
                                        callback('editProfile_res', res);
                                    }
                                });
                            }
                        });
                    }
                    
                    break;
                    /*** get girls ***/
                case 'notificationCount':
                
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                    var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    const getNoteCount = async function(req, res) {
                        let sqlnCount = 'SELECT count(n.id) AS ntCount FROM iw_tns_notification n WHERE n.view_status=0 AND n.delete_status=0 AND n.id IN (SELECT nb.note_id FROM iw_tns_notification_branch nb WHERE nb.type="branch" AND nb.branch_id="'+branch+'")';
                       let nCount = await myQuery(sqlnCount);
                         console.log(nCount);
                        res = {
                            status: 'success',
                            noteCount: nCount.result
                        };
                        callback('notificationCount_res', res);
                    }
                    getNoteCount();
                    break;


            }
        }
    }
    /** Check not empty **/
function checkNotEmpty(val) {
    if (val != '' && val != 0 && typeof(val) != 'undefined' && typeof(val) != 'null')
        return true;
    else return false;
}
/** Addslashes function **/
function addslashes(str) {
    if (str != '' && typeof(str) != "undefined" && typeof(str) != "null")
        return slashes.add(str);
    else return str;
}
/** Stripslashes function **/
function stripslashes(str) {
    if (str != '' && typeof(str) != "undefined" && typeof(str) != "null")
        return slashes.strip(str);
    else return str;
}