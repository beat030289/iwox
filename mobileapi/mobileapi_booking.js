module.exports = {
        mobileApi: function(reqData, callback) {
            var process = reqData.process;
            //console.log(process);
            switch (process) {
                /*** get room and girls Details ***/
                case 'bookingdata':
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                    var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    const getRoomData = async function(req, res) {
                        try{
                            let sqlroom = 'SELECT r.id,r.titel,(SELECT b.title FROM ' + iw_branch + ' b WHERE b.id="' + branch + '")AS branchname FROM ' + iw_room + ' r WHERE r.branch_id="' + branch + '" AND r.id NOT IN (SELECT bb.room_id FROM ' + iw_booking + ' bb WHERE bb.status =0 AND bb.branch_id="'+branch+'" AND bb.booking_date="'+today+'")';
                           
                            let sqlgirls = 'SELECT g.id,g.profilename AS girlsname,(SELECT gv.image FROM  iw_tns_girls_gallery  gv WHERE gv.girls_id=g.id ORDER BY  gv.id DESC LIMIT 1) AS gimage  FROM iw_tns_workplanner wg LEFT JOIN iw_tns_girls g ON wg.girls_id=g.id WHERE wg.workdate="'+today+'" AND wg.branch_id="'+branch+'" AND wg.girls_id NOT IN (SELECT bb.girls_id FROM iw_tns_booking  bb WHERE bb.status =0 AND bb.branch_id="'+branch+'" AND bb.booking_date="'+today+'")';
                            let room = await myQuery(sqlroom);
                            let girls = await myQuery(sqlgirls);
                            /*console.log(room);
                            console.log(girls);*/
                            res = {
                                status: 'success',
                                room: room.result,
                                girls: girls.result
                                
                            };
                            callback('bookingdata_res', res);
                           // throw new Error();
                        } catch(e) {
                           console.log(e);
                         }
                    }
                    getRoomData();
                    break;
                case 'bookingServiceData':
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    girls = (utls.checkNotEmpty(reqData.girls)) ? reqData.girls : '';
                  const getServiceData = async function(req, res) {
                        try {
                        let sqlservice = 'SELECT s.id,s.title,s.price,s.zeit AS zid,(SELECT z.zeit FROM ' + iw_zeit + ' z WHERE z.id =s.zeit) AS zeit,(SELECT zz.timedata FROM ' + iw_zeit + ' zz WHERE zz.id =s.zeit) AS timedata FROM ' + iw_service + ' s WHERE s.branch="' + branch + '"';
                        let sqlextra = 'SELECT e.id,e.extra,e.price FROM ' + iw_extratype + ' e WHERE e.id IN (SELECT ge.extra FROM ' + iw_girls_extra + ' ge WHERE ge.girls_id ="'+girls+'")';
                        let sqlgirlsservice = 'SELECT s.id,(SELECT st.service FROM '+iw_servicetype+' st WHERE st.id=s.service) AS servicename FROM '+iw_girls_service+' s WHERE s.girls_id="'+girls+'"';
                        console.log(sqlservice);
                        let service = await myQuery(sqlservice);
                        let extra = await myQuery(sqlextra);
                        let girlsservice = await myQuery(sqlgirlsservice);
                        /*console.log(service);
                        console.log(extra);*/
                        res = {
                            status: 'success',
                            service: service.result,
                            extra: extra.result,
                            girlsService: girlsservice.result
                        };
                        //throw new Error();
                        callback('bookingServiceData_res', res);
                        } catch(e) {
                           
                            console.log(e);
                            
                        }
                    }
                    getServiceData();
                   break;
                case 'bookingSubmitData':
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    oId = (utls.checkNotEmpty(reqData.oId)) ? reqData.oId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    girls = (utls.checkNotEmpty(reqData.girls)) ? reqData.girls : '';
                    customer = (utls.checkNotEmpty(reqData.customer)) ? reqData.customer : '';
                    customerType = (utls.checkNotEmpty(reqData.customertype)) ? reqData.customertype : '';
                    service = (utls.checkNotEmpty(reqData.service)) ? reqData.service : '';
                    extra = (utls.checkNotEmpty(reqData.extra)) ? reqData.extra : '';
                    room = (utls.checkNotEmpty(reqData.room)) ? reqData.room : '';
                    timeid = (utls.checkNotEmpty(reqData.timeid)) ? reqData.timeid : '';
                    totaltime = (utls.checkNotEmpty(reqData.totaltime)) ? reqData.totaltime : '';
                    amount = (utls.checkNotEmpty(reqData.amount)) ? reqData.amount : '';
                    paymentstatus = (utls.checkNotEmpty(reqData.paymentstatus)) ? reqData.paymentstatus : '';
                    paymenttype = (utls.checkNotEmpty(reqData.paymenttype)) ? reqData.paymenttype : '';
                    service_time = (utls.checkNotEmpty(reqData.service_time)) ? reqData.service_time : '';
                    voucherprice = (utls.checkNotEmpty(reqData.voucherprice)) ? reqData.voucherprice : '';
                    voucherid = (utls.checkNotEmpty(reqData.voucherid)) ? reqData.voucherid : '';
                    vouchertype = (utls.checkNotEmpty(reqData.vouchertype)) ? reqData.vouchertype : '';
                    serviceprice = (utls.checkNotEmpty(reqData.serviceprice)) ? reqData.serviceprice : '';
                    extraprice = (utls.checkNotEmpty(reqData.extraprice)) ? reqData.extraprice : '';
                    /*console.log("userId"+userId);
                    console.log("type"+type);
                    console.log("branch"+branch);*/
                     var extratotal=0;
                        if (extra != "" && extra != undefined && extra != null) {
                         var getArr = extraprice.split(",");
                        if (Array.isArray(getArr)) {
                            console.log("Array");
                            /*const gAmount = async function(req, res) {
                                let eid = await condtionSqlArray(getArr);
                                let sqlGetExtra = 'SELECT COALESCE(SUM(e.price),0) AS emount FROM  iw_mas_extratype e WHERE e.id IN (' + eid + ')';
                                let extraAmount = await myQuery(sqlGetExtra);
                                console.log(sqlGetExtra);
                                console.log(extraAmount.result);
                                extratotal = extraAmount.result[0].eamount;
                            }
                            gAmount();*/
                             getArr.forEach(function(gg){
                                extratotal=parseInt(extratotal)+parseInt(gg);
                             });
                         }
                         else
                         {
                            console.log("not Array");
                            extratotal=extra;
                            /*const gAmount = async function(req, res) {
                                let sqlGetExtra = 'SELECT COALESCE(SUM(e.price),0) AS emount FROM  iw_mas_extratype e WHERE e.id ="'+extra+'"';
                                let extraAmount = await myQuery(sqlGetExtra);
                                extratotal = extraAmount.result[0].eamount;
                                console.log(sqlGetExtra);
                                console.log(extraAmount.result);
                            }
                            gAmount();*/
                         }
                     }
                    stotal =0;
                    sstotal =0;
                    if(voucherid!="" && voucherid!=undefined && voucherid!=null)
                    {
                        stotal=parseInt(amount)-parseInt(voucherprice);
                        sstotal=parseInt(serviceprice)-parseInt(voucherprice);
                    }
                    else
                    {
                        stotal=parseInt(amount);
                        sstotal=parseInt(serviceprice);
                    }
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                    var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    var createdTime = dateFormat(vvDate, "HH:MM:ss");
                    time1 = totaltime+":00";
                    time2 = createdTime;
                    time3 = "00:05:00";
                    console.log("time1"+totaltime);
                    console.log("time2"+createdTime);
                    console.log("time3"+time3);
                    var spAmount = (parseInt(sstotal)/2);
                    var rountamount = Math.ceil(spAmount / 10) * 10;
                    var girlsamount = 0;
                    var branchamount = 0;
                    if ((spAmount % 10) == 0)
                    { 
                        girlsamount = spAmount;
                        branchamount = spAmount;
                    }
                    else
                    {
                        girlsamount = rountamount;
                        branchamount =(parseInt(sstotal)-parseInt(rountamount));
                    }
                    
                    
                    //var branchamount =(parseInt(sstotal)-parseInt(rountamount));
                   /* console.log("girlsamount"+girlsamount);
                    console.log("branchamount"+branchamount);
                    console.log("serviceprice"+serviceprice);
                    console.log("extraprice"+extraprice);
                    console.log("extratotal"+extratotal);*/
                    var cc = formatTime(timestrToSec(time1) + timestrToSec(time2) + timestrToSec(time3));
                    const bookingSubmit = async function(req, res) {
                        try{
                        // End time calcualtion function
                        let endtime = await formatTime(timestrToSec(time1) + timestrToSec(time2) + timestrToSec(time3));
                        console.log("endtime"+endtime);
                        startdate = today + " " + createdTime;
                        enddate = today + " " + endtime;
                        // new booking insert
                        let sqlbooking = 'INSERT INTO ' + iw_booking + ' (`oid`,`created_id`, `created_type`, `customer`, `customer_type`, `branch_id`, `created_date`, `servicetime`, `starttime`, `room_id`, `endtime`, `booking_date`, `amount`, `paymentstatus`, `startdate`, `enddate`, `girls_id`, `service_id`, `paymenttype`, `voucherprice`, `voucherid`, `totalamount`, `vouchertype`, `girlsamount`, `branchamount`, `serviceamount`, `extraamount`) VALUES ("' + oId + '","' + userId + '","' + type + '","' + customer + '","' + customerType + '","' + branch + '","' + createdDate + '","' + totaltime + '","' + createdTime + '","' + room + '","' + endtime + '","' + today + '","' + amount + '","' + paymentstatus + '","' + startdate + '","' + enddate + '","' + girls + '","' + service + '","' + paymenttype + '","' + voucherprice + '","' + voucherid + '","' + stotal + '","' + vouchertype + '","' + girlsamount + '","' + branchamount + '","' + serviceprice + '","' + extratotal + '")';
                        let booking = await myQuery(sqlbooking);
                        newid = booking.result.insertId;
                        let sqlservicelog = 'INSERT INTO iw_tns_servicelog (`booking_id`,`service_id`, `branch_id`, `girls_id`, `type`, `up_down`, `created_date`) VALUES ("' + newid + '","' + branch + '","' + service + '","' + girls + '","1","0","' + createdDate + '")';
                            let servicelog = await myQuery(sqlservicelog);
                        if(paymenttype!="")
                            {
                                let sqlpaymentlog = 'INSERT INTO iw_tns_paymentlog (`booking_id`,`paymenttype`, `price`, `type`,`created_date`,`paymentfor`,`booking_date`,`girls_id`) VALUES ("' + newid + '","' + paymenttype + '","' + amount + '","1","' + createdDate + '","servicefirst","' + today + '","' + girls + '")';
                                let paymentlog = await myQuery(sqlpaymentlog);
                            }
                      //booking Extra insert
                        if (extra != "" && extra != undefined && extra != null) {
                            let extra_book = await multiRowInSql(extra, newid);
                            let sqlbookingExtra = 'INSERT INTO ' + iw_booking_extra + ' (`booking_id`, `extra_id`) VALUES ' + extra_book;
                            let bookingExtra = await myQuery(sqlbookingExtra);
                            //console.log(extra_book);
                            let extra_book2 = await multiRowInSqlLog(extra, newid,branch,girls,"1","0",createdDate);
                            let sqlextralog = 'INSERT INTO iw_tns_extralog (`booking_id`, `extra_id`, `branch_id`, `girls_id`, `type`, `up_down`, `created_date`) VALUES ' + extra_book2;
                            let extralog = await myQuery(sqlextralog);
                            console.log(extralog.result);
                        }
                        res = {
                            status: 'success',
                        };
                    }catch(e)
                    {
                        console.log(e);
                    }

                        callback('bookingSubmitData_res', res);
                    }

                    bookingSubmit();
                    break;
                case 'checkdata':
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    //console.log("checkdata");

                    break;
                case 'getHistoryData':
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';

                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                    var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    var tdiff = dateFormat(vvDate, "HH:MM:ss");
                    separatedata ="";
                    if(type!="girl")
                    {
                        separatedata=' AND t.branch_id="'+branch+'"';
                    }
                    else
                    {
                        separatedata=' AND t.created_id="'+userId+'"';
                    }
                    const historyData = async function(req, res) {
                       let sqlBookingList = 'SELECT "'+tdiff+'" AS ctime,t.serviceamount,(SELECT COUNT(id) FROM iw_tns_girls_service gs WHERE gs.girls_id = t.girls_id AND gs.service!=t.service_id) AS serviceedit,(SELECT COUNT(et.booking_id) FROM iw_extrabooking et WHERE et.booking_id = t.id) AS extracount,t.endtime,t.id,t.girlsname,t.gimage,t.servicename,t.roomname,t.girls_id,DATE_FORMAT(t.created_date, "%d.%m.%Y") AS cdate,DATE_FORMAT(t.created_date, "%d.%m.%Y (%H:%i)") AS created_date,t.starttime,t.endtime,t.totalamount AS amount,t.paymentstatus,t.paymenttype,t.status,t.servicetime,t.service_id,t.extraamount FROM iw_transaction t WHERE t.booking_date="'+today+'" '+separatedata+' AND t.status!="2" ORDER BY t.id DESC ';
                        let bookingList = await myQuery(sqlBookingList);
                      console.log(bookingList.result);
                        let bookidArray = await getUniqueArray(arrayColumn(bookingList.result, 'id'));
                        console.log(bookidArray);
                        if(bookidArray!="")
                        {
                           let sqlExtraList = 'SELECT et.booking_id,et.extra FROM iw_extrabooking et WHERE et.booking_id IN (' + bookidArray + ')';
                            let extraList = await myQuery(sqlExtraList);
                            var edata = extraList.result;
                        console.log(extraList); 
                        }
                        else
                        {
                            var edata = [];
                        }
                        
                        restt = {
                            status: 'success',
                            bookingList: bookingList.result,
                            extra: edata,
                          
                            
                        };
                        callback('getHistoryData_res', restt);

                    }
                    historyData();
                break;
                case 'autoupdate':
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';

                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                    var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    var currenttime = dateFormat(vvDate, "yyyy-mm-dd HH:MM:s");
                    console.log(currenttime);
                    const autoData = async function(req, res) {
                        let sqlTimeList = 'SELECT b.id FROM ' + iw_booking + ' b WHERE b.created_id="' + userId + '" AND b.enddate< "' + currenttime + '" AND b.status=0';
                        let ttime = await myQuery(sqlTimeList);
                        console.log("auto updatecheck");
                        console.log(ttime.result);
                        if(ttime.result!="")
                        {
                             let bookidArray = await getUniqueArray(arrayColumn(ttime.result, 'id'));
                            let bookid = await condtionSqlArray(bookidArray);
                            let sqlbookupdate = 'UPDATE ' + iw_booking + ' SET status="1" WHERE id IN (' + bookid + ')';
                            let bookupdate = await myQuery(sqlbookupdate);
                           

                            restt = {
                                status: 'success',

                            };
                        }
                        else
                        {
                            restt = {
                                status: 'success',

                            };
                        }
                       
                        callback('autoupdate_res', restt);

                    }
                    autoData();
                break;
                case 'getBookGirlsRoom':
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                    var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    const getGirlsRoom = async function(req, res) {
                        let sqlroom = 'SELECT r.id,r.titel,(SELECT count(nb.status)  FROM iw_transaction nb WHERE nb.room_id=r.id AND nb.status=0 AND nb.booking_date="'+today+'")AS roomcount FROM iw_tns_room r WHERE r.branch_id ="'+branch+'"';
                     
                     let sqlgirls = 'SELECT g.id,g.profilename AS girlsname,(SELECT gv.image FROM  iw_tns_girls_gallery  gv WHERE gv.girls_id=g.id ORDER BY  gv.id DESC LIMIT 1) AS gimage,(SELECT count(nb.status)  FROM iw_transaction nb WHERE nb.girls_id=g.id AND nb.status=0 AND nb.booking_date="'+today+'")AS girlscount  FROM iw_tns_workplanner wg LEFT JOIN iw_tns_girls g ON wg.girls_id=g.id WHERE wg.workdate="'+today+'" AND wg.branch_id="'+branch+'"';
                        let room = await myQuery(sqlroom);
                        let girls = await myQuery(sqlgirls);
                        /*console.log(room);
                        console.log(girls);*/
                        res = {
                            status: 'success',
                            room: room.result,
                            girls: girls.result
                        };
                        callback('getBookGirlsRoom_res', res);
                    }
                    getGirlsRoom();
                    break;
                    case 'getTransaction':
                    userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                    type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                    branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                    bookingId = (utls.checkNotEmpty(reqData.bookingId)) ? reqData.bookingId : '';
                    sdate = (utls.checkNotEmpty(reqData.sdate)) ? reqData.sdate : '';
                    edate = (utls.checkNotEmpty(reqData.edate)) ? reqData.edate : '';
                    vvDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Europe/Zurich',

                    });
                    var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                    var today = dateFormat(vvDate, "yyyy-mm-dd");
                    var swhare="";
                    if(bookingId=="" && sdate=="" && edate=="")
                    {
                        console.log("all");
                        swhare+=' AND  t.branch_id="' + branch + '" AND t.booking_date="'+today+'" AND t.status!="2" ORDER BY t.id DESC';
                    }
                    else if (bookingId!="" && sdate=="" && edate=="")
                    {
                        console.log("bookid");
                        swhare+=' AND  t.branch_id="' + branch + '" AND t.id="'+bookingId+'" AND t.status!="2" ORDER BY t.id DESC';
                    }
                    else if (bookingId=="" && sdate!="" && edate!="")
                    {
                        swhare+=' AND  t.branch_id="' + branch + '" AND t.booking_date>="'+sdate+'" AND t.booking_date<="'+edate+'"  AND t.status!="2" ORDER BY t.id DESC';
                    }
                    else if (bookingId!="" && sdate!="" && edate!="")
                    {
                        swhare+=' AND  t.branch_id="' + branch + '" AND t.id="'+bookingId+'" AND t.booking_date>="'+sdate+'" AND t.booking_date<="'+edate+'" AND t.status!="2" ORDER BY t.id DESC';
                    }
                    const transData = async function(req, res) {
                        let sqlBookingList = 'SELECT t.serviceamount,t.extramount,t.id,t.girlsname,t.gimage,t.servicename,t.roomname,DATE_FORMAT(t.created_date, "%d.%m.%Y (%H:%i)") AS created_date,t.starttime,t.endtime,t.totalamount AS amount,t.paymentstatus,t.paymenttype,t.status,t.servicetime FROM iw_transaction t WHERE t.branch_id="'+branch+'"'+swhare;
                        let bookingList = await myQuery(sqlBookingList);
                        let bookidArray = await getUniqueArray(arrayColumn(bookingList.result, 'id'));
                        if(bookidArray!="")
                        {
                            let sqlExtraList = 'SELECT et.booking_id,et.extra FROM iw_extrabooking et WHERE et.booking_id IN (' + bookidArray + ')';
                            let extraList = await myQuery(sqlExtraList);
                            console.log(extraList);
                            var edata = extraList.result;
                        }
                        else
                        {
                            var edata = [];
                        }
                        
                        restt = {
                            status: 'success',
                            bookingList: bookingList.result,
                            extra: edata,
                            tq: sqlBookingList,
                        };
                        callback('getTransaction_res', restt);

                    }
                    transData();
                break;
                case 'offercodeSubmitData':
                userId = (utls.checkNotEmpty(reqData.userId)) ? reqData.userId : '';
                type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
                branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
                codetype = (utls.checkNotEmpty(reqData.codetype)) ? reqData.codetype : '';
                offercode = (utls.checkNotEmpty(reqData.offercode)) ? reqData.offercode : '';
                vvDate = new Date().toLocaleString('en-US', {
                    timeZone: 'Europe/Zurich',

                });
                var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                var ntoday = dateFormat(vvDate, "yyyy-mm-dd");
                var today = dateFormat(vvDate, "mm/dd/yyyy");
                checkcode = new Date(today).getTime();
                code ="";
                if(codetype=="offercode")
                {
                    code=offercode;
                }
                else
                {
                    code=app.locals.decrypt(offercode);
                }
                    const offerData = async function(req, res) {
                        try{
                            let sqlofferdata ='SELECT g.id,g.uid,g.type,g.price,g.percentage FROM '+iw_vouchers+' g WHERE g.uid="'+code+'" AND (g.expiry > "'+ntoday+'" || g.expiry = "'+ntoday+'")';
                            let getofferdata = await myQuery(sqlofferdata);
                            console.log('SELECT g.id,g.uid,g.type,g.price,g.percentage FROM '+iw_vouchers+' g WHERE g.uid="'+code+'" AND g.expiry <= "'+ntoday+'"');
                            console.log(getofferdata.result);
                           if(getofferdata.result=="")
                            {
                                restt = {
                                status: 'error',
                                offerList: [],
                                };
                            }
                            else
                            {
                                restt = {
                                status: 'success',
                                offerList: getofferdata.result,
                                };
                            }
                             
                            callback('offercodeSubmitData_res', restt);
                        }catch(e)
                        {
                            console.log(e);
                        }
                    }
                    offerData();
                break;
                case 'getpriceDetails':
                id = (utls.checkNotEmpty(reqData.id)) ? reqData.id : '';
               vvDate = new Date().toLocaleString('en-US', {
                    timeZone: 'Europe/Zurich',

                });
                var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
                var today = dateFormat(vvDate, "yyyy-mm-dd");
                separatedata ="";
                
                const getPrice = async function(req, res) {
                try{
                       let sqlBookingPrice = 'SELECT t.id,(SELECT s.price FROM  iw_tns_service s WHERE s.id=t.service_id) AS serviceprice,t.servicename,t.roomname,DATE_FORMAT(t.created_date, "%d.%m.%Y (%h:%i)") AS created_date,t.totalamount,t.paymentstatus,t.paymenttype,t.status,t.servicetime,t.amount,t.vouchertype,t.voucherid,t.voucherprice,DATE_FORMAT(t.created_date, "%d.%m.%Y") AS cdate FROM iw_transaction t WHERE t.id="'+id+'"';
                        let bookingPrice = await myQuery(sqlBookingPrice);
                        var priceId=(bookingPrice.result[0]).voucherid;
                        console.log("jhshrrhoh"+priceId);
                        var voucherPrice="";
                        if(priceId!="" )
                        {
                            let sqlvoucherPrice = 'SELECT p.price,p.percentage FROM iw_tns_vouchers p WHERE p.id="'+priceId+'"';
                            let voucherPriceData = await myQuery(sqlvoucherPrice);
                            voucherPrice=voucherPriceData.result;
                        }
                        else
                        {
                          voucherPrice=[];
                        }
                        let sqlExtraPrice = 'SELECT et.extra,et.booking_id,(SELECT e.price FROM iw_mas_extratype e WHERE e.id=et.extra_id) AS extraprice,(SELECT t.paymentstatus FROM iw_transaction t WHERE t.id=et.booking_id) AS pstatus FROM iw_extrabooking et WHERE et.booking_id ="'+id+'"';
                        let sqlbookingDetais = 'SELECT t.id,t.paymentstatus FROM iw_transaction t WHERE t.id="'+id+'"';
                        let extraPrice = await myQuery(sqlExtraPrice);
                        let bookingStatus = await myQuery(sqlbookingDetais);
                        restt = {
                            status: 'success',
                            bookingPrice: bookingPrice.result,
                            extraPrice: extraPrice.result,
                            voucherPrice: voucherPrice,
                            bookingStatus: bookingStatus.result,
                        };
                        callback('getpriceDetails_res', restt);
                }
                catch(e)
                {
                    console.log(e);
                }
            }
            getPrice();
            break;


            }
        }
    }
    /** Check not empty **/
function checkNotEmpty(val) {
    if (val != '' && val != 0 && typeof(val) != 'undefined' && typeof(val) != 'null')
        return true;
    else return false;
}
/** Addslashes function **/
function addslashes(str) {
    if (str != '' && typeof(str) != "undefined" && typeof(str) != "null")
        return slashes.add(str);
    else return str;
}
/** Stripslashes function **/
function stripslashes(str) {
    if (str != '' && typeof(str) != "undefined" && typeof(str) != "null")
        return slashes.strip(str);
    else return str;
}
// time added function
function timestrToSec(timestr) {
    var parts = timestr.split(":");
    return (parts[0] * 3600) +
        (parts[1] * 60) +
        (+parts[2]);
}

function pad(num) {
    if (num < 10) {
        return "0" + num;
    } else {
        return "" + num;
    }
}

function formatTime(seconds) {
    return [pad(Math.floor(seconds / 3600)),
        pad(Math.floor(seconds / 60) % 60),
        pad(seconds % 60),
    ].join(":");
}