module.exports = {
getdetails:function(req,callback)
    {
        //console.log("weekplannerdata");
        sql.runQuery('SELECT b.title,b.id,b.status FROM '+iw_branch+' b', function(errQry, result) {
            if (errQry) 
            {
                console.log(errQry);
                result = [];
                callback(null);
            }else {
                sql.runQuery('SELECT concat(g.first_name," ",g.last_name)as gname,g.id,g.status,(SELECT gv.image FROM '+iw_girls_gallery+' gv WHERE gv.girls_id=g.id ORDER BY  gv.id DESC LIMIT 1) AS pimage FROM '+iw_girls+' g', function(gerrQry, gresult) {
                    if (gerrQry) 
                    {
                        //console.log(gerrQry);
                        gresult = [];
                        callback(null);
                    } else {
                        /*console.log(gresult);
                        console.log(result);*/
                        callback({
                            status: 'success',
                            branch: result,
                            girls: gresult,
                        });
                    }
                });
            }  
        });
    },
    // Work creation
    workSubmit: function(req, res, callback) {
        var reqData = res.req.body;
        //console.log(reqData);
        cdate = (utls.checkNotEmpty(reqData.cdate)) ? reqData.cdate : '';
        branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
        girls = (utls.checkNotEmpty(reqData.girls)) ? reqData.girls : '';
        edittype = (utls.checkNotEmpty(reqData.edittype)) ? reqData.edittype : '';
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        var total = girls.length;
        var gdata =0;
        if(edittype=="")
        {
            if(girls.length>0)
            {
                    girls.forEach(function(girlsval){
                    sql.runQuery('SELECT id FROM '+iw_workplanner+' WHERE girls_id ="'+girlsval+'" AND branch_id="'+branch+'" AND workdate="'+cdate+'"', function(bserrQry, bsresult){
                        if(bsresult=="")
                        {
                            sql.insertTable2(iw_workplanner, 'branch_id,girls_id,workdate,created_date,status', '"' + branch + '","' + girlsval + '","' + cdate + '","' + createdDate + '","1"', function(sierrQry, sresult) {
                                if(sierrQry)
                                {
                                    console.log(sierrQry);
                                    res = {status: 'error',message: sierrQry};
                                    callback(null, res);
                                }
                                else
                                { 
                                    
                                    gdata =parseInt(gdata)+parseInt(1); 
                                    if(total == gdata)
                                    {
                                        res = {status: 'success',message: 'successfully'};
                                        callback(null, res);
                                    }
                                    else
                                    {

                                    }
                                    
                                }
                            });
                        }
                        else
                        {
                            gdata =parseInt(gdata)+parseInt(1); 
                            if(total == gdata)
                            {
                                res = {status: 'success',message: 'successfully'};
                                callback(null, res);
                            }
                            else
                            {

                            }
                        }
                    });
                });
            }
            else
            {
               res = {status: 'success',message: 'successfully'};
                            callback(null, res); 
            }
        }
        else
        {
            sql.runQuery('DELETE FROM '+iw_workplanner+' WHERE branch_id="'+branch+'" AND workdate="'+cdate+'"', function(derrQry, dresult){
                if(dresult)
                {
                    if(girls.length>0)
                    {
                        girls.forEach(function(girlsval){
                        sql.insertTable2(iw_workplanner, 'branch_id,girls_id,workdate,created_date,status', '"' + branch + '","' + girlsval + '","' + cdate + '","' + createdDate + '","1"', function(sierrQry, sresult) {
                                if(sierrQry)
                                {
                                    console.log(sierrQry);
                                    res = {status: 'error',message: sierrQry};
                                    callback(null, res);
                                }
                                else
                                { 
                                    
                                    gdata =parseInt(gdata)+parseInt(1); 
                                    if(total == gdata)
                                    {
                                        res = {status: 'success',message: 'successfully'};
                                        callback(null, res);
                                    }
                                    else
                                    {

                                    }
                                    
                                }
                            });
                        }); 
                    }
                    else
                    {
                         res = {status: 'success',message: 'successfully'};
                                        callback(null, res);
                    }
                   
                }
                else
                {

                }
            });

        }
            
        },
        // Work get data
        workgetdata: function(req, res, callback) {
            var reqData = res.req.body;
            sdate = (utls.checkNotEmpty(reqData.sdate)) ? reqData.sdate : '';
            edate = (utls.checkNotEmpty(reqData.edate)) ? reqData.edate : '';
            createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
            var cmdata = "";
            var datagetdate={};
            var fullday = {};
            if(sdate != edate)
            {
              var dDate = twoDifferentDate(sdate,edate);
                
                vkey=0;
                tkey=dDate.length;
                dDate.forEach(function(dd,key){
                    datagetdate[key]=dd;
                    fullday[key]=dd.split('-')[2];
                    vkey=parseInt(key)+parseInt(1);
                    if(vkey == tkey)
                    {
                        cmdata+="'"+dd+"'";
                    }
                    else
                    {
                        cmdata+="'"+dd+"',";
                    }
                    
                });  
            }
            else
            {
                 cmdata+="'"+sdate+"'";
            }
            
        const getworkData = async function(req, res) {
            let sqlworkdata = 'SELECT w.branch_id,w.workdate,(SELECT GROUP_CONCAT(wc.girls_id) FROM '+iw_workplanner+' wc WHERE wc.workdate=w.workdate AND wc.branch_id=w.branch_id) AS girlsid,(SELECT b.title FROM '+iw_branch+' b WHERE b.id=w.branch_id) AS branchname,(SELECT bc.color FROM '+iw_branch+' bc WHERE bc.id=w.branch_id) AS branchcolor,(SELECT count(bb.branch_id) FROM '+iw_workplanner+' bb WHERE bb.workdate=w.workdate) AS branchcount,DATE_FORMAT(w.workdate, "%d") AS day FROM '+iw_workplanner+' w WHERE w.workdate IN (' + cmdata + ') ORDER BY w.branch_id ASC, w.workdate ASC';
            let sqlbranchdata = 'SELECT title,id,color,place FROM '+iw_branch+'';
            let sqlworkgirls = 'SELECT g.profilename AS gname,g.id FROM '+iw_girls+' g';
           let workdata = await myQuery(sqlworkdata);
           let branchdata = await myQuery(sqlbranchdata);
           let girlsdata = await myQuery(sqlworkgirls);
             /*console.log(workdata.result);
             console.log(branchdata.result);
             console.log(girlsdata.result);*/
             var sresult =workdata.result;
             var bresult =branchdata.result;
             var gresult =girlsdata.result;
              var arrdata = new Array();
                var kf = {};
                var kfc = {};
                var kfd = {};
                var kfg = {};
                var gdata = {};
                gresult.forEach(function(ggdata,key){
                    var gid = ggdata.id;
                    gdata[gid] = ggdata.gname;
                });
                
                sresult.forEach(function(sdata,key){
                   var bdid = sdata.branch_id+sdata.day;
                   var dtid = sdata.day;
                   var cid = "c"+sdata.branch_id;
                   var gid = "g"+sdata.branch_id+sdata.day;
                    kf[bdid] = sdata.branchname;
                    kfc[dtid] = sdata.branchcount;
                    kfd[cid] = sdata.branchcolor;
                    kfg[gid] = sdata.girlsid;
                    
                 });
                 arrdata.push(kf);
                 arrdata.push(kfc);
                 arrdata.push(kfd);
                 arrdata.push(kfg);
                 //arrdata.push(fullday);
                 /*console.log(fullday)*/
                /*console.log(arrdata); 
                console.log(gdata); */
                /*console.log("dDate"); */ 
                //console.log(datagetdate);
               res = {status: 'success',workdata: sresult,branch: bresult,rdata: arrdata,gdata: gdata,datagetdate: datagetdate,fullday: fullday};
                callback(null, res);
        }
        getworkData();  
            
        },
        // Work creation
    girlsData: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("get girls Create");
        console.log(reqData);
        cdate = (utls.checkNotEmpty(reqData.cdate)) ? reqData.cdate : '';
        branch = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
        type = (utls.checkNotEmpty(reqData.type)) ? reqData.type : '';
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        const getworkgirlsData = async function(req, res) {
            let sqlgirls = 'SELECT g.profilename,g.id,g.status,(SELECT wcp.branch_id FROM '+iw_workplanner+' wcp WHERE wcp.workdate="'+cdate+'" AND wcp.girls_id=g.id) AS branch_id,(SELECT count(wc.girls_id) FROM '+iw_workplanner+' wc WHERE wc.branch_id="'+branch+'" AND wc.workdate="'+cdate+'" AND wc.girls_id=g.id) AS gcount,(SELECT count(b.girls_id) FROM '+iw_booking+' b WHERE b.branch_id="'+branch+'" AND b.booking_date="'+cdate+'" AND b.girls_id=g.id) AS bgcount,(SELECT gv.image FROM '+iw_girls_gallery+' gv WHERE gv.girls_id=g.id ORDER BY  gv.id DESC LIMIT 1) AS pimage FROM '+iw_girls+' g ';
            let sqlwgirls = 'SELECT w.girls_id FROM '+iw_workplanner+' w WHERE w.branch_id="'+branch+'" AND w.workdate="'+cdate+'"';
           let girls = await myQuery(sqlgirls);
           let wgirls = await myQuery(sqlwgirls);
           console.log('SELECT w.girls_id FROM '+iw_workplanner+' w WHERE w.branch_id="'+branch+'" AND w.workdate="'+cdate+'"');
           /*console.log(girls.result);
           console.log(wgirls.result);*/
           res = {status: 'success',girls: girls.result,wgirls: wgirls.result};
            callback(null, res);
        }
        getworkgirlsData();
        },
};
//Date different functionality
function twoDifferentDate(startDate,endDate){
    var listDate = [];
//var startDate ='2017-02-01';
//var endDate = '2017-02-10';
    var dateMove = new Date(startDate);
    var strDate = startDate;

    while (strDate < endDate){
      var strDate = dateMove.toISOString().slice(0,10);
      listDate.push(strDate);
      dateMove.setDate(dateMove.getDate()+1);
    };
    return listDate;
    //console.log(listDate);
}
