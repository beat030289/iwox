module.exports = {
    // initial data for branch add function
    getBranchdetails: function(req, callback) {
        // get username list model
        sql.runQuery('SELECT id,first_name,last_name  FROM ' + iw_user + ' WHERE role!="1"', function(uerrQry, uresult) {
            if (uerrQry) uresult = [];
            else {
                sql.runQuery('SELECT id,category  FROM ' + iw_category + '', function(cerrQry, cresult) {
                    if (cerrQry) category = [];
                    else {
                        sql.runQuery('SELECT branch_id,responsible,file,DATE_FORMAT(startdate, "%d.%m.%Y") AS startdate,DATE_FORMAT(enddate, "%d.%m.%Y") AS enddate FROM ' + iw_authorization + '', function(ccerrQry, branchauth) {
                            if (ccerrQry) branchauth = [];
                            else {
                                // console.log(uresult);
                                callback({
                                    status: 'success',
                                    users: uresult,
                                    category: cresult,
                                    branchauth: branchauth,
                                });
                            }
                        });
                    }
                });
            }
        });
    },
    getleaderdetails: function(req, res, callback) {
        // leader details
        var reqData = res.req.body;
        userId = (utls.checkNotEmpty(reqData.userid)) ? reqData.userid : '';
        sql.runQuery('SELECT id,first_name,last_name,mobile,email,function,id,password,status,branch,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date FROM ' + iw_user + ' WHERE id ="' + userId + '"', function(errQry, result) {
            if (errQry) {
                console.log(errQry);
                result = [];
                callback(null);
            } else {
                console.log(result);
                res = {
                    status: 'success',
                    message: 'successfully',
                    leaderdetails: result
                };
                callback(null, res);
            }
        });
    },
    // Branch creation
    branchCreatepage: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("Branch Create");
        //console.log(reqData);
        titel = (utls.checkNotEmpty(reqData.titel)) ? reqData.titel : '';
        category = (utls.checkNotEmpty(reqData.category)) ? reqData.category : '';
        address = (utls.checkNotEmpty(reqData.branch_address)) ? reqData.branch_address : '';
        postcode = (utls.checkNotEmpty(reqData.postcode)) ? reqData.postcode : '';
        place = (utls.checkNotEmpty(reqData.place)) ? reqData.place : '';
        canton = (utls.checkNotEmpty(reqData.canton)) ? reqData.canton : '';
        email = (utls.checkNotEmpty(reqData.email)) ? reqData.email : '';
        phone = (utls.checkNotEmpty(reqData.phone)) ? reqData.phone : '';
        website = (utls.checkNotEmpty(reqData.website)) ? reqData.website : '';
        editid = (utls.checkNotEmpty(reqData.editid)) ? reqData.editid : '';
        leader = (utls.checkNotEmpty(reqData.leader)) ? reqData.leader : '';
        /*permissionSdate = (utls.checkNotEmpty(reqData.permissionSdate)) ? reqData.permissionSdate : '';
        permissionEdate = (utls.checkNotEmpty(reqData.permissionEdate)) ? reqData.permissionEdate : '';
        permissionName = (utls.checkNotEmpty(reqData.permissionName)) ? reqData.permissionName : '';
        */
        localapprovalpermit = (utls.checkNotEmpty(reqData.localapprovalpermit)) ? reqData.localapprovalpermit : '';
        old_file_permit = (utls.checkNotEmpty(reqData.old_file_permit)) ? reqData.old_file_permit : '';
        localpermitid = (utls.checkNotEmpty(reqData.localpermitid)) ? reqData.localpermitid : '';
        localapproval = (utls.checkNotEmpty(reqData.localapproval)) ? reqData.localapproval : '';
        old_file = (utls.checkNotEmpty(reqData.old_file)) ? reqData.old_file : '';
        localid = (utls.checkNotEmpty(reqData.localid)) ? reqData.localid : '';
        roomapproval = (utls.checkNotEmpty(reqData.roomapproval)) ? reqData.roomapproval : '';
        old_file_room = (utls.checkNotEmpty(reqData.old_file_room)) ? reqData.old_file_room : '';
        roomid = (utls.checkNotEmpty(reqData.roomid)) ? reqData.roomid : '';
        old_branch_image = (utls.checkNotEmpty(reqData.old_branch_image)) ? reqData.old_branch_image : '';
        color = (utls.checkNotEmpty(reqData.button_color)) ? reqData.button_color : '';
        var branchEncodeId = "";
        var fileNameTime = (new Date().getTime());
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        if (editid == "") {
            sql.runQuery('SELECT id FROM ' + iw_branch + ' WHERE title ="' + titel + '"', function(serrQry, sresult) {
                if (sresult == "") {
                    sql.insertTable2(iw_branch, 'title,category,address,postcode,place,canton,phone,email,website,leader,created_date,status,color', '"' + titel + '","' + category + '","' + address + '","' + postcode + '","' + place + '","' + canton + '","' + phone + '","' + email + '","' + website + '","' + leader + '","' + createdDate + '","1","' + color + '"', function(sierrQry, sresult) {
                        if (sierrQry) {
                            console.log(sierrQry);
                            res = {
                                status: 'error',
                                message: sierrQry
                            };
                            callback(null, res);
                        } else {
                            bid = sresult;
                            branchEncodeId = app.locals.encrypt(sresult);
                            if (req.files != null) {
                                if (req.files["branchimage"]) {
                                    console.log(req.files.branchimage);
                                    if (req.files.branchimage != undefined) {
                                        let localImage = req.files.branchimage;
                                        var oldFileName = localImage.name;
                                        var mimetype = localImage.mimetype;
                                        var strFirstThreec = randomstring.generate(5);
                                        var fileExtension = oldFileName.replace(/^.*\./, '');
                                        var newFileName = strFirstThreec + fileNameTime + '.' + fileExtension;
                                        // image upload function
                                        const numFruit = imageupload_multi(req.files.branchimage, newFileName, mimetype, old_branch_image, file_link_branch);
                                        console.log("numFruit" + numFruit);
                                        //file insert
                                        sql.runQuery('UPDATE ' + iw_branch + ' SET image="' + newFileName + '"  WHERE id="' + bid + '" ', function(uperr, upresult) {
                                            if (uperr) {
                                                console.log(uperr);
                                            }
                                        });

                                    }
                                }
                            }
                            if (Array.isArray(reqData.localapprovalpermit)) {
                                console.log("localapprovalpermitall");

                                const reduceLoop = async function() {
                                    //var clength = localapproval.length;
                                    //var flag = false;
                                    //var countval=0;
                                    for (var i = 0; i < localapprovalpermit.length; i++) {
                                        keyval = localapprovalpermit[i];
                                        var dyval = 'localDocpermit' + keyval;
                                        var plsdate = (utls.checkNotEmpty(reqData['permissionSdate' + keyval])) ? reqData['permissionSdate' + keyval] : '';
                                        var pledate = (utls.checkNotEmpty(reqData['permissionEdate' + keyval])) ? reqData['permissionEdate' + keyval] : '';
                                        var pname = (utls.checkNotEmpty(reqData['permissionName' + keyval])) ? reqData['permissionName' + keyval] : '';
                                        if (plsdate != "" && pledate != "" && req.files[dyval] != undefined && pname != "") {
                                            //console.log(keyval);
                                            console.log("keyval" + keyval);
                                            console.log("keyval" + dyval);
                                            console.log("lsdate" + plsdate);
                                            console.log("ledate" + pledate);
                                            console.log("pname" + pname);
                                            let localImage = req.files[dyval];
                                            var oldFileName = localImage.name;
                                            var mimetype = localImage.mimetype;
                                            var strFirstThreec = randomstring.generate(5);
                                            var fileExtension = oldFileName.replace(/^.*\./, '');
                                            var newFileName = strFirstThreec + fileNameTime + '.' + fileExtension;
                                            // image upload function
                                            const numFruit = await imageupload_multi(req.files[dyval], newFileName, mimetype, old_file_permit[i], file_link_localapproval);
                                            console.log("numFruit" + numFruit);
                                            // file insert 
                                            if (numFruit == undefined) {
                                                console.log("numFruitcheck" + numFruit);
                                                dataSave = {
                                                    branch_id: bid,
                                                    startdate: plsdate,
                                                    enddate: pledate,
                                                    responsible: pname,
                                                    file: newFileName,
                                                    created_date: createdDate
                                                };
                                                console.log(dataSave);
                                                sql.insertValues(iw_authorization, dataSave, function(err, insertedID) {});
                                            }
                                        } else {
                                            console.log("noddata");
                                            if (plsdate != "" && pledate != "" && pname != "") {
                                                console.log("nofile new link data");
                                                dataSave = {
                                                    branch_id: bid,
                                                    startdate: plsdate,
                                                    enddate: pledate,
                                                    responsible: pname,
                                                    created_date: createdDate
                                                };
                                                console.log(dataSave);
                                                sql.insertValues(iw_authorization, dataSave, function(err, insertedID) {});
                                            }
                                        }
                                    }
                                }
                                reduceLoop();

                            } else {
                                console.log("localapprovalpermitone");
                                plsdate = (utls.checkNotEmpty(reqData['permissionSdate1'])) ? reqData['permissionSdate1'] : '';
                                pledate = (utls.checkNotEmpty(reqData['permissionEdate1'])) ? reqData['permissionEdate1'] : '';
                                pname = (utls.checkNotEmpty(reqData['permissionName1'])) ? reqData['permissionName1'] : '';

                                if (plsdate != "" && pledate != "" && pname != "" && (req.files.localDocpermit1 != undefined || req.files.localDocpermit1 != null)) {
                                    let localImage = req.files.localDocpermit1;
                                    var oldFileName = localImage.name;
                                    var mimetype = localImage.mimetype;
                                    var strFirstThreec = randomstring.generate(5);
                                    var fileExtension = oldFileName.replace(/^.*\./, '');
                                    var newFileName = strFirstThreec + fileNameTime + '.' + fileExtension;
                                    // image upload function
                                    const numFruit = imageupload_multi(req.files.localDocpermit1, newFileName, mimetype, old_file_permit, file_link_localapproval);
                                    console.log("numFruit" + numFruit);
                                    //file insert
                                    dataSave = {
                                        branch_id: bid,
                                        startdate: plsdate,
                                        enddate: pledate,
                                        responsible: pname,
                                        file: newFileName,
                                        created_date: createdDate
                                    };
                                    console.log(dataSave);
                                    sql.insertValues(iw_authorization, dataSave, function(err, insertedID) {});
                                } else {
                                    console.log("single nodata")
                                    if (plsdate != "" && pledate != "" && pname != "") {
                                        console.log("nofile new link data");
                                        dataSave = {
                                            branch_id: bid,
                                            startdate: plsdate,
                                            responsible: pname,
                                            enddate: pledate,
                                            created_date: createdDate
                                        };
                                        console.log(dataSave);
                                        sql.insertValues(iw_authorization, dataSave, function(err, insertedID) {});
                                    }
                                }
                            }

                            if (Array.isArray(reqData.localapproval)) {

                                const reduceLoop = async function() {
                                    //var clength = localapproval.length;
                                    //var flag = false;
                                    //var countval=0;
                                    for (var i = 0; i < localapproval.length; i++) {
                                        keyval = localapproval[i];
                                        var dyval = 'localDoc' + keyval;
                                        lsdate = (utls.checkNotEmpty(reqData['localSdate' + keyval])) ? reqData['localSdate' + keyval] : '';
                                        ledate = (utls.checkNotEmpty(reqData['localEdate' + keyval])) ? reqData['localEdate' + keyval] : '';

                                        if (lsdate != "" && ledate != "" && req.files[dyval] != undefined) {
                                            console.log(keyval);
                                            let localImage = req.files[dyval];
                                            var oldFileName = localImage.name;
                                            var mimetype = localImage.mimetype;
                                            var strFirstThreec = randomstring.generate(5);
                                            var fileExtension = oldFileName.replace(/^.*\./, '');
                                            var newFileName = strFirstThreec + fileNameTime + '.' + fileExtension;
                                            // image upload function
                                            const numFruit = await imageupload_multi(req.files[dyval], newFileName, mimetype, old_file[i], file_link_localapproval);
                                            console.log("numFruit" + numFruit);
                                            // file insert 
                                            dataSave = {
                                                branch_id: bid,
                                                startdate: lsdate,
                                                enddate: ledate,
                                                file: newFileName,
                                                created_date: createdDate
                                            };
                                            console.log(dataSave);
                                            sql.insertValues(iw_localapproval, dataSave, function(err, insertedID) {});
                                        } else {
                                            console.log("noddata");
                                            if (lsdate != "" && ledate != "") {
                                                console.log("nofile new link data");
                                                dataSave = {
                                                    branch_id: bid,
                                                    startdate: lsdate,
                                                    enddate: ledate,
                                                    created_date: createdDate
                                                };
                                                console.log(dataSave);
                                                sql.insertValues(iw_localapproval, dataSave, function(err, insertedID) {});
                                            }
                                        }
                                    }
                                }
                                reduceLoop();

                            } else {
                                lsdate = (utls.checkNotEmpty(reqData['localSdate1'])) ? reqData['localSdate1'] : '';
                                ledate = (utls.checkNotEmpty(reqData['localEdate1'])) ? reqData['localEdate1'] : '';

                                if (lsdate != "" && ledate != "" && (req.files.localDoc1 != undefined || req.files.localDoc1 != null)) {
                                    let localImage = req.files.localDoc1;
                                    var oldFileName = localImage.name;
                                    var mimetype = localImage.mimetype;
                                    var strFirstThreec = randomstring.generate(5);
                                    var fileExtension = oldFileName.replace(/^.*\./, '');
                                    var newFileName = strFirstThreec + fileNameTime + '.' + fileExtension;
                                    // image upload function
                                    const numFruit = imageupload_multi(req.files.localDoc1, newFileName, mimetype, old_file, file_link_localapproval);
                                    console.log("numFruit" + numFruit);
                                    //file insert
                                    dataSave = {
                                        branch_id: bid,
                                        startdate: lsdate,
                                        enddate: ledate,
                                        file: newFileName,
                                        created_date: createdDate
                                    };
                                    console.log(dataSave);
                                    sql.insertValues(iw_localapproval, dataSave, function(err, insertedID) {});
                                } else {
                                    console.log("single nodata")
                                    if (lsdate != "" && ledate != "") {
                                        console.log("nofile new link data");
                                        dataSave = {
                                            branch_id: bid,
                                            startdate: lsdate,
                                            enddate: ledate,
                                            created_date: createdDate
                                        };
                                        console.log(dataSave);
                                        sql.insertValues(iw_localapproval, dataSave, function(err, insertedID) {});
                                    }
                                }
                            }
                            //room uploaded
                            if (Array.isArray(reqData.roomapproval)) {
                                console.log("roomapp");
                                const reduceLoop = async function() {
                                    //var clength = localapproval.length;
                                    //var flag = false;
                                    //var countval=0;
                                    for (var i = 0; i < roomapproval.length; i++) {
                                        keyval = roomapproval[i];
                                        var dyval = 'roomimage' + keyval;
                                        title = (utls.checkNotEmpty(reqData['roomtitle' + keyval])) ? reqData['roomtitle' + keyval] : '';
                                        description = (utls.checkNotEmpty(reqData['roomdescription' + keyval])) ? reqData['roomdescription' + keyval] : '';
                                        /*console.log("title"+title);
                                        console.log("description"+description);
                                        console.log(req.files[dyval]);*/
                                        if (title != "" && req.files[dyval] != undefined) {
                                            console.log(keyval);
                                            let localImage = req.files[dyval];
                                            var oldFileName = localImage.name;
                                            var mimetype = localImage.mimetype;
                                            var strFirstThreec = randomstring.generate(5);
                                            var fileExtension = oldFileName.replace(/^.*\./, '');
                                            var newFileName = strFirstThreec + fileNameTime + '.' + fileExtension;
                                            // image upload function
                                            const numFruit = await imageupload_multi(req.files[dyval], newFileName, mimetype, old_file_room[i], file_link_room);
                                            console.log("numFruit" + numFruit);
                                            // file insert 
                                            dataSave = {
                                                branch_id: bid,
                                                titel: title,
                                                description: description,
                                                file: newFileName,
                                                created_date: createdDate
                                            };
                                            console.log(dataSave);
                                            sql.insertValues(iw_room, dataSave, function(err, insertedID) {});
                                        } else {
                                            console.log("noddata");
                                        }
                                    }
                                }
                                reduceLoop();

                            } else {
                                title = (utls.checkNotEmpty(reqData['roomtitle1'])) ? reqData['roomtitle1'] : '';
                                description = (utls.checkNotEmpty(reqData['roomdescription1'])) ? reqData['roomdescription1'] : '';
                                /*console.log("title"+title);
                                console.log("description"+description);
                                console.log("roomapps");
                                console.log(req.files.roomimage1);*/
                                if (title != "" && req.files.roomimage1 != undefined) {
                                    let localImage = req.files.roomimage1;
                                    var oldFileName = localImage.name;
                                    var mimetype = localImage.mimetype;
                                    var strFirstThreec = randomstring.generate(5);
                                    var fileExtension = oldFileName.replace(/^.*\./, '');
                                    var newFileName = strFirstThreec + fileNameTime + '.' + fileExtension;
                                    // image upload function
                                    const numFruit = imageupload_multi(req.files.roomimage1, newFileName, mimetype, old_file_room, file_link_room);
                                    console.log("numFruit" + numFruit);
                                    //file insert
                                    dataSave = {
                                        branch_id: bid,
                                        titel: title,
                                        description: description,
                                        file: newFileName,
                                        created_date: createdDate
                                    };
                                    console.log(dataSave);
                                    sql.insertValues(iw_room, dataSave, function(err, insertedID) {});
                                } else {
                                    console.log("single nodata")
                                }
                            }
                            res = {
                                status: 'success',
                                message: 'successfully',
                                branchid: branchEncodeId
                            };
                            callback(null, res);
                        }
                    });
                } else {
                    console.log(serrQry);
                    res = {
                        status: 'already',
                        message: 'successfully'
                    };
                    callback(null, res);
                }

            });
        } else {
            console.log("editfile");
            sql.runQuery('SELECT id FROM ' + iw_branch + ' WHERE title ="' + titel + '" AND id!="' + editid + '"', function(serrQry, sresult) {
                if (sresult == "") {
                    console.log("rsfds");
                    sql.runQuery('UPDATE ' + iw_branch + ' SET title="' + titel + '",category="' + category + '",address="' + address + '",postcode="' + postcode + '",place="' + place + '",canton="' + canton + '",phone="' + phone + '",email="' + email + '",website="' + website + '",leader="' + leader + '",status="1",created_date="' + createdDate + '",color="' + color + '" WHERE id="' + editid + '"', function(sierrQry, sresult) {
                        if (sierrQry) {
                            console.log(sierrQry);
                            res = {
                                status: 'error',
                                message: sierrQry
                            };
                            callback(null, res);
                        } else {
                            bid = editid;
                            branchEncodeId = app.locals.encrypt(editid);
                            if (req.files != null) {
                                if (req.files["branchimage"]) {
                                    if (req.files.branchimage != undefined) {
                                        let localImage = req.files.branchimage;
                                        var oldFileName = localImage.name;
                                        var mimetype = localImage.mimetype;
                                        var strFirstThreec = randomstring.generate(5);
                                        var fileExtension = oldFileName.replace(/^.*\./, '');
                                        var newFileName = strFirstThreec + fileNameTime + '.' + fileExtension;
                                        // image upload function
                                        const numFruit = imageupload_multi(req.files.branchimage, newFileName, mimetype, old_branch_image, file_link_branch);
                                        console.log("numFruit" + numFruit);
                                        //file insert
                                        sql.runQuery('UPDATE ' + iw_branch + ' SET image="' + newFileName + '"  WHERE id="' + editid + '" ', function(uperr, upresult) {
                                            if (uperr) {
                                                console.log(uperr);
                                            }
                                        });

                                    }
                                }
                            }
                            if (Array.isArray(reqData.localapprovalpermit)) {
                                console.log("array function");
                                const reduceLoop = async function() {
                                    for (var i = 0; i < localapprovalpermit.length; i++) {
                                        keyval = localapprovalpermit[i];
                                        var dyval = 'localDocpermit' + keyval;
                                        var plsdate = (utls.checkNotEmpty(reqData['permissionSdate' + keyval])) ? reqData['permissionSdate' + keyval] : '';
                                        var pledate = (utls.checkNotEmpty(reqData['permissionEdate' + keyval])) ? reqData['permissionEdate' + keyval] : '';
                                        var pname = (utls.checkNotEmpty(reqData['permissionName' + keyval])) ? reqData['permissionName' + keyval] : '';
                                        if (req.files != null) {

                                            if (plsdate != "" && pledate != "" && pname != "" && req.files[dyval] != undefined && req.files[dyval] != null && localpermitid[i] == "abc") {
                                                let localImage = req.files[dyval];
                                                var oldFileName = localImage.name;
                                                var mimetype = localImage.mimetype;
                                                var strFirstThreec = randomstring.generate(5);
                                                var fileExtension = oldFileName.replace(/^.*\./, '');
                                                var newFileName = strFirstThreec + fileNameTime + '.' + fileExtension;
                                                console.log("-------------------------------------------------");
                                                console.log("new file data");
                                                const numFruit = await imageupload_multi(req.files[dyval], newFileName, mimetype, old_file_permit[i], file_link_localapproval);
                                                if (numFruit == undefined) {
                                                    dataSave = {
                                                        branch_id: bid,
                                                        startdate: plsdate,
                                                        enddate: pledate,
                                                        responsible: pname,
                                                        file: newFileName,
                                                        created_date: createdDate
                                                    };
                                                    console.log(dataSave);
                                                    sql.insertValues(iw_authorization, dataSave, function(err, insertedID) {});
                                                }
                                                console.log("-------------------------------------------------");

                                            } else if (plsdate != "" && pledate != "" && pname != "" && req.files[dyval] != undefined && req.files[dyval] != null && localpermitid[i] != "abc") {
                                                let localImage = req.files[dyval];
                                                var oldFileName = localImage.name;
                                                var mimetype = localImage.mimetype;
                                                var strFirstThreec = randomstring.generate(5);
                                                var fileExtension = oldFileName.replace(/^.*\./, '');
                                                var newFileName = strFirstThreec + fileNameTime + '.' + fileExtension;
                                                console.log("-------------------------------------------------");
                                                console.log("file only update");
                                                const numFruit = await imageupload_multi(req.files[dyval], newFileName, mimetype, old_file_permit[i], file_link_localapproval);
                                                if (numFruit == undefined) {
                                                    sql.runQuery('UPDATE ' + iw_authorization + ' SET file="' + newFileName + '",startdate="' + plsdate + '",enddate="' + pledate + '",responsible="' + pname + '"  WHERE branch_id="' + editid + '" AND id="' + localpermitid[i] + '"', function(uperr, upresult) {
                                                        if (uperr) {
                                                            console.log(uperr);
                                                        }
                                                    });
                                                }
                                                console.log("-------------------------------------------------");
                                            }
                                            else if (plsdate != "" && pledate != "" && pname != "" && localpermitid[i] != "abc" && (req.files[dyval] == undefined || req.files[dyval] == null||req.files[dyval] =="")) {
                                            console.log("-------------------------------------------------");
                                            console.log("data only update nofiledata");
                                            sql.runQuery('UPDATE ' + iw_authorization + ' SET startdate="' + plsdate + '",enddate="' + pledate + '",responsible="' + pname + '"  WHERE branch_id="' + editid + '" AND id="' + localpermitid[i] + '"', function(uperr, upresult) {
                                                if (uperr) {
                                                    console.log(uperr);
                                                }
                                            });
                                            console.log("-------------------------------------------------");
                                            } else if (plsdate != "" && pledate != "" && pname != "" && localpermitid[i] == "abc" && (req.files[dyval] == undefined || req.files[dyval] == null||req.files[dyval] =="")) {
                                            console.log("-------------------------------------------------");
                                            console.log("new data only insert nofiledata");
                                            dataSave = {
                                                branch_id: bid,
                                                startdate: plsdate,
                                                enddate: pledate,
                                                responsible: pname,
                                                created_date: createdDate
                                            };
                                            console.log(dataSave);
                                            sql.insertValues(iw_authorization, dataSave, function(err, insertedID) {});
                                            console.log("-------------------------------------------------");
                                            }
                                        } else {
                                            if (plsdate != "" && pledate != "" && pname != "" && localpermitid[i] != "abc") {
                                                console.log("-------------------------------------------------");
                                                console.log("data only update");
                                                sql.runQuery('UPDATE ' + iw_authorization + ' SET startdate="' + plsdate + '",enddate="' + pledate + '",responsible="' + pname + '"  WHERE branch_id="' + editid + '" AND id="' + localpermitid[i] + '"', function(uperr, upresult) {
                                                    if (uperr) {
                                                        console.log(uperr);
                                                    }
                                                });
                                                console.log("-------------------------------------------------");
                                            } else if (plsdate != "" && pledate != "" && pname != "" && localpermitid[i] == "abc") {
                                                console.log("-------------------------------------------------");
                                                console.log("new data only insert");
                                                dataSave = {
                                                    branch_id: bid,
                                                    startdate: plsdate,
                                                    enddate: pledate,
                                                    responsible: pname,
                                                    created_date: createdDate
                                                };
                                                console.log(dataSave);
                                                sql.insertValues(iw_authorization, dataSave, function(err, insertedID) {});
                                                console.log("-------------------------------------------------");
                                            }
                                        }


                                    }

                                }
                                reduceLoop();
                            } else {
                                console.log("not array function");
                                plsdate = (utls.checkNotEmpty(reqData['permissionSdate1'])) ? reqData['permissionSdate1'] : '';
                                pledate = (utls.checkNotEmpty(reqData['permissionEdate1'])) ? reqData['permissionEdate1'] : '';
                                pname = (utls.checkNotEmpty(reqData['permissionName1'])) ? reqData['permissionName1'] : '';
                                if (req.files != null) {
                                    if (req.files["localDocpermit1"]) {

                                        if (plsdate != "" && pledate != "" && pname != "" && req.files["localDocpermit1"] != undefined && req.files["localDocpermit1"] != null && localpermitid == "abc") {
                                            let localImage = req.files.localDocpermit1;
                                            var oldFileName = localImage.name;
                                            var mimetype = localImage.mimetype;
                                            var strFirstThreec = randomstring.generate(5);
                                            var fileExtension = oldFileName.replace(/^.*\./, '');
                                            var newFileName = strFirstThreec + fileNameTime + '.' + fileExtension;
                                            console.log("-------------------------------------------------");
                                            console.log("singal new file data");
                                            const numFruit = imageupload_multi(req.files.localDocpermit1, newFileName, mimetype, old_file_permit, file_link_localapproval);
                                            if (numFruit == undefined) {
                                                dataSave = {
                                                    branch_id: bid,
                                                    startdate: plsdate,
                                                    enddate: pledate,
                                                    responsible: pname,
                                                    file: newFileName,
                                                    created_date: createdDate
                                                };
                                                console.log(dataSave);
                                                sql.insertValues(iw_authorization, dataSave, function(err, insertedID) {});
                                            }
                                            console.log("-------------------------------------------------");

                                        } else if (plsdate != "" && pledate != "" && pname != "" && req.req.files["localDocpermit1"] != undefined && req.files["localDocpermit1"] != null && localpermitid != "abc") {
                                            let localImage = req.files.localDocpermit1;
                                            var oldFileName = localImage.name;
                                            var mimetype = localImage.mimetype;
                                            var strFirstThreec = randomstring.generate(5);
                                            var fileExtension = oldFileName.replace(/^.*\./, '');
                                            var newFileName = strFirstThreec + fileNameTime + '.' + fileExtension;
                                            console.log("-------------------------------------------------");
                                            console.log("singal file only update");
                                            const numFruit = imageupload_multi(req.files.localDocpermit1, newFileName, mimetype, old_file_permit, file_link_localapproval);
                                            if (numFruit == undefined) {
                                                sql.runQuery('UPDATE ' + iw_authorization + ' SET file="' + newFileName + '",startdate="' + plsdate + '",enddate="' + pledate + '",responsible="' + pname + '"  WHERE branch_id="' + editid + '" AND id="' + localpermitid + '"', function(uperr, upresult) {
                                                    if (uperr) {
                                                        console.log(uperr);
                                                    }
                                                });
                                            }
                                            console.log("-------------------------------------------------");
                                        }
                                    } else {
                                        console.log("without image");
                                        if (plsdate != "" && pledate != "" && pname != "" && localpermitid != "abc") {
                                            console.log("-------------------------------------------------");
                                            console.log("single data only update");
                                            sql.runQuery('UPDATE ' + iw_authorization + ' SET startdate="' + plsdate + '",enddate="' + pledate + '",responsible="' + pname + '"  WHERE branch_id="' + editid + '" AND id="' + localpermitid + '"', function(uperr, upresult) {
                                                if (uperr) {
                                                    console.log(uperr);
                                                }
                                            });
                                            console.log("-------------------------------------------------");
                                        } else if (plsdate != "" && pledate != "" && pname != "" && localpermitid == "abc") {
                                            console.log("-------------------------------------------------");
                                            console.log("single new data only insert");
                                            dataSave = {
                                                branch_id: bid,
                                                startdate: plsdate,
                                                enddate: pledate,
                                                responsible: pname,
                                                created_date: createdDate
                                            };
                                            console.log(dataSave);
                                            sql.insertValues(iw_authorization, dataSave, function(err, insertedID) {});
                                            console.log("-------------------------------------------------");
                                        }
                                    }


                                }
                            }


                            if (Array.isArray(reqData.localapproval)) {
                                console.log("array function");
                                const reduceLoop = async function() {
                                    //var clength = localapproval.length;
                                    //var flag = false;
                                    //var countval=0;
                                    for (var i = 0; i < localapproval.length; i++) {
                                        keyval = localapproval[i];
                                        var dyval = 'localDoc' + keyval;
                                        console.log(dyval);
                                        lsdate = (utls.checkNotEmpty(reqData['localSdate' + keyval])) ? reqData['localSdate' + keyval] : '';
                                        ledate = (utls.checkNotEmpty(reqData['localEdate' + keyval])) ? reqData['localEdate' + keyval] : '';
                                        if (req.files != null) {
                                            if (req.files[dyval]) {
                                                console.log("new file enter");
                                                if (lsdate != "" && ledate != "" && req.files[dyval] != undefined) {
                                                    //console.log(keyval);
                                                    let localImage = req.files[dyval];
                                                    var oldFileName = localImage.name;
                                                    var mimetype = localImage.mimetype;
                                                    var strFirstThreec = randomstring.generate(5);
                                                    var fileExtension = oldFileName.replace(/^.*\./, '');
                                                    var newFileName = strFirstThreec + fileNameTime + '.' + fileExtension;
                                                    // image upload function
                                                    const numFruit = await imageupload_multi(req.files[dyval], newFileName, mimetype, old_file[i], file_link_localapproval);
                                                    console.log("numFruit" + numFruit);
                                                    if (numFruit == undefined) {
                                                        if (old_file[i] != "nodata" && old_file[i] != " " && old_file[i] != undefined && old_file[i] != null) {
                                                            console.log("preivous");
                                                            sql.runQuery('UPDATE ' + iw_localapproval + ' SET file="' + newFileName + '"  WHERE branch_id="' + editid + '" AND id="' + localid[i] + '"', function(uperr, upresult) {
                                                                if (uperr) {
                                                                    console.log(uperr);
                                                                }
                                                            });
                                                        } else {
                                                            // file insert 
                                                            console.log("new extra file upload");
                                                            dataSave = {
                                                                branch_id: bid,
                                                                startdate: lsdate,
                                                                enddate: ledate,
                                                                file: newFileName,
                                                                created_date: createdDate
                                                            };
                                                            console.log(dataSave);
                                                            sql.insertValues(iw_localapproval, dataSave, function(err, insertedID) {});
                                                        }
                                                    }

                                                } else {
                                                    console.log("nodata for insert");
                                                    if (lsdate != "" && ledate != "") {
                                                        console.log("nofile new link data");
                                                        dataSave = {
                                                            branch_id: bid,
                                                            startdate: lsdate,
                                                            enddate: ledate,
                                                            created_date: createdDate
                                                        };
                                                        console.log(dataSave);
                                                        sql.insertValues(iw_localapproval, dataSave, function(err, insertedID) {});
                                                    }
                                                    console.log("noddata");
                                                }
                                            }

                                        } else {
                                            console.log("ondata show");
                                            console.log("lsdate" + lsdate);
                                            console.log("localid[i]" + localid[i]);
                                            if (localid[i] != "") {
                                                sql.runQuery('UPDATE ' + iw_localapproval + ' SET startdate="' + lsdate + '",enddate="' + ledate + '"  WHERE branch_id="' + editid + '" AND id="' + localid[i] + '"', function(uperr, upresult) {
                                                    if (uperr) {
                                                        console.log(uperr);
                                                    }
                                                });
                                            } else {
                                                if (lsdate != "" && ledate != "") {
                                                    dataSave = {
                                                        branch_id: bid,
                                                        startdate: lsdate,
                                                        enddate: ledate,
                                                        created_date: createdDate
                                                    };
                                                    console.log(dataSave);
                                                    sql.insertValues(iw_localapproval, dataSave, function(err, insertedID) {});
                                                } else {
                                                    console.log("edit onefile");
                                                }

                                            }


                                        }
                                    }
                                }
                                reduceLoop();

                            } else {
                                console.log("with out file editdata start");
                                lsdate = (utls.checkNotEmpty(reqData['localSdate1'])) ? reqData['localSdate1'] : '';
                                ledate = (utls.checkNotEmpty(reqData['localEdate1'])) ? reqData['localEdate1'] : '';
                                if (req.files != null) {
                                    if (req.files["localDoc1"]) {
                                        console.log(req.files.localDoc1);
                                        if (lsdate != "" && ledate != "" && (req.files.localDoc1 != undefined || req.files.localDoc1 != null)) {
                                            console.log("step1");
                                            let localImage = req.files.localDoc1;
                                            var oldFileName = localImage.name;
                                            var mimetype = localImage.mimetype;
                                            var strFirstThreec = randomstring.generate(5);
                                            var fileExtension = oldFileName.replace(/^.*\./, '');
                                            var newFileName = strFirstThreec + fileNameTime + '.' + fileExtension;
                                            // image upload function
                                            const numFruit = imageupload_multi(req.files.localDoc1, newFileName, mimetype, old_file, file_link_localapproval);
                                            console.log("numFruit" + numFruit);
                                            if (old_file != "nodata" && old_file != " " && old_file != undefined && old_file != null) {
                                                console.log("step2");
                                                sql.runQuery('UPDATE ' + iw_localapproval + ' SET file="' + newFileName + '"  WHERE branch_id="' + editid + '" AND id="' + localid + '"', function(uperr, upresult) {
                                                    if (uperr) {
                                                        console.log(uperr);
                                                    }
                                                });
                                            } else {
                                                if (req.files.localDoc1 != undefined || req.files.localDoc1 != null) {
                                                    dataSave = {
                                                        branch_id: bid,
                                                        startdate: lsdate,
                                                        enddate: ledate,
                                                        file: newFileName,
                                                        created_date: createdDate
                                                    };
                                                    console.log(dataSave);
                                                    sql.insertValues(iw_localapproval, dataSave, function(err, insertedID) {});
                                                } else {
                                                    console.log("*******************************************************************");
                                                    dataSave = {
                                                        branch_id: bid,
                                                        startdate: lsdate,
                                                        enddate: ledate,
                                                        created_date: createdDate
                                                    };
                                                    console.log(dataSave);
                                                    sql.insertValues(iw_localapproval, dataSave, function(err, insertedID) {});
                                                }

                                            }

                                        } else {
                                            console.log("single nodata")
                                        }
                                    }
                                } else {
                                    console.log("with out file editdata");
                                    dataSave = {
                                        branch_id: bid,
                                        startdate: lsdate,
                                        enddate: ledate,
                                        created_date: createdDate
                                    };
                                    //console.log(dataSave);
                                    sql.insertValues(iw_localapproval, dataSave, function(err, insertedID) {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            console.log(insertedID);
                                        }
                                    });
                                }
                            }
                            //room uploaded
                            if (Array.isArray(reqData.roomapproval)) {

                                const reduceLoop = async function() {
                                    //var clength = localapproval.length;
                                    //var flag = false;
                                    //var countval=0;
                                    console.log("edit file upload");
                                    for (var i = 0; i < roomapproval.length; i++) {
                                        keyval = roomapproval[i];
                                        var dyval = 'roomimage' + keyval;

                                        title = (utls.checkNotEmpty(reqData['roomtitle' + keyval])) ? reqData['roomtitle' + keyval] : '';
                                        description = (utls.checkNotEmpty(reqData['roomdescription' + keyval])) ? reqData['roomdescription' + keyval] : '';
                                        console.log("title" + title);
                                        console.log("description" + description);
                                        console.log(req.files);
                                        if (req.files != null) {
                                            if (req.files[dyval]) {
                                                //console.log(req.files[dyval]);
                                                if (title != "" && req.files[dyval] != undefined) {
                                                    console.log("testdataval");
                                                    //console.log(dyval);
                                                    let localImage = req.files[dyval];
                                                    var oldFileName = localImage.name;
                                                    var mimetype = localImage.mimetype;
                                                    var strFirstThreec = randomstring.generate(5);
                                                    var fileExtension = oldFileName.replace(/^.*\./, '');
                                                    var newFileName = strFirstThreec + fileNameTime + '.' + fileExtension;
                                                    // image upload function
                                                    const numFruit = await imageupload_multi(req.files[dyval], newFileName, mimetype, old_file_room[i], file_link_room);
                                                    console.log("numFruit" + numFruit);
                                                    console.log("oldfile" + old_file_room[i]);
                                                    // file insert 
                                                    if (old_file_room[i] != "nodata" && old_file_room[i] != " " && old_file_room[i] != undefined && old_file_room[i] != null) {
                                                        sql.runQuery('UPDATE ' + iw_room + ' SET file="' + newFileName + '"  WHERE branch_id="' + editid + '" AND id="' + roomid[i] + '"', function(uperr, upresult) {
                                                            if (uperr) {
                                                                console.log(uperr);
                                                            }
                                                        });
                                                    } else {
                                                        // file insert 
                                                        dataSave = {
                                                            branch_id: bid,
                                                            titel: title,
                                                            description: description,
                                                            file: newFileName,
                                                            created_date: createdDate
                                                        };
                                                        console.log(dataSave);
                                                        sql.insertValues(iw_room, dataSave, function(err, insertedID) {});
                                                    }
                                                } else {
                                                    console.log("noddata");
                                                }
                                            }
                                        } else {
                                            console.log("noddata" + old_file_room[i]);
                                            if (old_file_room[i] != "nodata" && old_file_room[i] != " " && old_file_room[i] != undefined && old_file_room[i] != null) {
                                                sql.runQuery('UPDATE ' + iw_room + ' SET titel="' + title + '",description="' + description + '"  WHERE branch_id="' + editid + '" AND id="' + roomid[i] + '"', function(uperr, upresult) {
                                                    if (uperr) {
                                                        console.log(uperr);
                                                    }
                                                });
                                            } else {
                                                // file insert 
                                                dataSave = {
                                                    branch_id: bid,
                                                    titel: title,
                                                    description: description,
                                                    file: newFileName,
                                                    created_date: createdDate
                                                };
                                                console.log(dataSave);
                                                sql.insertValues(iw_room, dataSave, function(err, insertedID) {});
                                            }
                                        }
                                    }
                                }
                                reduceLoop();

                            } else {
                                title = (utls.checkNotEmpty(reqData['roomtitle1'])) ? reqData['roomtitle1'] : '';
                                description = (utls.checkNotEmpty(reqData['roomdescription1'])) ? reqData['roomdescription1'] : '';
                                if (req.files != null) {
                                    if (req.files["roomimage1"]) {
                                        if (title != "" && req.files.roomimage1 != undefined) {
                                            let localImage = req.files.roomimage1;
                                            var oldFileName = localImage.name;
                                            var mimetype = localImage.mimetype;
                                            var strFirstThreec = randomstring.generate(5);
                                            var fileExtension = oldFileName.replace(/^.*\./, '');
                                            var newFileName = strFirstThreec + fileNameTime + '.' + fileExtension;
                                            // image upload function
                                            const numFruit = imageupload_multi(req.files.roomimage1, newFileName, mimetype, old_file_room, file_link_room);
                                            console.log("numFruit" + numFruit);
                                            //file insert
                                            if (old_file_room != "nodata" && old_file_room != " " && old_file_room != undefined && old_file_room != null) {
                                                console.log("step2");
                                                sql.runQuery('UPDATE ' + iw_room + ' SET file="' + newFileName + '"  WHERE branch_id="' + editid + '" AND id="' + roomid + '"', function(uperr, upresult) {
                                                    if (uperr) {
                                                        console.log(uperr);
                                                    }
                                                });
                                            } else {
                                                console.log("step3");
                                                //file insert
                                                dataSave = {
                                                    branch_id: bid,
                                                    titel: title,
                                                    description: description,
                                                    file: newFileName,
                                                    created_date: createdDate
                                                };
                                                console.log(dataSave);
                                                sql.insertValues(iw_room, dataSave, function(err, insertedID) {});
                                            }
                                        } else {
                                            console.log("single nodata");
                                        }
                                    }
                                } else {
                                    console.log("single nodata edit");
                                    if (old_file_room != "nodata" && old_file_room != " " && old_file_room != undefined && old_file_room != null) {
                                        console.log("step2");
                                        sql.runQuery('UPDATE ' + iw_room + ' SET titel="' + title + '",description="' + description + '"  WHERE branch_id="' + editid + '" AND id="' + roomid + '"', function(uperr, upresult) {
                                            if (uperr) {
                                                console.log(uperr);
                                            }
                                        });
                                    } else {
                                        console.log("step3");
                                        //file insert
                                        dataSave = {
                                            branch_id: bid,
                                            titel: title,
                                            description: description,
                                            file: newFileName,
                                            created_date: createdDate
                                        };
                                        console.log(dataSave);
                                        sql.insertValues(iw_room, dataSave, function(err, insertedID) {});
                                    }
                                }

                            }
                            res = {
                                status: 'success',
                                message: 'successfully',
                                branchid: branchEncodeId
                            };
                            console.log(res);
                            callback(null, res);
                        }
                    });
                } else {
                    console.log(serrQry);
                    res = {
                        status: 'already',
                        message: 'successfully'
                    };
                    callback(null, res);
                }

            });

        }
    },
    // Get Branch Edit function
    editbranch: function(req, callback) {
        console.log("branch------------------------------------------------");
        console.log(req.query.branchid);
        //branchid = req.query.branchid;
        vvDate = new Date().toLocaleString('en-US', {
            timeZone: 'Europe/Zurich',
        });
        var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
        var today = dateFormat(vvDate, "yyyy-mm-dd");
        branchid = app.locals.decrypt(req.query.branchid);
        console.log(branchid);
        const getBranch = async function(req, res) {
            /*LEFT JOIN table2
ON table1.column_name = table2.column_name;*/
            let sqlbresult = 'SELECT (SELECT count(id)  FROM ' + iw_room + ' r WHERE r.branch_id="' + branchid + '")AS roomcount,(SELECT count(id)  FROM ' + iw_workplanner + ' w WHERE w.branch_id="' + branchid + '" AND w.workdate="' + today + '")AS girlscount,b.id,b.color,b.title,b.category,b.address,b.postcode,b.canton,b.place,b.image,b.email,b.phone,b.website,u.mobile as umobile,b.leader,u.email as uemail,u.id as userid,concat(u.first_name," ",u.last_name)as uname,(SELECT c.category FROM ' + iw_category + ' c WHERE b.category=c.id)as catename  FROM ' + iw_branch + ' b, ' + iw_user + ' u WHERE (u.id=b.leader OR b.leader="") AND b.id="' + branchid + '"';
            /*let sqlbresult = 'SELECT (SELECT count(id)  FROM '+iw_room+' r WHERE r.branch_id="'+branchid+'")AS roomcount,(SELECT count(id)  FROM '+iw_workplanner+' w WHERE w.branch_id="'+branchid+'" AND w.workdate="'+today+'")AS girlscount,b.id,b.color,b.title,b.category,b.address,b.postcode,b.canton,b.place,b.image,b.email,b.phone,b.website,u.mobile as umobile,u.email as uemail,u.id as userid,concat(u.first_name," ",u.last_name)as uname,(SELECT c.category FROM '+iw_category+' c WHERE b.category=c.id)as catename  FROM '+iw_branch+' b LEFT JOIN '+iw_user+' u ON (u.id=b.leader OR b.leader="") AND b.id="'+branchid+'" GROUP BY b.id'; */
            let sqluresult = 'SELECT id,first_name,last_name  FROM ' + iw_user + ' WHERE role!="1"';
            let sqlcresult = 'SELECT id,category  FROM ' + iw_category + '';
            let sqlbranchauth = 'SELECT a.id,a.branch_id,a.responsible,a.file,a.startdate,a.enddate,DATE_FORMAT(a.created_date, "%b, %d %Y") AS spdate,DATE_FORMAT(a.created_date, "%h %p") AS sptime,(SELECT concat(uc.first_name," " ,uc.last_name) AS cc FROM ' + iw_user + ' uc WHERE uc.id=a.responsible) AS uname FROM ' + iw_authorization + ' a WHERE a.branch_id="' + branchid + '"';
            let sqllocalauth = 'SELECT id,branch_id,startdate,enddate,file,DATE_FORMAT(created_date, "%b, %d %Y") AS spdate,DATE_FORMAT(created_date, "%h %p") AS sptime FROM ' + iw_localapproval + ' WHERE branch_id="' + branchid + '"';
            let sqlrresult = 'SELECT id,branch_id,titel,description,file FROM ' + iw_room + ' WHERE branch_id="' + branchid + '"';
            let sqlgresult = 'SELECT g.id,g.profilename AS girlsname,(SELECT gv.image FROM  iw_tns_girls_gallery  gv WHERE gv.girls_id=g.id ORDER BY  gv.id DESC LIMIT 1) AS gimage  FROM iw_tns_workplanner wg LEFT JOIN iw_tns_girls g ON wg.girls_id=g.id WHERE wg.workdate="' + today + '" AND wg.branch_id="' + branchid + '"';
            let bresult = await myQuery(sqlbresult);
            let uresult = await myQuery(sqluresult);
            let cresult = await myQuery(sqlcresult);
            let branchauth = await myQuery(sqlbranchauth);
            let localauth = await myQuery(sqllocalauth);
            let rresult = await myQuery(sqlrresult);
            let gresult = await myQuery(sqlgresult);

            res = {
                status: 'success',
                details: bresult.result,
                users: uresult.result,
                category: cresult.result,
                branchauth: branchauth.result,
                localauth: localauth.result,
                rooms: rresult.result,
                girls: gresult.result,
                sdata: sqlbresult,
            };
            console.log(res);
            callback(res);
        }
        getBranch();

    },
    getBranch: function(req, callback) {
        vvDate = new Date().toLocaleString('en-US', {
            timeZone: 'Europe/Zurich',
        });
        var createdDate = dateFormat(vvDate, "yyyy-mm-dd HH:MM");
        var today = dateFormat(vvDate, "yyyy-mm-dd");
        sql.runQuery('SELECT (SELECT count(id)  FROM ' + iw_room + ' r WHERE r.branch_id=b.id)AS roomcount,(SELECT count(id)  FROM ' + iw_workplanner + ' w WHERE w.branch_id=b.id AND w.workdate="' + today + '")AS girlscount,b.id,b.color,b.title,b.address,b.postcode,b.canton,b.image,b.place,b.email,b.phone,b.website,(SELECT c.category FROM ' + iw_category + ' c WHERE c.id=b.category) AS category,(SELECT concat(u.first_name," ",u.last_name) FROM ' + iw_user + ' u WHERE u.id=b.leader) AS leader  FROM ' + iw_branch + ' b ORDER BY b.id DESC LIMIT 10', function(berrQry, bresult) {
            if (berrQry) {
                console.log(berrQry);
                bresult = [];
            } else {
                sql.runQuery('SELECT DISTINCT b.category as id,(SELECT c.category FROM ' + iw_category + ' c WHERE c.id=b.category) AS categoryc FROM ' + iw_branch + ' b ', function(cerrQry, cresult) {
                    if (cerrQry) {
                        console.log(cerrQry);
                        cresult = [];
                    } else {
                        sql.runQuery('SELECT DISTINCT b.leader as id,(SELECT concat(u.first_name," ",u.last_name) FROM ' + iw_user + ' u WHERE u.id=b.leader) AS leaderc FROM ' + iw_branch + ' b ', function(lerrQry, lresult) {
                            if (lerrQry) {
                                console.log(lerrQry);
                                lresult = [];
                            } else {
                                console.log("checkdata-------------------");
                                res = {
                                    details: bresult,
                                    category: cresult,
                                    leader: lresult
                                };
                                console.log(res);
                                callback({
                                    status: 'success',
                                    details: bresult,
                                    category: cresult,
                                    leader: lresult,
                                });
                            }
                        });
                    }
                });
            }
        });
    },
    branchListpage: function(req, res, callback) {
        var reqData = res.req.body;
        sWhere = '';
        limit = (utls.checkNotEmpty(reqData.limit)) ? reqData.limit : '';
        vctitle = (utls.checkNotEmpty(reqData.vctitle)) ? reqData.vctitle : '';
        vccategory = (utls.checkNotEmpty(reqData.vccategory)) ? reqData.vccategory : '';
        vcroom = (utls.checkNotEmpty(reqData.vcroom)) ? reqData.vcroom : '';
        vcleader = (utls.checkNotEmpty(reqData.vcleader)) ? reqData.vcleader : '';
        vcplace = (utls.checkNotEmpty(reqData.vcplace)) ? reqData.vcplace : '';
        sWhere += typeof(vctitle) != 'undefined' && vctitle != '' && vctitle != 'all' ? ' AND b.title="' + vctitle + '" ' : '';
        sWhere += typeof(vccategory) != 'undefined' && vccategory != '' && vccategory != 'all' ? ' AND b.category="' + vccategory + '" ' : '';
        //sWhere+=typeof(vcroom)!='undefined' && vcroom!='' && vcroom!='all'?' AND title="'+vcroom+'" ':'';
        sWhere += typeof(vcleader) != 'undefined' && vcleader != '' && vcleader != 'all' ? ' AND b.leader="' + vcleader + '" ' : '';
        sWhere += typeof(vcplace) != 'undefined' && vcplace != '' && vcplace != 'all' ? ' AND b.place="' + vcplace + '" ' : '';


        sql.runQuery('SELECT b.id,b.color,b.title,b.address,b.postcode,b.canton,b.image,b.place,b.email,b.phone,b.website,(SELECT c.category FROM ' + iw_category + ' c WHERE c.id=b.category) AS category,(SELECT concat(u.first_name," ",u.last_name) FROM ' + iw_user + ' u WHERE u.id=b.leader) AS leader  FROM ' + iw_branch + ' b WHERE 1=1 ' + sWhere + ' ORDER BY b.id DESC LIMIT ' + limit + '', function(berrQry, bresult) {
            if (berrQry) {
                console.log(berrQry);
                bresult = [];
                res = {
                    status: 'error',
                    branchlist: bresult
                };
                //console.log(res);
                callback(null, res);
            } else {
                //console.log(bresult);
                var data = {
                    branchlist: bresult
                };
                app.render('branch-templates/branchlist', data, function(err, html) {
                    //console.log(err);
                    //console.log(html);
                    var res = {
                        status: 'error',
                        html: html
                    };
                    callback(null, res);
                });
            }
        });
    },

};

function imageupload_multi(imagefiledata, newfilename, mimetype, oldfile, destination) {

    /*console.log("start");
    console.log(imagefiledata);
    console.log(newfilename);
    console.log(mimetype);
    console.log(oldfile);
    console.log(destination);*/
    imagefiledata.mv(destination + newfilename, function(err) {
        if (err) {

        } else {
            console.log("uploaddone");
            /*uploadImageAws(newfilename, mimetype, "product", "product", newfilename, function(status, data) {
                if (status) {
                    fs.unlink(imageurlink_product + newfilename);
                    return true;
                }
            });*/
            //console.log(oldfile);
            if (oldfile != "nodata" && oldfile != " " && oldfile != undefined && oldfile != null) {
                fs.unlink(destination + oldfile);
                return true;
            } else {
                return true;

            }

        }


    });
}

//res.send(JSON.stringify({ "status": 200, "method": 'create', 'items': req.session.cartitems }));

//module.exports = Login;