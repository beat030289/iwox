module.exports = {
    // Extra Type creation
    createExtratype: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("extratype Create");
        //console.log(reqData);
        extratype = (utls.checkNotEmpty(reqData.extratype)) ? reqData.extratype : '';
        price = (utls.checkNotEmpty(reqData.price)) ? reqData.price : '';
        eid = (utls.checkNotEmpty(reqData.eid)) ? reqData.eid : '';
        nickname = (utls.checkNotEmpty(reqData.nickname)) ? reqData.nickname : '';
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        if(eid=="")
        {
           sql.runQuery('SELECT id FROM '+iw_extratype+' WHERE extra ="'+extratype+'"', function(serrQry, sresult){
                if(sresult=="")
                {
                   sql.insertTable2(iw_extratype, 'extra,price,created_date,status,nickname', '"' + extratype + '","'+price+'","' + createdDate + '","1","' + nickname + '"', function(sierrQry, sresult) {
                        if(sierrQry)
                        {
                            console.log(sierrQry);
                            res = {status: 'error',message: sierrQry};
                            callback(null, res);
                        }
                        else
                        {
                            res = {status: 'success',message: 'successfully'};
                            callback(null, res);
                        }
                    });
                }
                else
                {
                    console.log(serrQry);
                    res = {status: 'already',message: 'successfully'};
                    callback(null, res);  
                } 
            });
        }
        else
        {
            sql.runQuery('SELECT id FROM '+iw_extratype+' WHERE extra ="'+extratype+'" AND id!="'+eid+'"', function(serrQry, sresult){
                if(sresult=="")
                {
                   sql.runQuery('UPDATE '+iw_extratype+' SET extra="'+extratype+'",price="' + price + '",nickname="' + nickname + '" WHERE id="' + eid + '"', function(sierrQry, sresult){
                    if(sierrQry)
                        {
                            console.log(sierrQry);
                            res = {status: 'error',message: sierrQry};
                            callback(null, res);
                        }
                        else
                        {
                            res = {status: 'success',message: 'successfully'};
                            callback(null, res);
                        }
                    });
                }
                else
                {
                    console.log(serrQry);
                    res = {status: 'already',message: 'successfully'};
                    callback(null, res);  
                } 
            });
        }
        
            
        
    },
    //get extratype
    getExtratype: function(req, res, callback) {
        var reqData = res.req.body;
        extraid = (utls.checkNotEmpty(reqData.id)) ? app.locals.decrypt(reqData.id) : '';
        console.log("extratype get");
        console.log("extraid"+extraid);
        const getExtra = async function(req, res)
        {
            let sqlextra = 'SELECT e.extra,e.price,e.status,e.id,e.nickname FROM '+iw_extratype+' e WHERE e.id="'+extraid+'"';
            let extradata = await myQuery(sqlextra);

            res = {
            status: 'success',
            details: extradata.result,
            };
            console.log(res);
            callback(null, res); 
            
        }
        getExtra();
    },
    // Zeit creation
    createZeit: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("Zeit Create");
       // console.log(reqData);
        zeit = (utls.checkNotEmpty(reqData.zeit)) ? reqData.zeit : '';
        timedata = (utls.checkNotEmpty(reqData.timedata)) ? reqData.timedata : '';
        editid = (utls.checkNotEmpty(reqData.editid)) ? reqData.editid : '';
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        if(editid=="")
        {
            sql.runQuery('SELECT id FROM '+iw_zeit+' WHERE zeit ="'+zeit+'"', function(serrQry, sresult){
                    if(sresult=="")
                    {
                        console.log("Zeit Create1");
                       sql.insertTable2(iw_zeit, 'zeit,timedata,created_date,status', '"' + zeit + '","' + timedata + '","' + createdDate + '","1"', function(sierrQry, sresult) {
                            if(sierrQry)
                            {
                                console.log(sierrQry);
                                res = {status: 'error',message: sierrQry};
                                callback(null, res);
                            }
                            else
                            {
                                console.log("Zeit Create2");
                                res = {status: 'success',message: 'successfully'};
                                callback(null, res);
                            }
                        });
                    }
                    else
                    {
                        console.log(serrQry);
                        res = {status: 'already',message: 'successfully'};
                        callback(null, res);  
                    }
                
            });

        }
        else
        {

            sql.runQuery('SELECT id FROM '+iw_zeit+' WHERE zeit ="'+zeit+'" AND id!="'+editid+'"', function(serrQry, sresult){
                    if(sresult=="")
                    {
                        console.log("Zeit Create1");
                        sql.runQuery('UPDATE '+iw_zeit+' SET zeit="'+zeit+'",timedata="' + timedata + '" WHERE id="' + editid + '"', function(sierrQry, sresult) {
                            if(sierrQry)
                            {
                                console.log(sierrQry);
                                res = {status: 'error',message: sierrQry};
                                callback(null, res);
                            }
                            else
                            {
                                console.log("Zeit Create2");
                                res = {status: 'success',message: 'successfully'};
                                callback(null, res);
                            }
                        });
                    }
                    else
                    {
                        console.log(serrQry);
                        res = {status: 'already',message: 'successfully'};
                        callback(null, res);  
                    }
                
            });

        }
       
    },
    //get zeit
    getZeit: function(req, res, callback) {
        var reqData = res.req.body;
        id = (utls.checkNotEmpty(reqData.id)) ? app.locals.decrypt(reqData.id) : '';
        console.log("getdataflow");
        console.log("id"+id);
        const getZeitdata = async function(req, res)
        {
            let sqlgetdata = 'SELECT e.timedata,e.status,e.id FROM '+iw_zeit+' e WHERE e.id="'+id+'"';
            let finaldata = await myQuery(sqlgetdata);

            res = {
            status: 'success',
            details: finaldata.result,
            };
            console.log(res);
            callback(null, res); 
            
        }
        getZeitdata();
    },
     // permit person creation
    createPermitpage: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("permit Create");
        //console.log(reqData);
        pname = (utls.checkNotEmpty(reqData.pname)) ? reqData.pname : '';
        idcard = (utls.checkNotEmpty(reqData.idcard)) ? reqData.idcard : '';
        editid = (utls.checkNotEmpty(reqData.editid)) ? reqData.editid : '';
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        if(editid=="")
        {
            sql.runQuery('SELECT id FROM '+iw_permitperson+' WHERE name ="'+pname+'"', function(serrQry, sresult){
                    if(sresult=="")
                    {
                       sql.insertTable2(iw_permitperson, 'name,idcard,created_date,status', '"' + pname + '","'+idcard+'","' + createdDate + '","1"', function(sierrQry, sresult) {
                            if(sierrQry)
                            {
                                console.log(sierrQry);
                                res = {status: 'error',message: sierrQry};
                                callback(null, res);
                            }
                            else
                            {
                                res = {status: 'success',message: 'successfully'};
                                callback(null, res);
                            }
                        });
                    }
                    else
                    {
                        console.log(serrQry);
                        res = {status: 'already',message: 'successfully'};
                        callback(null, res);  
                    }
                
            });
        }
        else
        {
            sql.runQuery('SELECT id FROM '+iw_permitperson+' WHERE name ="'+pname+'" AND id!="'+editid+'"', function(serrQry, sresult){
                    if(sresult=="")
                    {
                        sql.runQuery('UPDATE '+iw_permitperson+' SET name="'+pname+'",idcard="' + idcard + '" WHERE id="' + editid + '"', function(sierrQry, sresult) {
                            if(sierrQry)
                            {
                                console.log(sierrQry);
                                res = {status: 'error',message: sierrQry};
                                callback(null, res);
                            }
                            else
                            {
                                res = {status: 'success',message: 'successfully'};
                                callback(null, res);
                            }
                        });
                    }
                    else
                    {
                        console.log(serrQry);
                        res = {status: 'already',message: 'successfully'};
                        callback(null, res);  
                    }
                
            });
        }
       
    },
    //get permit
    getPermit: function(req, res, callback) {
        var reqData = res.req.body;
        id = (utls.checkNotEmpty(reqData.id)) ? app.locals.decrypt(reqData.id) : '';
        console.log("getdataflow");
        console.log("id"+id);
        const getPermitdata = async function(req, res)
        {
            let sqlgetdata = 'SELECT e.name,e.status,e.id,e.idcard FROM '+iw_permitperson+' e WHERE e.id="'+id+'"';
            let finaldata = await myQuery(sqlgetdata);

            res = {
            status: 'success',
            details: finaldata.result,
            };
            console.log(res);
            callback(null, res); 
            
        }
        getPermitdata();
    },
    // Hair creation
    createhairpage: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("hair Create");
        //console.log(reqData);
        hair = (utls.checkNotEmpty(reqData.hair)) ? reqData.hair : '';
        editid = (utls.checkNotEmpty(reqData.editid)) ? reqData.editid : '';
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        if(editid=="")
        {
            sql.runQuery('SELECT id FROM '+iw_hair+' WHERE hair ="'+hair+'"', function(serrQry, sresult){
                    if(sresult=="")
                    {
                       sql.insertTable2(iw_hair, 'hair,created_date,status', '"' + hair + '","' + createdDate + '","1"', function(sierrQry, sresult) {
                            if(sierrQry)
                            {
                                console.log(sierrQry);
                                res = {status: 'error',message: sierrQry};
                                callback(null, res);
                            }
                            else
                            {
                                res = {status: 'success',message: 'successfully'};
                                callback(null, res);
                            }
                        });
                    }
                    else
                    {
                        console.log(serrQry);
                        res = {status: 'already',message: 'successfully'};
                        callback(null, res);  
                    }
                
            });
        }
        else
        {
            sql.runQuery('SELECT id FROM '+iw_hair+' WHERE hair ="'+hair+'" AND id!="'+editid+'"', function(serrQry, sresult){
                    if(sresult=="")
                    {
                        sql.runQuery('UPDATE '+iw_hair+' SET hair="'+hair+'" WHERE id="' + editid + '"', function(sierrQry, sresult) {
                            if(sierrQry)
                            {
                                console.log(sierrQry);
                                res = {status: 'error',message: sierrQry};
                                callback(null, res);
                            }
                            else
                            {
                                res = {status: 'success',message: 'successfully'};
                                callback(null, res);
                            }
                        });
                    }
                    else
                    {
                        console.log(serrQry);
                        res = {status: 'already',message: 'successfully'};
                        callback(null, res);  
                    }
                
            });
        }
        
    },
    //get Hair
    getHair: function(req, res, callback) {
        var reqData = res.req.body;
        id = (utls.checkNotEmpty(reqData.id)) ? app.locals.decrypt(reqData.id) : '';
        console.log("getdataflow");
        console.log("id"+id);
        const getHairdata = async function(req, res)
        {
            let sqlgetdata = 'SELECT e.hair,e.status,e.id FROM '+iw_hair+' e WHERE e.id="'+id+'"';
            let finaldata = await myQuery(sqlgetdata);

            res = {
            status: 'success',
            details: finaldata.result,
            };
            console.log(res);
            callback(null, res); 
            
        }
        getHairdata();
    },
    // Eyes creation
    createeyespage: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("eyes Create");
        //console.log(reqData);
        eyestype = (utls.checkNotEmpty(reqData.eyestype)) ? reqData.eyestype : '';
        editid = (utls.checkNotEmpty(reqData.editid)) ? reqData.editid : '';
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");

        if(editid=="")
        {
            sql.runQuery('SELECT id FROM '+iw_eyes+' WHERE eyes ="'+eyestype+'"', function(serrQry, sresult){
            if(sresult=="")
            {
               sql.insertTable2(iw_eyes, 'eyes,created_date,status', '"' + eyestype + '","' + createdDate + '","1"', function(sierrQry, sresult) {
                    if(sierrQry)
                    {
                        console.log(sierrQry);
                        res = {status: 'error',message: sierrQry};
                        callback(null, res);
                    }
                    else
                    {
                        res = {status: 'success',message: 'successfully'};
                        callback(null, res);
                    }
                });
            }
            else
            {
                console.log(serrQry);
                res = {status: 'already',message: 'successfully'};
                callback(null, res);  
            }

            });
        }
        else
        {
            console.log("edit"+editid);
             sql.runQuery('SELECT id FROM '+iw_eyes+' WHERE eyes ="'+eyestype+'" AND id!="'+editid+'"', function(serrQry, sresult){
            if(sresult=="")
            {
               sql.runQuery('UPDATE '+iw_eyes+' SET eyes="'+eyestype+'" WHERE id="' + editid + '"', function(sierrQry, sresult) {
                    if(sierrQry)
                    {
                        console.log(sierrQry);
                        res = {status: 'error',message: sierrQry};
                        callback(null, res);
                    }
                    else
                    {
                        res = {status: 'success',message: 'successfully'};
                        callback(null, res);
                    }
                });
            }
            else
            {
                console.log(serrQry);
                res = {status: 'already',message: 'successfully'};
                callback(null, res);  
            }

            });

        }
        
    },
     //get Eyes
    getEyes: function(req, res, callback) {
        var reqData = res.req.body;
        id = (utls.checkNotEmpty(reqData.id)) ? app.locals.decrypt(reqData.id) : '';
        console.log("getdataflow");
        console.log("id"+id);
        const getEyesdata = async function(req, res)
        {
            let sqlgetdata = 'SELECT e.eyes,e.status,e.id FROM '+iw_eyes+' e WHERE e.id="'+id+'"';
            let finaldata = await myQuery(sqlgetdata);

            res = {
            status: 'success',
            details: finaldata.result,
            };
            console.log(res);
            callback(null, res); 
            
        }
        getEyesdata();
    },
    // Shoes creation
    createshoespage: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("shoes Create");
        //console.log(reqData);
        shoestype = (utls.checkNotEmpty(reqData.shoestype)) ? reqData.shoestype : '';
        editid = (utls.checkNotEmpty(reqData.editid)) ? reqData.editid : '';
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        if(editid=="")
        {
            sql.runQuery('SELECT id FROM '+iw_shoes+' WHERE shoes ="'+shoestype+'"', function(serrQry, sresult){
                if(sresult=="")
                {
                   sql.insertTable2(iw_shoes, 'shoes,created_date,status', '"' + shoestype + '","' + createdDate + '","1"', function(sierrQry, sresult) {
                        if(sierrQry)
                        {
                            console.log(sierrQry);
                            res = {status: 'error',message: sierrQry};
                            callback(null, res);
                        }
                        else
                        {
                            res = {status: 'success',message: 'successfully'};
                            callback(null, res);
                        }
                    });
                }
                else
                {
                    console.log(serrQry);
                    res = {status: 'already',message: 'successfully'};
                    callback(null, res);  
                }

            });
        }
        else
        {
            sql.runQuery('SELECT id FROM '+iw_shoes+' WHERE shoes ="'+shoestype+'" AND id!="'+editid+'"', function(serrQry, sresult){
                if(sresult=="")
                {
                  sql.runQuery('UPDATE '+iw_shoes+' SET shoes="'+shoestype+'" WHERE id="' + editid + '"', function(sierrQry, sresult) {
                        if(sierrQry)
                        {
                            console.log(sierrQry);
                            res = {status: 'error',message: sierrQry};
                            callback(null, res);
                        }
                        else
                        {
                            res = {status: 'success',message: 'successfully'};
                            callback(null, res);
                        }
                    });
                }
                else
                {
                    console.log(serrQry);
                    res = {status: 'already',message: 'successfully'};
                    callback(null, res);  
                }

            });
        }
        
    },
    //get Shoes
    getShoes: function(req, res, callback) {
        var reqData = res.req.body;
        id = (utls.checkNotEmpty(reqData.id)) ? app.locals.decrypt(reqData.id) : '';
        console.log("getdataflowshow");
        console.log("id"+id);
        const getShoesdata = async function(req, res)
        {
            let sqlgetdata = 'SELECT e.shoes,e.status,e.id FROM '+iw_shoes+' e WHERE e.id="'+id+'"';
            let finaldata = await myQuery(sqlgetdata);

            res = {
            status: 'success',
            details: finaldata.result,
            };
            console.log(res);
            callback(null, res); 
            
        }
        getShoesdata();
    },
    // Shoes creation
    createclothpage: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("cloth Create");
        //console.log(reqData);
        clothtype = (utls.checkNotEmpty(reqData.clothtype)) ? reqData.clothtype : '';
        editid = (utls.checkNotEmpty(reqData.editid)) ? reqData.editid : '';
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        if(editid=="")
        {
            sql.runQuery('SELECT id FROM '+iw_cloth+' WHERE cloth ="'+clothtype+'"', function(serrQry, sresult){
                    if(sresult=="")
                    {
                       sql.insertTable2(iw_cloth, 'cloth,created_date,status', '"' + clothtype + '","' + createdDate + '","1"', function(sierrQry, sresult) {
                            if(sierrQry)
                            {
                                console.log(sierrQry);
                                res = {status: 'error',message: sierrQry};
                                callback(null, res);
                            }
                            else
                            {
                                res = {status: 'success',message: 'successfully'};
                                callback(null, res);
                            }
                        });
                    }
                    else
                    {
                        console.log(serrQry);
                        res = {status: 'already',message: 'successfully'};
                        callback(null, res);  
                    }
                
            });
        }
        else
        {
            sql.runQuery('SELECT id FROM '+iw_cloth+' WHERE cloth ="'+clothtype+'" AND id!="'+editid+'"', function(serrQry, sresult){
                    if(sresult=="")
                    {
                       sql.runQuery('UPDATE '+iw_cloth+' SET cloth="'+clothtype+'"  WHERE id="' + editid + '"', function(sierrQry, sresult) {
                            if(sierrQry)
                            {
                                console.log(sierrQry);
                                res = {status: 'error',message: sierrQry};
                                callback(null, res);
                            }
                            else
                            {
                                res = {status: 'success',message: 'successfully'};
                                callback(null, res);
                            }
                        });
                    }
                    else
                    {
                        console.log(serrQry);
                        res = {status: 'already',message: 'successfully'};
                        callback(null, res);  
                    }
                
            });
        }
        
    },
    //get cloth
        getCloth: function(req, res, callback) {
            var reqData = res.req.body;
            id = (utls.checkNotEmpty(reqData.id)) ? app.locals.decrypt(reqData.id) : '';
            console.log("getdataflowshow");
            console.log("id"+id);
            const getClothdata = async function(req, res)
            {
                let sqlgetdata = 'SELECT e.cloth,e.status,e.id FROM '+iw_cloth+' e WHERE e.id="'+id+'"';
                let finaldata = await myQuery(sqlgetdata);

                res = {
                status: 'success',
                details: finaldata.result,
                };
                console.log(res);
                callback(null, res); 
                
            }
            getClothdata();
        },
    // Civil Status creation
    createcivilstatuspage: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("civilstatus Create");
        //console.log(reqData);
        civilstatustype = (utls.checkNotEmpty(reqData.civilstatustype)) ? reqData.civilstatustype : '';
        editid = (utls.checkNotEmpty(reqData.editid)) ? reqData.editid : '';
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        if(editid=="")
        {
            sql.runQuery('SELECT id FROM '+iw_civilstatus+' WHERE civilstatus ="'+civilstatustype+'"', function(serrQry, sresult){
                if(sresult=="")
                {
                   sql.insertTable2(iw_civilstatus, 'civilstatus,created_date,status', '"' + civilstatustype + '","' + createdDate + '","1"', function(sierrQry, sresult) {
                        if(sierrQry)
                        {
                            console.log(sierrQry);
                            res = {status: 'error',message: sierrQry};
                            callback(null, res);
                        }
                        else
                        {
                            res = {status: 'success',message: 'successfully'};
                            callback(null, res);
                        }
                    });
                }
                else
                {
                    console.log(serrQry);
                    res = {status: 'already',message: 'successfully'};
                    callback(null, res);  
                }

            });
        }
        else
        {
           sql.runQuery('SELECT id FROM '+iw_civilstatus+' WHERE civilstatus ="'+civilstatustype+'" AND id!="'+editid+'"', function(serrQry, sresult){
                if(sresult=="")
                {
                   sql.runQuery('UPDATE '+iw_civilstatus+' SET civilstatus="'+civilstatustype+'" WHERE id="' + editid + '"', function(sierrQry, sresult) {
                        if(sierrQry)
                        {
                            console.log(sierrQry);
                            res = {status: 'error',message: sierrQry};
                            callback(null, res);
                        }
                        else
                        {
                            res = {status: 'success',message: 'successfully'};
                            callback(null, res);
                        }
                    });
                }
                else
                {
                    console.log(serrQry);
                    res = {status: 'already',message: 'successfully'};
                    callback(null, res);  
                }

            });
        }
        
    },
    //get civilstatus
        getCivilstatus: function(req, res, callback) {
            var reqData = res.req.body;
            id = (utls.checkNotEmpty(reqData.id)) ? app.locals.decrypt(reqData.id) : '';
            console.log("getdataflowshow");
            console.log("id"+id);
            const getCivilstatusdata = async function(req, res)
            {
                let sqlgetdata = 'SELECT e.civilstatus,e.status,e.id FROM '+iw_civilstatus+' e WHERE e.id="'+id+'"';
                let finaldata = await myQuery(sqlgetdata);

                res = {
                status: 'success',
                details: finaldata.result,
                };
                console.log(res);
                callback(null, res); 
                
            }
            getCivilstatusdata();
        },
    // privatepart creation
    createprivatepartpage: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("privatepart Create");
        //console.log(reqData);
        privateparttype = (utls.checkNotEmpty(reqData.privateparttype)) ? reqData.privateparttype : '';
        editid = (utls.checkNotEmpty(reqData.editid)) ? reqData.editid : '';
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        if(editid == "")
        {
            sql.runQuery('SELECT id FROM '+iw_privatepart+' WHERE privatepart ="'+privateparttype+'"', function(serrQry, sresult){
                if(sresult=="")
                {
                   sql.insertTable2(iw_privatepart, 'privatepart,created_date,status', '"' + privateparttype + '","' + createdDate + '","1"', function(sierrQry, sresult) {
                        if(sierrQry)
                        {
                            console.log(sierrQry);
                            res = {status: 'error',message: sierrQry};
                            callback(null, res);
                        }
                        else
                        {
                            res = {status: 'success',message: 'successfully'};
                            callback(null, res);
                        }
                    });
                }
                else
                {
                    console.log(serrQry);
                    res = {status: 'already',message: 'successfully'};
                    callback(null, res);  
                }
            
            });
        }
        else
        {
             sql.runQuery('SELECT id FROM '+iw_privatepart+' WHERE privatepart ="'+privateparttype+'" AND id!="'+editid+'"', function(serrQry, sresult){
                if(sresult=="")
                {
                   sql.runQuery('UPDATE '+iw_privatepart+' SET privatepart="'+privateparttype+'" WHERE id="' + editid + '"', function(sierrQry, sresult) {
                        if(sierrQry)
                        {
                            console.log(sierrQry);
                            res = {status: 'error',message: sierrQry};
                            callback(null, res);
                        }
                        else
                        {
                            res = {status: 'success',message: 'successfully'};
                            callback(null, res);
                        }
                    });
                }
                else
                {
                    console.log(serrQry);
                    res = {status: 'already',message: 'successfully'};
                    callback(null, res);  
                }
            
            });
        }
        
    },
    //get privatepart
        getPrivatepart: function(req, res, callback) {
            var reqData = res.req.body;
            id = (utls.checkNotEmpty(reqData.id)) ? app.locals.decrypt(reqData.id) : '';
            console.log("getdataflowshow");
            console.log("id"+id);
            const getPrivatepartdata = async function(req, res)
            {
                let sqlgetdata = 'SELECT e.privatepart,e.status,e.id FROM '+iw_privatepart+' e WHERE e.id="'+id+'"';
                let finaldata = await myQuery(sqlgetdata);

                res = {
                status: 'success',
                details: finaldata.result,
                };
                console.log(res);
                callback(null, res); 
                
            }
            getPrivatepartdata();
        },
     // Bust creation
    createbustpage: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("privatepart Create");
        //console.log(reqData);
        busttype = (utls.checkNotEmpty(reqData.busttype)) ? reqData.busttype : '';
        editid = (utls.checkNotEmpty(reqData.editid)) ? reqData.editid : '';
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        if(editid=="")
        {
            sql.runQuery('SELECT id FROM '+iw_bust+' WHERE bust ="'+busttype+'"', function(serrQry, sresult){
                if(sresult=="")
                {
                   sql.insertTable2(iw_bust, 'bust,created_date,status', '"' + busttype + '","' + createdDate + '","1"', function(sierrQry, sresult) {
                        if(sierrQry)
                        {
                            console.log(sierrQry);
                            res = {status: 'error',message: sierrQry};
                            callback(null, res);
                        }
                        else
                        {
                            res = {status: 'success',message: 'successfully'};
                            callback(null, res);
                        }
                    });
                }
                else
                {
                    console.log(serrQry);
                    res = {status: 'already',message: 'successfully'};
                    callback(null, res);  
                }
            
            });
        }
        else
        {
            sql.runQuery('SELECT id FROM '+iw_bust+' WHERE bust ="'+busttype+'" AND id!="'+editid+'"', function(serrQry, sresult){
                if(sresult=="")
                {
                   
     sql.runQuery('UPDATE '+iw_bust+' SET bust="'+busttype+'" WHERE id="' + editid + '"', function(sierrQry, sresult) {
                        if(sierrQry)
                        {
                            console.log(sierrQry);
                            res = {status: 'error',message: sierrQry};
                            callback(null, res);
                        }
                        else
                        {
                            res = {status: 'success',message: 'successfully'};
                            callback(null, res);
                        }
                    });
                }
                else
                {
                    console.log(serrQry);
                    res = {status: 'already',message: 'successfully'};
                    callback(null, res);  
                }
            
            });
        }
        
    },
    //get Bust
        getBust: function(req, res, callback) {
            var reqData = res.req.body;
            id = (utls.checkNotEmpty(reqData.id)) ? app.locals.decrypt(reqData.id) : '';
            console.log("getdataflowshow");
            console.log("id"+id);
            const getbustdata = async function(req, res)
            {
                let sqlgetdata = 'SELECT e.bust,e.status,e.id FROM '+iw_bust+' e WHERE e.id="'+id+'"';
                let finaldata = await myQuery(sqlgetdata);

                res = {
                status: 'success',
                details: finaldata.result,
                };
                console.log(res);
                callback(null, res); 
                
            }
            getbustdata();
        },
    // Nationality creation
    createnationalitypage: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("privatepart Create");
        //console.log(reqData);
        nationalitytype = (utls.checkNotEmpty(reqData.nationalitytype)) ? reqData.nationalitytype : '';
        editid = (utls.checkNotEmpty(reqData.editid)) ? reqData.editid : '';
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        if(editid=="")
        {
            sql.runQuery('SELECT id FROM '+iw_nationality+' WHERE nationality ="'+nationalitytype+'"', function(serrQry, sresult){
                if(sresult=="")
                {
                   sql.insertTable2(iw_nationality, 'nationality,created_date,status', '"' + nationalitytype + '","' + createdDate + '","1"', function(sierrQry, sresult) {
                        if(sierrQry)
                        {
                            console.log(sierrQry);
                            res = {status: 'error',message: sierrQry};
                            callback(null, res);
                        }
                        else
                        {
                            res = {status: 'success',message: 'successfully'};
                            callback(null, res);
                        }
                    });
                }
                else
                {
                    console.log(serrQry);
                    res = {status: 'already',message: 'successfully'};
                    callback(null, res);  
                }
            
            });
        }
        else
        {
            sql.runQuery('SELECT id FROM '+iw_nationality+' WHERE nationality ="'+nationalitytype+'" AND id!="'+editid+'"', function(serrQry, sresult){
                            if(sresult=="")
                            {
                                sql.runQuery('UPDATE '+iw_nationality+' SET nationality="'+nationalitytype+'" WHERE id="' + editid + '"', function(sierrQry, sresult) {
                                    if(sierrQry)
                                    {
                                        console.log(sierrQry);
                                        res = {status: 'error',message: sierrQry};
                                        callback(null, res);
                                    }
                                    else
                                    {
                                        res = {status: 'success',message: 'successfully'};
                                        callback(null, res);
                                    }
                                });
                            }
                            else
                            {
                                console.log(serrQry);
                                res = {status: 'already',message: 'successfully'};
                                callback(null, res);  
                            }
                        
             });
        }
        
    },
    //get Nationality
        getNationality: function(req, res, callback) {
            var reqData = res.req.body;
            id = (utls.checkNotEmpty(reqData.id)) ? app.locals.decrypt(reqData.id) : '';
            console.log("getdataflowshow");
            console.log("id"+id);
            const getnationalitydata = async function(req, res)
            {
                let sqlgetdata = 'SELECT e.nationality,e.status,e.id FROM '+iw_nationality+' e WHERE e.id="'+id+'"';
                let finaldata = await myQuery(sqlgetdata);

                res = {
                status: 'success',
                details: finaldata.result,
                };
                console.log(res);
                callback(null, res); 
                
            }
            getnationalitydata();
        },
};



