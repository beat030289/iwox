module.exports = {
    // Service Type creation
    createServicetype: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("servicetype Create");
        //console.log(reqData);
        servicetype = (utls.checkNotEmpty(reqData.servicetype)) ? reqData.servicetype : '';
        editid = (utls.checkNotEmpty(reqData.editid)) ? reqData.editid : '';
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        if(editid=="")
        {
            console.log("create");
                sql.runQuery('SELECT id FROM '+iw_servicetype+' WHERE service ="'+servicetype+'"', function(serrQry, sresult){
                    if(sresult=="")
                    {
                       sql.insertTable2(iw_servicetype, 'service,created_date,status', '"' + servicetype + '","' + createdDate + '","1"', function(sierrQry, sresult) {
                            if(sierrQry)
                            {
                                console.log(sierrQry);
                                res = {status: 'error',message: sierrQry};
                                callback(null, res);
                            }
                            else
                            {
                                res = {status: 'success',message: 'successfully'};
                                callback(null, res);
                            }
                        });
                    }
                    else
                    {
                        console.log(serrQry);
                        res = {status: 'already',message: 'successfully'};
                        callback(null, res);  
                    }
                
            });
        }
        else
        {
            console.log("edit");
              sql.runQuery('SELECT id FROM '+iw_servicetype+' WHERE service ="'+servicetype+'" AND id!="'+editid+'"', function(serrQry, sresult){
                    if(sresult=="")
                    {
                       sql.runQuery('UPDATE '+iw_servicetype+' SET service="'+servicetype+'"  WHERE id="' + editid + '"', function(sierrQry, sresult) {
                            if(sierrQry)
                            {
                                console.log(sierrQry);
                                res = {status: 'error',message: sierrQry};
                                callback(null, res);
                            }
                            else
                            {
                                res = {status: 'success',message: 'successfully'};
                                callback(null, res);
                            }
                        });
                    }
                    else
                    {
                        console.log(serrQry);
                        res = {status: 'already',message: 'successfully'};
                        callback(null, res);  
                    }
                
            });  
        }
        
    },
    // Service Type creation
    createService: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("service Create form");
        console.log(reqData);
        title = (utls.checkNotEmpty(reqData.title)) ? reqData.title : '';
        sbranch = (utls.checkNotEmpty(reqData.sbranch)) ? reqData.sbranch : '';
        szeit = (utls.checkNotEmpty(reqData.szeit)) ? reqData.szeit : '';
        sprice = (utls.checkNotEmpty(reqData.sprice)) ? reqData.sprice : '';
        sstatus = (utls.checkNotEmpty(reqData.sstatus)) ? reqData.sstatus : '';
        editid = (utls.checkNotEmpty(reqData.editid)) ? reqData.editid : '';
        /*servicetype = (utls.checkNotEmpty(reqData.servicetype)) ? reqData.servicetype : '';
        extratype = (utls.checkNotEmpty(reqData.extratype)) ? reqData.extratype : '';
        
        etime = (utls.checkNotEmpty(reqData.etime)) ? reqData.etime : '';
        eprice = (utls.checkNotEmpty(reqData.eprice)) ? reqData.eprice : '';
        mprice = (utls.checkNotEmpty(reqData.mprice)) ? reqData.mprice : '';*/
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        if(editid=="")
        {
            sql.runQuery('SELECT id FROM '+iw_service+' WHERE title ="'+title+'"', function(sserrQry, ssresult){
                if(ssresult=="")
                {
                   sql.insertTable2(iw_service, 'title,branch,zeit,price,created_date,status', '"' + title + '","' + sbranch + '","' + szeit + '","' + sprice + '","' + createdDate + '","'+sstatus+'"', function(sierrQry, sresult) {
                        if(sierrQry)
                        {
                            console.log(sierrQry);
                            res = {status: 'error',message: sierrQry};
                            callback(null, res);
                        }
                        else
                        {
                            sid = app.locals.encrypt(sresult);
                            res = {status: 'success',message: 'successfully',id: sid};
                            callback(null, res);
                        }
                    });
                }
                else
                {
                    console.log(sserrQry);
                    res = {status: 'success',message: 'successfully'};
                    console.log(res);
                    callback(null, res);  
                }
                
            });
        }
        else
        {
             sql.runQuery('SELECT id FROM '+iw_service+' WHERE title ="'+title+'" AND id!="'+editid+'"', function(sserrQry, ssresult){
                if(ssresult=="")
                {
                   sql.runQuery('UPDATE '+iw_service+' SET title="'+title+'",branch="' + sbranch + '",zeit="' + szeit + '",price="' + sprice + '",created_date="' + createdDate + '",status="' + sstatus + '" WHERE id="' + editid + '"', function(sierrQry, sresult) {
                        if(sierrQry)
                        {
                            console.log(sierrQry);
                            res = {status: 'error',message: sierrQry};
                            callback(null, res);
                        }
                        else
                        {
                             sid = app.locals.encrypt(editid);
                              
                            res = {status: 'success',message: 'successfully',id: sid};
                            console.log(res);
                            callback(null, res);
                        }
                    });
                }
                else
                {
                    console.log(sserrQry);
                    res = {status: 'already',message: 'successfully'};
                    callback(null, res);  
                }
                
            });
                
        }
    },
    getService: function(req, callback) {
        // get username list model
           const serviceData = async function(req, res) {
        let sqlbresult = 'SELECT id,title  FROM '+iw_branch;
        let sqlsresult = 'SELECT id,service  FROM '+iw_servicetype+' WHERE status="1"';
        let sqleresult = 'SELECT id,extra,price  FROM '+iw_extratype+' WHERE status="1"';
        let sqlzresult = 'SELECT id,zeit  FROM '+iw_zeit+' WHERE status="1"';
            let bresult = await myQuery(sqlbresult);
           let sresult = await myQuery(sqlsresult);
           let eresult = await myQuery(sqleresult);
           let zresult = await myQuery(sqlzresult);
        callback({
                status: 'success',
                branch: bresult.result,
                services: sresult.result,
                extra: eresult.result,
                zeit: zresult.result,
                });
                       
        }
        serviceData()
    },
    //edit service
     editservice: function(req, callback) {
        console.log(req.query.serviceid);
        //serviceid = req.query.serviceid;
        serviceid = app.locals.decrypt(req.query.serviceid);
        console.log(serviceid);
           const serviceEditData = async function(req, res) {
        let sqlssresult = 'SELECT id,title,branch,zeit,price,status,etime,eprice,mprice  FROM '+iw_service+' WHERE id="'+serviceid+'"';
        let sqlbresult ='SELECT id,title  FROM '+iw_branch;
        let sqlsresult = 'SELECT id,service  FROM '+iw_servicetype+' WHERE status="1"';
        let sqleresult = 'SELECT id,extra,price  FROM '+iw_extratype+' WHERE status="1"';
        let sqlzresult = 'SELECT id,zeit  FROM '+iw_zeit+' WHERE status="1"';
        let sqlxresult = 'SELECT id,extra  FROM '+iw_serviceextra+' WHERE service_id="'+serviceid+'"';
        let sqlscresult = 'SELECT id,service  FROM '+iw_serviceservicetype+' WHERE service_id="'+serviceid+'"';
        
       let ssresult = await myQuery(sqlssresult);
       let bresult = await myQuery(sqlbresult);
       let sresult = await myQuery(sqlsresult);
       let eresult = await myQuery(sqleresult);
       let zresult = await myQuery(sqlzresult);
       let xresult = await myQuery(sqlxresult);
       let scresult = await myQuery(sqlscresult);
        callback({
            status: 'success',
            details: ssresult.result,
            branch: bresult.result,
            services: sresult.result,
            extra: eresult.result,
            zeit: zresult.result,
            extradata: xresult.result,
            servicedata: scresult.result,
            });
                       
        }
        serviceEditData(); 
    },
    getServiceData:function(req, callback)
    {
        
        sql.runQuery('SELECT s.id,s.title,(SELECT b.title FROM '+iw_branch+' b WHERE s.branch=b.id) AS branch,(SELECT z.zeit FROM '+iw_zeit+' z WHERE s.zeit=z.id) AS zeit,(SELECT count(ss.id) FROM '+iw_serviceservicetype+' ss WHERE s.id=ss.service_id)AS servicecount,(SELECT count(es.id) FROM '+iw_serviceextra+' es WHERE s.id=es.service_id)AS extracount,s.price,s.status  FROM '+iw_service+' s ORDER BY s.id DESC LIMIT 12', function(sserr, ssresult) {
            if (sserr) ssresult = [];
                else{
                  sql.runQuery('SELECT id,title FROM '+iw_branch+'', function(bberr, bbresult) {
                    if (bberr) bbresult = [];
                        else{
                            sql.runQuery('SELECT id,title FROM '+iw_service+'', function(snerr, snresult) {
                            if (snerr) snresult = [];
                                else{
                                    sql.runQuery('SELECT id,zeit FROM '+iw_zeit+'', function(zerr, zresult) {
                                    if (zerr) zresult = [];
                                        else{
                                            callback({
                                            status: 'success',
                                            details: ssresult,
                                            branch: bbresult,
                                            sname: snresult,
                                            stime: zresult,
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                  }
        });
    },
    getServiceSpearate:function(req, callback)
    {
        console.log(req.query.serviceid);
        //serviceid = req.query.serviceid;
        serviceid = app.locals.decrypt(req.query.serviceid);
        var paginator = new pagination.SearchPaginator({prelink:'/', current: 3, rowsPerPage: 20, totalResult: 50});
    console.log(paginator.getPaginationData());
       console.log(serviceid);
        sql.runQuery('SELECT s.id,s.title,(SELECT b.title FROM '+iw_branch+' b WHERE s.branch=b.id) AS branch,(SELECT z.zeit FROM '+iw_zeit+' z WHERE s.zeit=z.id) AS zeit,(SELECT count(ss.id) FROM '+iw_serviceservicetype+' ss WHERE s.id=ss.service_id)AS servicecount,(SELECT count(es.id) FROM '+iw_serviceextra+' es WHERE s.id=es.service_id)AS extracount,s.price,s.status  FROM '+iw_service+' s WHERE id="'+serviceid+'"', function(sserr, ssresult) {
                if (sserr) 
                {
                    console.log(sserr);
                    ssresult = [];
                }
                else{
                     sql.runQuery('SELECT e.id,e.extra,(SELECT ee.extra FROM '+iw_extratype+' ee WHERE ee.id = e.extra )AS extraname,(SELECT eec.price FROM '+iw_extratype+' eec WHERE eec.id = e.extra )AS extraprice  FROM '+iw_serviceextra+' e WHERE e.service_id="'+serviceid+'"', function(xerr, xresult) {
                        if (xerr) 
                        {
                            console.log(xerr);
                            xresult=[];
                        }
                        else {
                        sql.runQuery('SELECT s.id,s.service,(SELECT ss.service FROM '+iw_servicetype+' ss WHERE ss.id = s.service )AS servicename  FROM '+iw_serviceservicetype+' s WHERE service_id="'+serviceid+'"', function(serr, scresult) {
                            if (serr)
                            {
                                console.log(serr);
                                scresult=[];
                            }
                            else {
                                console.log(ssresult);
                                console.log(xresult);
                                console.log(scresult);
                                callback({
                                status: 'success',
                                details: ssresult,
                                extradata: xresult,
                                servicedata: scresult,
                                });
                            }
                        });
                    }
                });
            }
        });
    },
     serviceListpage: function(req, res, callback) {
        var reqData = res.req.body;
        console.log(reqData);
        sWhere='';
        limit = (utls.checkNotEmpty(reqData.limit)) ? reqData.limit : '';
        vcservice = (utls.checkNotEmpty(reqData.vcservice)) ? reqData.vcservice : '';
        vctime = (utls.checkNotEmpty(reqData.vctime)) ? reqData.vctime : '';
        vcbranch = (utls.checkNotEmpty(reqData.vcbranch)) ? reqData.vcbranch : '';
        vcprice = (utls.checkNotEmpty(reqData.vcprice)) ? reqData.vcprice : '';
        vcstatus = (utls.checkNotEmpty(reqData.vcstatus)) ? reqData.vcstatus : '';
       if(vcprice!="all")
       {
        var price = vcprice.split("-");
        sWhere+=typeof(vcprice)!='undefined' && vcprice!='' && vcprice!='all'?' AND s.price >= "' + price[0]+'" AND s.price <= "' + price[1]+'" ':'';
       }
        sWhere+=typeof(vcservice)!='undefined' && vcservice!='' && vcservice!='all'?' AND s.title LIKE "' + vcservice+ '%" ':'';
        sWhere+=typeof(vctime)!='undefined' && vctime!='' && vctime!='all'?' AND s.zeit="'+vctime+'" ':'';
        sWhere+=typeof(vcbranch)!='undefined' && vcbranch!='' && vcbranch!='all'?' AND s.branch="'+vcbranch+'" ':'';
        sWhere+=typeof(vcstatus)!='undefined' && vcstatus!='' && vcstatus!='all'?' AND s.status="'+vcstatus+'" ':'';
        
      sql.runQuery('SELECT s.id,s.title,(SELECT b.title FROM '+iw_branch+' b WHERE s.branch=b.id) AS branch,(SELECT z.zeit FROM '+iw_zeit+' z WHERE s.zeit=z.id) AS zeit,(SELECT count(ss.id) FROM '+iw_serviceservicetype+' ss WHERE s.id=ss.service_id)AS servicecount,(SELECT count(es.id) FROM '+iw_serviceextra+' es WHERE s.id=es.service_id)AS extracount,s.price,s.status  FROM '+iw_service+' s  WHERE 1=1 '+sWhere+' ORDER BY s.id DESC LIMIT '+limit+'', function(sserr, ssresult) {
            if (sserr) ssresult = [];
                else{
                    var data = {servicelist: ssresult};
                        app.render('pages/service/servicelist', data, function(err, html) {
                        //console.log(err);
                        //console.log(html);
                        var res = {status: 'error', html: html };
                            callback(null, res);
                        });
                }
        });
    },
    //get extratype
    getServicetype: function(req, res, callback) {
        var reqData = res.req.body;
        servicetypeid = (utls.checkNotEmpty(reqData.id)) ? app.locals.decrypt(reqData.id) : '';
        console.log("service type get");
        console.log("servicetypeid"+servicetypeid);
        const getServiceData = async function(req, res)
        {
            let sqlService = 'SELECT e.service,e.status,e.id FROM '+iw_servicetype+' e WHERE e.id="'+servicetypeid+'"';
            let servicedata = await myQuery(sqlService);

            res = {
            status: 'success',
            details: servicedata.result,
            };
            console.log(res);
            callback(null, res); 
            
        }
        getServiceData();
    },
};



