module.exports = {
    getdetails: function(req, callback) {
        console.log("weekplannerdata");
        const getEmployee = async function(req, res) {
            let sqlbranch ='SELECT b.title,b.id,b.status FROM ' + iw_branch + ' b';             
            let sqlgresult = 'SELECT concat(g.first_name," ",g.last_name)as gname,g.id,g.status FROM iw_tns_user g WHERE g.role!="1"';           
            let resultc = await myQuery(sqlbranch);
            let gresult = await myQuery(sqlgresult);
            
           res = {
                status: 'success',
                branch: resultc.result,
                girls: gresult.result
            };
            callback(res);
            }
            getEmployee();
    },
    getworkplanner: function(req, res, callback) {
        var reqData = res.req.body;
        cdate = (utls.checkNotEmpty(reqData.cdate)) ? reqData.cdate : '';
        branchid = (utls.checkNotEmpty(reqData.branch)) ? reqData.branch : '';
        employeename = (utls.checkNotEmpty(reqData.employeename)) ? reqData.employeename : '';
        editid = (utls.checkNotEmpty(reqData.editid)) ? reqData.editid : '';
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        console.log("weekplannerdata");
        if (editid != "") {
            
            const getEditEmployee = async function(req, res) {
                let sqlresultc = 'SELECT b.title,b.id,b.status FROM ' + iw_branch + ' b';
                let sqlwresult ='SELECT id,cdate,branch_id,lead FROM ' + iw_workplannergirls + ' WHERE id="' + editid + '"';             
                let sqlwtresult ='SELECT id,starttime,endtime FROM ' + iw_workplanner_time + ' WHERE wg_id="' + editid + '" ORDER BY id ASC';             
                let sqlgresult = 'SELECT concat(g.first_name," ",g.last_name)as gname,g.id,g.status FROM iw_tns_user g WHERE g.role!="1"';
                let resultc = await myQuery(sqlresultc);
                let wresult = await myQuery(sqlwresult);
                let wtresult = await myQuery(sqlwtresult);
                let gresult = await myQuery(sqlgresult);
                
                   res = {
                        status: 'success',
                        branch: resultc.result,
                        workplanner: wresult.result,
                        worktime: wtresult.result,
                        girls: gresult.result
                    };
                    callback(null, res);
                }
                getEditEmployee();
        } else {
                const getEmployee = async function(req, res) {
                    let sqlbranch ='SELECT b.title,b.id,b.status FROM ' + iw_branch + ' b';             
                    let sqlgresult = 'SELECT concat(g.first_name," ",g.last_name)as gname,g.id,g.status FROM iw_tns_user g WHERE g.role!="1"';
                    let resultc = await myQuery(sqlbranch);
                    let gresult = await myQuery(sqlgresult);
                    
                   res = {
                        status: 'success',
                        branch: resultc.result,
                        workplanner: [],
                        worktime: [],
                        girls: gresult.result
                    };
                    callback(null, res);
            }
            getEmployee();
        }
    },
    // Workplanner creation
    workplannerSubmit: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("workplanner Create");
        console.log(reqData);
        cdate = (utls.checkNotEmpty(reqData.cdate)) ? reqData.cdate : '';
        branchid = (utls.checkNotEmpty(reqData.branchid)) ? reqData.branchid : '';
        employeename = (utls.checkNotEmpty(reqData.employeename)) ? reqData.employeename : '';
        editid = (utls.checkNotEmpty(reqData.editid)) ? reqData.editid : '';
        starttime = (utls.checkNotEmpty(reqData.starttime)) ? reqData.starttime : '';
        endtime = (utls.checkNotEmpty(reqData.endtime)) ? reqData.endtime : '';
        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        if (editid == "") {
            sql.insertTable2(iw_workplannergirls, 'branch_id,cdate,lead,created_date,status', '"' + branchid + '","' + cdate + '","' + employeename + '","' + createdDate + '","1"', function(sierrQry, sresult) {
                if (sierrQry) {
                    console.log(sierrQry);
                    res = {
                        status: 'error',
                        message: sierrQry
                    };
                    callback(null, res);
                } else {
                    id = sresult;
                    if (Array.isArray(reqData.starttime)) {
                        console.log("arraytest");
                        starttime.forEach(function(pmdata, key) {
                            stime = (utls.checkNotEmpty(starttime[key])) ? starttime[key] : '';
                            etime = (utls.checkNotEmpty(endtime[key])) ? endtime[key] : '';
                            dataSave = {
                                wg_id: id,
                                starttime: stime,
                                endtime: etime
                            };
                            console.log(dataSave);
                            if (stime != "" && etime != "") {
                                sql.insertValues(iw_workplanner_time, dataSave, function(err, insertedID) {});
                            }

                        });
                    } else {
                        console.log("single");
                        if (starttime != "" && endtime != "") {
                            dataSave = {
                                wg_id: id,
                                starttime: starttime,
                                endtime: endtime
                            };
                            console.log(dataSave);
                            sql.insertValues(iw_workplanner_time, dataSave, function(err, insertedID) {});
                        }

                    }
                    res = {
                        status: 'success',
                        message: 'successfully'
                    };
                    callback(null, res);
                }
            });

        } else {

            sql.runQuery('UPDATE ' + iw_workplannergirls + ' SET branch_id="' + branchid + '",cdate="' + cdate + '",lead="' + employeename + '" WHERE id="' + editid + '"', function(sierrQry, sresult) {
                if (sierrQry) {
                    console.log(sierrQry);
                    res = {
                        status: 'error',
                        message: sierrQry
                    };
                    callback(null, res);
                } else {
                    sql.runQuery('DELETE FROM ' + iw_workplanner_time + ' WHERE wg_id="' + editid + '"', function(err_d, result_d) {
                        if (result_d) {
                            id = editid;
                            if (Array.isArray(reqData.starttime)) {
                                starttime.forEach(function(pmdata, key) {
                                    stime = (utls.checkNotEmpty(starttime[key])) ? starttime[key] : '';
                                    etime = (utls.checkNotEmpty(endtime[key])) ? endtime[key] : '';
                                    dataSave = {
                                        wg_id: id,
                                        starttime: stime,
                                        endtime: etime
                                    };
                                    console.log(dataSave);
                                    if (stime != "" && etime != "") {
                                        sql.insertValues(iw_workplanner_time, dataSave, function(err, insertedID) {});
                                    }

                                });
                            } else {
                                if (starttime != "" && endtime != "") {
                                    dataSave = {
                                        wg_id: id,
                                        starttime: starttime,
                                        endtime: endtime
                                    };
                                    console.log(dataSave);
                                    sql.insertValues(iw_workplanner_time, dataSave, function(err, insertedID) {});
                                }

                            }
                        }
                    });
                    res = {
                        status: 'success',
                        message: 'successfully'
                    };
                    callback(null, res);
                }
            });
        }

    },
    // Workplanner Remove
    workplannerRemove: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("workplanner Remove");
        console.log(reqData);
        id = (utls.checkNotEmpty(reqData.id)) ? reqData.id : '';
        sql.runQuery('DELETE FROM ' + iw_workplanner + ' WHERE id="' + editid + '"', function(err_d, result_d) {
            if (result_d) {
                sql.runQuery('DELETE FROM ' + iw_workplanner_time + ' WHERE wg_id="' + editid + '"', function(err_w, result_w) {
                    if (result_w) {
                        res = {
                            status: 'success',
                            message: 'successfully'
                        };
                        callback(null, res);
                    }
                });
            }
        });

    },
    // Work get data
    workgetdata: function(req, res, callback) {
        var reqData = res.req.body;
        console.log("work get data");
        console.log(reqData);
        sdate = (utls.checkNotEmpty(reqData.sdate)) ? reqData.sdate : '';
        edate = (utls.checkNotEmpty(reqData.edate)) ? reqData.edate : '';
        branchtype = (utls.checkNotEmpty(reqData.branchtype)) ? reqData.branchtype : '';
        createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
        if (branchtype != "all") {
            bval = ' AND branch_id ="' + branchtype + '"';
            bvala = ' AND w.branch_id ="' + branchtype + '"';
        } else {
            bval = ' ';
            bvala = ' ';
        }

        var cmdata = "";
        if (sdate != edate) {
            var dDate = twoDifferentDate(sdate, edate);

            vkey = 0;
            tkey = dDate.length;
            dDate.forEach(function(dd, key) {
                vkey = parseInt(key) + parseInt(1);
                if (vkey == tkey) {
                    cmdata += "'" + dd + "'";
                } else {
                    cmdata += "'" + dd + "',";
                }

            });
        } else {
            cmdata += "'" + sdate + "'";
        }

        // console.log(dDate);
        //console.log(cmdata);
        //var workcount = {};
        var coldata = [];
        var arrdata = {};
        var workHours = {};
        sql.runQuery('SELECT count(lead) AS employeecount,cdate  FROM ' + iw_workplannergirls + ' WHERE cdate IN (' + cmdata + ') ' + bval + ' GROUP BY cdate', function(sierrQry, sresult) {
            if (sierrQry) {
                console.log(sierrQry);
                sresult = [];
            } else {
                sql.runQuery('SELECT mdata FROM (SELECT count(cdate)as mdata FROM ' + iw_workplannergirls + ' WHERE cdate IN (' + cmdata + ')' + bval + ' GROUP BY cdate) tmp order by mdata desc limit 1', function(cerrQry, cresult) {
                    if (cerrQry) {
                        console.log(cerrQry);
                        cresult = [];
                    } else {
                        sql.runQuery('SELECT count(cdate) AS pcount,DATE_FORMAT(cdate, "%d") AS day FROM ' + iw_workplannergirls + ' WHERE cdate IN (' + cmdata + ')' + bval + ' GROUP BY cdate', function(clerrQry, clresult) {
                            if (clerrQry) {
                                console.log(clerrQry);
                                clresult = [];
                            } else {
                                sql.runQuery('SELECT GROUP_CONCAT(w.id) AS sid,tw.id AS tid,DATE_FORMAT(w.cdate, "%d") AS day,GROUP_CONCAT(tw.starttime) AS stime,GROUP_CONCAT(tw.endtime) AS etime  FROM ' + iw_workplannergirls + ' w,' + iw_workplanner_time + ' tw WHERE w.cdate IN (' + cmdata + ')' + bvala + ' AND tw.wg_id=w.id GROUP BY w.id ORDER BY tw.id ASC', function(terrQry, tresult) {
                                    if (terrQry) {
                                        console.log(terrQry);
                                        tresult = [];
                                    } else {
                                        for (i = 0; i < tresult.length; i++) {
                                            var sstime = "";
                                            var eetime = "";
                                            var idwords = (tresult[i].sid).split(',');
                                            if (idwords.length > 1) {
                                                var cid = idwords[0];
                                                //console.log("cid"+cid);
                                                var st = (tresult[i].stime).split(',');
                                                var et = (tresult[i].etime).split(',');
                                                var stcount = st.length;
                                                var stcheck = 0;
                                                for (var p = 0; p < st.length; p++) {
                                                    //console.log(st[p]);
                                                    sd = tresult[i].stime;
                                                    ed = tresult[i].etime;
                                                    sstime += '<label for="">' + st[p] + ' - ' + et[p] + '</label>';
                                                    stcheck = stcheck + 1;
                                                    if (stcheck == stcount) {
                                                        workHours[cid] = sstime;
                                                    }
                                                }
                                            } else {
                                                var cid = tresult[i].sid;
                                                sstime += '<label for="">' + tresult[i].stime + ' - ' + tresult[i].etime + '</label>';
                                                workHours[cid] = sstime;
                                            }


                                        }
                                        var kf = {};
                                        clresult.forEach(function(cldata, key) {
                                            kf[cldata.day] = cldata.pcount;
                                        });
                                        coldata.push(kf);

                                        sql.runQuery('SELECT w.id,w.lead,w.branch_id,(SELECT b.title FROM ' + iw_branch + ' b WHERE b.id=w.branch_id)AS branchname,(SELECT bc.color FROM ' + iw_branch + ' bc WHERE bc.id=w.branch_id)AS branchcolor,(SELECT concat(g.first_name," ",g.last_name)as gname FROM ' + iw_user + ' g WHERE g.id=w.lead)AS girlsname,w.cdate,DATE_FORMAT(w.cdate, "%d") AS day  FROM ' + iw_workplannergirls + ' w WHERE w.cdate IN (' + cmdata + ')' + bvala + ' ', function(werrQry, wresult) {
                                            if (werrQry) {
                                                console.log(werrQry);
                                            } else {
                                                //console.log(wresult);
                                                var ttotalArray = [];
                                                wresult.forEach(function(sdata, key) {
                                                    var newdata = {};
                                                    var secdata = {};
                                                    idd = sdata.day;
                                                    var occurrences = ttotalArray.filter(function(val) {
                                                        return val === idd;
                                                    }).length;
                                                    /*console.log(occurrences); 
                                                    console.log(sdata.day+'---'+occurrences);*/
                                                    newdata['branchname'] = sdata.branchname;
                                                    newdata['girlsname'] = sdata.girlsname;
                                                    newdata['branch_id'] = sdata.branch_id;
                                                    newdata['lead'] = sdata.lead;
                                                    newdata['branchcolor'] = sdata.branchcolor;
                                                    newdata['id'] = sdata.id;
                                                    //secdata[occurrences]=newdata;
                                                    arrdata[sdata.day + occurrences] = newdata;
                                                    ttotalArray.push(sdata.day);
                                                });
                                                res = {
                                                    status: 'success',
                                                    workdata: wresult,
                                                    rdata: sresult,
                                                    ccount: cresult,
                                                    fdata: arrdata,
                                                    colcount: coldata,
                                                    timecount: workHours
                                                };
                                                //console.log(res)
                                                callback(null, res);

                                            }
                                        });

                                    }
                                });
                            }
                        });
                    }
                });
            }

        });

    },

};
//Date different functionality
function twoDifferentDate(startDate, endDate) {
    var listDate = [];
    //var startDate ='2017-02-01';
    //var endDate = '2017-02-10';
    var dateMove = new Date(startDate);
    var strDate = startDate;

    while (strDate < endDate) {
        var strDate = dateMove.toISOString().slice(0, 10);
        listDate.push(strDate);
        dateMove.setDate(dateMove.getDate() + 1);
    };
    return listDate;
    //console.log(listDate);
}