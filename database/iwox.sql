-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 11, 2022 at 08:17 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iwox`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `demo`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `demo`;
CREATE TABLE IF NOT EXISTS `demo` (
`id` int(11)
,`starttime` varchar(20)
,`service_id` varchar(20)
,`gid` bigint(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `iw_amountlist`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `iw_amountlist`;
CREATE TABLE IF NOT EXISTS `iw_amountlist` (
`wiramount` decimal(33,0)
,`reakamount` decimal(33,0)
,`kkamount` decimal(33,0)
,`euroamount` decimal(33,0)
,`girlsname` varchar(250)
,`girls_id` varchar(20)
,`workdate` varchar(20)
,`extraamount` decimal(32,0)
,`iwoxprice` decimal(34,0)
,`servicecount` bigint(21)
,`totalamount` decimal(32,0)
,`girlsrealamount` double
,`girlsamount` decimal(32,0)
,`branchamount` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `iw_extrabooking`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `iw_extrabooking`;
CREATE TABLE IF NOT EXISTS `iw_extrabooking` (
`extra_id` varchar(20)
,`booking_id` int(11)
,`extra` varchar(250)
);

-- --------------------------------------------------------

--
-- Table structure for table `iw_mas_category`
--

DROP TABLE IF EXISTS `iw_mas_category`;
CREATE TABLE IF NOT EXISTS `iw_mas_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_mas_category`
--

INSERT INTO `iw_mas_category` (`id`, `category`, `created_date`, `status`) VALUES
(1, 'category1', '2020-04-09 08:04:31', 1),
(2, 'category2', '2020-04-09 08:04:31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_mas_extratype`
--

DROP TABLE IF EXISTS `iw_mas_extratype`;
CREATE TABLE IF NOT EXISTS `iw_mas_extratype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `extra` varchar(250) NOT NULL,
  `nickname` varchar(250) NOT NULL,
  `price` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_mas_extratype`
--

INSERT INTO `iw_mas_extratype` (`id`, `extra`, `nickname`, `price`, `status`, `created_date`) VALUES
(1, 'extr1', '', '12', 1, '2020-04-09 08:04:31'),
(2, 'extra2', '', '10', 1, '2020-04-09 08:04:31'),
(3, 'exra31', '', '121', 1, '2020-06-03 14:06:48'),
(4, 'aefwetwe', 'etewetwejth6ut', '20', 1, '2020-06-12 11:06:00'),
(5, 'u6u6', '6u6', '200', 1, '2020-06-12 11:06:19');

-- --------------------------------------------------------

--
-- Table structure for table `iw_mas_servicetype`
--

DROP TABLE IF EXISTS `iw_mas_servicetype`;
CREATE TABLE IF NOT EXISTS `iw_mas_servicetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_mas_servicetype`
--

INSERT INTO `iw_mas_servicetype` (`id`, `service`, `status`, `created_date`) VALUES
(1, 'sdcd', 1, '2020-04-09 08:04:31'),
(2, 'testservice2', 1, '2020-04-09 08:04:31'),
(3, 'testservice12', 1, '2020-06-08 07:06:38'),
(4, '15', 1, '2020-07-29 05:07:27');

-- --------------------------------------------------------

--
-- Table structure for table `iw_mas_userrole`
--

DROP TABLE IF EXISTS `iw_mas_userrole`;
CREATE TABLE IF NOT EXISTS `iw_mas_userrole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_mas_userrole`
--

INSERT INTO `iw_mas_userrole` (`id`, `role`, `status`) VALUES
(1, 'SuperAdmin', 1),
(2, 'user', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_mas_webmodules`
--

DROP TABLE IF EXISTS `iw_mas_webmodules`;
CREATE TABLE IF NOT EXISTS `iw_mas_webmodules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_mas_webmodules`
--

INSERT INTO `iw_mas_webmodules` (`id`, `name`, `status`) VALUES
(1, 'Übersicht', 1),
(2, 'Bericht', 1),
(3, 'Filiale', 1),
(4, 'Wochenplan', 1),
(5, 'Dienstleistung', 1),
(6, 'Girls', 1),
(7, 'Gutscheine', 1),
(8, 'Treuepunkte', 1),
(9, 'Werbung', 1),
(10, 'Kunden', 1),
(11, 'Benachrichtigungen', 1),
(12, 'Schulden', 1),
(13, 'Benutzer', 1),
(14, 'Services', 1),
(15, 'Extras', 1),
(16, 'Zeit', 1),
(17, 'Arbeitsplan', 1),
(18, 'Rubble', 1),
(19, 'Service Zettel', 1),
(20, 'Bewertung', 1),
(21, 'Kategorie', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_mas_zeit`
--

DROP TABLE IF EXISTS `iw_mas_zeit`;
CREATE TABLE IF NOT EXISTS `iw_mas_zeit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zeit` varchar(250) NOT NULL,
  `timedata` varchar(20) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_mas_zeit`
--

INSERT INTO `iw_mas_zeit` (`id`, `zeit`, `timedata`, `created_date`, `status`) VALUES
(1, '6 Stunde ', '06:00', '2020-04-09 08:04:31', 1),
(2, '30 min ', '00:30', '2020-04-09 08:04:31', 1),
(3, '7 Stunde 6 min ', '07:06', '2020-06-08 07:06:41', 1),
(4, '15 min ', '00:15', '2020-07-29 05:07:27', 1),
(5, '45 min ', '00:45', '2020-07-29 05:07:27', 1),
(6, '1 Stunde ', '01:00', '2020-07-29 05:07:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_advertise`
--

DROP TABLE IF EXISTS `iw_tns_advertise`;
CREATE TABLE IF NOT EXISTS `iw_tns_advertise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `url` text NOT NULL,
  `fromdate` varchar(20) NOT NULL,
  `todate` varchar(20) NOT NULL,
  `image` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_advertise`
--

INSERT INTO `iw_tns_advertise` (`id`, `title`, `url`, `fromdate`, `todate`, `image`, `created_date`, `status`) VALUES
(9, 'ryf', 'fjyfjfj', '27.05.2020', '08.06.2020', 'bYO3H1591338388641.jpg', '2020-06-05 08:06:30', 1),
(10, 'service1', 'ypi', '04.06.2020', '02.06.2020', 'NvoIM1591348749578.jpg', '2020-06-05 08:06:21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_authorization`
--

DROP TABLE IF EXISTS `iw_tns_authorization`;
CREATE TABLE IF NOT EXISTS `iw_tns_authorization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `startdate` varchar(20) NOT NULL,
  `enddate` varchar(20) NOT NULL,
  `responsible` varchar(250) NOT NULL,
  `file` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_authorization`
--

INSERT INTO `iw_tns_authorization` (`id`, `branch_id`, `startdate`, `enddate`, `responsible`, `file`, `created_date`) VALUES
(6, 1, '27.04.2020', '03.05.2020', '2', '', '2020-05-05 09:05:26'),
(5, 1, '04.05.2020', '10.05.2020', '3', '', '2020-05-05 09:05:26'),
(8, 2, '02.06.2020', '06.06.2020', '2', '', '2020-06-02 14:06:47'),
(10, 4, '11.06.2020', '27.06.2020', '2', '', '2020-06-03 08:06:02'),
(11, 5, '06.06.2020', '11.06.2020', '2', '', '2020-06-03 08:06:59'),
(12, 6, '03.06.2020', '04.06.2020', '2', '', '2020-06-03 08:06:59'),
(19, 7, '02.06.2020', '20.06.2020', '2', '', '2020-06-03 16:06:54'),
(33, 10, '10.06.2020', '17.06.2020', '2', '', '2020-06-04 06:06:44'),
(38, 11, '11.06.2020', '16.06.2020', '2', '', '2020-06-04 09:06:29'),
(46, 27, '', '', '2', 'WqttI1592203308974.pdf', '2020-06-15 08:06:04'),
(47, 27, '02.06.2020', '02.06.2020', '4', 'h9Wz71592203308974.pdf', '2020-06-15 08:06:04'),
(48, 28, '', '', '2', 'uTtj11592203786993.pdf', '2020-06-15 08:06:39'),
(49, 28, '02.06.2020', '02.06.2020', '2', 'CGdsi1592203786993.pdf', '2020-06-15 08:06:39'),
(50, 29, '', '', '2', 'tYDUN1592204060069.pdf', '2020-06-15 08:06:39'),
(51, 29, '03.06.2020', '04.06.2020', '2', 'eV3sd1592204060069.pdf', '2020-06-15 08:06:39'),
(52, 30, '', '', '2', 'Gp2zn1592204265552.pdf', '2020-06-15 08:06:39'),
(53, 30, '19.06.2020', '16.06.2020', '2', 'MbydJ1592204265552.pdf', '2020-06-15 08:06:39'),
(65, 32, '04.06.2020', '04.06.2020', '2', 'Uf2wW1592217604010.pdf', '2020-06-15 12:06:37'),
(77, 35, '28.05.2020', '27.05.2020', '3', '', '2020-06-19 08:06:11'),
(78, 35, '29.05.2020', '27.05.2020', '4', 'lBPd41592549047908.pdf', '2020-06-19 08:06:11'),
(79, 36, '18.06.2020', '16.06.2020', '2', 'WPmv31592549172669.pdf', '2020-06-19 08:06:11');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_booking`
--

DROP TABLE IF EXISTS `iw_tns_booking`;
CREATE TABLE IF NOT EXISTS `iw_tns_booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` varchar(20) NOT NULL,
  `created_id` varchar(20) NOT NULL,
  `created_type` varchar(20) NOT NULL,
  `customer` varchar(20) NOT NULL,
  `customer_type` varchar(20) NOT NULL,
  `branch_id` varchar(20) NOT NULL,
  `room_id` varchar(20) NOT NULL,
  `girls_id` varchar(20) NOT NULL,
  `service_id` varchar(20) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `booking_date` varchar(20) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `serviceamount` int(11) NOT NULL,
  `extraamount` int(11) NOT NULL,
  `eservice_id` varchar(20) NOT NULL,
  `eservicetime` varchar(20) NOT NULL,
  `eserviceamount` int(11) NOT NULL,
  `egirlsamount` int(11) NOT NULL,
  `ebranchamount` int(11) NOT NULL,
  `paymentstatus` int(11) NOT NULL,
  `paymenttype` varchar(250) NOT NULL,
  `pointprice` int(11) NOT NULL,
  `rubbleprice` int(11) NOT NULL,
  `voucherprice` int(11) NOT NULL,
  `voucherid` varchar(250) NOT NULL,
  `vouchertype` varchar(20) NOT NULL,
  `totalamount` varchar(20) NOT NULL,
  `girlsamount` int(11) NOT NULL,
  `branchamount` int(11) NOT NULL,
  `servicetime` varchar(20) NOT NULL,
  `starttime` varchar(20) NOT NULL,
  `endtime` varchar(20) NOT NULL,
  `startdate` varchar(20) NOT NULL,
  `enddate` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `reason` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_booking`
--

INSERT INTO `iw_tns_booking` (`id`, `oid`, `created_id`, `created_type`, `customer`, `customer_type`, `branch_id`, `room_id`, `girls_id`, `service_id`, `created_date`, `booking_date`, `amount`, `serviceamount`, `extraamount`, `eservice_id`, `eservicetime`, `eserviceamount`, `egirlsamount`, `ebranchamount`, `paymentstatus`, `paymenttype`, `pointprice`, `rubbleprice`, `voucherprice`, `voucherid`, `vouchertype`, `totalamount`, `girlsamount`, `branchamount`, `servicetime`, `starttime`, `endtime`, `startdate`, `enddate`, `status`, `reason`) VALUES
(1, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '2', '2020-08-03 15:46', '2020-08-03', '52', 30, 22, '', '', 0, 0, 0, 1, 'Visa', 0, 0, 0, '', '', '52', 20, 10, '00:30', '15:46:49', '16:21:49', '2020-08-03 15:46:49', '2020-08-03 16:21:49', 1, ''),
(2, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '4', '2020-08-05 09:40', '2020-08-05', '90', 90, 0, '', '', 0, 0, 0, 1, 'Mastercard', 0, 0, 0, '', '', '90', 50, 40, '01:00', '09:40:29', '10:45:29', '2020-08-05 09:40:29', '2020-08-05 10:45:29', 1, ''),
(3, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '2', '2020-08-05 14:39', '2020-08-05', '72', 50, 22, '', '', 0, 0, 0, 1, '', 0, 0, 0, '', '', '72', 30, 20, '00:30', '14:39:42', '15:14:42', '2020-08-05 14:39:42', '2020-08-05 15:14:42', 1, ''),
(4, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '2', '2', '4', '2020-08-05 14:40', '2020-08-05', '100', 100, 0, '', '', 0, 0, 0, 0, 'Post', 0, 0, 0, '', '', '100', 50, 50, '01:00', '14:40:20', '15:45:20', '2020-08-05 14:40:20', '2020-08-05 15:45:20', 1, ''),
(5, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '3', '3', '2', '2020-08-05 14:40', '2020-08-05', '50', 50, 0, '', '', 0, 0, 0, 0, '', 0, 0, 0, '', '', '50', 30, 20, '00:30', '14:40:35', '15:15:35', '2020-08-05 14:40:35', '2020-08-05 15:15:35', 1, ''),
(6, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '2', '2020-08-06 06:49', '2020-08-06', '72', 50, 22, '', '', 0, 0, 0, 1, 'Visa', 0, 0, 0, '', '', '72', 30, 20, '00:30', '06:49:34', '07:24:34', '2020-08-06 06:49:34', '2020-08-06 07:24:34', 1, ''),
(7, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '2', '2', '2', '2020-08-06 06:50', '2020-08-06', '50', 50, 0, '', '', 0, 0, 0, 1, 'Twint', 0, 0, 0, '', '', '50', 30, 20, '00:30', '06:50:06', '07:25:06', '2020-08-06 06:50:06', '2020-08-06 07:25:06', 1, ''),
(8, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '2', '2020-08-06 08:04', '2020-08-06', '72', 50, 22, '', '', 0, 0, 0, 1, 'Visa', 0, 0, 0, '', '', '0', 30, 20, '00:30', '08:04:53', '08:39:53', '2020-08-06 08:04:53', '2020-08-06 08:39:53', 1, ''),
(9, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '2', '2020-08-06 12:07', '2020-08-06', '72', 50, 0, '', '', 0, 0, 0, 1, 'EuroCash', 0, 0, 10, '1', '1', '62', 30, 20, '00:30', '12:07:56', '12:42:56', '2020-08-06 12:07:56', '2020-08-06 12:42:56', 1, ''),
(10, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '2', '2', '1', '2020-08-06 15:33', '2020-08-06', '25', 25, 0, '', '', 0, 0, 0, 1, 'Twint', 0, 0, 0, '', '', '25', 20, 5, '00:15', '15:33:05', '15:53:05', '2020-08-06 15:33:05', '2020-08-06 15:53:05', 1, ''),
(11, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '2', '2020-08-07 06:44', '2020-08-07', '72', 50, 22, '', '', 0, 0, 0, 1, 'Twint', 0, 0, 0, '', '', '72', 30, 20, '00:30', '06:44:21', '07:19:21', '2020-08-07 06:44:21', '2020-08-07 07:19:21', 1, ''),
(12, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '2', '2020-08-10 05:59', '2020-08-10', '72', 50, 22, '', '', 0, 0, 0, 1, 'Post', 0, 0, 0, '', '', '72', 30, 20, '00:30', '05:59:58', '06:34:58', '2020-08-10 05:59:58', '2020-08-10 06:34:58', 1, ''),
(13, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '3', '1', '1', '2020-08-11 06:06', '2020-08-11', '47', 25, 22, '', '', 0, 0, 0, 1, 'Visa', 0, 0, 0, '', '', '47', 20, 5, '00:15', '06:06:25', '06:26:25', '2020-08-11 06:06:25', '2020-08-11 06:26:25', 2, 'Kunde nicht zufrieden'),
(14, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '4', '2020-08-11 07:07', '2020-08-11', '132', 100, 32, '', '', 0, 0, 0, 1, 'Mastercard', 0, 0, 0, '', '', '132', 50, 50, '01:00', '07:07:20', '08:12:20', '2020-08-11 07:07:20', '2020-08-11 08:12:20', 2, 'Gilr war nicht in Ordnung'),
(15, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '2', '2020-08-11 08:36', '2020-08-11', '45', 35, 10, '', '', 0, 0, 0, 1, 'Twint', 0, 0, 0, '', '', '45', 20, 15, '00:30', '08:36:43', '09:11:43', '2020-08-11 08:36:43', '2020-08-11 09:11:43', 2, 'Gilr war nicht in Ordnung'),
(16, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '2', '2020-08-11 13:56', '2020-08-11', '62', 40, 22, '', '', 0, 0, 0, 1, 'Visa', 0, 0, 0, '', '', '62', 20, 20, '00:30', '13:56:18', '14:31:18', '2020-08-11 13:56:18', '2020-08-11 14:31:18', 2, 'Anderes'),
(17, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '2', '2020-08-11 13:58', '2020-08-11', '92', 50, 42, '', '', 0, 0, 0, 1, 'Visa', 0, 0, 0, '', '', '92', 30, 20, '00:30', '13:58:29', '14:33:29', '2020-08-11 13:58:29', '2020-08-11 14:33:29', 1, ''),
(18, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '2', '2020-08-12 06:28', '2020-08-12', '72', 50, 22, '', '', 0, 0, 0, 1, 'Mastercard', 0, 0, 0, '', '', '72', 30, 20, '00:30', '06:28:48', '07:03:48', '2020-08-12 06:28:48', '2020-08-12 07:03:48', 1, ''),
(19, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '3', '2020-08-12 12:13', '2020-08-12', '87', 65, 22, '', '', 0, 0, 0, 1, 'Twint', 0, 0, 0, '', '', '87', 40, 25, '00:45', '12:13:36', '13:03:36', '2020-08-12 12:13:36', '2020-08-12 13:03:36', 1, ''),
(20, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '1', '2020-08-13 06:17', '2020-08-13', '172', 25, 22, '2', '00:30', 50, 30, 20, 1, 'Mastercard', 0, 0, 0, '', '', '172', 20, 5, '00:15', '06:17:08', '06:37:08', '2020-08-13 06:17:08', '2020-08-13 06:37:08', 1, ''),
(21, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '2', '2020-08-13 07:28', '2020-08-13', '75', 50, 22, '3', '00:45', 75, 40, 35, 1, 'Mastercard', 0, 0, 0, '', '', '0', 30, 20, '00:30', '07:28:21', '08:03:21', '2020-08-13 07:28:21', '2020-08-13 08:03:21', 1, ''),
(22, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '2', '2020-08-13 12:37', '2020-08-13', '60', 50, 10, '', '', 0, 0, 0, 1, 'Mastercard', 0, 0, 0, '', '', '60', 30, 20, '00:30', '12:37:01', '13:12:01', '2020-08-13 12:37:01', '2020-08-13 13:12:01', 0, ''),
(23, '2', '1245', 'user', 'Stammkunde', 'Vorgestellt', '1', '1', '1', '2', '2020-08-14 11:45', '2020-08-14', '52', 30, 22, '', '', 0, 0, 0, 1, 'Post', 0, 0, 0, '', '', '52', 20, 10, '00:30', '11:45:46', '12:20:46', '2020-08-14 11:45:46', '2020-08-14 12:20:46', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_booking_extra`
--

DROP TABLE IF EXISTS `iw_tns_booking_extra`;
CREATE TABLE IF NOT EXISTS `iw_tns_booking_extra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `extra_id` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `booking_id` (`booking_id`),
  KEY `booking_id_2` (`booking_id`),
  KEY `booking_id_3` (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_booking_extra`
--

INSERT INTO `iw_tns_booking_extra` (`id`, `booking_id`, `extra_id`, `status`) VALUES
(1, 1, '1', 0),
(2, 1, '2', 0),
(9, 3, '1', 0),
(10, 3, '2', 0),
(11, 6, '1', 0),
(12, 6, '2', 0),
(13, 8, '1', 0),
(14, 8, '2', 0),
(15, 9, '1', 0),
(16, 9, '2', 0),
(17, 11, '1', 0),
(18, 11, '2', 0),
(19, 12, '1', 0),
(20, 12, '2', 0),
(21, 13, '1', 0),
(22, 13, '2', 0),
(23, 14, '1', 0),
(25, 14, '4', 0),
(29, 15, '2', 0),
(30, 16, '1', 0),
(31, 16, '2', 0),
(33, 17, '1', 0),
(34, 17, '2', 0),
(35, 17, '4', 0),
(36, 18, '1', 0),
(37, 18, '2', 0),
(38, 19, '1', 0),
(39, 19, '2', 0),
(40, 20, '1', 0),
(41, 20, '2', 0),
(42, 21, '1', 0),
(43, 21, '2', 0),
(44, 22, '2', 0),
(45, 23, '1', 0),
(46, 23, '2', 0);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_booking_girls`
--

DROP TABLE IF EXISTS `iw_tns_booking_girls`;
CREATE TABLE IF NOT EXISTS `iw_tns_booking_girls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` varchar(20) NOT NULL,
  `girls_id` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_booking_service`
--

DROP TABLE IF EXISTS `iw_tns_booking_service`;
CREATE TABLE IF NOT EXISTS `iw_tns_booking_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` varchar(20) NOT NULL,
  `service_id` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_branch`
--

DROP TABLE IF EXISTS `iw_tns_branch`;
CREATE TABLE IF NOT EXISTS `iw_tns_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `color` varchar(250) NOT NULL,
  `category` varchar(20) NOT NULL,
  `address` varchar(250) NOT NULL,
  `place` varchar(250) NOT NULL,
  `postcode` varchar(20) NOT NULL,
  `canton` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `image` varchar(250) NOT NULL,
  `website` varchar(250) NOT NULL,
  `leader` varchar(20) NOT NULL,
  `onsignalid` text NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_branch`
--

INSERT INTO `iw_tns_branch` (`id`, `title`, `color`, `category`, `address`, `place`, `postcode`, `canton`, `email`, `phone`, `image`, `website`, `leader`, `onsignalid`, `created_date`, `status`) VALUES
(1, 'branch1', '#59cc45', '1', 'Rathausquai 9, 6004 Luzern, Switzerland', 'Luzern', '6004', 'Luzern', 'ibkrmm@gmail.com', '09159976776', 'OHyXC1586423193821.png', '', '2', 'f88359e9-adcb-4c64-bfd4-9f5136ccb329', '2020-05-05 09:05:26', 1),
(2, 'branch2', '#3a449c', '1', 'Rathausquai 9, 6004 Luzern, Switzerland', 'rameswaram', '623526', 'Luzern', 'ibkrmm@gmail.com', '09159976776', '', '', '2', '', '2020-06-02 14:06:47', 1),
(3, 'branch3', '#1c5ab8', '1', 'Albisstrasse 24, 8038 Zürich, Switzerland', 'Zürich', '8038', 'Zürich', '', '147852642', 'hAI7A1591162143579.jpg', '', '', '', '2020-06-03 07:06:34', 1),
(4, 'branch4', '#2cb820', '1', 'Rathausquai 9, 6004 Luzern, Switzerland', 'Luzern', '6004', 'Luzern', '', '147852369', 'zjF4q1591167164634.jpg', '', '', '', '2020-06-03 08:06:02', 1),
(5, 'branch5', '#38b324', '1', 'Rathausquai 9, 6004 Luzern, Switzerland', 'Luzern', '6004', 'Luzern', '', '741258963', 'tlh811591167458631.jpg', '', '', '', '2020-06-03 08:06:59', 1),
(6, 'branch6', '#a8532e', '1', 'Rathausquai 9, 6004 Luzern, Switzerland', 'Luzern', '6004', 'Luzern', '', '145236987', '', '', '', '', '2020-06-03 08:06:59', 1),
(7, 'branch7', '#55a639', '1', 'Rathausquai 9, 6004 Luzern, Switzerland', 'Luzern', '6004', 'Luzern', '', '1478523695', 'IvT1P1591168120676.jpg', '', '', '', '2020-06-03 16:06:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_bust`
--

DROP TABLE IF EXISTS `iw_tns_bust`;
CREATE TABLE IF NOT EXISTS `iw_tns_bust` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bust` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_bust`
--

INSERT INTO `iw_tns_bust` (`id`, `bust`, `created_date`, `status`) VALUES
(1, '75A', '2020-02-20 08:02:50', 1),
(2, '85A', '2020-02-20 08:02:50', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_chat`
--

DROP TABLE IF EXISTS `iw_tns_chat`;
CREATE TABLE IF NOT EXISTS `iw_tns_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `senderid` varchar(20) NOT NULL,
  `receiverid` varchar(20) NOT NULL,
  `message` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_chat`
--

INSERT INTO `iw_tns_chat` (`id`, `senderid`, `receiverid`, `message`, `created_date`, `status`) VALUES
(1, '1', '3', 'k/\'j', '2020-06-04 12:06:03', 0),
(2, '1', '3', '\'uk\'', '2020-06-04 12:06:03', 0),
(3, '1', '3', '/\'kuykukuyku', '2020-06-04 12:06:03', 0),
(4, '1', '5', 'yukuykuk', '2020-06-04 12:06:03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_civilstatus`
--

DROP TABLE IF EXISTS `iw_tns_civilstatus`;
CREATE TABLE IF NOT EXISTS `iw_tns_civilstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `civilstatus` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_civilstatus`
--

INSERT INTO `iw_tns_civilstatus` (`id`, `civilstatus`, `created_date`, `status`) VALUES
(1, 'ledig', '2020-02-20 07:02:27', 1),
(2, 'verheiratet', '2020-02-20 07:02:27', 1),
(3, 'verwitwet', '2020-02-20 07:02:27', 1),
(4, 'geschieden', '2020-02-20 07:02:27', 1),
(5, 'getrennt', '2020-02-20 07:02:27', 1),
(6, 'Lebenspartner', '2020-02-20 07:02:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_cloth`
--

DROP TABLE IF EXISTS `iw_tns_cloth`;
CREATE TABLE IF NOT EXISTS `iw_tns_cloth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cloth` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_cloth`
--

INSERT INTO `iw_tns_cloth` (`id`, `cloth`, `created_date`, `status`) VALUES
(1, '34', '2020-02-20 06:02:55', 1),
(2, '36', '2020-02-20 06:02:55', 1),
(3, '38', '2020-02-20 06:02:55', 1),
(4, '40', '2020-02-20 06:02:55', 1),
(5, '42', '2020-02-20 06:02:55', 1),
(6, '44', '2020-02-20 06:02:55', 1),
(7, '46', '2020-02-20 06:02:55', 1),
(8, '48', '2020-02-20 06:02:55', 1),
(9, '50', '2020-02-20 06:02:55', 1),
(10, '52', '2020-02-20 06:02:55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_customer`
--

DROP TABLE IF EXISTS `iw_tns_customer`;
CREATE TABLE IF NOT EXISTS `iw_tns_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch` varchar(250) NOT NULL,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL,
  `badge` varchar(250) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(250) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `password` varchar(250) NOT NULL,
  `mdpassword` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_customer`
--

INSERT INTO `iw_tns_customer` (`id`, `branch`, `first_name`, `last_name`, `badge`, `mobile`, `email`, `dob`, `password`, `mdpassword`, `created_date`, `status`) VALUES
(1, '', 'Balakrishnan', 'I', 'Standard', '09159976776', 'test@gamil.com', '', '123456', 'e10adc3949ba59abbe56e057f20f883e', '2020-05-06 05:05:48', 1),
(2, '', 'Balakrishnan', 'I', 'Premium', '09159976774', 'test@gmail.com', '07.05.2020', '123456', 'e10adc3949ba59abbe56e057f20f883e', '2020-05-06 05:05:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_download`
--

DROP TABLE IF EXISTS `iw_tns_download`;
CREATE TABLE IF NOT EXISTS `iw_tns_download` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` varchar(20) NOT NULL,
  `date_created` varchar(20) NOT NULL,
  `filenumber` int(11) NOT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_download`
--

INSERT INTO `iw_tns_download` (`sno`, `branch_id`, `date_created`, `filenumber`) VALUES
(1, '1', '2020-06-16', 3),
(3, '1', '2020-06-17', 2),
(4, '1', '2020-06-19', 3),
(5, '1', '2020-06-24', 1),
(6, '1', '2020-07-24', 1),
(7, '1', '2020-08-12', 1),
(8, '1', '2020-08-13', 1),
(9, '1', '2020-08-14', 3);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_downloadlist`
--

DROP TABLE IF EXISTS `iw_tns_downloadlist`;
CREATE TABLE IF NOT EXISTS `iw_tns_downloadlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` varchar(20) NOT NULL,
  `filenumber` varchar(20) NOT NULL,
  `branch_id` varchar(20) NOT NULL,
  `girls_id` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_downloadlist`
--

INSERT INTO `iw_tns_downloadlist` (`id`, `date_created`, `filenumber`, `branch_id`, `girls_id`) VALUES
(1, '2020-08-13', 'v1', '1', ''),
(2, '2020-08-14', 'v1', '1', ''),
(3, '2020-08-14', 'v2', '1', ''),
(4, '2020-08-14', 'v3', '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_expenditure`
--

DROP TABLE IF EXISTS `iw_tns_expenditure`;
CREATE TABLE IF NOT EXISTS `iw_tns_expenditure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `amount` int(11) NOT NULL,
  `comments` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `branch_id` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_expenditure`
--

INSERT INTO `iw_tns_expenditure` (`id`, `name`, `amount`, `comments`, `created_date`, `branch_id`) VALUES
(8, 'fsgf', 10, 'ssfgs', '2020-07-09', '1'),
(9, 'dgdag', 12, 'sfaf', '2020-07-09', '1'),
(10, 'sCs', 10, 'dsvgdvd', '2020-07-10', '1');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_extralog`
--

DROP TABLE IF EXISTS `iw_tns_extralog`;
CREATE TABLE IF NOT EXISTS `iw_tns_extralog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `extra_id` varchar(250) NOT NULL,
  `type` varchar(10) NOT NULL COMMENT '1->new,2-change',
  `up_down` varchar(20) NOT NULL COMMENT '1->up,2->down, 0->new',
  `branch_id` varchar(250) NOT NULL,
  `girls_id` varchar(250) NOT NULL,
  `created_date` varchar(250) NOT NULL,
  `reason` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `booking_id` (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_extralog`
--

INSERT INTO `iw_tns_extralog` (`id`, `booking_id`, `extra_id`, `type`, `up_down`, `branch_id`, `girls_id`, `created_date`, `reason`) VALUES
(1, 1, '1', '1', '0', '1', '1', '2020-08-03 15:46', ''),
(2, 1, '2', '1', '0', '1', '1', '2020-08-03 15:46', ''),
(3, 2, '1', '1', '0', '1', '1', '2020-08-05 09:40', ''),
(4, 2, '1', '2', '2', '1', '1', '2020-08-05 13:26', 'rhtetht'),
(5, 2, '1', '2', '1', '1', '1', '2020-08-05 13:34', ''),
(6, 2, '1', '2', '2', '1', '1', '2020-08-05 13:35', 'fdvbfd'),
(7, 2, '1', '2', '1', '1', '1', '2020-08-05 13:37', ''),
(8, 2, '4', '2', '1', '1', '1', '2020-08-05 13:37', ''),
(9, 2, '4', '2', '2', '1', '1', '2020-08-05 13:38', 'rtuytrurturt'),
(10, 2, '1', '2', '2', '1', '1', '2020-08-05 13:47', 'dfbfbdf'),
(11, 2, '1', '2', '1', '1', '1', '2020-08-05 13:53', ''),
(12, 2, '1', '2', '2', '1', '1', '2020-08-05 13:54', 'ertert ggfjgjfgj'),
(13, 2, '1', '2', '1', '1', '1', '2020-08-05 13:58', ''),
(14, 2, '1', '2', '2', '1', '1', '2020-08-05 13:58', 'sxvsf gfdhg'),
(15, 3, '1', '1', '0', '1', '1', '2020-08-05 14:39', ''),
(16, 3, '2', '1', '0', '1', '1', '2020-08-05 14:39', ''),
(17, 6, '1', '1', '0', '1', '1', '2020-08-06 06:49', ''),
(18, 6, '2', '1', '0', '1', '1', '2020-08-06 06:49', ''),
(19, 8, '1', '1', '0', '1', '1', '2020-08-06 08:04', ''),
(20, 8, '2', '1', '0', '1', '1', '2020-08-06 08:04', ''),
(21, 9, '1', '1', '0', '1', '1', '2020-08-06 12:07', ''),
(22, 9, '2', '1', '0', '1', '1', '2020-08-06 12:07', ''),
(23, 11, '1', '1', '0', '1', '1', '2020-08-07 06:44', ''),
(24, 11, '2', '1', '0', '1', '1', '2020-08-07 06:44', ''),
(25, 12, '1', '1', '0', '1', '1', '2020-08-10 05:59', ''),
(26, 12, '2', '1', '0', '1', '1', '2020-08-10 05:59', ''),
(27, 13, '1', '1', '0', '1', '1', '2020-08-11 06:06', ''),
(28, 13, '2', '1', '0', '1', '1', '2020-08-11 06:06', ''),
(29, 14, '1', '1', '0', '1', '1', '2020-08-11 07:07', ''),
(30, 14, '2', '1', '0', '1', '1', '2020-08-11 07:07', ''),
(31, 14, '4', '2', '1', '1', '1', '2020-08-11 08:34', ''),
(32, 14, '2', '2', '2', '1', '1', '2020-08-11 08:35', 'Kunde nicht zufrieden'),
(33, 15, '1', '1', '0', '1', '1', '2020-08-11 08:36', ''),
(34, 15, '2', '1', '0', '1', '1', '2020-08-11 08:36', ''),
(35, 15, '2', '2', '2', '1', '1', '2020-08-11 08:37', 'Gilr war nicht in Ordnung'),
(36, 15, '1', '2', '2', '1', '1', '2020-08-11 13:10', 'Kunde nicht zufrieden'),
(37, 15, '1', '2', '1', '1', '1', '2020-08-11 13:41', ''),
(38, 15, '1', '2', '2', '1', '1', '2020-08-11 13:42', 'Gilr war nicht in Ordnung'),
(39, 15, '2', '2', '1', '1', '1', '2020-08-11 13:45', ''),
(40, 16, '1', '1', '0', '1', '1', '2020-08-11 13:56', ''),
(41, 16, '2', '1', '0', '1', '1', '2020-08-11 13:56', ''),
(42, 16, '3', '1', '0', '1', '1', '2020-08-11 13:56', ''),
(43, 16, '3', '2', '2', '1', '1', '2020-08-11 13:56', 'Gilr war nicht in Ordnung'),
(44, 17, '1', '1', '0', '1', '1', '2020-08-11 13:58', ''),
(45, 17, '2', '1', '0', '1', '1', '2020-08-11 13:58', ''),
(46, 17, '4', '1', '0', '1', '1', '2020-08-11 13:58', ''),
(47, 18, '1', '1', '0', '1', '1', '2020-08-12 06:28', ''),
(48, 18, '2', '1', '0', '1', '1', '2020-08-12 06:28', ''),
(49, 19, '1', '1', '0', '1', '1', '2020-08-12 12:13', ''),
(50, 19, '2', '1', '0', '1', '1', '2020-08-12 12:13', ''),
(51, 19, '3', '1', '0', '1', '1', '2020-08-12 12:13', ''),
(52, 19, '3', '2', '2', '1', '1', '2020-08-12 12:14', 'Anderes'),
(53, 20, '1', '1', '0', '1', '1', '2020-08-13 06:17', ''),
(54, 20, '2', '1', '0', '1', '1', '2020-08-13 06:17', ''),
(55, 21, '1', '1', '0', '1', '1', '2020-08-13 07:28', ''),
(56, 21, '2', '1', '0', '1', '1', '2020-08-13 07:28', ''),
(57, 22, '2', '1', '0', '1', '1', '2020-08-13 12:37', ''),
(58, 23, '1', '1', '0', '1', '1', '2020-08-14 11:45', ''),
(59, 23, '2', '1', '0', '1', '1', '2020-08-14 11:45', '');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_eyes`
--

DROP TABLE IF EXISTS `iw_tns_eyes`;
CREATE TABLE IF NOT EXISTS `iw_tns_eyes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eyes` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_eyes`
--

INSERT INTO `iw_tns_eyes` (`id`, `eyes`, `created_date`, `status`) VALUES
(1, 'helles Eisblau', '2020-02-20 05:02:23', 1),
(2, 'Blaugrau', '2020-02-20 05:02:23', 1),
(3, 'Himmelblau', '2020-02-20 05:02:23', 1),
(4, 'Saphirblau', '2020-02-20 05:02:23', 1),
(5, 'Bernsteinfarben', '2020-02-20 05:02:23', 1),
(6, 'Karamelbraun', '2020-02-20 05:02:23', 1),
(7, 'Haselnussbraun', '2020-02-20 05:02:23', 1),
(8, 'tiefes Dunkelbraun', '2020-02-20 05:02:23', 1),
(9, 'Hellgrün', '2020-02-20 05:02:23', 1),
(10, 'Grüngrau', '2020-02-20 05:02:23', 1),
(11, 'Schilfgrün', '2020-02-20 05:02:23', 1),
(12, 'Smaragdgrün', '2020-02-20 05:02:23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_girls`
--

DROP TABLE IF EXISTS `iw_tns_girls`;
CREATE TABLE IF NOT EXISTS `iw_tns_girls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profilename` varchar(250) NOT NULL,
  `weight` varchar(20) NOT NULL,
  `bust` varchar(250) NOT NULL,
  `oberweitetype` varchar(250) NOT NULL,
  `privatepart` varchar(250) NOT NULL,
  `alcohol` varchar(20) NOT NULL,
  `age` varchar(20) NOT NULL,
  `hair` varchar(250) NOT NULL,
  `readymadecloth` varchar(250) NOT NULL,
  `tattoo` varchar(250) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `size` varchar(20) NOT NULL,
  `eyes` varchar(250) NOT NULL,
  `shoes` varchar(250) NOT NULL,
  `smoke` varchar(250) NOT NULL,
  `escort` varchar(250) NOT NULL,
  `aboutme` text NOT NULL,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL,
  `dob` varchar(250) NOT NULL,
  `pnationality` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `place` varchar(250) NOT NULL,
  `postcode` varchar(250) NOT NULL,
  `canton` varchar(250) NOT NULL,
  `land` varchar(250) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(250) NOT NULL,
  `civilstatus` varchar(250) NOT NULL,
  `children` varchar(250) NOT NULL,
  `whatsapp` varchar(250) NOT NULL,
  `facebook` varchar(250) NOT NULL,
  `instagram` varchar(250) NOT NULL,
  `imageidf` varchar(250) NOT NULL,
  `imageidb` varchar(250) NOT NULL,
  `imagepf` varchar(250) NOT NULL,
  `imagepb` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `tabid` varchar(20) NOT NULL,
  `pin` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_girls`
--

INSERT INTO `iw_tns_girls` (`id`, `profilename`, `weight`, `bust`, `oberweitetype`, `privatepart`, `alcohol`, `age`, `hair`, `readymadecloth`, `tattoo`, `nationality`, `size`, `eyes`, `shoes`, `smoke`, `escort`, `aboutme`, `first_name`, `last_name`, `dob`, `pnationality`, `address`, `place`, `postcode`, `canton`, `land`, `mobile`, `email`, `civilstatus`, `children`, `whatsapp`, `facebook`, `instagram`, `imageidf`, `imageidb`, `imagepf`, `imagepb`, `created_date`, `status`, `tabid`, `pin`) VALUES
(1, 'test1', '42', '75A', 'silikon', 'Glatt rasiert', 'Ja', 'test', 'helllichtblond', '34', 'Ja', '', '140', 'helles Eisblau', '31', 'Ja', 'Ja', 'test data', 'girls', 'girls1', '2019-10-10', '', 'Rathausquai 9, 6004 Luzern, Switzerland', 'Luzern', '6004', '', 'Switzerland', '09159976776', 'ibkrmm@gmail.com', 'ledig', '', '', '', '', 'JY7eJ1586423351946.jpg', 'GSrui1586423351946.jpg', 'qP6Hx1586423351946.jpg', 'B9IX01586423351946.jpg', '2020-07-30 09:07:41', 1, '1122', '1212'),
(2, 'test2', '45', '75A', '', 'Glatt rasiert', 'Ja', '25', 'helllichtblond', '34', 'Ja', '', '124', 'helles Eisblau', '31', 'Ja', 'Ja', 'test', 'girls2', 'girls2', '2020-04-08', '', 'Rathausquai 9, 6004 Luzern, Switzerland', 'Luzern', '6004', '', 'Switzerland', '1234567811', 'ibkrmm@gmail.com', 'ledig', 'Ja', '', '', '', 'Oixym1586501971774.jpg', 'dvACW1586501971774.jpg', '7hiXt1586501971774.jpg', 'u3sl71586501971774.jpg', '2020-04-10 08:04:18', 1, '2237', '0000'),
(3, 'test3', '38', '75A', '', 'Glatt rasiert', 'Ja', '22', 'helllichtblond', '34', 'Ja', 'Albanien', '142', 'helles Eisblau', '31', 'Ja', 'Ja', 'testdata', 'girls2', 'girls2', '2017-09-19', 'Albanien', 'Rathausquai 9, 6004 Luzern, Switzerland', 'Luzern', '6004', '', 'Switzerland', '1425789635', 'tt@gmail.com', 'ledig', 'Ja', '', '', '', 'V8pIs1586753846501.jpg', 'zkib51586753846501.jpg', 'AIOBm1586753846501.jpg', 'ehpsm1586753846501.jpg', '2020-04-13 06:04:56', 1, '6792', '0000'),
(4, 'test4', '25', '75A', '', 'Glatt rasiert', 'Ja', '25', 'helllichtblond', '34', 'Ja', 'Andorra', '142', 'helles Eisblau', '31', 'Ja', 'Ja', 'dsfdsf', 'girls4', 'girls4', '2020-04-16', 'Albanien', 'Rathausquai 9, 6004 Luzern, Switzerland', 'Luzern', '6004', '', 'Switzerland', '147852369', 'girls@gmail.com', 'ledig', 'Ja', '', '', '', 'puvBi1586753958850.jpg', 'e9HWn1586753958850.jpg', 'fOkFC1586753958850.jpg', '9uoHP1586753958850.jpg', '2020-04-13 06:04:56', 1, '5678', '0000'),
(5, 'test5', '42', '75A', '', 'Glatt rasiert', 'Ja', '28', 'helllichtblond', '34', 'Ja', 'Albanien', '133', 'helles Eisblau', '31', 'Ja', 'Ja', 'dfaege', 'girls5', 'girls5', '2020-04-13', 'Albanien', 'Rathausquai 9, 6004 Luzern, Switzerland', 'Luzern', '6004', '', 'Switzerland', '2558963147', 'girls5@gmail.com', 'ledig', 'Ja', '', '', '', 'conWL1586754065142.jpg', '1zmJe1586754065142.jpg', 'EINrU1586754065142.jpg', 'm6CFC1586754065142.jpg', '2020-04-13 06:04:56', 1, '8673', '0000'),
(6, 'test6', '48', '75A', '', 'Glatt rasiert', 'Ja', '25', 'helllichtblond', '34', 'Ja', 'Albanien', '150', 'helles Eisblau', '31', 'Ja', 'Ja', 'aadsdd', 'girls6', 'girls', '2020-04-13', 'Albanien', 'Rathausquai 9, 6004 Luzern, Switzerland', 'Luzern', '6004', '', 'Switzerland', '159847236', 'test@gmail.com', 'ledig', 'Ja', '', '', '', 'gVEvy1586785157166.jpg', 'Rft5c1586785157166.jpg', 'rOqFf1586785157166.jpg', 'LAEbD1586785157166.jpg', '2020-04-13 15:04:06', 1, '5022', '0000'),
(7, 'test7', '50', '75A', '', 'Glatt rasiert', 'Ja', '28', 'helllichtblond', '34', 'Ja', 'Albanien', '142', 'helles Eisblau', '31', 'Ja', 'Ja', 'czvv', 'girls7', 'girls', '2020-04-13', 'Albanien', 'Rathausquai 9, 6004 Luzern, Switzerland', 'Luzern', '6004', '', 'Switzerland', '1478523695', 'test@gmail.com', 'ledig', 'Ja', '', '', '', 'uMHNb1586785301755.jpg', 'BE1ki1586785301755.jpg', 'VDerA1586785301755.jpg', 'ijRfN1586785301755.jpg', '2020-05-05 09:05:26', 1, '7156', '0000'),
(65, 'testdata', '', '75A', 'natur', '', '', '24', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Y0znA1592544608939.png', '', '', '', '2020-06-19 08:06:00', 1, '7662', '0000'),
(66, 'WFEFE', '', '', '', '', '', '24', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'j8kWe1592547596370.jpg', '', '', '', '2020-06-19 08:06:00', 1, '5068', '0000'),
(67, 'Anaisa -', '', '', '', '', '', '24', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-06-19 09:06:06', 1, '6022', '0000'),
(68, 'WFEFE', '', '', '', '', '', '24', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-06-19 09:06:06', 1, '8817', '0000'),
(69, 'wafeqfeqfeqfefefe', '', '', '', '', '', '25', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-07-02 08:07:57', 1, '7628', '0000'),
(70, 'SCasc', '', '', '', '', '', '25', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-07-30 09:07:41', 1, '6355', '0000'),
(71, 'xbfxgfs', '', '', '', '', '', '26', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-07-30 09:07:41', 1, '4245', '0000');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_girls_extra`
--

DROP TABLE IF EXISTS `iw_tns_girls_extra`;
CREATE TABLE IF NOT EXISTS `iw_tns_girls_extra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `girls_id` int(11) NOT NULL,
  `extra` varchar(20) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `girls_id` (`girls_id`),
  KEY `girls_id_2` (`girls_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_girls_extra`
--

INSERT INTO `iw_tns_girls_extra` (`id`, `girls_id`, `extra`, `created_date`) VALUES
(1, 71, '1', '2020-07-30 09:07:41'),
(2, 71, '2', '2020-07-30 09:07:41'),
(3, 71, '3', '2020-07-30 09:07:41'),
(4, 71, '4', '2020-07-30 09:07:41'),
(5, 71, '5', '2020-07-30 09:07:41'),
(6, 70, '1', '2020-07-30 09:07:41'),
(7, 70, '2', '2020-07-30 09:07:41'),
(8, 70, '3', '2020-07-30 09:07:41'),
(9, 70, '4', '2020-07-30 09:07:41'),
(10, 70, '5', '2020-07-30 09:07:41'),
(11, 1, '1', '2020-07-30 09:07:41'),
(12, 1, '2', '2020-07-30 09:07:41'),
(13, 1, '3', '2020-07-30 09:07:41'),
(14, 1, '4', '2020-07-30 09:07:41'),
(15, 1, '5', '2020-07-30 09:07:41');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_girls_gallery`
--

DROP TABLE IF EXISTS `iw_tns_girls_gallery`;
CREATE TABLE IF NOT EXISTS `iw_tns_girls_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `girls_id` varchar(20) NOT NULL,
  `image` varchar(250) NOT NULL,
  `viewid` int(11) NOT NULL,
  `listid` varchar(20) NOT NULL,
  `uid` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `girls_id` (`girls_id`),
  KEY `girls_id_2` (`girls_id`)
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_girls_gallery`
--

INSERT INTO `iw_tns_girls_gallery` (`id`, `girls_id`, `image`, `viewid`, `listid`, `uid`, `created_date`) VALUES
(1, '1', 'u0An2Xz1586423255497.jpg', 0, '1', '42h6p0k8sjf01m', ''),
(2, '2', 'tHCEbYo1586501858278.jpg', 0, '', 'jmkh636gk8tu85mx', ''),
(3, '3', 'Mj8P1sH1586753750534.jpg', 0, '', 'dwfu1qgk8y05msv', ''),
(4, '4', 'GT9mxDd1586753884463.jpg', 0, '', 'dwfu1qgk8y09y9s', '2020-04-20'),
(5, '5', 'MoUL50x1586753999347.jpg', 0, '', 'dwfu1qgk8y0ce12', '2020-04-20'),
(6, '6', 'kdNQWuC1586785063058.jpg', 0, '', '2j052ck8yitwa7', '2020-04-20'),
(7, '7', '15I5pLj1586785194804.jpg', 0, '', '2j052ck8yix3at', '2020-04-20'),
(8, '8', '4ou795j1591176691861.jpg', 0, '', '1zj05wgkaz5ht1n', '2020-06-03'),
(9, '9', 'Ct88XWi1591178349637.jpeg', 0, '', '1zj05wgkaz6hbgo', '2020-06-03'),
(11, '10', 'w8FgxOo1591179959173.jpg', 0, '', '1zj06mwkaz7fyry', '2020-06-03'),
(13, '11', 'E9rbIJh1591183082766.jpg', 0, '', '1zj06mwkaz9azsa', '2020-06-03'),
(14, '12', 't9tH0QC1591331026391.jpg', 0, '', '1zj03bwkb1pdp4t', '2020-06-05'),
(18, '12', 'M3HEieK1591788521862.jpg', 0, '', '1zj04gkkb99s0u1', '2020-06-10'),
(24, '13', 'gKK3UpG1591853807583.jpg', 0, '', '1zj01q0kbacn81c', '2020-06-11'),
(25, '14', 'SIMrtTg1591875805555.jpg', 0, '', '1zj04ockbapqblv', '2020-06-11'),
(26, '15', 'fvgieaV1591937535088.jpg', 0, '', '1zj05akkbbqgzfw', '2020-06-12'),
(34, '16', 'xUOEOdR1591960101608.jpg', 0, '', '1zj05o0kbc3x97p', '2020-06-12'),
(35, '17', '3iAykyp1591960136175.jpg', 0, '', '1zj05o0kbc3y4xa', '2020-06-12'),
(36, '18', 'QBuvDO11592221335516.jpg', 0, '', '1zj0308kbgfg9s5', '2020-06-15'),
(37, '19', 'HkwBykL1592222762635.jpg', 0, '', '1zj0378kbggb6pk', '2020-06-15'),
(38, '20', '1Y5oAwR1592222967323.jpg', 0, '', '1zj0378kbggeyvn', '2020-06-15'),
(39, '21', 'i0exVRB1592223338944.jpg', 0, '', '1zj0564kbggmvxf', '2020-06-15'),
(41, '23', 'ccHudHB1592223616833.jpg', 0, '', '1zj039kkbggt3zg', '2020-06-15'),
(42, '24', 'U1yqNoi1592223670228.jpg', 0, '', '1zj039kkbgguej2', '2020-06-15'),
(43, '25', 'gkjpgn31592223713353.png', 0, '', '1zj039kkbggvmwz', '2020-06-15'),
(44, '26', 'gY2wSJL1592368438766.jpg', 0, '', '1zj0350kbiv0v2i', '2020-06-17'),
(45, '27', 'hsBbWvS1592368528659.jpg', 0, '', '1zj0350kbiv2s1v', '2020-06-17'),
(46, '28', 'D4q8tlc1592368573238.jpg', 0, '', '1zj0350kbiv45e7', '2020-06-17'),
(47, '29', 'BxaGS8f1592369239241.jpg', 0, '', '1zj05cgkbiviimo', '2020-06-17'),
(48, '30', 'BYVAhSl1592369447803.jpg', 0, '', '1zj03iskbivmwsm', '2020-06-17'),
(49, '31', 'Ik3nhGf1592369493739.jpg', 0, '', '1zj03iskbivo17y', '2020-06-17'),
(50, '32', 'L129eIX1592369592737.jpg', 0, '', '1zj03iskbivq0au', '2020-06-17'),
(51, '33', 'RdTS98Z1592371117436.jpg', 0, '', '1zj02ygkbiwmp8k', '2020-06-17'),
(52, '34', 'fce8TtH1592387461087.jpg', 0, '', '1zj01bskbj6co58', '2020-06-17'),
(53, '35', 'mL23Iev1592388051068.jpg', 0, '', '1zj05aokbj6p4kw', '2020-06-17'),
(56, '36', 'HPr8z3k1592389138216.jpg', 0, '', '1zj04xokbj7cjdn', '2020-06-17'),
(57, '36', 'JkN7iw21592389138225.jpg', 0, '', '1zj04xokbj7cjdn', '2020-06-17'),
(58, '36', 'aRQn0vc1592389138233.png', 0, '', '1zj04xokbj7cjdn', '2020-06-17'),
(235, '65', '7iHmfM61592544526816.jpg', 1, '1', '1zj04w8kblruvpa', '2020-06-19'),
(236, '65', 'u9z3y5o1592544526823.jpg', 0, '2', '1zj04w8kblruvpa', '2020-06-19'),
(237, '66', 'F7glwJZ1592547575738.jpg', 0, '', '1zj02fkkbltog5y', '2020-06-19'),
(238, '66', 'rZ2S2rw1592547575743.jpg', 0, '', '1zj02fkkbltog5y', '2020-06-19'),
(242, '67', '32fW46G1592551624532.jpg', 1, '1', '1zj03ngkblw3lbt', '2020-06-19'),
(243, '67', 'bHOURdb1592551624541.jpg', 0, '2', '1zj03ngkblw3lbt', '2020-06-19'),
(244, '67', 'BpqaZWy1592551624545.jpeg', 1, '3', '1zj03ngkblw3lbt', '2020-06-19'),
(248, '68', 'SPYmiPv1592565122694.jpg', 0, '1', '1zj03ngkbm44tug', '2020-06-19'),
(249, '69', 'ClwBul01593666319257.jpg', 0, '1', '1zj03owkc4bqqrr', '2020-07-02'),
(250, '70', 'jhNM4Ox1593670724676.jpg', 0, '1', '1zj02hokc4edfp4', '2020-07-02'),
(251, '71', 'H8H3E4l1593671090253.jpg', 0, '1', '1zj02z8kc4elew5', '2020-07-02');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_girls_idproof`
--

DROP TABLE IF EXISTS `iw_tns_girls_idproof`;
CREATE TABLE IF NOT EXISTS `iw_tns_girls_idproof` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `girls_id` int(11) NOT NULL,
  `imageidf` varchar(250) NOT NULL,
  `imageidb` varchar(250) NOT NULL,
  `imagepf` varchar(250) NOT NULL,
  `imagepb` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iw_tns_girls_idproof_ibfk_1` (`girls_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_girls_permit`
--

DROP TABLE IF EXISTS `iw_tns_girls_permit`;
CREATE TABLE IF NOT EXISTS `iw_tns_girls_permit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `girls_id` int(11) NOT NULL,
  `startdate` varchar(20) NOT NULL,
  `enddate` varchar(20) NOT NULL,
  `responsible` varchar(250) NOT NULL,
  `file` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `girls_id` (`girls_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_girls_permit`
--

INSERT INTO `iw_tns_girls_permit` (`id`, `girls_id`, `startdate`, `enddate`, `responsible`, `file`, `created_date`) VALUES
(1, 7, '27.04.2020', '03.05.2020', '1', '', '2020-05-05 09:05:26'),
(2, 7, '04.05.2020', '10.05.2020', '1', '', '2020-05-05 09:05:26'),
(16, 66, '12.06.2020', '03.06.2020', '3', '', '2020-06-19 08:06:00'),
(17, 66, '02.06.2020', '11.06.2020', '1', 'hhbrw1592547596370.pdf', '2020-06-19 08:06:00');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_girls_pin`
--

DROP TABLE IF EXISTS `iw_tns_girls_pin`;
CREATE TABLE IF NOT EXISTS `iw_tns_girls_pin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `girls_id` int(11) NOT NULL,
  `pin` varchar(20) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `girls_id` (`girls_id`),
  KEY `girls_id_2` (`girls_id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_girls_pin`
--

INSERT INTO `iw_tns_girls_pin` (`id`, `girls_id`, `pin`, `created_date`) VALUES
(2, 2, 'NEW', '2020-04-10 08:04:18'),
(3, 3, 'TOP', '2020-04-13 06:04:56'),
(4, 4, 'NEW', '2020-04-13 06:04:56'),
(5, 5, 'NEW', '2020-04-13 06:04:56'),
(6, 6, 'NEW', '2020-04-13 15:04:06'),
(8, 7, 'NEW', '2020-05-05 09:05:26'),
(64, 65, '', '2020-06-19 07:06:49'),
(65, 66, '', '2020-06-19 08:06:00'),
(66, 67, '', '2020-06-19 09:06:06'),
(67, 68, '', '2020-06-19 09:06:06'),
(68, 69, '', '2020-07-02 05:07:42'),
(69, 70, '', '2020-07-02 08:07:57'),
(70, 71, '', '2020-07-02 08:07:04'),
(71, 1, 'NEW', '2020-07-30 09:07:41');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_girls_service`
--

DROP TABLE IF EXISTS `iw_tns_girls_service`;
CREATE TABLE IF NOT EXISTS `iw_tns_girls_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `girls_id` int(11) NOT NULL,
  `service` varchar(20) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `girls_id` (`girls_id`),
  KEY `girls_id_2` (`girls_id`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_girls_service`
--

INSERT INTO `iw_tns_girls_service` (`id`, `girls_id`, `service`, `created_date`) VALUES
(2, 2, '1', '2020-04-10 08:04:18'),
(3, 3, '1', '2020-04-13 06:04:56'),
(4, 4, '2', '2020-04-13 06:04:56'),
(5, 5, '1', '2020-04-13 06:04:56'),
(6, 6, '1', '2020-04-13 15:04:06'),
(8, 7, '2', '2020-05-05 09:05:26'),
(105, 65, '1', '2020-06-19 08:06:00'),
(106, 66, '1', '2020-06-19 08:06:00'),
(107, 67, '1', '2020-06-19 09:06:06'),
(109, 68, '2', '2020-06-19 09:06:06'),
(134, 69, '1', '2020-07-02 08:07:57'),
(135, 69, '2', '2020-07-02 08:07:57'),
(136, 69, '3', '2020-07-02 08:07:57'),
(139, 71, '1', '2020-07-30 09:07:41'),
(140, 70, '1', '2020-07-30 09:07:41'),
(141, 1, '1', '2020-07-30 09:07:41'),
(142, 1, '2', '2020-07-30 09:07:41'),
(143, 1, '3', '2020-07-30 09:07:41');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_guestinfo`
--

DROP TABLE IF EXISTS `iw_tns_guestinfo`;
CREATE TABLE IF NOT EXISTS `iw_tns_guestinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` varchar(20) NOT NULL,
  `branch_id` varchar(250) NOT NULL,
  `notopen` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `selection` int(11) NOT NULL,
  `busy` int(11) NOT NULL,
  `club` int(11) NOT NULL,
  `others` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `comments` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_guestinfo`
--

INSERT INTO `iw_tns_guestinfo` (`id`, `created_date`, `branch_id`, `notopen`, `price`, `selection`, `busy`, `club`, `others`, `total`, `comments`) VALUES
(2, '2020-07-09', '1', 1, 1, 2, 1, 58, 1, 64, 'x SVdavd fdhdthghgd\ndsgsdffdfd\nsdvdvsdfds\nsdfsdfdsfd'),
(4, '2020-07-10', '1', 1, 1, 1, 1, 1, 1, 6, 'gsfgfgf');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_hair`
--

DROP TABLE IF EXISTS `iw_tns_hair`;
CREATE TABLE IF NOT EXISTS `iw_tns_hair` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hair` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_hair`
--

INSERT INTO `iw_tns_hair` (`id`, `hair`, `created_date`, `status`) VALUES
(1, 'helllichtblond', '2020-02-19 14:02:28', 1),
(2, 'lichtblond', '2020-02-19 14:02:28', 1),
(3, 'hellblond', '2020-02-19 14:02:28', 1),
(4, 'mittelblond', '2020-02-19 14:02:28', 1),
(5, 'dunkelblond', '2020-02-19 14:02:28', 1),
(6, 'hellbraun', '2020-02-19 14:02:28', 1),
(7, 'mittelbraun', '2020-02-19 14:02:28', 1),
(8, 'dunkelbraun', '2020-02-19 14:02:28', 1),
(9, 'schwarz', '2020-02-19 14:02:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_localapproval`
--

DROP TABLE IF EXISTS `iw_tns_localapproval`;
CREATE TABLE IF NOT EXISTS `iw_tns_localapproval` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `startdate` varchar(20) NOT NULL,
  `enddate` varchar(20) NOT NULL,
  `file` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_localapproval`
--

INSERT INTO `iw_tns_localapproval` (`id`, `branch_id`, `startdate`, `enddate`, `file`, `created_date`) VALUES
(35, 35, '', '', '', '2020-06-19 08:06:11'),
(36, 36, '04.06.2020', '28.05.2020', '', '2020-06-19 08:06:11'),
(37, 36, '12.06.2020', '18.06.2020', '', '2020-06-19 08:06:11'),
(28, 12, '13.06.2020', '13.06.2020', 'cGI891591265050315.jpg', '2020-06-04 11:06:34'),
(27, 12, '04.06.2020', '05.06.2020', '', '2020-06-04 11:06:34'),
(26, 12, '10.07.2020', '12.06.2020', '', '2020-06-04 11:06:34'),
(25, 12, '04.06.2020', '03.06.2020', '', '2020-06-04 11:06:34'),
(24, 12, '04.06.2020', '06.06.2020', '', '2020-06-04 11:06:34'),
(23, 12, '12.06.2020', '12.06.2020', '9x9kN1591264803891.jpeg', '2020-06-04 11:06:34'),
(22, 12, '04.06.2020', '03.06.2020', 'mwLeR1591264764331.jpg', '2020-06-04 11:06:34');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_nationality`
--

DROP TABLE IF EXISTS `iw_tns_nationality`;
CREATE TABLE IF NOT EXISTS `iw_tns_nationality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nationality` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_nationality`
--

INSERT INTO `iw_tns_nationality` (`id`, `nationality`, `created_date`, `status`) VALUES
(1, 'Albanien', '2020-04-07 23:04:21', 1),
(2, 'Algerien', '2020-04-07 23:04:21', 1),
(3, 'Afghanistan', '2020-04-07 23:04:21', 1),
(4, 'Andorra', '2020-04-07 23:04:21', 1),
(5, 'Argentinien', '2020-04-07 23:04:21', 1),
(6, 'Armenien', '2020-04-07 23:04:21', 1),
(7, 'Aserbaidschan', '2020-04-07 23:04:21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_notification`
--

DROP TABLE IF EXISTS `iw_tns_notification`;
CREATE TABLE IF NOT EXISTS `iw_tns_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(250) NOT NULL,
  `branchtype` varchar(250) NOT NULL,
  `title` varchar(250) NOT NULL,
  `total` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `buttontext` varchar(250) NOT NULL,
  `url` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `website` varchar(250) NOT NULL,
  `facebook` varchar(250) NOT NULL,
  `instagram` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `view_status` int(11) NOT NULL,
  `delete_status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_notification`
--

INSERT INTO `iw_tns_notification` (`id`, `type`, `branchtype`, `title`, `total`, `description`, `buttontext`, `url`, `email`, `mobile`, `phone`, `website`, `facebook`, `instagram`, `created_date`, `status`, `view_status`, `delete_status`) VALUES
(1, 'branch', 'all', 'service1', '5', 'sdgsdgd fdgsdfgsf', 'sdgsd', 'dsgsd', 'ibkrmm@gmail.com', '09159976776', '09159976776', 'sdds', 'sdgds', 'dsgds', '2020-07-20 07:07:04', 1, 0, 0),
(2, 'branch', 'branch', 'test1', '5', 'fdhdf gntghtd', 'sdgsd', 'http://3.127.12.202/', 'ibkrmm@gmail.com', '09159976776', '09159976776', 'dgfgfd', 'sdgds', 'fdgfd', '2020-07-20 07:07:04', 1, 0, 0),
(3, 'branch', 'branch', ',jnknoi', '45', 'lkjoijoj jubihoij', 'sdgsd', 'http://3.127.12.202/', 'ibkrmm@gmail.com', '09159976776', '09159976776', 'www.penthouse9.ch', 'sdgds', 'dsgds', '2020-07-20 07:07:20', 1, 0, 0),
(4, 'customer', 'all', 'w4rwe4r', '1', 'wdqewd', 'fadfa', 'http://3.127.12.202/', 'ibkrmm@gmail.com', '09159976776', '09159976776', 'www.penthouse9.ch', 'qwewqe', 'qadewqd', '2020-07-20 07:07:20', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_notification_branch`
--

DROP TABLE IF EXISTS `iw_tns_notification_branch`;
CREATE TABLE IF NOT EXISTS `iw_tns_notification_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note_id` varchar(20) NOT NULL,
  `branch_id` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_notification_branch`
--

INSERT INTO `iw_tns_notification_branch` (`id`, `note_id`, `branch_id`, `type`) VALUES
(1, '1', '1', 'branch'),
(2, '1', '2', 'branch'),
(3, '1', '3', 'branch'),
(4, '1', '4', 'branch'),
(5, '1', '5', 'branch'),
(6, '1', '6', 'branch'),
(7, '1', '7', 'branch'),
(8, '2', '1', 'branch'),
(9, '3', '2', 'branch');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_notification_gallery`
--

DROP TABLE IF EXISTS `iw_tns_notification_gallery`;
CREATE TABLE IF NOT EXISTS `iw_tns_notification_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(250) NOT NULL,
  `uid` varchar(250) NOT NULL,
  `note_id` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_notification_gallery`
--

INSERT INTO `iw_tns_notification_gallery` (`id`, `image`, `uid`, `note_id`) VALUES
(1, 'oCZg4uv1594871834457.jpg', '1zj04hkkco9h2yt', ''),
(2, 'u5TlW0W1594871834466.jpg', '1zj04hkkco9h2yt', ''),
(3, '3gDLCMp1594876320045.jpg', '1zj03lwkcoc5nju', ''),
(4, 'wW0TWEX1594876320138.jpg', '1zj03lwkcoc5nju', ''),
(5, 'WpQqNaS1594876459526.jpg', '1zj04lokcoc8kn0', ''),
(6, 'ZWjZhEZ1594876459638.jpg', '1zj04lokcoc8kn0', ''),
(7, 'K45Tx041594877052599.jpg', '1zj03pokcocl92p', ''),
(8, '3ra6kAc1594877052609.jpg', '1zj03pokcocl92p', ''),
(9, 'Y9Qm3BR1594878571584.jpg', '1zj0584kcodi7x8', '6'),
(10, 'd3a9LqQ1594878571595.jpg', '1zj0584kcodi7x8', '6'),
(11, 'HWHvlhE1594880596051.jpg', '1zj0478kcoepf1a', '8'),
(12, 'dkvPQaQ1594880596173.jpg', '1zj0478kcoepf1a', '8'),
(13, 'j3sk9Co1594880820435.jpg', '1zj04x4kcoeu1tc', '12'),
(14, 'sqRoCv01594880820447.jpg', '1zj04x4kcoeu1tc', '12'),
(15, 'NsXMZz11594880820456.jpg', '1zj04x4kcoeu1tc', '12'),
(16, 'xSjFwNk1594983108200.jpg', '1zj05mgkcq3qlec', '13'),
(17, 'aYDgvP61594983108214.jpg', '1zj05mgkcq3qlec', '13'),
(18, 'PXjO1Y61594983108226.jpg', '1zj05mgkcq3qlec', '13'),
(19, 'PKs4x5q1594983351749.jpg', '1zj01ockcq3vy6s', '15'),
(20, 'wZzmkZh1594983590193.jpg', '1zj05hskcq417ww', '17'),
(21, 'YocQ0eP1594983885203.jpg', '1zj05s0kcq47bzp', '18'),
(22, '4HeTCkZ1594984041131.jpg', '1zj05ywkcq4avhn', '19'),
(23, 'AbsN1Yi1594984198866.jpg', '1zj0ugkcq4e655', '20'),
(24, '3GqhtvG1594985180978.jpg', '1zj048ckcq4z9iy', '1'),
(25, 'JG90Gzc1594985318665.jpg', '1zj060gkcq5286r', '2'),
(26, 'yegh0ZH1594985616531.jpg', '1zj05okkcq58n2s', '3'),
(27, 'jKiaerX1594985824372.jpg', '1zj05z8kcq5czak', '4'),
(28, 'svWM3yb1594986346409.jpg', '1zj0294kcq5oadw', '5'),
(29, 'g2sngjp1594986507113.jpg', '1zj05l0kcq5rqpo', '6'),
(30, 'tbY4CXq1594986665918.jpg', '1zj05bkkcq5v4og', '7'),
(31, 'AogeTKz1594986776495.jpg', '1zj06aokcq5xk3n', '8'),
(32, 'wBR8RIF1594986890403.jpg', '1zj062kkcq5zyp7', '10'),
(33, 'kl5EtiB1594987193586.jpg', '1zj01ugkcq66fla', '11'),
(34, 'YheoNPI1594987850747.jpg', '1zj06hkkcq6km8r', '12'),
(35, 'mTAUwdl1595217178054.jpg', '1zj03yskctz3o9o', ''),
(36, 'CmF1l7S1595217298342.jpg', '1zj0700kctz67f2', ''),
(37, 'r4krfm01595217420727.jpg', '1zj06rwkctz8vw1', '1'),
(38, 'uVUBQV61595217601994.jpg', '1zj04lgkctzctzm', '2'),
(39, 'GDWmjxm1595217908443.jpg', '1zj03q8kctzje8g', '3'),
(40, 'rRi1Ost1595217961097.jpg', '1zj03q8kctzkbyy', ''),
(41, '9c1etaj1595218121245.jpg', '1zj021skctzny1q', ''),
(42, 'WibUqJ41595218327848.jpg', '1zj049ckctzsddl', '4'),
(43, 'JB0oTJ21595219141633.jpg', '1zj06t8kcu09uoh', '1'),
(44, 'RTUpovS1595219349332.jpg', '1zj02nckcu0ebkx', '1'),
(45, '6MIucTK1595219714090.jpg', '1zj04h0kcu0m4cb', '2'),
(46, 'gPMxp3c1595219856730.jpg', '1zj05f8kcu0p9f0', '3'),
(47, 'uxAFuZB1595219943190.jpg', '1zj05f8kcu0qqi2', '4'),
(48, 'Tr9Pu4v1595220166264.jpg', '1zj05f8kcu0vmni', '6'),
(49, 'IHNETpf1595220456030.jpg', '1zj01owkcu11nyy', '8'),
(50, 'ogmO44m1595220456039.jpg', '1zj01owkcu11nyy', '8'),
(51, '92pqHb41595220583134.jpg', '1zj01owkcu14o8g', '9'),
(52, 'dTB9qqc1595220910013.jpg', '1zj01owkcu1bj6n', ''),
(53, 'oqqksIh1595221038910.jpg', '1zj01owkcu1ed3q', ''),
(54, 'ayN2nGq1595221158947.jpg', '1zj01owkcu1gwq5', ''),
(55, 'Y0IJfVu1595221350348.jpg', '1zj01owkcu1kpg6', '11'),
(56, '0AUT1tR1595221604517.jpg', '1zj02gckcu1qce8', '13'),
(57, 'y0XimmH1595221819574.jpg', '1zj02gckcu1v7ot', '15'),
(58, 'uEeVIF31595222022379.jpg', '1zj03tskcu1zkix', '16'),
(59, 'AVtnt0B1595222158500.jpg', '1zj04fskcu22jwh', '17'),
(60, 'tP3tFuL1595223203276.jpg', '1zj04fskcu2opqr', '1'),
(61, 'SVqfNGy1595223248676.jpg', '1zj04fskcu2psnd', '2'),
(62, '5M6J2q11595223254700.jpg', '1zj04fskcu2psnd', '2'),
(63, 'ZAYMs4I1595223260773.jpg', '1zj04fskcu2psnd', '2'),
(64, '5xdiI2F1595223403679.jpg', '1zj03agkcu2srx3', '3'),
(65, '6JFSc631595223403688.jpg', '1zj03agkcu2srx3', '3'),
(66, 'Gx7g54L1595223403696.jpg', '1zj03agkcu2srx3', '3'),
(67, 'xCSfaRI1595246293770.jpg', '1zj03agkcugc87g', '4'),
(68, '1xYN2kL1595246293775.jpg', '1zj03agkcugc87g', '4'),
(69, 'A75PMPl1595246293782.jpg', '1zj03agkcugc87g', '4'),
(70, 'dDTJcqy1595248339086.jpg', '1zj03lgkcuhe9uo', ''),
(71, 'pK3jqF91595248339094.jpg', '1zj03lgkcuhe9uo', ''),
(72, 'Nz8xQuz1595248582784.jpg', '1zj03lgkcuht3kx', ''),
(73, 'QgChTY01595248582791.jpg', '1zj03lgkcuht3kx', ''),
(74, 'NF8RcF11595248582804.jpg', '1zj03lgkcuht3kx', '');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_paymentlog`
--

DROP TABLE IF EXISTS `iw_tns_paymentlog`;
CREATE TABLE IF NOT EXISTS `iw_tns_paymentlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `paymenttype` varchar(250) NOT NULL,
  `price` int(11) NOT NULL,
  `type` varchar(10) NOT NULL COMMENT '1-->add,2-->minus',
  `booking_date` varchar(20) NOT NULL,
  `girls_id` int(11) NOT NULL,
  `paymentfor` varchar(250) NOT NULL,
  `reason` text NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `booking_id` (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_paymentlog`
--

INSERT INTO `iw_tns_paymentlog` (`id`, `booking_id`, `paymenttype`, `price`, `type`, `booking_date`, `girls_id`, `paymentfor`, `reason`, `created_date`, `status`) VALUES
(1, 1, 'Visa', 72, '1', '2020-08-03', 1, 'servicefirst', '', '2020-08-03 15:46', 0),
(2, 1, 'cash', 20, '2', '2020-08-03', 1, 'cash', 'dgdfgfdg', '2020-08-03 15:47', 0),
(3, 2, 'Mastercard', 37, '1', '2020-08-05', 1, 'servicefirst', '', '2020-08-05 09:40', 0),
(4, 2, 'cash', 10, '2', '2020-08-05', 1, 'cash', 'dgfg', '2020-08-05 09:42', 0),
(5, 2, 'cash', 1, '2', '2020-08-05', 1, 'cash', 'fgjhftgg', '2020-08-05 13:26', 0),
(6, 2, 'cash', 12, '2', '2020-08-05', 1, 'extra', '', '2020-08-05 13:26', 0),
(7, 2, 'Twint', 12, '1', '2020-08-05', 1, 'extra', '', '2020-08-05 13:34', 0),
(8, 2, 'cash', 12, '2', '2020-08-05', 1, 'extra', '', '2020-08-05 13:35', 0),
(9, 2, 'Wir2', 12, '1', '2020-08-05', 1, 'extra', '', '2020-08-05 13:37', 0),
(10, 2, 'Mastercard', 20, '1', '2020-08-05', 1, 'extra', '', '2020-08-05 13:37', 0),
(11, 2, 'cash', 20, '2', '2020-08-05', 1, 'extra', '', '2020-08-05 13:38', 0),
(12, 2, 'cash', 12, '2', '2020-08-05', 1, 'extra', '', '2020-08-05 13:47', 0),
(13, 2, 'Mastercard', 12, '1', '2020-08-05', 1, 'extra', '', '2020-08-05 13:53', 0),
(14, 2, 'cash', 12, '2', '2020-08-05', 1, 'extra', '', '2020-08-05 13:54', 0),
(15, 2, 'Post', 12, '1', '2020-08-05', 1, 'extra', '', '2020-08-05 13:58', 0),
(16, 2, 'cash', 12, '2', '2020-08-05', 1, 'extra', '', '2020-08-05 13:58', 0),
(17, 2, 'cash', 10, '2', '2020-08-05', 1, 'cash', 'tujyt gfj', '2020-08-05 13:59', 0),
(18, 4, 'Post', 100, '1', '2020-08-05', 2, 'servicefirst', '', '2020-08-05 14:40', 0),
(19, 6, 'Visa', 72, '1', '2020-08-06', 1, 'servicefirst', '', '2020-08-06 06:49', 0),
(20, 8, 'Post', 0, '1', '2020-08-06', 1, 'servicefirst', '', '2020-08-06 11:28', 0),
(21, 8, 'Visa', 0, '1', '2020-08-06', 1, 'servicefirst', '', '2020-08-06 11:30', 0),
(22, 8, 'Visa', 0, '1', '2020-08-06', 1, 'servicefirst', '', '2020-08-06 11:36', 0),
(23, 7, 'Twint', 50, '1', '2020-08-06', 2, 'servicefirst', '', '2020-08-06 12:06', 0),
(24, 9, 'EuroCash', 62, '1', '2020-08-06', 1, 'servicefirst', '', '2020-08-06 12:08', 0),
(25, 10, 'Twint', 25, '1', '2020-08-06', 2, 'servicefirst', '', '2020-08-06 15:33', 0),
(26, 11, 'Twint', 72, '1', '2020-08-07', 1, 'servicefirst', '', '2020-08-07 06:44', 0),
(27, 12, 'Post', 72, '1', '2020-08-10', 1, 'servicefirst', '', '2020-08-10 05:59', 0),
(28, 13, 'Visa', 47, '1', '2020-08-11', 1, 'servicefirst', '', '2020-08-11 06:06', 1),
(29, 14, 'Mastercard', 72, '1', '2020-08-11', 1, 'servicefirst', '', '2020-08-11 07:07', 1),
(30, 14, 'EuroCash', 20, '1', '2020-08-11', 1, 'extra', '', '2020-08-11 08:34', 1),
(31, 14, 'cash', 10, '2', '2020-08-11', 1, 'extra', '', '2020-08-11 08:35', 1),
(32, 15, 'Twint', 72, '1', '2020-08-11', 1, 'servicefirst', '', '2020-08-11 08:36', 1),
(33, 15, 'cash', 10, '2', '2020-08-11', 1, 'extra', '', '2020-08-11 08:37', 1),
(34, 15, 'cash', 25, '2', '2020-08-11', 1, 'service', '', '2020-08-11 09:11', 1),
(35, 15, 'cash', 12, '2', '2020-08-11', 1, 'extra', '', '2020-08-11 13:10', 1),
(36, 15, 'cash', 10, '2', '2020-08-11', 1, 'cash', 'Gilr war nicht in Ordnung', '2020-08-11 13:40', 1),
(37, 15, 'Twint', 12, '1', '2020-08-11', 1, 'extra', '', '2020-08-11 13:41', 1),
(38, 15, 'cash', 15, '2', '2020-08-11', 1, 'service', '', '2020-08-11 13:41', 1),
(39, 15, 'cash', 12, '2', '2020-08-11', 1, 'extra', '', '2020-08-11 13:42', 1),
(40, 15, 'Twint', 10, '1', '2020-08-11', 1, 'extra', '', '2020-08-11 13:45', 1),
(41, 15, 'cash', 5, '2', '2020-08-11', 1, 'cash', 'Anderes', '2020-08-11 13:46', 1),
(42, 15, 'cash', 5, '2', '2020-08-11', 1, 'cash', 'Gilr war nicht in Ordnung', '2020-08-11 13:49', 1),
(43, 15, 'cash', 5, '2', '2020-08-11', 1, 'cash', 'Gilr war nicht in Ordnung', '2020-08-11 13:49', 1),
(44, 16, 'Visa', 193, '1', '2020-08-11', 1, 'servicefirst', '', '2020-08-11 13:56', 1),
(45, 16, 'cash', 121, '2', '2020-08-11', 1, 'extra', '', '2020-08-11 13:56', 1),
(46, 16, 'cash', 10, '2', '2020-08-11', 1, 'cash', 'Gilr war nicht in Ordnung', '2020-08-11 13:57', 1),
(47, 17, 'Visa', 92, '1', '2020-08-11', 1, 'servicefirst', '', '2020-08-11 13:58', 0),
(48, 18, 'Mastercard', 72, '1', '2020-08-12', 1, 'servicefirst', '', '2020-08-12 06:28', 0),
(49, 19, 'Twint', 243, '1', '2020-08-12', 1, 'servicefirst', '', '2020-08-12 12:13', 0),
(50, 19, 'cash', 25, '2', '2020-08-12', 1, 'service', '', '2020-08-12 12:13', 0),
(51, 19, 'cash', 10, '2', '2020-08-12', 1, 'cash', 'Gilr war nicht in Ordnung', '2020-08-12 12:13', 0),
(52, 19, 'cash', 121, '2', '2020-08-12', 1, 'extra', '', '2020-08-12 12:14', 0),
(53, 20, 'Mastercard', 47, '1', '2020-08-13', 1, 'servicefirst', '', '2020-08-13 06:17', 0),
(54, 21, 'Mastercard', 72, '1', '2020-08-13', 1, 'servicefirst', '', '2020-08-13 07:28', 0),
(55, 20, 'Twint', 75, '1', '2020-08-13', 1, 'eservice', '', '2020-08-13 12:21', 0),
(56, 20, 'Visa', 50, '1', '2020-08-13', 1, 'eservice', '', '2020-08-13 12:24', 0),
(57, 22, 'Mastercard', 60, '1', '2020-08-13', 1, 'servicefirst', '', '2020-08-13 12:37', 0),
(58, 23, 'Post', 72, '1', '2020-08-14', 1, 'servicefirst', '', '2020-08-14 11:45', 0),
(59, 23, 'cash', 20, '2', '2020-08-14', 1, 'cash', 'Anderes', '2020-08-14 11:46', 0);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_permit_person`
--

DROP TABLE IF EXISTS `iw_tns_permit_person`;
CREATE TABLE IF NOT EXISTS `iw_tns_permit_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `idcard` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_permit_person`
--

INSERT INTO `iw_tns_permit_person` (`id`, `name`, `idcard`, `created_date`, `status`) VALUES
(1, 'test', 'L', '2020-02-19 14:02:28', 1),
(2, 'test2', 'B', '2020-02-19 14:02:28', 1),
(3, 'test3', 'C', '2020-02-19 14:02:28', 1),
(4, 'test4', 'E', '2020-06-08 08:06:02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_pin`
--

DROP TABLE IF EXISTS `iw_tns_pin`;
CREATE TABLE IF NOT EXISTS `iw_tns_pin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pin` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_pin`
--

INSERT INTO `iw_tns_pin` (`id`, `pin`) VALUES
(1, 'NEW'),
(2, 'TOP'),
(3, 'ESCORT');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_points`
--

DROP TABLE IF EXISTS `iw_tns_points`;
CREATE TABLE IF NOT EXISTS `iw_tns_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `branch_id` varchar(20) NOT NULL,
  `points` varchar(20) NOT NULL,
  `shortdescription` text NOT NULL,
  `longdescription` text NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_points`
--

INSERT INTO `iw_tns_points` (`id`, `title`, `branch_id`, `points`, `shortdescription`, `longdescription`, `created_date`, `status`) VALUES
(1, 'tdg', '1', '10', 'dgfdg', 'fdgf', '2020-03-03 11:03:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_point_gallery`
--

DROP TABLE IF EXISTS `iw_tns_point_gallery`;
CREATE TABLE IF NOT EXISTS `iw_tns_point_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `point_id` varchar(20) NOT NULL,
  `image` varchar(250) NOT NULL,
  `uid` text NOT NULL,
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_point_gallery`
--

INSERT INTO `iw_tns_point_gallery` (`id`, `point_id`, `image`, `uid`, `created_date`) VALUES
(1, '1', 'xraa7iS1583231619238.jpg', 'oi2swk7br7w1v', ''),
(2, '1', 'NpD9fJp1583231619250.jpg', 'oi2swk7br7w1v', '');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_privatepart`
--

DROP TABLE IF EXISTS `iw_tns_privatepart`;
CREATE TABLE IF NOT EXISTS `iw_tns_privatepart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `privatepart` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_privatepart`
--

INSERT INTO `iw_tns_privatepart` (`id`, `privatepart`, `created_date`, `status`) VALUES
(1, 'Glatt rasiert', '2020-02-20 07:02:44', 1),
(2, 'teil rasiert', '2020-02-20 07:02:44', 1),
(3, 'behaart', '2020-02-20 07:02:44', 1),
(4, 'teil behart', '2020-02-20 07:02:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_revenue`
--

DROP TABLE IF EXISTS `iw_tns_revenue`;
CREATE TABLE IF NOT EXISTS `iw_tns_revenue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `amount` int(11) NOT NULL,
  `comments` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `branch_id` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_revenue`
--

INSERT INTO `iw_tns_revenue` (`id`, `name`, `amount`, `comments`, `created_date`, `branch_id`) VALUES
(5, 'zfsaf', 2, 'sacsacs', '2020-07-09', '1'),
(6, 'DFc', 10, 'cscs', '2020-07-10', '1');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_room`
--

DROP TABLE IF EXISTS `iw_tns_room`;
CREATE TABLE IF NOT EXISTS `iw_tns_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `titel` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `file` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_room`
--

INSERT INTO `iw_tns_room` (`id`, `branch_id`, `titel`, `description`, `file`, `created_date`) VALUES
(1, 1, 'room1', 'testroom1', 'xAARh1587368138203.jpg', '2020-04-20 09:04:03'),
(2, 1, 'room2', 'testroom2', '0estJ1587368273164.jpg', '2020-04-20 09:04:03'),
(3, 1, 'room3', 'testroom3', 'V4RBl1587368273164.jpg', '2020-04-20 09:04:03'),
(4, 1, 'room4', 'testroom4', 'sT0nV1587368273164.jpg', '2020-04-20 09:04:03'),
(5, 1, 'room5', 'testrom', 'Kx3mb1587368273164.jpg', '2020-04-20 09:04:03'),
(6, 2, 'xgfg', 'dfgfd', '2r9Jm1591100011632.jpg', '2020-06-02 14:06:47'),
(7, 4, 'dvvs', 'svsc', 'VFv831591167164634.jpg', '2020-06-03 08:06:02'),
(8, 7, 'zvdv', 'zdvzdvzd', 'PiWDM1591168214302.jpg', '2020-06-03 09:06:59'),
(9, 7, 'xvdsvd', '', 'xtYfR1591168536622.jpg', '2020-06-03 09:06:20'),
(10, 10, 'jhkuykukyuk9595dthdth', '35415454', 'W5uhL1591244894744.jpeg', '2020-06-04 05:06:00'),
(11, 10, 'yuiuyiuyiy96558cgngn', 'oioioioioio5485', 'XrRQR1591244894744.jpeg', '2020-06-04 05:06:00'),
(12, 9, 'SCsacsgfrg59452684894', 'sfvfsvf35495+5', 'Os3Nx1591246267173.jpg', '2020-06-04 06:06:44'),
(13, 9, 'zvvdv', 'dsvdvd', 'agUll1591246297800.jpeg', '2020-06-04 06:06:27'),
(14, 8, '', '', 'iTf8G1591254569393.jpg', '2020-06-04 06:06:27'),
(15, 11, '12upopou', '', 'QJLdZ1591257315622.jpg', '2020-06-04 09:06:29'),
(16, 11, 'z ddupopip', 'dvdvsduipip', 'LJbkW1591257315622.jpg', '2020-06-04 09:06:29'),
(22, 12, ';.', '', 'Czoya1591850774343.jpg', '2020-06-11 06:06:56'),
(25, 32, '', '', 'undefined', '2020-06-15 09:06:12'),
(26, 35, '', '', 'undefined', '2020-06-19 08:06:11'),
(27, 36, 'dvvs', 'AcC', '7VNWR1592549172669.jpg', '2020-06-19 08:06:11');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_rubble`
--

DROP TABLE IF EXISTS `iw_tns_rubble`;
CREATE TABLE IF NOT EXISTS `iw_tns_rubble` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `cdate` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_rubble`
--

INSERT INTO `iw_tns_rubble` (`id`, `title`, `cdate`, `description`, `image`, `created_date`, `status`) VALUES
(1, 'cvc', '06-05-2020', 'xcvc', 'h6g9U1591781217965.jpg', '2020-05-06 14:05:49', 1),
(2, 'tujuj123', '09-06-2020', 'tujutju963', 'XYlAi1591781205217.jpg', '2020-06-08 05:06:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_service`
--

DROP TABLE IF EXISTS `iw_tns_service`;
CREATE TABLE IF NOT EXISTS `iw_tns_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `branch` varchar(20) NOT NULL,
  `zeit` varchar(20) NOT NULL,
  `etime` varchar(20) NOT NULL,
  `price` varchar(250) NOT NULL,
  `eprice` varchar(20) NOT NULL,
  `mprice` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `branch` (`branch`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_service`
--

INSERT INTO `iw_tns_service` (`id`, `title`, `branch`, `zeit`, `etime`, `price`, `eprice`, `mprice`, `status`, `created_date`) VALUES
(1, '15min', '1', '4', '', '25', '', '', 1, '2020-08-03 15:08:22'),
(2, '30', '1', '2', '', '50', '', '', 1, '2020-08-03 15:08:22'),
(3, '45', '1', '5', '', '75', '', '', 1, '2020-08-03 15:08:22'),
(4, '1h', '1', '6', '', '100', '', '', 1, '2020-08-03 15:08:22');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_servicelog`
--

DROP TABLE IF EXISTS `iw_tns_servicelog`;
CREATE TABLE IF NOT EXISTS `iw_tns_servicelog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `service_id` varchar(250) NOT NULL,
  `branch_id` varchar(250) NOT NULL,
  `girls_id` varchar(250) NOT NULL,
  `type` varchar(10) NOT NULL COMMENT '1->new,2-change',
  `up_down` varchar(20) NOT NULL COMMENT '1->up,2->down, 0->new',
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `booking_id` (`booking_id`),
  KEY `booking_id_2` (`booking_id`),
  KEY `booking_id_3` (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_servicelog`
--

INSERT INTO `iw_tns_servicelog` (`id`, `booking_id`, `service_id`, `branch_id`, `girls_id`, `type`, `up_down`, `created_date`) VALUES
(1, 1, '1', '3', '1', '1', '0', '2020-08-03 15:15'),
(2, 1, '3', '1', '1', '2', '2', '2020-08-03 15:15'),
(3, 1, '1', '2', '1', '1', '0', '2020-08-03 15:46'),
(4, 1, '2', '1', '1', '2', '2', '2020-08-03 15:47'),
(5, 2, '1', '1', '1', '1', '0', '2020-08-05 09:40'),
(6, 2, '1', '1', '1', '2', '2', '2020-08-05 09:42'),
(7, 2, '1', '1', '1', '2', '2', '2020-08-05 13:26'),
(8, 2, '4', '1', '1', '2', '1', '2020-08-05 13:53'),
(9, 2, '4', '1', '1', '2', '2', '2020-08-05 13:59'),
(10, 3, '1', '2', '1', '1', '0', '2020-08-05 14:39'),
(11, 4, '1', '4', '2', '1', '0', '2020-08-05 14:40'),
(12, 5, '1', '2', '3', '1', '0', '2020-08-05 14:40'),
(13, 6, '1', '2', '1', '1', '0', '2020-08-06 06:49'),
(14, 7, '1', '2', '2', '1', '0', '2020-08-06 06:50'),
(15, 8, '1', '2', '1', '1', '0', '2020-08-06 08:04'),
(16, 9, '1', '2', '1', '1', '0', '2020-08-06 12:07'),
(17, 10, '1', '1', '2', '1', '0', '2020-08-06 15:33'),
(18, 11, '1', '2', '1', '1', '0', '2020-08-07 06:44'),
(19, 12, '1', '2', '1', '1', '0', '2020-08-10 05:59'),
(20, 13, '1', '1', '1', '1', '0', '2020-08-11 06:06'),
(21, 14, '1', '2', '1', '1', '0', '2020-08-11 07:07'),
(22, 14, '4', '1', '1', '2', '1', '2020-08-11 08:34'),
(23, 15, '1', '2', '1', '1', '0', '2020-08-11 08:36'),
(24, 15, '1', '1', '1', '2', '2', '2020-08-11 09:11'),
(25, 15, '3', '1', '1', '2', '1', '2020-08-11 13:10'),
(26, 15, '3', '1', '1', '2', '2', '2020-08-11 13:40'),
(27, 15, '2', '1', '1', '2', '2', '2020-08-11 13:41'),
(28, 15, '2', '1', '1', '2', '2', '2020-08-11 13:46'),
(29, 15, '2', '1', '1', '2', '2', '2020-08-11 13:49'),
(30, 15, '2', '1', '1', '2', '2', '2020-08-11 13:49'),
(31, 16, '1', '2', '1', '1', '0', '2020-08-11 13:56'),
(32, 16, '2', '1', '1', '2', '2', '2020-08-11 13:57'),
(33, 17, '1', '2', '1', '1', '0', '2020-08-11 13:58'),
(34, 18, '1', '2', '1', '1', '0', '2020-08-12 06:28'),
(35, 19, '1', '4', '1', '1', '0', '2020-08-12 12:13'),
(36, 19, '3', '1', '1', '2', '2', '2020-08-12 12:13'),
(37, 19, '3', '1', '1', '2', '2', '2020-08-12 12:13'),
(38, 20, '1', '1', '1', '1', '0', '2020-08-13 06:17'),
(39, 21, '1', '2', '1', '1', '0', '2020-08-13 07:28'),
(40, 20, '1', '1', '1', '2', '1', '2020-08-13 08:37'),
(41, 21, '3', '1', '1', '1', '1', '2020-08-13 12:16'),
(42, 20, '3', '1', '1', '1', '1', '2020-08-13 12:21'),
(43, 20, '2', '1', '1', '1', '1', '2020-08-13 12:24'),
(44, 22, '1', '2', '1', '1', '0', '2020-08-13 12:37'),
(45, 23, '1', '2', '1', '1', '0', '2020-08-14 11:45'),
(46, 23, '2', '1', '1', '2', '2', '2020-08-14 11:46');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_service_extra`
--

DROP TABLE IF EXISTS `iw_tns_service_extra`;
CREATE TABLE IF NOT EXISTS `iw_tns_service_extra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` varchar(20) NOT NULL,
  `extra` varchar(20) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service_id` (`service_id`),
  KEY `extra` (`extra`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_service_extra`
--

INSERT INTO `iw_tns_service_extra` (`id`, `service_id`, `extra`, `created_date`) VALUES
(6, '1', '1', '2020-06-11 09:06:07'),
(5, '3', '2', '2020-04-30 07:04:54'),
(3, '4', '2', '2020-04-30 07:04:28'),
(7, '1', '2', '2020-06-11 09:06:07'),
(8, '1', '3', '2020-06-11 09:06:07');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_service_servicetype`
--

DROP TABLE IF EXISTS `iw_tns_service_servicetype`;
CREATE TABLE IF NOT EXISTS `iw_tns_service_servicetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` varchar(20) NOT NULL,
  `service` varchar(20) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service_id` (`service_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_service_servicetype`
--

INSERT INTO `iw_tns_service_servicetype` (`id`, `service_id`, `service`, `created_date`) VALUES
(7, '1', '1', '2020-06-11 09:06:07'),
(2, '2', '2', '2020-04-15 13:04:40'),
(6, '3', '2', '2020-04-30 07:04:54'),
(4, '4', '2', '2020-04-30 07:04:28');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_setting`
--

DROP TABLE IF EXISTS `iw_tns_setting`;
CREATE TABLE IF NOT EXISTS `iw_tns_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(20) NOT NULL,
  `module` varchar(250) NOT NULL,
  `userid` int(11) NOT NULL,
  `uadd` int(11) NOT NULL,
  `uedit` int(11) NOT NULL,
  `uview` int(11) NOT NULL,
  `udelete` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `userid_2` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=469 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_setting`
--

INSERT INTO `iw_tns_setting` (`id`, `role`, `module`, `userid`, `uadd`, `uedit`, `uview`, `udelete`) VALUES
(1, '1', '1', 1, 1, 1, 1, 1),
(2, '1', '2', 1, 1, 1, 1, 1),
(3, '1', '3', 1, 1, 1, 1, 1),
(4, '1', '4', 1, 1, 1, 1, 1),
(5, '1', '5', 1, 1, 1, 1, 1),
(6, '1', '6', 1, 1, 1, 1, 1),
(7, '1', '7', 1, 1, 1, 1, 1),
(8, '1', '8', 1, 1, 1, 1, 1),
(9, '1', '9', 1, 1, 1, 1, 1),
(10, '1', '10', 1, 1, 1, 1, 1),
(11, '1', '11', 1, 1, 1, 1, 1),
(12, '1', '12', 1, 1, 1, 1, 1),
(13, '1', '13', 1, 1, 1, 1, 1),
(14, '1', '14', 1, 1, 1, 1, 1),
(15, '1', '15', 1, 1, 1, 1, 1),
(16, '1', '16', 1, 1, 1, 1, 1),
(17, '1', '17', 1, 1, 1, 1, 1),
(18, '1', '18', 1, 1, 1, 1, 1),
(19, '1', '19', 1, 1, 1, 1, 1),
(20, '1', '20', 1, 1, 1, 1, 1),
(321, '1', '21', 1, 1, 1, 1, 1),
(385, '2', '1', 2, 1, 1, 1, 1),
(386, '2', '2', 2, 1, 1, 1, 1),
(387, '2', '3', 2, 1, 1, 1, 1),
(388, '2', '4', 2, 1, 1, 1, 1),
(389, '2', '5', 2, 1, 1, 1, 1),
(390, '2', '6', 2, 1, 1, 1, 1),
(391, '2', '7', 2, 1, 1, 1, 1),
(392, '2', '8', 2, 1, 1, 1, 1),
(393, '2', '9', 2, 1, 1, 1, 1),
(394, '2', '10', 2, 1, 1, 1, 1),
(395, '2', '11', 2, 1, 1, 1, 1),
(396, '2', '12', 2, 1, 1, 1, 1),
(397, '2', '13', 2, 1, 1, 1, 1),
(398, '2', '14', 2, 1, 1, 1, 1),
(399, '2', '15', 2, 1, 1, 1, 1),
(400, '2', '16', 2, 1, 1, 1, 1),
(401, '2', '17', 2, 1, 1, 1, 1),
(402, '2', '18', 2, 1, 1, 1, 1),
(403, '2', '19', 2, 1, 1, 1, 1),
(404, '2', '20', 2, 1, 1, 1, 1),
(405, '2', '21', 2, 1, 1, 1, 1),
(406, '2', '1', 3, 1, 1, 1, 1),
(407, '2', '2', 3, 1, 1, 1, 1),
(408, '2', '3', 3, 1, 1, 1, 1),
(409, '2', '4', 3, 1, 1, 1, 1),
(410, '2', '5', 3, 1, 1, 1, 1),
(411, '2', '6', 3, 1, 1, 1, 1),
(412, '2', '7', 3, 1, 1, 1, 1),
(413, '2', '8', 3, 1, 1, 1, 1),
(414, '2', '9', 3, 1, 1, 1, 1),
(415, '2', '10', 3, 1, 1, 1, 1),
(416, '2', '11', 3, 1, 1, 1, 1),
(417, '2', '12', 3, 1, 1, 1, 1),
(418, '2', '13', 3, 1, 1, 1, 1),
(419, '2', '14', 3, 1, 1, 1, 1),
(420, '2', '15', 3, 1, 1, 1, 1),
(421, '2', '16', 3, 1, 1, 1, 1),
(422, '2', '17', 3, 1, 1, 1, 1),
(423, '2', '18', 3, 1, 1, 1, 1),
(424, '2', '19', 3, 1, 1, 1, 1),
(425, '2', '20', 3, 1, 1, 1, 1),
(426, '2', '21', 3, 1, 1, 1, 1),
(427, '2', '1', 4, 1, 1, 1, 1),
(428, '2', '2', 4, 1, 1, 1, 1),
(429, '2', '3', 4, 1, 1, 1, 1),
(430, '2', '4', 4, 1, 1, 1, 1),
(431, '2', '5', 4, 1, 1, 1, 1),
(432, '2', '6', 4, 1, 1, 1, 1),
(433, '2', '7', 4, 1, 1, 1, 1),
(434, '2', '8', 4, 1, 1, 1, 1),
(435, '2', '9', 4, 1, 1, 1, 1),
(436, '2', '10', 4, 1, 1, 1, 1),
(437, '2', '11', 4, 1, 1, 1, 1),
(438, '2', '12', 4, 1, 1, 1, 1),
(439, '2', '13', 4, 1, 1, 1, 1),
(440, '2', '14', 4, 1, 1, 1, 1),
(441, '2', '15', 4, 1, 1, 1, 1),
(442, '2', '16', 4, 1, 1, 1, 1),
(443, '2', '17', 4, 1, 1, 1, 1),
(444, '2', '18', 4, 1, 1, 1, 1),
(445, '2', '19', 4, 1, 1, 1, 1),
(446, '2', '20', 4, 1, 1, 1, 1),
(447, '2', '21', 4, 1, 1, 1, 1),
(448, '2', '1', 5, 1, 1, 1, 1),
(449, '2', '2', 5, 1, 1, 1, 1),
(450, '2', '3', 5, 1, 1, 1, 1),
(451, '2', '4', 5, 1, 1, 1, 1),
(452, '2', '5', 5, 1, 1, 1, 1),
(453, '2', '6', 5, 1, 1, 1, 1),
(454, '2', '7', 5, 1, 1, 1, 1),
(455, '2', '8', 5, 1, 1, 1, 1),
(456, '2', '9', 5, 1, 1, 1, 1),
(457, '2', '10', 5, 1, 1, 1, 1),
(458, '2', '11', 5, 1, 1, 1, 1),
(459, '2', '12', 5, 1, 1, 1, 1),
(460, '2', '13', 5, 1, 1, 1, 1),
(461, '2', '14', 5, 1, 1, 1, 1),
(462, '2', '15', 5, 1, 1, 1, 1),
(463, '2', '16', 5, 1, 1, 1, 1),
(464, '2', '17', 5, 1, 1, 1, 1),
(465, '2', '18', 5, 1, 1, 1, 1),
(466, '2', '19', 5, 1, 1, 1, 1),
(467, '2', '20', 5, 1, 1, 1, 1),
(468, '2', '21', 5, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_shoes`
--

DROP TABLE IF EXISTS `iw_tns_shoes`;
CREATE TABLE IF NOT EXISTS `iw_tns_shoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shoes` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_shoes`
--

INSERT INTO `iw_tns_shoes` (`id`, `shoes`, `created_date`, `status`) VALUES
(1, '31', '2020-02-20 06:02:27', 1),
(2, '32', '2020-02-20 06:02:27', 1),
(3, '33', '2020-02-20 06:02:27', 1),
(4, '34', '2020-02-20 06:02:27', 1),
(5, '35', '2020-02-20 06:02:27', 1),
(6, '36', '2020-02-20 06:02:27', 1),
(7, '37', '2020-02-20 06:02:27', 1),
(8, '38', '2020-02-20 06:02:27', 1),
(9, '39', '2020-02-20 06:02:27', 1),
(10, '40', '2020-02-20 06:02:27', 1),
(11, '41', '2020-02-20 06:02:27', 1),
(12, '42', '2020-02-20 06:02:27', 1),
(13, '43', '2020-02-20 06:02:27', 1),
(14, '44', '2020-02-20 06:02:27', 1),
(15, '45', '2020-02-20 06:02:27', 1),
(16, '46', '2020-02-20 06:02:27', 1),
(17, '47', '2020-02-20 06:02:27', 1),
(18, '48', '2020-02-20 06:02:27', 1),
(19, '49', '2020-02-20 06:02:27', 1),
(20, '50', '2020-02-20 06:02:27', 1),
(21, '51', '2020-02-20 06:02:27', 1),
(22, '52', '2020-02-20 06:02:27', 1),
(23, '53', '2020-02-20 06:02:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_user`
--

DROP TABLE IF EXISTS `iw_tns_user`;
CREATE TABLE IF NOT EXISTS `iw_tns_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tabid` varchar(20) NOT NULL,
  `pin` varchar(20) NOT NULL,
  `role` varchar(20) NOT NULL,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(250) NOT NULL,
  `ptime` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `mdpassword` varchar(250) NOT NULL,
  `branch` varchar(100) NOT NULL,
  `function` varchar(100) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1->Active,0->inactive',
  `user_permission` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_user`
--

INSERT INTO `iw_tns_user` (`id`, `tabid`, `pin`, `role`, `first_name`, `last_name`, `mobile`, `email`, `ptime`, `password`, `mdpassword`, `branch`, `function`, `created_date`, `status`, `user_permission`) VALUES
(1, '', '', '1', 'Admin', 'iwox', '123456789', 'ibkrmm@gmail.com', '2020-07-22 12:02:58', '123456', 'e10adc3949ba59abbe56e057f20f883e', 'Zürich', 'CEO', '2020-01-07 13:01:58', 1, ''),
(2, '1245', '1452', '2', 'Beat', 'Baumann', '123456780', 'test@gmail.com', '', '1234', '81dc9bdb52d04dc20036dbd8313ed055', '1', 'Betreuer', '2020-01-31 07:01:36', 1, ''),
(3, '1425', '1414', '2', 'test1', 'test', '123456781', 'test@gmail.com', '', '123456', 'e10adc3949ba59abbe56e057f20f883e', '1', 'CEO', '2020-04-10 06:04:41', 1, ''),
(4, '1118', '5328', '2', 'test1', 'test', '123456700', 'test@gmail.com', '', '123456', 'e10adc3949ba59abbe56e057f20f883e', '1', 'CEO', '2020-04-10 07:04:22', 1, ''),
(5, '4276', '7709', '2', 'Balakrishnan', 'I', '09159976776', 't@gmail.com', '', '123456', 'e10adc3949ba59abbe56e057f20f883e', '1', 'CEO', '2020-06-03 12:06:07', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_vouchers`
--

DROP TABLE IF EXISTS `iw_tns_vouchers`;
CREATE TABLE IF NOT EXISTS `iw_tns_vouchers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(250) NOT NULL,
  `title` varchar(250) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `price` varchar(250) NOT NULL,
  `percentage` varchar(250) NOT NULL,
  `expiry` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `dateverify` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_vouchers`
--

INSERT INTO `iw_tns_vouchers` (`id`, `uid`, `title`, `mobile`, `price`, `percentage`, `expiry`, `type`, `dateverify`, `created_date`, `status`) VALUES
(1, 'gKAFOXjJ', 'service1fghfghg', '091599767760', '10', '', '2020-09-19', '1', '1600453800000', '2020-08-06 08:08:17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_workplanner`
--

DROP TABLE IF EXISTS `iw_tns_workplanner`;
CREATE TABLE IF NOT EXISTS `iw_tns_workplanner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` varchar(20) NOT NULL,
  `girls_id` varchar(20) NOT NULL,
  `workdate` varchar(20) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `branch_id` (`branch_id`),
  KEY `girls_id` (`girls_id`)
) ENGINE=MyISAM AUTO_INCREMENT=388 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_workplanner`
--

INSERT INTO `iw_tns_workplanner` (`id`, `branch_id`, `girls_id`, `workdate`, `created_date`, `status`) VALUES
(1, '1', '1', '2020-04-15', '2020-04-15 09:04:04', 1),
(2, '1', '2', '2020-04-15', '2020-04-15 09:04:04', 1),
(3, '1', '3', '2020-04-15', '2020-04-15 09:04:04', 1),
(5, '1', '4', '2020-04-15', '2020-04-15 09:04:04', 1),
(6, '1', '5', '2020-04-15', '2020-04-15 09:04:04', 1),
(7, '1', '6', '2020-04-15', '2020-04-15 09:04:04', 1),
(8, '1', '7', '2020-04-15', '2020-04-15 09:04:04', 1),
(9, '1', '1', '2020-04-23', '2020-04-23 08:04:44', 1),
(10, '1', '2', '2020-04-23', '2020-04-23 08:04:44', 1),
(11, '1', '3', '2020-04-23', '2020-04-23 08:04:44', 1),
(12, '1', '4', '2020-04-23', '2020-04-23 08:04:44', 1),
(13, '1', '5', '2020-04-23', '2020-04-23 08:04:44', 1),
(14, '1', '7', '2020-04-23', '2020-04-23 08:04:44', 1),
(15, '1', '1', '2020-04-24', '2020-04-24 05:04:14', 1),
(16, '1', '2', '2020-04-24', '2020-04-24 05:04:14', 1),
(17, '1', '3', '2020-04-24', '2020-04-24 05:04:14', 1),
(18, '1', '4', '2020-04-24', '2020-04-24 05:04:14', 1),
(19, '1', '5', '2020-04-24', '2020-04-24 05:04:14', 1),
(20, '1', '6', '2020-04-24', '2020-04-24 05:04:14', 1),
(21, '1', '7', '2020-04-24', '2020-04-24 05:04:14', 1),
(22, '1', '1', '2020-04-27', '2020-04-27 07:04:46', 1),
(23, '1', '2', '2020-04-27', '2020-04-27 07:04:46', 1),
(24, '1', '1', '2020-04-28', '2020-04-28 05:04:20', 1),
(25, '1', '2', '2020-04-28', '2020-04-28 05:04:20', 1),
(26, '1', '3', '2020-04-28', '2020-04-28 05:04:20', 1),
(27, '1', '4', '2020-04-28', '2020-04-28 05:04:20', 1),
(28, '1', '5', '2020-04-28', '2020-04-28 05:04:20', 1),
(29, '1', '6', '2020-04-28', '2020-04-28 05:04:20', 1),
(30, '1', '7', '2020-04-28', '2020-04-28 05:04:20', 1),
(31, '1', '1', '2020-04-29', '2020-04-28 05:04:20', 1),
(32, '1', '2', '2020-04-29', '2020-04-28 05:04:20', 1),
(33, '1', '3', '2020-04-29', '2020-04-28 05:04:20', 1),
(34, '1', '4', '2020-04-29', '2020-04-28 05:04:20', 1),
(35, '1', '5', '2020-04-29', '2020-04-28 05:04:20', 1),
(36, '1', '6', '2020-04-29', '2020-04-28 05:04:20', 1),
(37, '1', '7', '2020-04-29', '2020-04-28 05:04:20', 1),
(38, '1', '1', '2020-05-04', '2020-05-04 05:05:31', 1),
(39, '1', '2', '2020-05-04', '2020-05-04 05:05:31', 1),
(40, '1', '3', '2020-05-04', '2020-05-04 05:05:31', 1),
(41, '1', '4', '2020-05-04', '2020-05-04 05:05:31', 1),
(42, '1', '5', '2020-05-04', '2020-05-04 05:05:31', 1),
(43, '1', '6', '2020-05-04', '2020-05-04 05:05:31', 1),
(44, '1', '7', '2020-05-04', '2020-05-04 05:05:31', 1),
(45, '1', '6', '2020-05-05', '2020-05-04 07:05:07', 1),
(46, '1', '7', '2020-05-05', '2020-05-04 07:05:07', 1),
(47, '1', '1', '2020-05-06', '2020-05-04 12:05:52', 1),
(48, '1', '2', '2020-05-06', '2020-05-04 12:05:52', 1),
(49, '1', '3', '2020-05-06', '2020-05-04 12:05:52', 1),
(50, '1', '4', '2020-05-06', '2020-05-04 12:05:52', 1),
(51, '1', '5', '2020-05-06', '2020-05-04 12:05:52', 1),
(52, '1', '6', '2020-05-06', '2020-05-04 12:05:52', 1),
(53, '1', '7', '2020-05-06', '2020-05-04 12:05:52', 1),
(54, '1', '1', '2020-05-07', '2020-05-06 06:05:50', 1),
(55, '1', '2', '2020-05-07', '2020-05-06 06:05:50', 1),
(56, '1', '3', '2020-05-07', '2020-05-06 06:05:50', 1),
(57, '1', '4', '2020-05-07', '2020-05-06 06:05:50', 1),
(58, '1', '5', '2020-05-07', '2020-05-06 06:05:50', 1),
(59, '1', '6', '2020-05-07', '2020-05-06 06:05:50', 1),
(60, '1', '7', '2020-05-07', '2020-05-06 06:05:50', 1),
(61, '1', '1', '2020-05-11', '2020-05-15 06:05:49', 1),
(62, '1', '2', '2020-05-11', '2020-05-15 06:05:49', 1),
(63, '1', '6', '2020-05-03', '2020-05-15 06:05:49', 1),
(64, '1', '1', '2020-05-03', '2020-05-15 06:05:49', 1),
(65, '1', '2', '2020-05-03', '2020-05-15 06:05:49', 1),
(66, '1', '3', '2020-05-03', '2020-05-15 06:05:49', 1),
(67, '1', '4', '2020-05-03', '2020-05-15 06:05:49', 1),
(68, '1', '5', '2020-05-03', '2020-05-15 06:05:49', 1),
(69, '1', '6', '2020-05-03', '2020-05-15 06:05:49', 1),
(70, '1', '7', '2020-05-03', '2020-05-15 06:05:49', 1),
(71, '1', '1', '2020-05-03', '2020-05-15 06:05:49', 1),
(72, '1', '7', '2020-05-03', '2020-05-15 06:05:49', 1),
(73, '1', '1', '2020-05-13', '2020-05-15 06:05:49', 1),
(74, '1', '4', '2020-05-13', '2020-05-15 06:05:49', 1),
(75, '1', '5', '2020-05-13', '2020-05-15 06:05:49', 1),
(76, '1', '6', '2020-05-13', '2020-05-15 06:05:49', 1),
(77, '1', '1', '2020-05-19', '2020-05-19 07:05:30', 1),
(78, '1', '2', '2020-05-19', '2020-05-19 07:05:30', 1),
(79, '1', '3', '2020-05-19', '2020-05-19 07:05:30', 1),
(80, '1', '4', '2020-05-19', '2020-05-19 07:05:30', 1),
(81, '1', '5', '2020-05-19', '2020-05-19 07:05:30', 1),
(82, '1', '6', '2020-05-19', '2020-05-19 07:05:30', 1),
(83, '1', '7', '2020-05-19', '2020-05-19 07:05:30', 1),
(84, '1', '1', '2020-05-29', '2020-05-29 13:05:02', 1),
(85, '1', '2', '2020-05-29', '2020-05-29 13:05:02', 1),
(86, '1', '3', '2020-05-29', '2020-05-29 13:05:02', 1),
(87, '1', '4', '2020-05-29', '2020-05-29 13:05:02', 1),
(88, '1', '5', '2020-05-29', '2020-05-29 13:05:02', 1),
(89, '1', '1', '2020-06-01', '2020-06-01 06:06:45', 1),
(90, '1', '2', '2020-06-01', '2020-06-01 06:06:45', 1),
(91, '1', '3', '2020-06-01', '2020-06-01 06:06:45', 1),
(92, '1', '4', '2020-06-01', '2020-06-01 06:06:45', 1),
(93, '1', '5', '2020-06-01', '2020-06-01 06:06:45', 1),
(94, '1', '6', '2020-06-01', '2020-06-01 06:06:45', 1),
(95, '1', '7', '2020-06-01', '2020-06-01 06:06:45', 1),
(96, '1', '1', '2020-06-02', '2020-06-02 05:06:31', 1),
(97, '1', '2', '2020-06-02', '2020-06-02 05:06:31', 1),
(98, '1', '3', '2020-06-02', '2020-06-02 05:06:31', 1),
(99, '1', '4', '2020-06-02', '2020-06-02 05:06:31', 1),
(100, '1', '5', '2020-06-02', '2020-06-02 05:06:31', 1),
(101, '1', '6', '2020-06-02', '2020-06-02 05:06:31', 1),
(102, '1', '7', '2020-06-02', '2020-06-02 05:06:31', 1),
(103, '1', '1', '2020-06-03', '2020-06-03 06:06:48', 1),
(104, '1', '2', '2020-06-03', '2020-06-03 06:06:48', 1),
(105, '2', '3', '2020-06-03', '2020-06-03 06:06:54', 1),
(106, '2', '4', '2020-06-03', '2020-06-03 06:06:54', 1),
(107, '2', '5', '2020-06-03', '2020-06-03 06:06:54', 1),
(108, '1', '1', '2020-06-04', '2020-06-04 08:06:19', 1),
(109, '1', '2', '2020-06-04', '2020-06-04 08:06:19', 1),
(110, '1', '3', '2020-06-04', '2020-06-04 08:06:19', 1),
(111, '1', '4', '2020-06-04', '2020-06-04 08:06:19', 1),
(112, '1', '5', '2020-06-04', '2020-06-04 08:06:19', 1),
(113, '1', '4', '2020-06-06', '2020-06-04 08:06:19', 1),
(114, '1', '5', '2020-06-06', '2020-06-04 08:06:19', 1),
(115, '1', '6', '2020-06-06', '2020-06-04 08:06:19', 1),
(116, '1', '7', '2020-06-06', '2020-06-04 08:06:19', 1),
(117, '1', '8', '2020-06-06', '2020-06-04 08:06:19', 1),
(118, '2', '5', '2020-06-05', '2020-06-04 08:06:19', 1),
(119, '2', '6', '2020-06-05', '2020-06-04 08:06:19', 1),
(120, '2', '7', '2020-06-05', '2020-06-04 08:06:19', 1),
(121, '2', '8', '2020-06-05', '2020-06-04 08:06:19', 1),
(122, '3', '6', '2020-06-01', '2020-06-04 08:06:19', 1),
(123, '3', '7', '2020-06-01', '2020-06-04 08:06:19', 1),
(124, '3', '8', '2020-06-01', '2020-06-04 08:06:19', 1),
(125, '3', '9', '2020-06-01', '2020-06-04 08:06:19', 1),
(126, '3', '2', '2020-06-04', '2020-06-04 08:06:19', 1),
(127, '3', '5', '2020-06-04', '2020-06-04 08:06:19', 1),
(128, '3', '7', '2020-06-07', '2020-06-04 08:06:19', 1),
(129, '3', '8', '2020-06-07', '2020-06-04 08:06:19', 1),
(130, '3', '9', '2020-06-07', '2020-06-04 08:06:19', 1),
(131, '3', '10', '2020-06-07', '2020-06-04 08:06:19', 1),
(132, '4', '8', '2020-06-02', '2020-06-04 08:06:19', 1),
(133, '5', '1', '2020-06-04', '2020-06-04 08:06:19', 1),
(134, '5', '7', '2020-06-04', '2020-06-04 08:06:19', 1),
(135, '5', '5', '2020-06-06', '2020-06-04 08:06:19', 1),
(136, '5', '6', '2020-06-06', '2020-06-04 08:06:19', 1),
(137, '4', '6', '2020-06-04', '2020-06-04 08:06:19', 1),
(138, '5', '1', '2020-06-01', '2020-06-04 08:06:19', 1),
(139, '5', '2', '2020-06-01', '2020-06-04 08:06:19', 1),
(140, '5', '3', '2020-06-01', '2020-06-04 08:06:19', 1),
(141, '5', '4', '2020-06-01', '2020-06-04 08:06:19', 1),
(142, '5', '5', '2020-06-01', '2020-06-04 08:06:19', 1),
(143, '5', '6', '2020-06-01', '2020-06-04 08:06:19', 1),
(144, '5', '7', '2020-06-01', '2020-06-04 08:06:19', 1),
(145, '5', '8', '2020-06-01', '2020-06-04 08:06:19', 1),
(146, '5', '9', '2020-06-01', '2020-06-04 08:06:19', 1),
(147, '5', '10', '2020-06-01', '2020-06-04 08:06:19', 1),
(148, '5', '11', '2020-06-01', '2020-06-04 08:06:19', 1),
(149, '1', '1', '2020-06-04', '2020-06-04 11:06:34', 1),
(150, '1', '2', '2020-06-04', '2020-06-04 11:06:34', 1),
(151, '1', '3', '2020-06-04', '2020-06-04 11:06:34', 1),
(152, '1', '4', '2020-06-04', '2020-06-04 11:06:34', 1),
(153, '1', '5', '2020-06-04', '2020-06-04 11:06:34', 1),
(154, '12', '1', '2020-06-04', '2020-06-04 11:06:34', 1),
(155, '12', '7', '2020-06-04', '2020-06-04 11:06:34', 1),
(156, '12', '8', '2020-06-04', '2020-06-04 12:06:03', 1),
(157, '12', '9', '2020-06-04', '2020-06-04 12:06:03', 1),
(158, '12', '10', '2020-06-04', '2020-06-04 12:06:03', 1),
(171, '1', '1', '2020-06-08', '2020-06-08 16:06:15', 1),
(172, '1', '2', '2020-06-08', '2020-06-08 16:06:15', 1),
(173, '1', '3', '2020-06-08', '2020-06-08 16:06:15', 1),
(196, '11', '2', '2020-06-12', '2020-06-09 08:06:31', 1),
(195, '11', '1', '2020-06-12', '2020-06-09 08:06:31', 1),
(185, '3', '1', '2020-06-10', '2020-06-09 06:06:11', 1),
(194, '1', '5', '2020-06-09', '2020-06-09 06:06:48', 1),
(193, '1', '4', '2020-06-09', '2020-06-09 06:06:48', 1),
(192, '1', '3', '2020-06-09', '2020-06-09 06:06:48', 1),
(191, '1', '2', '2020-06-09', '2020-06-09 06:06:48', 1),
(197, '11', '3', '2020-06-12', '2020-06-09 08:06:31', 1),
(198, '11', '4', '2020-06-12', '2020-06-09 08:06:31', 1),
(199, '11', '5', '2020-06-12', '2020-06-09 08:06:31', 1),
(200, '6', '6', '2020-06-12', '2020-06-09 08:06:31', 1),
(201, '6', '7', '2020-06-12', '2020-06-09 08:06:31', 1),
(202, '6', '8', '2020-06-12', '2020-06-09 08:06:31', 1),
(203, '6', '9', '2020-06-12', '2020-06-09 08:06:31', 1),
(204, '6', '10', '2020-06-12', '2020-06-09 08:06:31', 1),
(253, '1', '2', '2020-06-24', '2020-06-24 05:06:21', 1),
(252, '1', '1', '2020-06-24', '2020-06-24 05:06:21', 1),
(207, '1', '7', '2020-07-27', '2020-06-09 13:06:49', 1),
(208, '2', '1', '2020-08-01', '2020-06-10 05:06:57', 1),
(214, '1', '3', '2020-06-11', '2020-06-11 09:06:07', 1),
(213, '1', '2', '2020-06-11', '2020-06-11 09:06:07', 1),
(212, '1', '1', '2020-06-11', '2020-06-11 09:06:07', 1),
(215, '1', '4', '2020-06-11', '2020-06-11 09:06:07', 1),
(216, '1', '5', '2020-06-11', '2020-06-11 09:06:07', 1),
(217, '1', '6', '2020-06-11', '2020-06-11 09:06:07', 1),
(218, '1', '7', '2020-06-11', '2020-06-11 09:06:07', 1),
(235, '1', '5', '2020-06-16', '2020-06-16 06:06:30', 1),
(234, '1', '4', '2020-06-16', '2020-06-16 06:06:30', 1),
(233, '1', '3', '2020-06-16', '2020-06-16 06:06:30', 1),
(232, '1', '2', '2020-06-16', '2020-06-16 06:06:30', 1),
(231, '1', '1', '2020-06-16', '2020-06-16 06:06:30', 1),
(237, '1', '1', '2020-06-17', '2020-06-17 06:06:41', 1),
(238, '1', '2', '2020-06-17', '2020-06-17 06:06:41', 1),
(239, '1', '3', '2020-06-17', '2020-06-17 06:06:41', 1),
(240, '1', '4', '2020-06-17', '2020-06-17 06:06:41', 1),
(241, '1', '5', '2020-06-17', '2020-06-17 06:06:41', 1),
(242, '1', '1', '2020-06-19', '2020-06-19 13:06:58', 1),
(243, '1', '2', '2020-06-19', '2020-06-19 13:06:58', 1),
(244, '1', '3', '2020-06-19', '2020-06-19 13:06:58', 1),
(245, '1', '4', '2020-06-19', '2020-06-19 13:06:58', 1),
(246, '1', '5', '2020-06-19', '2020-06-19 13:06:58', 1),
(247, '1', '1', '2020-06-23', '2020-06-23 09:06:25', 1),
(248, '1', '2', '2020-06-23', '2020-06-23 09:06:25', 1),
(249, '1', '3', '2020-06-23', '2020-06-23 09:06:25', 1),
(250, '1', '4', '2020-06-23', '2020-06-23 09:06:25', 1),
(251, '1', '5', '2020-06-23', '2020-06-23 09:06:25', 1),
(254, '1', '3', '2020-06-24', '2020-06-24 05:06:21', 1),
(255, '1', '4', '2020-06-24', '2020-06-24 05:06:21', 1),
(256, '1', '5', '2020-06-24', '2020-06-24 05:06:21', 1),
(257, '1', '6', '2020-06-24', '2020-06-24 05:06:21', 1),
(258, '1', '7', '2020-06-24', '2020-06-24 05:06:21', 1),
(259, '1', '1', '2020-06-26', '2020-06-26 13:06:10', 1),
(260, '1', '2', '2020-06-26', '2020-06-26 13:06:10', 1),
(261, '1', '3', '2020-06-26', '2020-06-26 13:06:10', 1),
(262, '1', '4', '2020-06-26', '2020-06-26 13:06:10', 1),
(263, '1', '5', '2020-06-26', '2020-06-26 13:06:10', 1),
(264, '1', '1', '2020-07-01', '2020-07-01 06:07:34', 1),
(265, '1', '2', '2020-07-01', '2020-07-01 06:07:34', 1),
(266, '1', '3', '2020-07-01', '2020-07-01 06:07:34', 1),
(267, '1', '4', '2020-07-01', '2020-07-01 06:07:34', 1),
(268, '1', '5', '2020-07-01', '2020-07-01 06:07:34', 1),
(269, '2', '7', '2020-07-01', '2020-07-01 06:07:34', 1),
(270, '2', '65', '2020-07-01', '2020-07-01 06:07:34', 1),
(271, '1', '1', '2020-07-02', '2020-07-02 08:07:04', 1),
(272, '1', '2', '2020-07-02', '2020-07-02 08:07:04', 1),
(273, '1', '3', '2020-07-02', '2020-07-02 08:07:04', 1),
(274, '1', '4', '2020-07-02', '2020-07-02 08:07:04', 1),
(275, '1', '5', '2020-07-02', '2020-07-02 08:07:04', 1),
(285, '1', '5', '2020-07-03', '2020-07-03 06:07:04', 1),
(284, '1', '4', '2020-07-03', '2020-07-03 06:07:04', 1),
(283, '1', '3', '2020-07-03', '2020-07-03 06:07:04', 1),
(282, '1', '2', '2020-07-03', '2020-07-03 06:07:04', 1),
(281, '1', '1', '2020-07-03', '2020-07-03 06:07:04', 1),
(286, '1', '1', '2020-07-08', '2020-07-08 05:07:46', 1),
(287, '1', '2', '2020-07-08', '2020-07-08 05:07:46', 1),
(288, '1', '3', '2020-07-08', '2020-07-08 05:07:46', 1),
(289, '1', '4', '2020-07-08', '2020-07-08 05:07:46', 1),
(290, '1', '5', '2020-07-08', '2020-07-08 05:07:46', 1),
(291, '1', '6', '2020-07-08', '2020-07-08 05:07:46', 1),
(292, '1', '7', '2020-07-08', '2020-07-08 05:07:46', 1),
(293, '1', '1', '2020-07-09', '2020-07-09 05:07:22', 1),
(294, '1', '2', '2020-07-09', '2020-07-09 05:07:22', 1),
(295, '1', '3', '2020-07-09', '2020-07-09 05:07:22', 1),
(296, '1', '4', '2020-07-09', '2020-07-09 05:07:22', 1),
(297, '1', '5', '2020-07-09', '2020-07-09 05:07:22', 1),
(298, '1', '6', '2020-07-09', '2020-07-09 05:07:22', 1),
(299, '1', '7', '2020-07-09', '2020-07-09 05:07:22', 1),
(300, '1', '1', '2020-07-10', '2020-07-10 05:07:38', 1),
(301, '1', '2', '2020-07-10', '2020-07-10 05:07:38', 1),
(302, '1', '3', '2020-07-10', '2020-07-10 05:07:38', 1),
(303, '1', '4', '2020-07-10', '2020-07-10 05:07:38', 1),
(304, '1', '5', '2020-07-10', '2020-07-10 05:07:38', 1),
(305, '1', '6', '2020-07-10', '2020-07-10 05:07:38', 1),
(306, '1', '7', '2020-07-10', '2020-07-10 05:07:38', 1),
(307, '1', '1', '2020-07-13', '2020-07-13 05:07:03', 1),
(308, '1', '2', '2020-07-13', '2020-07-13 05:07:03', 1),
(309, '1', '3', '2020-07-13', '2020-07-13 05:07:03', 1),
(310, '1', '4', '2020-07-13', '2020-07-13 05:07:03', 1),
(311, '1', '1', '2020-07-15', '2020-07-15 05:07:27', 1),
(312, '1', '2', '2020-07-15', '2020-07-15 05:07:27', 1),
(313, '1', '3', '2020-07-15', '2020-07-15 05:07:27', 1),
(314, '1', '4', '2020-07-15', '2020-07-15 05:07:27', 1),
(315, '1', '1', '2020-07-16', '2020-07-16 05:07:02', 1),
(316, '1', '2', '2020-07-16', '2020-07-16 05:07:02', 1),
(317, '1', '3', '2020-07-16', '2020-07-16 05:07:02', 1),
(318, '1', '4', '2020-07-16', '2020-07-16 05:07:02', 1),
(319, '1', '5', '2020-07-16', '2020-07-16 05:07:02', 1),
(320, '1', '1', '2020-07-23', '2020-07-23 06:07:17', 1),
(321, '1', '2', '2020-07-23', '2020-07-23 06:07:17', 1),
(322, '1', '3', '2020-07-23', '2020-07-23 06:07:17', 1),
(323, '1', '4', '2020-07-23', '2020-07-23 06:07:17', 1),
(324, '1', '1', '2020-07-24', '2020-07-24 06:07:25', 1),
(325, '1', '2', '2020-07-24', '2020-07-24 06:07:25', 1),
(326, '1', '3', '2020-07-24', '2020-07-24 06:07:25', 1),
(327, '1', '4', '2020-07-24', '2020-07-24 06:07:25', 1),
(345, '1', '4', '2020-07-30', '2020-07-30 05:07:38', 1),
(344, '1', '3', '2020-07-30', '2020-07-30 05:07:38', 1),
(343, '1', '2', '2020-07-30', '2020-07-30 05:07:38', 1),
(342, '1', '1', '2020-07-30', '2020-07-30 05:07:38', 1),
(341, '1', '5', '2020-07-29', '2020-07-29 05:07:27', 1),
(340, '1', '4', '2020-07-29', '2020-07-29 05:07:27', 1),
(339, '1', '3', '2020-07-29', '2020-07-29 05:07:27', 1),
(338, '1', '2', '2020-07-29', '2020-07-29 05:07:27', 1),
(337, '1', '1', '2020-07-29', '2020-07-29 05:07:27', 1),
(346, '1', '1', '2020-07-31', '2020-07-31 05:07:26', 1),
(347, '1', '2', '2020-07-31', '2020-07-31 05:07:26', 1),
(348, '1', '3', '2020-07-31', '2020-07-31 05:07:26', 1),
(349, '1', '4', '2020-07-31', '2020-07-31 05:07:26', 1),
(350, '1', '5', '2020-07-31', '2020-07-31 05:07:26', 1),
(351, '1', '1', '2020-08-03', '2020-08-03 05:08:43', 1),
(352, '1', '2', '2020-08-03', '2020-08-03 05:08:43', 1),
(353, '1', '3', '2020-08-03', '2020-08-03 05:08:43', 1),
(354, '1', '4', '2020-08-03', '2020-08-03 05:08:43', 1),
(355, '1', '5', '2020-08-03', '2020-08-03 05:08:43', 1),
(356, '1', '1', '2020-08-05', '2020-08-05 09:08:16', 1),
(357, '1', '2', '2020-08-05', '2020-08-05 09:08:16', 1),
(358, '1', '3', '2020-08-05', '2020-08-05 09:08:16', 1),
(359, '1', '4', '2020-08-05', '2020-08-05 09:08:16', 1),
(360, '1', '1', '2020-08-06', '2020-08-06 06:08:38', 1),
(361, '1', '2', '2020-08-06', '2020-08-06 06:08:38', 1),
(362, '1', '3', '2020-08-06', '2020-08-06 06:08:38', 1),
(363, '1', '4', '2020-08-06', '2020-08-06 06:08:38', 1),
(364, '1', '1', '2020-08-07', '2020-08-07 06:08:35', 1),
(365, '1', '2', '2020-08-07', '2020-08-07 06:08:35', 1),
(366, '1', '3', '2020-08-07', '2020-08-07 06:08:35', 1),
(367, '1', '4', '2020-08-07', '2020-08-07 06:08:35', 1),
(368, '1', '1', '2020-08-10', '2020-08-10 05:08:44', 1),
(369, '1', '2', '2020-08-10', '2020-08-10 05:08:44', 1),
(370, '1', '3', '2020-08-10', '2020-08-10 05:08:44', 1),
(371, '1', '4', '2020-08-10', '2020-08-10 05:08:44', 1),
(372, '1', '1', '2020-08-11', '2020-08-11 06:08:12', 1),
(373, '1', '2', '2020-08-11', '2020-08-11 06:08:12', 1),
(374, '1', '3', '2020-08-11', '2020-08-11 06:08:12', 1),
(375, '1', '4', '2020-08-11', '2020-08-11 06:08:12', 1),
(376, '1', '1', '2020-08-12', '2020-08-12 05:08:11', 1),
(377, '1', '2', '2020-08-12', '2020-08-12 05:08:11', 1),
(378, '1', '3', '2020-08-12', '2020-08-12 05:08:11', 1),
(379, '1', '4', '2020-08-12', '2020-08-12 05:08:11', 1),
(380, '1', '1', '2020-08-13', '2020-08-12 14:08:32', 1),
(381, '1', '2', '2020-08-13', '2020-08-12 14:08:32', 1),
(382, '1', '3', '2020-08-13', '2020-08-12 14:08:32', 1),
(383, '1', '4', '2020-08-13', '2020-08-12 14:08:32', 1),
(384, '1', '1', '2020-08-14', '2020-08-14 06:08:09', 1),
(385, '1', '2', '2020-08-14', '2020-08-14 06:08:09', 1),
(386, '1', '3', '2020-08-14', '2020-08-14 06:08:09', 1),
(387, '1', '4', '2020-08-14', '2020-08-14 06:08:09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_workplannergirls`
--

DROP TABLE IF EXISTS `iw_tns_workplannergirls`;
CREATE TABLE IF NOT EXISTS `iw_tns_workplannergirls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` varchar(20) NOT NULL,
  `cdate` varchar(20) NOT NULL,
  `lead` varchar(250) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `branch_id` (`branch_id`),
  KEY `lead` (`lead`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_workplannergirls`
--

INSERT INTO `iw_tns_workplannergirls` (`id`, `branch_id`, `cdate`, `lead`, `created_date`, `status`) VALUES
(1, '1', '2020-04-12', '2', '2020-04-10 11:04:01', 1),
(2, '1', '2020-04-10', '1', '2020-04-10 11:04:01', 1),
(3, '1', '2020-04-13', '1', '2020-04-13 06:04:56', 1),
(4, '1', '2020-04-14', '2', '2020-04-13 06:04:56', 1),
(5, '1', '2020-04-13', '4', '2020-04-13 06:04:56', 1),
(6, '1', '2020-04-13', '5', '2020-04-13 06:04:56', 1),
(7, '1', '2020-04-28', '3', '2020-04-28 05:04:20', 1),
(8, '1', '2020-04-28', '1', '2020-04-28 05:04:20', 1),
(9, '1', '2020-05-04', '2', '2020-05-04 13:05:37', 1),
(15, '1', '2020-06-03', '2', '2020-06-03 07:06:34', 1),
(14, '1', '2020-05-07', '4', '2020-05-06 06:05:50', 1),
(13, '1', '2020-05-06', '2', '2020-05-06 06:05:50', 1),
(16, '1', '2020-06-03', '2', '2020-06-03 07:06:10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iw_tns_workplanner_time`
--

DROP TABLE IF EXISTS `iw_tns_workplanner_time`;
CREATE TABLE IF NOT EXISTS `iw_tns_workplanner_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wg_id` varchar(20) NOT NULL,
  `starttime` varchar(20) NOT NULL,
  `endtime` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wg_id` (`wg_id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iw_tns_workplanner_time`
--

INSERT INTO `iw_tns_workplanner_time` (`id`, `wg_id`, `starttime`, `endtime`) VALUES
(1, '1', '00:15', '00:15'),
(2, '2', '00:15', '00:15'),
(3, '3', '00:15', '00:15'),
(7, '4', '03:15', '04:45'),
(6, '4', '00:30', '00:30'),
(8, '5', '00:15', '00:15'),
(9, '6', '00:15', '00:15'),
(13, '7', '01:45', '03:00'),
(12, '7', '00:15', '00:30'),
(14, '8', '00:15', '00:45'),
(15, '8', '01:00', '01:30'),
(16, '9', '00:15', '00:15'),
(17, '10', '00:15', '00:15'),
(18, '11', '00:15', '00:15'),
(19, '12', '00:15', '00:15'),
(20, '13', '00:15', '00:15'),
(21, '13', '04:15', '04:15'),
(23, '14', '00:15', '00:15'),
(25, '15', '00:15', '00:15'),
(26, '15', '04:00', '04:30'),
(27, '16', '00:15', '00:15'),
(28, '16', '00:15', '00:15'),
(29, '16', '00:15', '00:15'),
(30, '16', '00:15', '00:15'),
(31, '16', '00:15', '00:15');

-- --------------------------------------------------------

--
-- Stand-in structure for view `iw_transaction`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `iw_transaction`;
CREATE TABLE IF NOT EXISTS `iw_transaction` (
`id` int(11)
,`oid` varchar(20)
,`created_id` varchar(20)
,`created_type` varchar(20)
,`customer` varchar(20)
,`customer_type` varchar(20)
,`branch_id` varchar(20)
,`girls_id` varchar(20)
,`girlsname` varchar(250)
,`gimage` varchar(250)
,`service_id` varchar(20)
,`servicename` varchar(250)
,`roomname` varchar(250)
,`room_id` int(11)
,`created_date` varchar(20)
,`starttime` varchar(20)
,`endtime` varchar(20)
,`amount` varchar(20)
,`paymentstatus` int(11)
,`paymenttype` varchar(250)
,`status` int(11)
,`servicetime` varchar(20)
,`booking_date` varchar(20)
,`startdate` varchar(20)
,`enddate` varchar(20)
,`voucherid` varchar(250)
,`voucherprice` int(11)
,`vouchertype` varchar(20)
,`totalamount` varchar(20)
,`girlsamount` int(11)
,`branchamount` int(11)
,`serviceamount` int(11)
,`extraamount` int(11)
,`pointprice` int(11)
,`rubbleprice` int(11)
,`eservice_id` varchar(20)
,`eservicetime` varchar(20)
,`eserviceamount` int(11)
,`egirlsamount` int(11)
,`ebranchamount` int(11)
,`reason` text
,`eservicename` varchar(250)
);

-- --------------------------------------------------------

--
-- Table structure for table `testtable`
--

DROP TABLE IF EXISTS `testtable`;
CREATE TABLE IF NOT EXISTS `testtable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testtable`
--

INSERT INTO `testtable` (`id`, `time`) VALUES
(1, '2020-04-29 08:30:23');

-- --------------------------------------------------------

--
-- Structure for view `demo`
--
DROP TABLE IF EXISTS `demo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `demo`  AS  select `b`.`id` AS `id`,`b`.`starttime` AS `starttime`,`b`.`service_id` AS `service_id`,(select `g`.`id` from `iw_tns_girls` `g` where (`g`.`id` = `b`.`girls_id`)) AS `gid` from `iw_tns_booking` `b` ;

-- --------------------------------------------------------

--
-- Structure for view `iw_amountlist`
--
DROP TABLE IF EXISTS `iw_amountlist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `iw_amountlist`  AS  select (select (sum((case when (((`p4`.`paymenttype` = 'Wir1') or (`p4`.`paymenttype` = 'Wir2')) and (`p4`.`type` = '1')) then `p4`.`price` else 0 end)) - sum((case when (((`p4`.`paymenttype` = 'Wir1') or (`p4`.`paymenttype` = 'Wir2')) and (`p4`.`type` = '2')) then `p4`.`price` else 0 end))) from `iw_tns_paymentlog` `p4` where ((`p4`.`girls_id` = `w`.`girls_id`) and (`p4`.`booking_date` = curdate()) and (`p4`.`status` = 0))) AS `wiramount`,(select (sum((case when ((`p3`.`paymenttype` = 'Reka') and (`p3`.`type` = '1')) then `p3`.`price` else 0 end)) - sum((case when ((`p3`.`paymenttype` = 'Reka') and (`p3`.`type` = '2')) then `p3`.`price` else 0 end))) from `iw_tns_paymentlog` `p3` where ((`p3`.`girls_id` = `w`.`girls_id`) and (`p3`.`booking_date` = curdate()) and (`p3`.`status` = 0))) AS `reakamount`,(select (sum((case when (((`p2`.`paymenttype` = 'POST') or (`p2`.`paymenttype` = 'Visa') or (`p2`.`paymenttype` = 'Mastercard') or (`p2`.`paymenttype` = 'American') or (`p2`.`paymenttype` = 'DinersClub') or (`p2`.`paymenttype` = 'Twint')) and (`p2`.`type` = '1')) then `p2`.`price` else 0 end)) - sum((case when (((`p2`.`paymenttype` = 'POST') or (`p2`.`paymenttype` = 'Visa') or (`p2`.`paymenttype` = 'Mastercard') or (`p2`.`paymenttype` = 'American') or (`p2`.`paymenttype` = 'DinersClub') or (`p2`.`paymenttype` = 'Twint')) and (`p2`.`type` = '2')) then `p2`.`price` else 0 end))) from `iw_tns_paymentlog` `p2` where ((`p2`.`girls_id` = `w`.`girls_id`) and (`p2`.`booking_date` = curdate()) and (`p2`.`status` = 0))) AS `kkamount`,(select (sum((case when ((`p1`.`paymenttype` = 'EuroCash') and (`p1`.`type` = '1')) then `p1`.`price` else 0 end)) - sum((case when ((`p1`.`paymenttype` = 'EuroCash') and (`p1`.`type` = '2')) then `p1`.`price` else 0 end))) from `iw_tns_paymentlog` `p1` where ((`p1`.`girls_id` = `w`.`girls_id`) and (`p1`.`booking_date` = curdate()) and (`p1`.`status` = 0))) AS `euroamount`,(select `g`.`profilename` from `iw_tns_girls` `g` where (`g`.`id` = `w`.`girls_id`)) AS `girlsname`,`w`.`girls_id` AS `girls_id`,`w`.`workdate` AS `workdate`,coalesce(sum(`t`.`extraamount`),0) AS `extraamount`,coalesce(((sum(`t`.`voucherprice`) + sum(`t`.`pointprice`)) + sum(`t`.`rubbleprice`)),0) AS `iwoxprice`,(select count(`tt`.`service_id`) from `iw_transaction` `tt` where ((`tt`.`booking_date` = curdate()) and (convert(`w`.`girls_id` using utf8mb4) = convert(`tt`.`girls_id` using utf8mb4)))) AS `servicecount`,coalesce(sum(`t`.`serviceamount`),0) AS `totalamount`,coalesce((sum(`t`.`totalamount`) / 2),0) AS `girlsrealamount`,coalesce(sum(`t`.`girlsamount`),0) AS `girlsamount`,coalesce(sum(`t`.`branchamount`),0) AS `branchamount` from (`iw_tns_workplanner` `w` left join `iw_transaction` `t` on(((convert(`w`.`girls_id` using utf8mb4) = convert(`t`.`girls_id` using utf8mb4)) and (`t`.`booking_date` = curdate())))) where (`w`.`workdate` = curdate()) group by `w`.`girls_id` ;

-- --------------------------------------------------------

--
-- Structure for view `iw_extrabooking`
--
DROP TABLE IF EXISTS `iw_extrabooking`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `iw_extrabooking`  AS  select `be`.`extra_id` AS `extra_id`,`be`.`booking_id` AS `booking_id`,(select `e`.`extra` from `iw_mas_extratype` `e` where (`e`.`id` = `be`.`extra_id`)) AS `extra` from (`iw_tns_booking` `b` join `iw_tns_booking_extra` `be` on((`b`.`id` = `be`.`booking_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `iw_transaction`
--
DROP TABLE IF EXISTS `iw_transaction`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `iw_transaction`  AS  select `b`.`id` AS `id`,`b`.`oid` AS `oid`,`b`.`created_id` AS `created_id`,`b`.`created_type` AS `created_type`,`b`.`customer` AS `customer`,`b`.`customer_type` AS `customer_type`,`b`.`branch_id` AS `branch_id`,`b`.`girls_id` AS `girls_id`,`g`.`profilename` AS `girlsname`,(select `gv`.`image` from `iw_tns_girls_gallery` `gv` where (`gv`.`girls_id` = `b`.`girls_id`) order by `gv`.`id` desc limit 1) AS `gimage`,`b`.`service_id` AS `service_id`,(select `s`.`title` from `iw_tns_service` `s` where (`s`.`id` = `b`.`service_id`)) AS `servicename`,`r`.`titel` AS `roomname`,`r`.`id` AS `room_id`,`b`.`created_date` AS `created_date`,`b`.`starttime` AS `starttime`,`b`.`endtime` AS `endtime`,`b`.`amount` AS `amount`,`b`.`paymentstatus` AS `paymentstatus`,`b`.`paymenttype` AS `paymenttype`,`b`.`status` AS `status`,`b`.`servicetime` AS `servicetime`,`b`.`booking_date` AS `booking_date`,`b`.`startdate` AS `startdate`,`b`.`enddate` AS `enddate`,`b`.`voucherid` AS `voucherid`,`b`.`voucherprice` AS `voucherprice`,`b`.`vouchertype` AS `vouchertype`,`b`.`totalamount` AS `totalamount`,`b`.`girlsamount` AS `girlsamount`,`b`.`branchamount` AS `branchamount`,`b`.`serviceamount` AS `serviceamount`,`b`.`extraamount` AS `extraamount`,`b`.`pointprice` AS `pointprice`,`b`.`rubbleprice` AS `rubbleprice`,`b`.`eservice_id` AS `eservice_id`,`b`.`eservicetime` AS `eservicetime`,`b`.`eserviceamount` AS `eserviceamount`,`b`.`egirlsamount` AS `egirlsamount`,`b`.`ebranchamount` AS `ebranchamount`,`b`.`reason` AS `reason`,(select `s`.`title` from `iw_tns_service` `s` where (`s`.`id` = `b`.`eservice_id`)) AS `eservicename` from ((`iw_tns_booking` `b` left join `iw_tns_room` `r` on((`b`.`room_id` = `r`.`id`))) left join `iw_tns_girls` `g` on((`b`.`girls_id` = `g`.`id`))) ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `iw_tns_girls_extra`
--
ALTER TABLE `iw_tns_girls_extra`
  ADD CONSTRAINT `iw_tns_girls_extra_ibfk_1` FOREIGN KEY (`girls_id`) REFERENCES `iw_tns_girls` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `iw_tns_girls_idproof`
--
ALTER TABLE `iw_tns_girls_idproof`
  ADD CONSTRAINT `iw_tns_girls_idproof_ibfk_1` FOREIGN KEY (`girls_id`) REFERENCES `iw_tns_girls` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `iw_tns_girls_permit`
--
ALTER TABLE `iw_tns_girls_permit`
  ADD CONSTRAINT `iw_tns_girls_permit_ibfk_1` FOREIGN KEY (`girls_id`) REFERENCES `iw_tns_girls` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `iw_tns_girls_pin`
--
ALTER TABLE `iw_tns_girls_pin`
  ADD CONSTRAINT `iw_tns_girls_pin_ibfk_1` FOREIGN KEY (`girls_id`) REFERENCES `iw_tns_girls` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `iw_tns_girls_service`
--
ALTER TABLE `iw_tns_girls_service`
  ADD CONSTRAINT `iw_tns_girls_service_ibfk_1` FOREIGN KEY (`girls_id`) REFERENCES `iw_tns_girls` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `iw_tns_setting`
--
ALTER TABLE `iw_tns_setting`
  ADD CONSTRAINT `iw_tns_setting_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `iw_tns_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
