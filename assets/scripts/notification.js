

/****** Notification Submit ******/
/* Arguments -  Title, branch,total,description,email,mobile,phone,website,facebook,instagram*/
$(document).on('click', '.notificationSubmit', function(e) {
    var form_data = new FormData($('form#notificationForm')[0]);
    var maintype = $("input[name='maintype']:checked").val();
    var branchtype = $('#branchtype').val();
    var branch = $('#branch').val();
    var title = $('#title').val();
    var total = $('#total').val();
    var description = $('#description').val();
    var buttontext = $('#buttontext').val();
    var url = $('#url').val();
    var email = $('#email').val();
    var mobile = $('#mobile').val();
    var phone = $('#phone').val();
    var description = $('#description').val();
    var website = $('#website').val();
    var facebook = $('#facebook').val();
    var instagram = $('#instagram').val();
    var imagecount = $('#imagecount').val();
    console.log(maintype);
    console.log(branchtype);
    console.log(branch);
    if(branchtype=="branch")
    {
        if(branch==null)
        {
            $('#branch').closest('.form-group').find('.mandatory').html('Wählen Filiale');
            $('#branch').focus();
            return false;
        }

    }
    if (branchtype == '') {
        $('#branchtype').closest('.form-group').find('.mandatory').html('Wählen Filiale');
        $('#branchtype').focus();
        return false;
    }
    else if (branchtype == "cust" && (branch==null  || branch=="" || branch==undefined)) {
        $('#branch').closest('.form-group').find('.mandatory').html('Wählen Sie Zweig');
        $('#branch').focus();
        return false;
    } 
    else if (title == '') {
        $('#title').closest('.form-group').find('.mandatory').html('Titel eingeben');
        $('#title').focus();
        return false;
    } 
    else if (total=='') {
        $('#total').closest('.form-group').find('.mandatory').html('Geben Sie die Gesamtzahl ein');
        $('#total').focus();
        return false;
    }
    else if (description == '') {
        $('#description').closest('.form-group').find('.mandatory').html('Beschreibung eingeben');
        $('#description').focus();
        return false;
    } 
    else if (buttontext == '') {
        $('#buttontext').closest('.form-group').find('.mandatory').html('Geben Sie den Schaltflächentext ein');
        $('#buttontext').focus();
        return false;
    } 
    else if (url == '') {
        $('#url').closest('.form-group').find('.mandatory').html('URL eingeben');
        $('#url').focus();
        return false;
    } 
    else if (!ValidateEmail(email) || email == '')  {
        $('#email').closest('.form-group').find('.mandatory').html('Geben Sie die E-Mail-ID ein');
        $('#email').focus();
        return false;
    } 
    else if(!validateMobile(phone) || phone == '') {
        $('#phone').closest('.form-group').find('.mandatory').html('Telefon eingeben');
        $('#phone').focus();
        return false;
    } 
    else if(!validateMobile(mobile) || mobile == '') {
        $('#mobile').closest('.form-group').find('.mandatory').html('Geben Sie die Handynummer ein');
        $('#mobile').focus();
        return false;
    } 
    
    else if (website == '') {
        $('#website').closest('.form-group').find('.mandatory').html('Website betreten');
        $('#website').focus();
        return false;
    } 
    else if (facebook == '') {
        $('#facebook').closest('.form-group').find('.mandatory').html('Geben Sie ein Facebook');
        $('#facebook').focus();
        return false;
    } 
    else if (instagram == '') {
        $('#instagram').closest('.form-group').find('.mandatory').html('Geben Sie ein Instagram ein');
        $('#instagram').focus();
        return false;
    }
    else if (imagecount == '' || imagecount == 0) {
        alertify.alert('Geben Sie mindestens ein Bild ein');
        return false;
    } 
     else {
        $('.notificationSubmit').prop('disabled', true);
       $.ajax({
            url: 'ajax-actions_notification',
             method: 'POST',
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,   
            success: function(res) {
                console.log(res);
                if (res.status == 'success' && res.result.status == 'success') {
                    location.href='notification'; 
                    //alertify.alert('Erfolgreich erstellt');
                 } 
                 
                 else {
                    console.log("errror page");
                    alertify.alert('ungültiger Prozess erneut versuchen');
                    return false;
                }
            }
        });
    }
    return false;

});

function noteType()
{
   var bType = $("#branchtype").val();
   console.log(bType);
    if(bType =="branch")
    {
        $("#branchfield").show();
    }
    else
    {
        $("#branchfield").hide();
    }
}

//user search option
$(document).on('click', '.noteSearchsubmit', function() {
    var cbranch = $('#cbranch').val();
    var ctitle = $('#ctitle').val();
    var cpoint = $('#cpoint').val();
    var cdate = $('#cdate').val();
    var cstatus = $('#cstatus').val();
    console.log(cbranch);
    console.log(ctitle);
    console.log(cpoint);
    console.log(cdate);
    console.log(cstatus);
    var dataParams = [{
                        "name": "cbranch",
                        "value": cbranch
                    },
                    {
                        "name": "ctitle",
                        "value": ctitle
                    },
                    {
                        "name": "cpoint",
                        "value": cpoint
                    },
                    {
                        "name": "cdate",
                        "value": cdate
                    },
                    {
                        "name": "cstatus",
                        "value": cstatus
                    }];

                    Load_DataTables("pointview", dataParams);
});
//user search option
$(document).on('click', '.removeNoteFilter', function() {
    console.log("sdfbdsf");
    $('#cbranch').val("All");
    $('#ctitle').val("All");
    $('#cpoint').val("All");
    $('#cdate').val("");
    $('#cstatus').val("1");
    $("#show-filter").removeClass("show");
    Load_DataTables("pointview");
    
});
// preview  Title
$(document).on('keyup', 'form#notificationForm #title', function() {
    
    var s_title = $("#title").val();
    $("#noteTitleview").html(s_title);
});
// preview description 
$(document).on('keyup', 'form#notificationForm #description', function() {
    var s_des = $("#description").val();
    //console.log(s_des);
    $("#sdes").html(s_des);
});
// preview Buttontext 
$(document).on('keyup', 'form#notificationForm #buttontext', function() {
     var btx = $("#buttontext").val();
    $("#btx").html(btx);
});

// preview Notification phone
$(document).on('keyup', 'form#notificationForm #phone', function() {
    var phone = $("#phone").val();
    if (phone != "") {
        $("#notephone").show();
    } else {
        $("#notephone").hide();
    }

});
// preview Notification website
$(document).on('keyup', 'form#notificationForm #website', function() {
    var website = $("#website").val();
    if (website != "") {
        $("#noteweb").show();
    } else {
        $("#noteweb").hide();
    }

});
// preview Notification Email
$(document).on('keyup', 'form#notificationForm #email', function() {
    var email = $("#email").val();
    console.log(email);
    if (email != "") {
        $("#noteemail").show();
    } else {
        $("#noteemail").hide();
    }

});
// preview Notification Instagram
$(document).on('keyup', 'form#notificationForm #instagram', function() {
    var instagram = $("#instagram").val();
    if (instagram != "") {
        $("#noteinst").show();
    } else {
        $("#noteinst").hide();
    }

});
// preview Notification Email
$(document).on('keyup', 'form#notificationForm #facebook', function() {
    //$("#product_name").keyup(function(){
    var facebook = $("#facebook").val();
    if (facebook != "") {
        $("#notefb").show();
    } else {
        $("#notefb").hide();
    }

});


/*** Drag and Drop Functionality ***/
function showThumbnailnote(files) {
    var intype = '';
    var filede = 0;
    console.log("file--" + files.length);
    var img_cnt = files.length;
    console.log("img_cnt" + img_cnt);
    var cfiledtaa = 0;
    for (var i = 0; i < img_cnt; i++) {
        cfiledtaa = parseInt(cfiledtaa) + 1;
        console.log("filede" + filede);
        //var file=files[filede];
        var file = files[i];
        console.log(file);
        var fileName, fileExtension, fileSize, fileType, dateModified;
        fileName = files[i].name;
        fileExtension = fileName.replace(/^.*\./, '');
        if (fileExtension == 'png' || fileExtension == 'jpg' || fileExtension == 'jpeg') {

            readImageFile(file); // GET IMAGE INFO USING fileReader().
        } else {
            fileSize = files[i].size; // FILE SIZE.
            fileType = files[i].type; // FILE TYPE.
            dateModified = files[i].lastModifiedDate; // FILE LAST MODIFIED.
            alertify.alert('Falscher Dateityp-' + fileName + 'nur png, jpg, jpeg erlaubt');
            filede = parseInt(filede) + 1;
        }

        function readImageFile(file) {
            var reader = new FileReader(); // CREATE AN NEW INSTANCE.

            reader.onload = function(e) {
                var img = new Image();
                img.src = e.target.result;

                img.onload = function() {
                    var w = this.width;
                    var h = this.height;
                    //if (w == 800 && h == 800) {
                        var fd = new FormData();
                        fd.append('file', file);
                        var uid = $("#u_id").val();
                        var editid = $("#editid").val();
                        fd.append('uid', uid);
                        fd.append('editid', editid);
                        //uploadData(fd);
                        $.ajax({
                            url: 'ajax-actions_galleryuploadnote',
                            method: 'POST',
                            dataType: 'JSON',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: fd,
                            success: function(res) {
                                //$('.upload-progress').hide();
                                appendHtml = '';
                                showimage = '';
                                console.log(res);
                                if (res.result.success = "success") {
                                    var cctl = res.result.uploadfilename.length;
                                    console.log("cctl" + cctl);
                                    for (i = 0; i < cctl; i++) {
                                        console.log(res.result.uploadfilename[i]);
                                        console.log(res.result.img_id[i]);
                                        console.log(res.result.img_eid[i]);

                                        appendHtml += '<li class="gallery_image"><img src="' + aws_viewlink_notification+ res.result.uploadfilename[i] + '" width="42" height="42"><a href="javascript:void(0)" data-id="' + res.result.img_eid[i] + '" data-vid="v' + res.result.img_id[i] + '" class="remove-image-note" data-name="' + res.result.uploadfilename[i] + '"><i class="fa fa-close"></i></a></li>';
                                        showimage += '<div id="v' + res.result.img_id[i] + '" class="item" ><img src="' + aws_viewlink_notification + res.result.uploadfilename[i] + '"/></div>';
                                    }

                                    var img_count = $("#imagecount").val();
                                    var total_count = parseInt(img_count) + parseInt(cctl);
                                    $("#imagecount").val(total_count);
                                    $("#girls-gallery-images").append(appendHtml);
                                    $("#show-images").append(showimage);
                                    filede = parseInt(filede) + 1;
                                    $('#show-images').data('owl.carousel').destroy();
                                    carousel_reinit();
                                }


                            }

                        });
                   /* } else {

                        alertify.alert('Invalid file size-' + fileName + ' 800x800px allowed');
                        filede = parseInt(filede) + 1;
                    }*/
                }
            };
            reader.readAsDataURL(file);
        }
    }
}

// gallery AJAX request and upload file
function uploadDatanote(formdata) {
  console.log(formdata);
    console.log("senddata");
    $.ajax({
        url: 'ajax-actions_galleryuploadnote',
        method: 'POST',
        dataType: 'JSON',
        cache: false,
        contentType: false,
        processData: false,
        data: formdata,
        success: function(res) {
            //$('.upload-progress').hide();
            appendHtml = '';
            showimage = '';
            console.log(res);
            var cctl = res.result.uploadfilename.length;
            console.log("cctl" + cctl);
            for (i = 0; i < cctl; i++) {
                /*console.log(res.result.uploadfilename[i]);
                console.log(res.result.img_id[i]);
                console.log(res.result.img_eid[i]);*/
                appendHtml += '<li class="gallery_image"><img src="' + aws_viewlink_notification + res.result.uploadfilename[i] + '" width="42" height="42"><a href="javascript:void(0)" data-id="' + res.result.img_eid[i] + '" data-vid="v' + res.result.img_id[i] + '" class="remove-image-note" data-name="' + res.result.uploadfilename[i] + '"><i class="fa fa-close"></i></a></li>';
                showimage += '<div id="v' + res.result.img_id[i] + '" class="item" ><img src="' + aws_viewlink_notification + res.result.uploadfilename[i] + '"/></div>';

            }

            var img_count = $("#imagecount").val();
            var total_count = parseInt(img_count) + parseInt(cctl);
            $("#imagecount").val(total_count);
            $("#girls-gallery-images").append(appendHtml);
            $("#show-images").append(showimage);
            $('#show-images').data('owl.carousel').destroy();
            carousel_reinit();
        },
        /*xhr: function() {
            return fileUploadProgress('upload-progress');
        }*/
    });
}
/**gallery  Remove Image **/
/**Argument -  galleryid, File Name **/
$(document).on('click', '.gallery_image a.remove-image-note', function(e) {
  console.log("deletedata");
    var thiz = $(this);
    alertify.confirm('Do you want delete this?', function() {
        thiz.closest('li').remove();
        record_id = thiz.attr('data-id');
        fileName = thiz.attr('data-name');
        vid = thiz.attr('data-vid');
        $("#"+vid).html("");
        var currentIndex = $('div .item #' + vid).index() + 1;
        console.log("imageNo: " + currentIndex);
        var indexToRemove = currentIndex;
        $(".owl-carousel").trigger('remove.owl.carousel', [indexToRemove]).trigger('refresh.owl.carousel');
        var img_count = $("#imagecount").val();
        var total_count = parseInt(img_count) - parseInt(1);
        $("#imagecount").val(total_count);
        //console.log(typeof(fileDeldata));
        /*fileDeldata.push(record_id);
        fileDelname.push(fileName);*/
        
        /*$("#fileDeldata").val(record_id);
        $("#fileDelname").val(fileName);*/
        
        deleteRecords(record_id, 'iw_tns_notification_gallery', 'id', fileName);
        //$('.chosen-select').chosen();
    });
});
function carousel_reinit()
{
    $('.mobil-content .banner-slider').owlCarousel({
      items:1,
      loop:false,
      nav:false,
      dots :true,
      margin:0
  });
}