
/****** User creation Submit ******/
/* Arguments -  Mobilnumber, email,firstname,lastname,function,status,branch,password*/
$(document).on('click', '.usresubmit', function(e) {
    var branch = $('#branch').val();
    var type = $('#type').val();
    var firstName = $('#firstName').val();
    var lastName = $('#lastName').val();
    var mobile = $('#mobile').val();
    var password = $('#password').val();
    var email = $('#email').val();
    var userstatus = $('#status').val();
    var formData=$('#userForm').serialize();
    if (branch == '') {
        $('#branch').closest('.form-group').find('.mandatory').html('Wählen Filiale');
        $('#branch').focus();
        return false;
    }
    else if (type == '') {
        $('#type').closest('.form-group').find('.mandatory').html('Funktion auswählen');
        $('#type').focus();
        return false;
    } 
    else if (firstName == '') {
        $('#firstName').closest('.form-group').find('.mandatory').html('Bitte Vornamen eingeben');
        $('#firstName').focus();
        return false;
    } 
    else if (lastName == '') {
        $('#lastName').closest('.form-group').find('.mandatory').html('Nachnamen eingeben');
        $('#lastName').focus();
        return false;
    } 
    else if (!validateMobile(mobile) || mobile == '') {
        $('#mobile').closest('.form-group').find('.mandatory').html('Geben Sie die richtige Handynummer ein');
        $('#mobile').focus();
        return false;
    } else if (password == '') {
        $('#password').closest('.form-group').find('.mandatory').html('Passwort eingeben');
        $('#password').focus();
        return false;
    } 
    else if (!ValidateEmail(email) || email == '') {
        $('#email').closest('.form-group').find('.mandatory').html('Geben Sie die E-Mail ein');
        $('#email').focus();
        return false;
    }else if (userstatus == '') {
        $('#status').closest('.form-group').find('.mandatory').html('Status auswählen');
        $('#status').focus();
        return false;
    }  else {
       $.ajax({
            url: 'ajax-actions_usercreate',
            method: 'POST',
            dataType: 'JSON',
            data: formData,   
            success: function(res) {
                console.log(res);
                if (res.status == 'success' && res.result.status == 'success') {
                    location.href='user'; 
                    //alertify.alert('Erfolgreich erstellt');
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('Handynummer existiert bereits');
                    return false;
                 }
                 else {
                    console.log("errror page");
                    alertify.alert('ungültiger Prozess erneut versuchen');
                    return false;
                }
            }
        });
    }
    return false;

});

// Separate userdetails image 
// Arguments : userid
$(document).on('click', '.userdetilsshow', function() {
    //$(".open-right-drawer").addClass("is-active");
    var thiz = $(this);
    var id = $(thiz).attr("data-userid");
    console.log(id);
    $.ajax({
            url: 'ajax-actions_separate',
            method: 'POST',
            data: {
                userid: id
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res.result.userdetails[0].first_name);
                var userdetails = res.result.userdetails[0];
                if (res.status == 'success' && res.result.status == 'success') {
                    var st = userdetails.status=='1'?'Aktiv':'inaktiv';
                    var userDetailsData='<tr><td class="lang_Function">Funktion</td><td><strong>'+userdetails.function+'</strong></td></tr><tr><td class="lang_Branch">Filiale</td><td><strong>'+userdetails.branch+'</strong></td></tr><tr><td class="lang_Firstname">Vorname</td><td><strong>'+userdetails.first_name+'</strong></td>/tr><tr><td class="lang_Surname">Nachname</td><td><strong>'+userdetails.last_name+'</strong></td></tr><tr><td class="lang_Mobile">Mobil</td><td><strong>'+userdetails.mobile+'</strong></td>/tr><tr><td class="lang_Email">E-Mail</td><td><strong>'+userdetails.email+'</strong></td></tr><tr><td class="lang_Createdon">Erstellt am</td><td><strong>'+userdetails.created_date+'</strong></td></tr><tr><td class="lang_Status">Status</td><td><strong>'+st+'</strong></td></tr><tr><td class="">Pin</td><td><strong><div class="position-relative form-group"><input name="password" id="password" type="password" value="'+userdetails.pin+'" class="form-control" readonly><span class="eye-icon"><i toggle="#password"class="fa fa-eye toggle-password"></i></span></div></strong></td></tr>'; 
                   
                    console.log(userDetailsData);
                    $("#userSeparateDetails").html(userDetailsData); 
                    $('.userEdit').attr('href', 'edituser?userid='+id);
                    $(".userRemove").attr("data-id", id);
                    $(".userRemove").attr("data-table", 'iw_tns_user');
                    $(".userRemove").attr("data-column", 'id');
                    $(".userRemove").attr("data-file", '');
                     //Ldata();
                    rightPopupopen(); 
                 } else {
                    
                }
            }
        });
    return false;
    
});

//user search option
$(document).on('click', '.userSearchsubmit', function() {
    var branch = $('#cbranch').val();
    var type = $('#ctype').val();
    var userName = $('#userName').val();
    var mobile = $('#cmobile').val();
    var userstatus = $('#cuserstatus').val();
    console.log(branch);
    console.log(type);
    console.log(userName);
    console.log(mobile);
    console.log(userstatus);
    var dataParams = [{
                        "name": "username",
                        "value": userName
                    },
                    {
                        "name": "branch",
                        "value": branch
                    },
                    {
                        "name": "type",
                        "value": type
                    },
                    {
                        "name": "mobile",
                        "value": mobile
                    },
                    {
                        "name": "userstatus",
                        "value": userstatus
                    }];

                    Load_DataTables("user", dataParams);
});
//user search option
$(document).on('click', '.removeUserFilter', function() {
    console.log("sdfbdsf");
    $('#cbranch').val("all");
    $('#ctype').val("all");
    $('#userName').val("");
    $('#cmobile').val("");
    $('#cuserstatus').val("1");
    //$("#show-filter").removeClass("show");
    Load_DataTables("user");
    
});

