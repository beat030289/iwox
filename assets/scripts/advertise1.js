/****** Advertise Submit ******/
/* Arguments -  Title, url,fromdate,todate,image*/
$(document).on('click', '.advertiseSubmit', function(e) {
    var form_data = new FormData($('form#advertiseForm')[0]);
    var title = $('#title').val();
    var url = $('#url').val();
    var fromdate = $('#fromdate').val();
    var todate = $('#todate').val();
    var advertiseimage = $('#advertise_1').val();
    var oldfile = $('#oldfile').val();
    
    if (title == '') {
        $('#title').closest('.form-group').find('.mandatory').html('Titel eingeben');
        $('#title').focus();
        return false;
    } 
    else if (url == '') {
        $('#url').closest('.form-group').find('.mandatory').html('URL eingeben');
        $('#url').focus();
        return false;
    }
    else if (fromdate == '') {
        $('#fromdate').closest('.form-group').find('.mandatory').html('Geben Sie ab Datum ein');
        $('#fromdate').focus();
        return false;
    } 
    else if (todate == '') {
        $('#todate').closest('.form-group').find('.mandatory').html('Bisher eingeben');
        $('#todate').focus();
        return false;
    } 
    else if (advertiseimage == '' && oldfile=='') {
        $('#advertise_1').closest('.form-group').find('.mandatory').html('Bild hochladen');
        $('#advertise_1').focus();
         return false;
    } 
     else {
       $.ajax({
            url: 'ajax-actions_advertise',
             method: 'POST',
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,   
            success: function(res) {
                console.log(res);
                if (res.status == 'success' && res.result.status == 'success') {
                    location.href='advertise'; 
                    //alertify.alert('Erfolgreich erstellt');
                 } 
                 
                 else {
                    console.log("errror page");
                    alertify.alert('ungültiger Prozess erneut versuchen');
                    return false;
                }
            }
        });
    }
    return false;

});
/* Image loader Function */
var imageLoader1 = document.getElementById('advertise_1');
if(imageLoader1)
{

    imageLoader1.addEventListener('change', handleImage1, false);
}

function handleImage1(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
        
        $('.advertiseup_1 img').attr('src',event.target.result);
    }
    reader.readAsDataURL(e.target.files[0]);
}


//user search option
$(document).on('click', '.advertiseSearchsubmit', function() {
    var ctitle = $('#ctitle').val();
    var cdate = $('#cdate').val();
    var cstatus = $('#cstatus').val();
    console.log(ctitle);
    console.log(cdate);
    console.log(cstatus);
    var dataParams = [
                    {
                        "name": "ctitle",
                        "value": ctitle
                    },
                    
                    {
                        "name": "cdate",
                        "value": cdate
                    },
                    {
                        "name": "cstatus",
                        "value": cstatus
                    }];

                    Load_DataTables("advertiseview", dataParams);
});
//advertise search option
$(document).on('click', '.removeAdvertiseFilter', function() {
    console.log("sdfbdsf");
    $('#ctitle').val("All");
    $('#cdate').val("");
    $('#cstatus').val("All");
    $("#show-filter").removeClass("show");
    Load_DataTables("advertiseview");
    
});


// Separate Advertise image 
// Arguments : userid
$(document).on('click', '.advertisedetilsshow', function() {
    //$(".open-right-drawer").addClass("is-active");
    var thiz = $(this);
    var id = $(thiz).attr("data-advertiseid");
    console.log(id);
    $.ajax({
            url: 'ajax-actions_advertiseseparate',
            method: 'POST',
            data: {
                advertiseid: id
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                console.log(res.result.advertisedetails[0].title);
                var advertisedetails = res.result.advertisedetails[0];
                if (res.status == 'success' && res.result.status == 'success') {
                    var st = advertisedetails.status=='1'?'Aktiv':'inaktiv';
                    var advertiseDetailsData='<tr><td>Titel</td><td><strong>'+advertisedetails.title+'</strong></td></tr><tr><td colspan="2"><img src="'+aws_viewlink_advertise+''+advertisedetails.image+'" alt="" style="width:100%";></td></tr><tr><td>URL</td><td><strong>'+advertisedetails.url+'</strong></td></tr><tr><td>Dauer</td><td><strong>'+advertisedetails.fromdate+' - '+advertisedetails.todate+'</strong></td></tr><tr><td>Erstellt am</td><td><strong>'+advertisedetails.created_date+'</strong></td></tr><tr><td>Status</td><td><strong>'+st+'</strong></td></tr>'; 
                   
                    console.log(advertiseDetailsData);
                    $("#AdvertiseSeparateDetails").html(advertiseDetailsData); 
                    $('.advertiseEdit').attr('href', 'editAdvertise?addid='+id);
                    $(".advertiseRemove").attr("data-id", id);
                    $(".advertiseRemove").attr("data-table", 'iw_tns_advertise');
                    $(".advertiseRemove").attr("data-column", 'id');
                    $(".advertiseRemove").attr("data-file", advertisedetails.image);
                     //Ldata();
                    rightPopupopen(); 
                 } else {
                    
                }
            }
        });
    return false;
    
});

