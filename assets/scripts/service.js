// Create Services type
// Arguments : service type
$(document).on('click', '.servicetypesubmit', function() {
    console.log(servicetype);
    var servicetype = $('#servicetypec').val();
    var editid = $('#editid').val();
    if (servicetype == "") {
        $('#servicetypec').closest('.form-group').find('.mandatory').html('Geben Sie den Dienstnamen ein');
        $('#servicetypec').focus();
        return false;
    } else {
        $.ajax({
            url: 'ajax-actions_servicetype',
            method: 'POST',
            data: {
                servicetype: servicetype,
                editid: editid,
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                if (res.status == 'success' && res.result.status == 'success') {
                    $('#servicetypec').val("");
                    $('#editid').val("");
                    $(".app-drawer-wrapper").removeClass("drawer-open");
                    $(".app-drawer-overlay").addClass("d-none");
                    Load_DataTables("servicetype");
                } else if (res.status == 'success' && res.result.status == 'already') {
                    alertify.alert('Dienstname wird bereits beendet');
                    return false;
                } else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
    }
    return false;
});
// value remove function
$(document).on('click', '.stypeclear', function() {
    console.log("testdata sval");
    rightPopupclose();
    $('#servicetypec').val("");
});
// Create Services
// Arguments : Title,price ,branch,zeit,servicetype,extratype
$(document).on('click', '.serviceSubmit', function() {
    console.log("serviceSubmit");
    var formss = $('form#serviceForm');
    var form_data = new FormData(formss[0]);
    var title = $('#title').val();
    var branch = $('#sbranch').val();
    var zeit = $('#szeit').val();
    var price = $('#sprice').val();
    var status = $('#sstatus').val();
   /* var etime = $('#etime').val();
    var eprice = $('#eprice').val();
    var mprice = $('#mprice').val();
    var servicetype = $('#servicetype').val();*/

    if (title == "") {
        $('#title').closest('.form-group').find('.mandatory').html('Geben Sie einen Titel ein');
        $('#title').focus();
        return false;
    } else if (branch == "") {
        $('#sbranch').closest('.form-group').find('.mandatory').html('Wählen Sie eine Filiale aus');
        $('#sbranch').focus();
        return false;
    } else if (zeit == "") {
        $('#szeit').closest('.form-group').find('.mandatory').html('Zeit wählen');
        $('#szeit').focus();
        return false;
    } else if (price == "" || price < 1) {
        $('#sprice').closest('.form-group').find('.mandatory').html('Geben Sie einen gültigen Preis ein');
        $('#sprice').focus();
        return false;
    } else if (status == "") {
        $('#sstatus').closest('.form-group').find('.mandatory').html('Status auswählen');
        $('#sstatus').focus();
        return false;
    }
    /* else if (etime == "") {
        $('#etime').closest('.form-group').find('.mandatory').html('Wählen Sie Optionen für zusätzliche Zeit');
        $('#etime').focus();
        return false;
    } else if (eprice == "") {
        $('#eprice').closest('.form-group').find('.mandatory').html('Geben Sie den zusätzlichen Zeitpreis ein');
        $('#eprice').focus();
        return false;
    } else if (mprice == "") {
        $('#mprice').closest('.form-group').find('.mandatory').html('Mindestpreis eingeben');
        $('#mprice').focus();
        return false;
    } else if (servicetype == "" || servicetype == null || servicetype == undefined) {
        $('.sstype').html('Wählen Sie den Servicetyp');
        $('#sstatus').focus();
        return false;
    }*/ else {
        console.log("testdtata");
        $.ajax({
            url: 'ajax-actions_servicecreate',
            method: 'POST',
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            success: function(res) {
                console.log(res);
                if (res.status == 'success' && res.result.status == 'success') {
                    //location.href = 'dienstleistung';
                    var serviceid =res.result.id;
                    //alert(serviceid);
                    location.href='viewdienstleistung?serviceid='+serviceid; 
                    //alertify.alert('Erfolgreich erstellt');
                } else if (res.status == 'success' && res.result.status == 'already') {
                    alertify.alert('Servicetitel bereits vorhanden');
                    return false;
                } else {
                    console.log("errror page");
                    alertify.alert('ungültiger Prozess erneut versuchen');
                    return false;
                }
            }
        });
    }
    return false;
});
// Service listiing
$(document).on('click', '.servicelistfun', function() {

    var vcservice = $('#vcservice').val();
    var vcbranch = $('#vcbranch').val();
    var vcprice = $('#vcprice').val();
    var vctime = $('#vctime').val();
    var vcstatus = $('#vcstatus').val();
    var st = 0;
    var et = 12;
    var limit = st + ',' + et;
    $.ajax({
        type: 'POST',
        url: 'ajax-actions_serviceList',
        data: {
            limit: limit,
            vcservice: vcservice,
            vcbranch: vcbranch,
            vcprice: vcprice,
            vcstatus: vcstatus,
            vctime: vctime,
        },
        dataType: 'JSON',
        success: function(res) {
            console.log(res);

            $('.total-service').html(res.result.html);
        }
    });

    return false;

});
$(document).on('click', '.removeserviceFilter', function() {
    console.log("sdfbdsf");
    $('#vcservice').val("all");
    $('#vcbranch').val("all");
    $('#vcprice').val("all");
    $('#vctime').val("all");
    $('#vcstatus').val("all");
    $("#show-filter").removeClass("show");
    $(".servicelistfun").trigger('click');

});
// window scroll function
$(window).scroll(function() {
    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
        var div_count = $('.slist').length;
        //alert(div_count);
        console.log(div_count);
        if (div_count >= 12) {
            var start = div_count;
            var end = div_count + 12;
            var limit = start + ',' + end;
            var vcservice = $('#vcservice').val();
            var vcbranch = $('#vcbranch').val();
            var vcprice = $('#vcprice').val();
            var vctime = $('#vctime').val();
            var vcstatus = $('#vcstatus').val();
            $.ajax({
                type: 'POST',
                url: 'ajax-actions_serviceList',
                data: {
                    limit: limit,
                    vcservice: vcservice,
                    vcbranch: vcbranch,
                    vcprice: vcprice,
                    vcstatus: vcstatus,
                    vctime: vctime,
                },
                dataType: 'JSON',
                success: function(res) {
                    console.log(res);

                    $('.total-service').append(res.result.html);
                }
            });
            return false;
        }
    }
});
//edit service type
$(document).on('click', '.edit_servicetype', function() {
   /* alert($(this).data("id"));*/
    var eid = $(this).data("id");
    if(eid=="")
    {
        $('#servicetypec').closest('.form-group').find('.mandatory').html('Ungültiger Prozess');
        $('#servicetypec').focus();
        return false; 
    }
    
    else
    {
        $.ajax({
            url: 'ajax-actions_servicetype_edit',
            method: 'POST',
            data: {
                id: eid
            },
            dataType: 'JSON',
            success: function(res) {
                /*console.log(res);
                alert(res.status);
                alert(res.result.details);*/
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#servicetypec').val(res.result.details[0].service);
                    $('#editid').val(res.result.details[0].id);
                    $(".app-drawer-wrapper").addClass("drawer-open");
                    $(".app-drawer-overlay").removeClass("d-none");
                    //Load_DataTables("extratype");
                 } 
                 
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});