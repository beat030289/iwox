function getChartService(a) {
    //console.log("checkdata");
    // console.log(o(chartColors.orange).alpha(.5));
    var sName = [];
    var sPoint = [];
    var sColor = [];
    $.ajax({
        type: 'POST',
        url: 'ajax-actions_chartList',
        data: {
            type: "service",

        },
        dataType: 'JSON',
        success: function(res) {
            //console.log(res);
            sName = res.result.serviceName;
            sPoint = res.result.servicePoint;
            sName.forEach(function(index, value) {
                var hue = 'rgba(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ', 0.5)';;
                sColor.push(hue);
            });
            //console.log(sColor);
            h = {
                data: {
                    datasets: [{
                        data: sPoint,
                        backgroundColor: sColor,
                        label: "My dataset"
                    }],
                    labels: sName
                },
                options: {
                    responsive: !0,
                    legend: {
                        position: "right"
                    },
                    title: {
                        display: !1,
                        text: "Chart.js Polar Area Chart"
                    },
                    scale: {
                        ticks: {
                            beginAtZero: !0
                        },
                        reverse: !1
                    },
                    animation: {
                        animateRotate: !1,
                        animateScale: !0
                    }
                }
            }
            if (sName.length > 0) {
                var o = document.getElementById("polar-chart");
                if (a != undefined)
                    window.myPolarArea = a.a.PolarArea(o, h);
            }


        }
    });
}

function getChartUser(a) {
    var uName = [];
    var uPoint = [];
    var uColor = [];
    $.ajax({
        type: 'POST',
        url: 'ajax-actions_chartList',
        data: {
            type: "user",

        },
        dataType: 'JSON',
        success: function(res) {
           // console.log(res);
            uName = res.result.userName;
            uPoint = res.result.userPoint;
            uName.forEach(function(index, value) {
                var hue = 'rgba(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ', 0.5)';
                uColor.push(hue);
            });
            //console.log(uColor);
            d = {
                type: "doughnut",
                data: {
                    datasets: [{
                        data: uPoint,
                        backgroundColor: uColor,
                        label: "Dataset 1"
                    }],
                    labels: uName
                },
                options: {
                    responsive: !0,
                    legend: {
                        position: "top"
                    },
                    title: {
                        display: !1,
                        text: "Chart.js Doughnut Chart"
                    },
                    animation: {
                        animateScale: !0,
                        animateRotate: !0
                    }
                }
            }
            if (uName.length > 0) {
                var i = document.getElementById("doughnut-chart").getContext("2d");
                window.myDoughnut = new a.a(i, d)
            }
        }

    });
}