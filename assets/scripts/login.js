/****Password hide******/
$(document).on('click', '.toggle-password', function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});
/****** Login Submit ******/
/* Arguments -  Mobilnumber, Password*/
$(document).on('keyup', '#mobile,#password', function(event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == 13) {
        $('.loginsubmit').trigger('click');
    }
});
$(document).on('click', '.loginsubmit', function(e) {
    var mobile = $('#mobile').val();
    var password = $('#password').val();
    var test =  validateMobile(mobile);
    console.log(test);
    if (!validateMobile(mobile) || mobile == '') {
        $('.mandatory').html('Geben Sie eine gültige Mobilfunknummer ein');
        $('#mobile').focus();
        return false;
    } else if (password == "") {
        $('.mandatory').html('Passwort eingeben');
        $('#password').focus();
        return false;
    } else {
        $.ajax({
            url: 'ajax-actions_login',
            method: 'POST',
            data: {
                password: password,
                mobile: mobile
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                if (res.status == 'success' && res.result.status == 'success') {
                    
                    location.href='dashboard';  
                 } else {
                    console.log("errror page");
                    //alertify.alert('Handy oder Passwort ungültig');
                    $('.mandatory').html('nHandy oder Passwort ungültig');
                    $('#mobile').focus();
                    return false;
                }
            }
        });
    }
    return false;
});
/****** Change Password Submit ******/
/* Arguments -  Mobilnumber, Password*/
$(document).on('click', '.changePassword', function() {
    var oldPassword = $('#oldPassword').val();
    var newPassword = $('#newPassword').val();
    var newConPassword = $('#newConPassword').val();
     if (oldPassword == '') {
        $('.mandatory').html('Altes Passwort eingeben');
        $('#oldPassword').focus();
        return false;
    } else if (newPassword == "") {
        $('.mandatory').html('Neues Passwort eingeben');
        $('#newPassword').focus();
        return false;
    }  else if (newConPassword == "") {
        $('.mandatory').html('neues Passwort Passwort bestätigen stimmt nicht überein');
        $('#newConPassword').focus();
        return false;
    }  else if (newPassword != newConPassword) {
        $('#newPassword').focus();
        return false;
    } 
     else {
        $('.mandatory').html('');
        $.ajax({
            url: 'ajax-actions_changePassword',
            method: 'POST',
            data: {
                password: newPassword,
                oldpassword: oldPassword
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                if (res.status == 'success' && res.result.status == 'success') {
                    //alertify.alert('Erfolgreich aktualisiert');
                    location.href='/logout';  
                 } else if(res.status == 'success' && res.result.status == 'invalid') {
                    //alertify.alert('altes Passwort ungültig');
                    $('.mandatory').html('altes Passwort ungültig');
                    $('#oldPassword').focus();
                    return false;
                }
                else {
                    $('.mandatory').html('Wiederholen Sie die ungültige Eingabe');
                    return false;
                }
            }
        });
    }
    return false;
});
/****** profile update Submit ******/
/* Arguments -  Mobilnumber, branch,type,email,phone*/
$(document).on('click', '.profileupdate', function() {
    var branch = $('#branch').val();
    var firstName = $('#firstName').val();
    var lastName = $('#lastName').val();
    var type = $('#type').val();
    var mobile = $('#mobile').val();
    var email = $('#email').val();
    var formData=$('#userProfileForm').serialize();
    console.log(formData);
    if (branch == '') {
        $('#branch').closest('.form-group').find('.mandatory').html('Wählen Filiale');
        $('#branch').focus();
        return false;
    }
    else if (type == '') {
        $('#type').closest('.form-group').find('.mandatory').html('Funktion auswählen');
        $('#type').focus();
        return false;
    } 
    else if (firstName == '') {
        $('#firstName').closest('.form-group').find('.mandatory').html('Bitte Vornamen eingeben');
        $('#firstName').focus();
        return false;
    } 
    else if (lastName == '') {
        $('#lastName').closest('.form-group').find('.mandatory').html('Nachnamen eingeben');
        $('#lastName').focus();
        return false;
    } 
    else if (!validateMobile(mobile) || mobile == '') {
        $('#mobile').closest('.form-group').find('.mandatory').html('Geben Sie die richtige Handynummer ein');
        $('#mobile').focus();
        return false;
    } 
    else if (!ValidateEmail(email) || email == '') {
        $('#email').closest('.form-group').find('.mandatory').html('Geben Sie die richtige Handynummer ein');
        $('#email').focus();
        return false;
    }else {
       $.ajax({
            url: 'ajax-actions_profileUpdate',
            method: 'POST',
            dataType: 'JSON',
            data: formData,   
            success: function(res) {
                console.log(res);
                if (res.status == 'success' && res.result.status == 'success') {
                    location.href='profile'; 
                    //alertify.alert('Erfolgreich erstellt');
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('Handynummer existiert bereits');
                    return false;
                 }
                 else {
                    console.log("errror page");
                    alertify.alert('ungültiger Prozess erneut versuchen');
                    return false;
                }
            }
        });
    }
    return false;
});


/** Forgotpassword**/
/* Argument - EmailID */
$(document).on('click', '.forgotpassword', function() {
    var email = $('#email').val();
    console.log(email);
    if (!ValidateEmail(email)) {
       $('.mandatory').html(' Geben Sie eine richtige E-Mail-Adresse ein');
        $('#email').focus();
        return false;
    } else {
        $.ajax({
            url: 'ajax-actions_forgotpassword',
            method: 'POST',
            data: {
                email: email
            },
            dataType: 'JSON',
            success: function(res) {
                 if (res.result.status == "success") {
                    //location.href='forgotpassword';
                    alertify.alert('E-Mail vergessen E-Mail hat Ihre E-Mail-ID erfolgreich gesendet');
                    location.href='/';
                } else if (res.result.status == "already") {
                    alertify.alert(res.result.message);
                    $('#email').focus();
                    return false;
                } else {
                    alertify.alert('Enter Proper Email ID');
                    $('#email').focus();
                    return false;
                }
              
            }
        });
    }
    return false;
});

/* Reset Password*/
/* Argument - New Password, Confirm Password */
$(document).on('click', '.resetpassword', function() {
    var form_id = 'form#rp_form';
    var formss = $('form#rp_form');
    var form_data = new FormData(formss[0]);
    var password = $('#password').val();
    var confirmpassword = $('#confirmpassword').val();
    console.log(password);
    console.log(confirmpassword);

    if (password == "") {
        $('.mandatory').html('Geben Sie ein neues Kennwort ein');
        $('#confirmpassword').focus();
        return false;
    } else if (confirmpassword == "") {
        $('.mandatory').html('Geben Sie ein Bestätigungskennwort ein');
        $('#confirmpassword').focus();
        return false;
    } else if (password != confirmpassword) {
        $('.mandatory').html('Ihr Passwort und Ihr Bestätigungspasswort stimmen nicht überein');
        return false;
    } else {
        $.ajax({
            url: 'ajax-actions_resetpassword',
            method: 'POST',
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            success: function(res) {
                console.log(res);
                if (res.result.status == "success") {
                    location.href = '/';
                    //alertify.alert('Forgot mail Email sent your email ID successfully');                  
                } else if (res.result.status == "already") {
                    alertify.alert(res.result.message);
                    $('#password').focus();
                    return false;
                } else {
                    alertify.alert('Invalid Email ID');
                    $('#password').focus();
                    return false;
                }
            }
        });
    }
    return false;
});