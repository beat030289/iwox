
var fileDeldata=new Array();
var fileDelname=new Array();
/****** Pointcreated Submit ******/
/* Arguments -  Title, branch,point,shortdescription,longdescription*/
$(document).on('click', '.pointsSubmit', function(e) {
    var form_data = new FormData($('form#pointsForm')[0]);
    var branch = $('#branch').val();
    var title = $('#title').val();
    var sdescription = $('#sdescription').val();
    var ldescription = $('#ldescription').val();
    var cpoint = $('#cpoint').val();
    var imagecount = $('#imagecount').val();
    
    if (branch == '') {
        $('#branch').closest('.form-group').find('.mandatory').html('Wählen Filiale');
        $('#branch').focus();
        return false;
    }
    else if (title == '') {
        $('#title').closest('.form-group').find('.mandatory').html('Titel eingeben');
        $('#title').focus();
        return false;
    } 
    else if (cpoint == ''|| cpoint < 1 || cpoint > 10) {
        $('#cpoint').closest('.form-group').find('.mandatory').html('Geben Sie den richtigen Punkt ein');
        $('#cpoint').focus();
        return false;
    }
    else if (sdescription == '') {
        $('#sdescription').closest('.form-group').find('.mandatory').html('Geben Sie eine lange Beschreibung ein');
        $('#sdescription').focus();
        return false;
    } 
    else if (ldescription == '') {
        $('#ldescription').closest('.form-group').find('.mandatory').html('Nachnamen eingeben');
        $('#ldescription').focus();
        return false;
    } 
    else if (imagecount == '' || imagecount == 0) {
        alertify.alert('Geben Sie mindestens ein Bild ein');
        return false;
    } 
     else {
       $.ajax({
            url: 'ajax-actions_pointcreation',
             method: 'POST',
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,   
            success: function(res) {
                console.log(res);
                if (res.status == 'success' && res.result.status == 'success') {
                    location.href='pointview'; 
                    //alertify.alert('Erfolgreich erstellt');
                 } 
                 
                 else {
                    console.log("errror page");
                    alertify.alert('ungültiger Prozess erneut versuchen');
                    return false;
                }
            }
        });
    }
    return false;

});



//user search option
$(document).on('click', '.pointSearchsubmit', function() {
    var cbranch = $('#cbranch').val();
    var ctitle = $('#ctitle').val();
    var cpoint = $('#cpoint').val();
    var cdate = $('#cdate').val();
    var cstatus = $('#cstatus').val();
    console.log(cbranch);
    console.log(ctitle);
    console.log(cpoint);
    console.log(cdate);
    console.log(cstatus);
    var dataParams = [{
                        "name": "cbranch",
                        "value": cbranch
                    },
                    {
                        "name": "ctitle",
                        "value": ctitle
                    },
                    {
                        "name": "cpoint",
                        "value": cpoint
                    },
                    {
                        "name": "cdate",
                        "value": cdate
                    },
                    {
                        "name": "cstatus",
                        "value": cstatus
                    }];

                    Load_DataTables("pointview", dataParams);
});
//user search option
$(document).on('click', '.removePointFilter', function() {
    console.log("sdfbdsf");
    $('#cbranch').val("All");
    $('#ctitle').val("All");
    $('#cpoint').val("All");
    $('#cdate').val("");
    $('#cstatus').val("1");
    $("#show-filter").removeClass("show");
    Load_DataTables("pointview");
    
});
// preview Point Title
$(document).on('keyup', 'form#pointsForm #title', function() {
    
    var s_title = $("#title").val();
    $("#pointTitleview").html(s_title);
});
// preview shortdescription Title
$(document).on('keyup', 'form#pointsForm #sdescription', function() {
    var s_des = $("#sdescription").val();
    $("#sdes").html(s_des);
});
// preview Point Title
$(document).on('keyup', 'form#pointsForm #ldescription', function() {
     var l_description = $("#ldescription").val();
    $("#des").html(l_description);
});
// preview Product Point
$(document).on('keyup', 'form#pointsForm #cpoint', function() {
    var ppoint = $("#cpoint").val();
    if(ppoint!="" && ppoint<11)
    {
    	var showpoint = "";
	    for (i = 0; i < ppoint; i++) {
	        var pval = parseInt(i) + parseInt(1);
	       /* console.log(pval);*/
	        showpoint += '<li><a href="#"><span>' + pval + '</span></a></li>';

	    }
	    $("#viewpointshow").html(showpoint);
    }
});

// pointvalidation
$(document).on('keyup', '.pointvalcheck', function() {
     var pcount = $("#cpoint").val();
     console.log("pointcheck");
     console.log(pcount);
     if(pcount=="" || pcount>10)
     {
     	$("#cpoint").val("");
     	$('#cpoint').closest('.form-group').find('.mandatory').html('Geben Sie weniger als 11 Punkte ein');
       
     }
     else
     {
     	//$("#cpoint").val("");
     }
});


/*** Drag and Drop Functionality ***/
function showThumbnailpoint(files) {
    var intype = '';
    var filede = 0;
    console.log("file--" + files.length);
    var img_cnt = files.length;
    console.log("img_cnt" + img_cnt);
    var cfiledtaa = 0;
    for (var i = 0; i < img_cnt; i++) {
        cfiledtaa = parseInt(cfiledtaa) + 1;
        console.log("filede" + filede);
        //var file=files[filede];
        var file = files[i];
        console.log(file);
        var fileName, fileExtension, fileSize, fileType, dateModified;
        fileName = files[i].name;
        fileExtension = fileName.replace(/^.*\./, '');
        if (fileExtension == 'png' || fileExtension == 'jpg' || fileExtension == 'jpeg') {

            readImageFile(file); // GET IMAGE INFO USING fileReader().
        } else {
            fileSize = files[i].size; // FILE SIZE.
            fileType = files[i].type; // FILE TYPE.
            dateModified = files[i].lastModifiedDate; // FILE LAST MODIFIED.
            alertify.alert('Falscher Dateityp-' + fileName + 'nur png, jpg, jpeg erlaubt');
            filede = parseInt(filede) + 1;
        }

        function readImageFile(file) {
            var reader = new FileReader(); // CREATE AN NEW INSTANCE.

            reader.onload = function(e) {
                var img = new Image();
                img.src = e.target.result;

                img.onload = function() {
                    var w = this.width;
                    var h = this.height;
                    //if (w == 800 && h == 800) {
                        var fd = new FormData();
                        fd.append('file', file);
                        var uid = $("#u_id").val();
                        var editid = $("#editid").val();
                        fd.append('uid', uid);
                        fd.append('editid', editid);
                        //uploadData(fd);
                        $.ajax({
                            url: 'ajax-actions_galleryuploadpoint',
                            method: 'POST',
                            dataType: 'JSON',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: fd,
                            success: function(res) {
                                //$('.upload-progress').hide();
                                appendHtml = '';
                                showimage = '';
                                console.log(res);
                                if (res.result.success = "success") {
                                    var cctl = res.result.uploadfilename.length;
                                    console.log("cctl" + cctl);
                                    for (i = 0; i < cctl; i++) {
                                        console.log(res.result.uploadfilename[i]);
                                        console.log(res.result.img_id[i]);
                                        console.log(res.result.img_eid[i]);

                                        appendHtml += '<li class="gallery_image"><img src="' + aws_viewlink_point+ res.result.uploadfilename[i] + '" width="42" height="42"><a href="javascript:void(0)" data-id="' + res.result.img_eid[i] + '" data-vid="v' + res.result.img_id[i] + '" class="remove-image-point" data-name="' + res.result.uploadfilename[i] + '"><i class="fa fa-close"></i></a></li>';
                                        showimage += '<div id="v' + res.result.img_id[i] + '" class="item" ><img src="' + aws_viewlink_point + res.result.uploadfilename[i] + '"/></div>';
                                    }

                                    var img_count = $("#imagecount").val();
                                    var total_count = parseInt(img_count) + parseInt(cctl);
                                    $("#imagecount").val(total_count);
                                    $("#girls-gallery-images").append(appendHtml);
                                    $("#show-images").append(showimage);
                                    filede = parseInt(filede) + 1;
                                    $('#show-images').data('owl.carousel').destroy();
                                    carousel_reinit();
                                }


                            }

                        });
                   /* } else {

                        alertify.alert('Invalid file size-' + fileName + ' 800x800px allowed');
                        filede = parseInt(filede) + 1;
                    }*/
                }
            };
            reader.readAsDataURL(file);
        }
    }
}

// gallery AJAX request and upload file
function uploadDatapoint(formdata) {
  console.log(formdata);
    console.log("senddata");
    $.ajax({
        url: 'ajax-actions_galleryuploadpoint',
        method: 'POST',
        dataType: 'JSON',
        cache: false,
        contentType: false,
        processData: false,
        data: formdata,
        success: function(res) {
            //$('.upload-progress').hide();
            appendHtml = '';
            showimage = '';
            console.log(res);
            var cctl = res.result.uploadfilename.length;
            console.log("cctl" + cctl);
            for (i = 0; i < cctl; i++) {
                /*console.log(res.result.uploadfilename[i]);
                console.log(res.result.img_id[i]);
                console.log(res.result.img_eid[i]);*/
                appendHtml += '<li class="gallery_image"><img src="' + aws_viewlink_point + res.result.uploadfilename[i] + '" width="42" height="42"><a href="javascript:void(0)" data-id="' + res.result.img_eid[i] + '" data-vid="v' + res.result.img_id[i] + '" class="remove-image-point" data-name="' + res.result.uploadfilename[i] + '"><i class="fa fa-close"></i></a></li>';
                showimage += '<div id="v' + res.result.img_id[i] + '" class="item" ><img src="' + aws_viewlink_point + res.result.uploadfilename[i] + '"/></div>';

            }

            var img_count = $("#imagecount").val();
            var total_count = parseInt(img_count) + parseInt(cctl);
            $("#imagecount").val(total_count);
            $("#girls-gallery-images").append(appendHtml);
            $("#show-images").append(showimage);
            $('#show-images').data('owl.carousel').destroy();
            carousel_reinit();
        },
        /*xhr: function() {
            return fileUploadProgress('upload-progress');
        }*/
    });
}
/**gallery  Remove Image **/
/**Argument -  galleryid, File Name **/
$(document).on('click', '.gallery_image a.remove-image-point', function(e) {
  console.log("deletedata");
    var thiz = $(this);
    alertify.confirm('Do you want delete this?', function() {
        thiz.closest('li').remove();
        record_id = thiz.attr('data-id');
        fileName = thiz.attr('data-name');
        vid = thiz.attr('data-vid');
        $("#"+vid).html("");
        var currentIndex = $('div .item #' + vid).index() + 1;
        console.log("imageNo: " + currentIndex);
        var indexToRemove = currentIndex;
        $(".owl-carousel").trigger('remove.owl.carousel', [indexToRemove]).trigger('refresh.owl.carousel');
        var img_count = $("#imagecount").val();
        var total_count = parseInt(img_count) - parseInt(1);
        $("#imagecount").val(total_count);
        //console.log(typeof(fileDeldata));
        /*fileDeldata.push(record_id);
        fileDelname.push(fileName);*/
        
        /*$("#fileDeldata").val(record_id);
        $("#fileDelname").val(fileName);*/
        
        deleteRecords(record_id, 'iw_tns_point_gallery', 'id', fileName);
        //$('.chosen-select').chosen();
    });
});
function carousel_reinit()
{
    $('.mobil-content .banner-slider').owlCarousel({
      items:1,
      loop:false,
      nav:false,
      dots :true,
      margin:0
  });
}