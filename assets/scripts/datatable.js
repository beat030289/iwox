/* Data table Show Functionality */
/* Arguments -  table ID*/
function Load_DataTables(page = '', arguements = '') {
    var page_name = page.split('/');
    var page = page_name[0];
    var first_element = page_name[1];
    var second_element = page_name[2];
    var third_element = page_name[3];
    if (page != '') {
        var values = {};
        console.log(page);
        switch (page) {
            case 'user':
                var table_id = 'user';
                values = {
                    sAjaxSource: 'data_tables/user',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'servicetype':
                var table_id = 'servicetype';
                values = {
                    sAjaxSource: 'data_tables/servicetype',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'extratype':
                var table_id = 'extratype';
                values = {
                    sAjaxSource: 'data_tables/extratype',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'zeit':
                var table_id = 'zeit';
                values = {
                    sAjaxSource: 'data_tables/zeit',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'category':
                var table_id = 'category';
                values = {
                    sAjaxSource: 'data_tables/category',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'customer':
                var table_id = 'customer';
                values = {
                    sAjaxSource: 'data_tables/customer',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'permit':
                var table_id = 'permit';
                values = {
                    sAjaxSource: 'data_tables/permit',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'hair':
                var table_id = 'hair';
                values = {
                    sAjaxSource: 'data_tables/hair',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'eyes':
                var table_id = 'eyes';
                values = {
                    sAjaxSource: 'data_tables/eyes',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'shoes':
                var table_id = 'shoes';
                values = {
                    sAjaxSource: 'data_tables/shoes',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'cloth':
                var table_id = 'cloth';
                values = {
                    sAjaxSource: 'data_tables/cloth',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'civilstatus':
                var table_id = 'civilstatus';
                values = {
                    sAjaxSource: 'data_tables/civilstatus',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'privatepart':
                var table_id = 'privatepart';
                values = {
                    sAjaxSource: 'data_tables/privatepart',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'bust':
                var table_id = 'bust';
                values = {
                    sAjaxSource: 'data_tables/bust',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'nationality':
                var table_id = 'nationality';
                values = {
                    sAjaxSource: 'data_tables/nationality',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'vouchers':
                var table_id = 'vouchers';
                values = {
                    sAjaxSource: 'data_tables/vouchers',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'pointview':
                var table_id = 'pointview';
                values = {
                    sAjaxSource: 'data_tables/pointview',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
            case 'advertiseview':
                var table_id = 'advertiseview';
                values = {
                    sAjaxSource: 'data_tables/advertiseview',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
                case 'notificationview':
                var table_id = 'notificationview';
                values = {
                    sAjaxSource: 'data_tables/notificationview',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);
                            });
                        }
                    }
                };
                break;
                case 'debtview':
                var table_id = 'debtview';
                values = {
                    sAjaxSource: 'data_tables/debtview',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);

                            });
                        }
                       // console.log(aoData);
                    }
                };
                break;
                case 'rubbleview':
                var table_id = 'rubbleview';
                values = {
                    sAjaxSource: 'data_tables/rubbleview',
                    serverSide: true,
                    autoWidth: true,
                    bDestroy: true,
                    fnServerParams: function(aoData) {
                        if (arguements != '') {
                            $.each(arguements, function(key, value) {
                                aoData.push(value);

                            });
                        }
                       // console.log(aoData);
                    }
                };
                break;

        }
        if (table_id != '') {
           // console.log(values);
           //$("#" + table_id).css({'display': 'none'});
            ld_DataTable = $('#' + table_id).dataTable(values);

        }
    }
}