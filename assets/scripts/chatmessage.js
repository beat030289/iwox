/* Chat Submit Function */
$(document).on('keyup', '#chatmessage', function(e) {
    if (e.keyCode == 13) {
        var message = $("#chatmessage").val();
        console.log("Enter pressed");
        receiverId = $('.userlistshow li.active').attr("data-id");
        //alert(receiverId);
        currentdate = formatAMPM(new Date);
        currentday = dateFormatz(new Date);
        if (message != "" && message != undefined && message != null) {
            $("#showchat").append('<li class="chat-box-wrapper chat-box-wrapper-right"><div><div class="chat-box">' + message + '</div><small class="opacity-6"><i class="fa fa-calendar-alt mr-1"></i>' + currentdate + ' | ' + currentday + '</small></div> <div><div class="avatar-icon-wrapper ml-1"> <div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg"></div><div class="avatar-icon avatar-icon-lg rounded"><img src="images/user.jpg" alt=""></div></div></div></li>');
            	$('#chmove').animate({scrollTop: $("#chmove ul li").last().offset().top}, 0);
             
            socket.emit('iwox_chat', {
                process: "chatsend",
                message: message,
                receiverid: receiverId
            });
            $("#chatmessage").val("");
        }
        //$("#chmove ul li").last().css( "color", "green" );
       
      // $('.selectuser li.active').trigger('click');
    }
});
/* after Chat Submit Function */
socket.on('submit_chat', (data) => {

    console.log(data);
    if (data.status = "success") {
        //getchat();
    }
});
// function get chat details
function getchat() {
    console.log("work");
    socket.emit('iwox_chat', {
        process: "getchat",

    });
}
//separate getdata
$(document).on('click', '.selectuser', function(e) {
    console.log("worksp");
    var id = $($(this)).attr("data-id");
    var sname = $($(this)).attr("data-fullname");
    $('.userlistshow li.active').removeClass('active');
    $('.userlistshow #ac' + id).addClass('active')
    $('#cwfname').html(sname);
    //alert(id);
    socket.emit('iwox_chat', {
        process: "getchatsparate",
        receiverid: id,

    });
});
// Display chat
socket.on('display_chat', (data) => {

    var showmessage = "";
    var userlidata = "";
    var gdata = data.chatdata;
    var udata = data.userlist;
    var rid = "";
    var userTime = "";
    console.log(data);
    udata.forEach(function(ucdata) {
    	if(ucdata.stime!=null && ucdata.stime!="" && ucdata.stime!=undefined)
    	{
    		//userTime = jQuery.timeago(ucdata.stime);
    		userTime = ucdata.stime;
    	}
    	else
    	{
    		userTime = ""
    	}
    	
        userlidata += '<li class="list-group-item selectuser" data-id="' + ucdata.id + '" id="ac' + ucdata.id + '" data-fullname="' + ucdata.name + '"><div class="widget-content p-0" ><div class="widget-content-wrapper"><div class="avatar-icon-wrapper mr-2"><div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg"></div><div class="avatar-icon avatar-icon-lg rounded"><img src="images/user.jpg" alt=""></div></div><div class="widget-content-left"><div class="widget-heading">' + ucdata.name + '</div><div class="widget-subheading"></div><div class="text-sm opacity-6"><i class="fa fa-clock">'+userTime+'</i> </div></div></div></div></li>';


    });
    if (gdata.length > 0) {
        gdata.forEach(function(cdata) {
            var datec = new Date(cdata.sdate);
            var dayname = dateFormatz(datec);
            if (cdata.senderid == data.sender) {

                showmessage += '<li class="chat-box-wrapper chat-box-wrapper-right"><div><div class="chat-box">' + cdata.message + '</div><small class="opacity-6"><i class="fa fa-calendar-alt mr-1"></i>' + cdata.created_date + ' | ' + dayname + '</small></div> <div><div class="avatar-icon-wrapper ml-1"> <div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg"></div><div class="avatar-icon avatar-icon-lg rounded"><img src="images/user.jpg" alt=""></div></div></div></li>';
            } else {
                showmessage += '<li class="chat-box-wrapper"> <div><div class="avatar-icon-wrapper mr-1"><div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg"></div><div class="avatar-icon avatar-icon-lg rounded"><img src="images/user.jpg" alt=""></div></div></div><div><div class="chat-box">' + cdata.message + '</div><small class="opacity-6"><i class="fa fa-calendar-alt mr-1"></i>' + cdata.created_date + ' | ' + dayname + '</small></div></li>';
            }
            // left side active
            if(cdata.receiverid != data.sender)
            {
            	rid = cdata.receiverid;
            }
            
        });
        $("#showchat").html(showmessage);
        $('#chmove').animate({scrollTop: $("#chmove ul li").last().offset().top}, 0);
    }
    else
    {
    	$("#showchat").html(showmessage);

    }

   	$("#userlistshow").html(userlidata);
    
    console.log("rid"+rid);
    if (rid == "") {
    	console.log("firstcondition"+rid);
        $('.userlistshow li.active').removeClass('active');
        $(".userlistshow li").first().addClass('active');
        var sname = $('.userlistshow li.active').attr("data-fullname");
        $('#cwfname').html(sname);
    } else {
        $('.userlistshow li.active').removeClass('active');
        $('.userlistshow #ac' + rid).addClass('active');
        var sname = $('.userlistshow li.active').attr("data-fullname");
        $('#cwfname').html(sname);
    }
    //$("#chmove ul li").last().css( "color", "red" );
   
  
    

});
socket.on('display_chat_sparate', (data) => {
    //alert(data);
    var showmessage = "";
    var userlidata = "";
    var rid = "";
    var gdata = data.chatdata;
    console.log(data);
    if (gdata.length > 0) {
        gdata.forEach(function(cdata) {
            var datec = new Date(cdata.sdate);
            var dayname = dateFormatz(datec);
            if (cdata.senderid == data.sender) {

                showmessage += '<li class="chat-box-wrapper chat-box-wrapper-right"><div><div class="chat-box">' + cdata.message + '</div><small class="opacity-6"><i class="fa fa-calendar-alt mr-1"></i>' + cdata.created_date + ' | ' + dayname + '</small></div> <div><div class="avatar-icon-wrapper ml-1"> <div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg"></div><div class="avatar-icon avatar-icon-lg rounded"><img src="images/user.jpg" alt=""></div></div></div></li>';
            } else {
                showmessage += '<li class="chat-box-wrapper"> <div><div class="avatar-icon-wrapper mr-1"><div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg"></div><div class="avatar-icon avatar-icon-lg rounded"><img src="images/user.jpg" alt=""></div></div></div><div><div class="chat-box">' + cdata.message + '</div><small class="opacity-6"><i class="fa fa-calendar-alt mr-1"></i>' + cdata.created_date + ' | ' + dayname + '</small></div></li>';
            }
            // left side active
            if(cdata.receiverid != data.sender)
            {
            	rid = cdata.receiverid;
            }
        });
          	$("#showchat").html(showmessage);
   			$('#chmove').animate({scrollTop: $("#chmove ul li").last().offset().top}, 0);
    } else {
        	showmessage = "";
         $("#showchat").html(showmessage);
  }

    
});
/* New Added  */
socket.on('triggerNewaddMessage', function(res) {
    //console.log(res);
    var datec = new Date(res.mtime);
    /*currentdate = formatAMPM(datec);
    currentday = dateFormatz(datec);*/
    var receiver_id = $('.userlistshow li.active').attr('data-id');
       currentdate = formatAMPM(new Date);
        currentday = dateFormatz(new Date);
        if (res.message != "" && res.message != undefined && res.message != null && res.senderid==receiver_id) {
            $("#showchat").append('<li class="chat-box-wrapper"> <div><div class="avatar-icon-wrapper mr-1"><div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg"></div><div class="avatar-icon avatar-icon-lg rounded"><img src="images/user.jpg" alt=""></div></div></div><div><div class="chat-box">' + res.message + '</div><small class="opacity-6"><i class="fa fa-calendar-alt mr-1"></i>' + currentdate + ' | ' + currentday + '</small></div></li>');
            $('#chmove').animate({scrollTop: $("#chmove ul li").last().offset().top}, 0);
        }

});
/* tigger creation notification count */
$(window).load(function() {
    socket.emit('iwox_chat', {
        process: "getNotecount",
      });
});
socket.on('triggerNoteCount', function(res) {
    socket.emit('iwox_chat', {
        process: "getNotecount",
        

    });
});
/* view count updated chat */
socket.on('displayNoteCount', function(response) {
	//alert(response.chatcount);
    if (response.chatcount == 0) {
        $(".chatCountView").hide();
    } else {
        $("#chatNoteCount").html(response.chatcount);
        $(".chatCountView").show();
    }
});
// day format
function dateFormatz(aDate) {
    //var aDate ="2020-03-12";
    var day = aDate.getDate();
    var month = aDate.getMonth() + 1;
    var year = aDate.getFullYear();
    var recDate = day + '-' + month + '-' + year;

    var today = new Date();
    var t_day = today.getDate();
    var t_month = today.getMonth() + 1;
    var t_year = today.getFullYear();
    var curDate = t_day + '-' + t_month + '-' + t_year;
    var yesDate = (t_day - 1) + '-' + t_month + '-' + t_year;
    var yesDateBefore = (t_day - 2) + '-' + t_month + '-' + t_year;

    var dateString = '';
    if (recDate == curDate) dateString = 'Heute';
    else if (recDate == yesDate) dateString = 'Gestern';
    else if (recDate == yesDateBefore) dateString = 'Vorgestern ';
    else dateString = aDate.toDateString().split(' ').slice(1).join(' ');

    return dateString;
}


// time format
function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}
//alert(jQuery.timeago("2020-03-17 11:03:28") );