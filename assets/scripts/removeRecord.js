/** Delete records **/
function deleteRecords(record_id, record_table, record_column, fileName) {
    record_id = typeof(record_id) == 'undefined' ? '' : record_id;
    record_table = typeof(record_table) == 'undefined' ? '' : record_table;
    record_column = typeof(record_column) == 'undefined' ? '' : record_column;
    fileName = typeof(fileName) == 'undefined' ? '' : fileName;
    $.ajax({
        url: 'ajax-actions_removeRecord',
        dataType: 'json',
        type: 'post',
        data: {
            record_id: record_id,
            record_table: record_table,
            record_column: record_column,
            fileName: fileName
        },
        success: function(res) {
            //return true;
            console.log(res);
            if (res.result.status == "success") {
                if (record_table == 'iw_tns_user') {
                    Load_DataTables("user");
                    rightPopupclose();
                }
                else if(record_table == 'iw_mas_servicetype')
                {
                    Load_DataTables("servicetype");

                }
                else if(record_table == 'iw_mas_extratype')
                {
                    Load_DataTables("extratype");
                    
                }
                else if(record_table == 'iw_mas_zeit')
                {
                    Load_DataTables("zeit");
                    
                }
                else if(record_table == 'iw_mas_category')
                {
                    Load_DataTables("category");
                    
                }
                else if(record_table == 'iw_tns_customer')
                {
                    Load_DataTables("customer");
                    
                }
                else if(record_table == 'iw_tns_permit_person')
                {
                    Load_DataTables("permit");
                    
                }
                 else if(record_table == 'iw_tns_hair')
                {
                    Load_DataTables("hair");
                    
                }
                 else if(record_table == 'iw_tns_eyes')
                {
                    Load_DataTables("eyes");
                    
                }
                 else if(record_table == 'iw_tns_shoes')
                {
                    Load_DataTables("shoes");
                    
                }
                 else if(record_table == 'iw_tns_cloth')
                {
                    Load_DataTables("cloth");
                    
                }
                 else if(record_table == 'iw_tns_civilstatus')
                {
                    Load_DataTables("civilstatus");
                    
                }
                else if(record_table == 'iw_tns_privatepart')
                {
                    Load_DataTables("privatepart");
                    
                }
                else if(record_table == 'iw_tns_bust')
                {
                    Load_DataTables("bust");
                    
                }
                else if(record_table == 'iw_tns_nationality')
                {
                    Load_DataTables("nationality");
                    
                }
                else if(record_table == 'iw_tns_points')
                {
                    Load_DataTables("pointview");
                    
                }
                else if(record_table == 'iw_tns_advertise')
                {
                    Load_DataTables("advertiseview");
                    rightPopupclose();
                    
                }


            } else {
                //console.log("test");
            }

        }
    });
}

/** Remove Recrod **/
$(document).on('click', '.remove_recrod', function() {
    var thiz = $(this);

    alertify.confirm('Are you sure, Do you want delete this ?',
        function() {
            var record_id = $(thiz).attr("data-id");
            var record_table = $(thiz).attr("data-table");
            var record_column = $(thiz).attr("data-column");
            var fileName = $(thiz).attr("data-file");
           console.log(record_id);
           console.log(record_table);
           console.log(record_column);
           console.log(fileName);
                deleteRecords(record_id, record_table, record_column, fileName);
          }
    );
});
// idprrof remove girls
function girlsIdproofRemove(record_id, record_table, record_column, fileName) {
    record_id = typeof(record_id) == 'undefined' ? '' : record_id;
    record_table = typeof(record_table) == 'undefined' ? '' : record_table;
    record_column = typeof(record_column) == 'undefined' ? '' : record_column;
    fileName = typeof(fileName) == 'undefined' ? '' : fileName;
    $.ajax({
        url: 'ajax-actions_removeRecord_girls',
        dataType: 'json',
        type: 'post',
        data: {
            record_id: record_id,
            record_table: record_table,
            record_column: record_column,
            fileName: fileName
        },
        success: function(res) {
            //return true;
            console.log(res);
            if (res.result.status == "success") {
                


            } else {
                //console.log("test");
            }

        }
    });
}