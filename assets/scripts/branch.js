
/****** Branch creation Submit ******/
/* Arguments -  title,category,Adddress,place,postcode,canton,email,phone,Website,leader,leader Email,leader Mobile*/
$(document).on('click', '.branchSubmit', function(e) {
  //alert("branchSubmit");
    var formss = $('form#brnachForm');
    var form_data = new FormData(formss[0]);
    var titel = $('#titel').val();
    var category = $('#category').val();
    var branch_address = $('#branch_address').val();
    var place = $('#place').val();
    var postcode = $('#postcode').val();
    var canton = $('#canton').val();
    var email = $('#email').val();
    var phone = $('#phone').val();
    var website = $('#website').val();
    var leader = $('#leader').val();
    var leadermobile = $('#leadermobile').val();
    var leaderemail = $('#leaderemail').val();
    var button_color = $('#button_color').val();
    //var formData=$('#userForm').serialize();
    if (button_color == '') {
        $('#button_color').closest('.form-group').find('.mandatory').html('Wähle Farbe');
        $('#button_color').focus();
        return false;
    }
    if (titel == '') {
        $('#titel').closest('.form-group').find('.mandatory').html('Filialtitel eingeben');
        $('#titel').focus();
        return false;
    }
    else if (category == '') {
        $('#category').closest('.form-group').find('.mandatory').html('Kategorie wählen');
        $('#category').focus();
        return false;
    } 
    else if (branch_address == '') {
        $('#branch_address').closest('.form-group').find('.mandatory').html('Geben Sie die richtige Adresse ein');
        $('#branch_address').focus();
        return false;
    } 
    else if (postcode == '') {
        $('#postcode').closest('.form-group').find('.mandatory').html('Geben Sie die richtige Adresse ein');
        $('#postcode').focus();
        return false;
    } 
    else if (place == '') {
        $('#place').closest('.form-group').find('.mandatory').html('Geben Sie die richtige Adresse ein');
        $('#place').focus();
        return false;
    } 
    else if (canton == '') {
        $('#canton').closest('.form-group').find('.mandatory').html('Geben Sie die richtige Adresse ein');
        $('#canton').focus();
        return false;
    } 
    /*else if (!ValidateEmail(email) || email == '') {
        $('#email').closest('.form-group').find('.mandatory').html('E-Mail-Nummer eingeben');
        $('#email').focus();
        return false;
    }*/
    else if (!validateMobile(phone) || phone == '') {
        $('#phone').closest('.form-group').find('.mandatory').html('Telefonnummer eingeben');
        $('#phones').focus();
        return false;
    } /*else if (leader == '') {
        $('#leader').closest('.form-group').find('.mandatory').html('wähle Leiter');
        $('#leader').focus();
        return false;
    } */
    /*else if (leadermobile == '') {
        $('#leadermobile').closest('.form-group').find('.mandatory').html('wähle Leiter');
        $('#leadermobile').focus();
        return false;
    } 
    else if (leaderemail == '') {
        $('#leaderemail').closest('.form-group').find('.mandatory').html('wähle Leiter');
        $('#leaderemail').focus();
        return false;
    } */
     else if (old_branch_image == ''&& branchimage=="") {
        ('.imagecheck').html('Zweigbild hinzufügen');
        return false;
    }  
    else {
      console.log("testdtata");
      $(".branchSubmit").prop('disabled', true);
       $.ajax({
            url: 'ajax-actions_branchcreate',
            method: 'POST',
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,   
            success: function(res) {
                console.log(res);
                //alert(res.branchid);
                if (res.status == 'success' && res.result.status == 'success') {
                  var branchid =res.result.branchid;
                  //alert(branchid);
                 location.href='filialedetails?branchid='+branchid; 
                   //location.href='filiale'; 
                    //alertify.alert('Erfolgreich erstellt');
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('Der Filialname wird bereits beendet');
                    return false;
                 }
                 else {
                    console.log("errror page");
                    alertify.alert('ungültiger Prozess erneut versuchen');
                    return false;
                }
            }
        });
    }
    return false;

});

function getleaderdata()
{
    var leader = $('#leader').val();
    console.log(leader);
    $.ajax({
            url: 'ajax-actions_getleader',
            method: 'POST',
            data: {
                userid: leader
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
               console.log(res.result.leaderdetails[0].email);
                if (res.status == 'success' && res.result.status == 'success') {
                    $('#leadermobile').val(res.result.leaderdetails[0].mobile);
                    $('#leaderemail').val(res.result.leaderdetails[0].email);
                } else {
                    
                }
            }
        });
    return false;
    

}
//branch local auth append
$(document).on('click', '.localauthAdd', function(e) {
  console.log("test"+local_auth);
  local_auth = parseInt(local_auth)+parseInt(1);
  //var addcount=$("#localapproval").val();
  //var flcount = parseInt(addcount)+parseInt(1);
     $("#localapprove").append('<div id="lc'+local_auth+'"><div class="form-row"><input type="hidden" value="'+local_auth+'" id="localapproval" name="localapproval"><input type="hidden" value="nodata" id="old_file" name="old_file"> <input type="hidden" value="" id="localid" name="localid"><div class="col-lg-4 col-md-4"><div class="position-relative form-group"><label for=""><span>Bewilligung von</span></label><input type="text" class="form-control swissSdate" value="" name="localSdate'+local_auth+'" readonly></div></div><div class="col-lg-4 col-md-4"><div class="position-relative form-group"><label for=""><span>Bewilligung bis</span></label><input type="text" class="form-control swissSdate" value=""  name="localEdate'+local_auth+'" readonly></div></div><div class="col-lg-2 col-md-2"><div class="position-relative form-group"><label for=""><span>Datei hochladen</span></label><input type="file" class="form-control" name="localDoc'+local_auth+'" accept="application/pdf"></div></div><div class="col-lg-2 col-md-2"><div class="position-relative form-group"><label for="">&nbsp;</label><a href="javascript:void(0)" class="btn btn-lg btn-block btn-primary localauthRemove" data-id="lc'+local_auth+'" >Löschen</a></div></div></div></div>');
      //$("#localapproval").val(flcount);
      dateAuto_startdate();
      dateAuto_enddate();



});
//branch authu append
$(document).on('click', '.branchauthAdd', function(e) {
  console.log("test"+branch_auth);
  branch_auth = parseInt(branch_auth)+parseInt(1);
  //alert(branch_auth);
    $("#branchauth").append('<div id="ba'+branch_auth+'"><input type="hidden" value="'+branch_auth+'" id="localapprovalpermit" name="localapprovalpermit"><input type="hidden" value="nodata" id="old_file_permit" name="old_file_permit"><input type="hidden" value="abc" id="localpermitid" name="localpermitid"><div class="form-row"><div class="col-lg-3 col-md-3"><div class="position-relative form-group"><label for="">Bewilligung von</label><input type="text" class="form-control swissSdate" value=""  name="permissionSdate'+branch_auth+'" readonly></div></div><div class="col-lg-3 col-md-3"><div class="position-relative form-group"><label for="">Bewilligung bis</label><input type="text" class="form-control swissSdate" value=""  name="permissionEdate'+branch_auth+'" readonly></div></div><div class="col-lg-2 col-md-2"><div class="position-relative form-group"><label for="">Verantwortliche</label><select type="select" name="permissionName'+branch_auth+'" class="custom-select">'+namelistdata+'</select></div></div><div class="col-lg-2 col-md-2"><div class="position-relative form-group"><label for=""><span class="lang_Uploadfile">Datei hochladen</span></label><input type="file" class="form-control" name="localDocpermit'+branch_auth+'" accept="application/pdf"></div></div><div class="col-lg-2 col-md-2"><div class="position-relative form-group"><label for="">&nbsp;</label><a href="javascript:void(0)" class="btn btn-lg btn-block btn-primary branchauthRemove" data-id="ba'+branch_auth+'" >Löschen</a></div></div></div></div>');
      dateAuto_startdate();
      dateAuto_enddate();
});
// Remove Branch Auth
$(document).on('click', '.branchauthRemove', function(e) {
  console.log("Remove"+branch_auth);
  var thiz = $(this);
  var remove_id = $(thiz).attr("data-id");
   console.log("Remove"+remove_id);
  $("#"+remove_id).remove();

});
// Remove Branch Auth
$(document).on('click', '.localauthRemove', function(e) {
  console.log("Removec"+local_auth);
  var thiz = $(this);
  var remove_id = $(thiz).attr("data-id");
  console.log("Remove"+remove_id);
  $("#"+remove_id).remove();
  //var addcount=$("#localapproval").val();
  //var flcount = parseInt(addcount)-parseInt(1);
  //$("#localapproval").val(flcount);

});
//rooms  append
$(document).on('click', '.roomsAdd', function(e) {
  console.log("roomtest"+room_auth);
  room_auth = parseInt(room_auth)+parseInt(1);
    $("#roomdata").append('<div class="col-lg-3 col-md-4" id="rc'+room_auth+'"><input type="hidden" value="'+room_auth+'" id="roomapproval" name="roomapproval"><input type="hidden" value="nodata" id="old_file_room" name="old_file_room"><input type="hidden" value="" id="roomid" name="roomid"><div class="position-relative"><div class="uploader fileup'+room_auth+'" ><div class="action-icons"><span data-toggle="tooltip" data-original-title="Löschen" class="del btn-icon btn-icon-only btn btn-danger"><i class="pe-7s-trash btn-icon-wrapper removeImagefilebranch" data-id="fileup'+room_auth+'" data-deid="rc'+room_auth+'" data-file="" data-fileid=""> </i></span></div><img src="../images/dropzone.png"/> <input type="file" name="roomimage'+room_auth+'"  id="filePhoto'+room_auth+'" class="fPhoto filePhoto'+room_auth+'"  data-id="filePhoto'+room_auth+'" data-imval="fileup'+room_auth+'" /></div></div><div class="position-relative form-group mt-3"><input type="text" class="form-control" name="roomtitle'+room_auth+'" placeholder="Titel"></div><div class="position-relative form-group"><textarea rows="3" class="form-control" name="roomdescription'+room_auth+'" placeholder="Beschreibung"></textarea></div></div>');

});
/* Image loader Function */
var comval ="";
$(document).on('click', '.fPhoto', function(e) {
  console.log("image test");
  var thiz = $(this);
  var id = $(thiz).attr("data-id");
  var imval = $(thiz).attr("data-imval");
  comval = $(thiz).attr("data-imval");
  console.log("comval"+comval);
  var imageLoader = document.getElementById(id);
if (imageLoader) {
    console.log(imageLoader);
    imageLoader.addEventListener('change', handleImage, false);
}
else
{
  console.log("nodata");
}

});
function handleImage(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
        console.log(event);
        $('.'+comval+' img').attr('src',event.target.result);
    }
    reader.readAsDataURL(e.target.files[0]);
}
//image remove functionality
$(document).on('click', '.removeImagefilebranch', function(e) {
  var thiz = $(this);
  var fileName = $(thiz).attr("data-file");
  var record_id = $(thiz).attr("data-fileid");
  var iddata = $(thiz).attr("data-deid");
  //alert(iddata+"\\"+fileName+"\\"+record_id);
 //$( '#'+iddata).remove();
 // $('.'+iddata+' img').attr('src',"../images/dropzone.png");
  if(fileName=="" && record_id==""){
    $( '#'+iddata).remove();
  }
  else
  {
    alertify.confirm('Möchten Sie dies löschen?', function() {
        $('#'+iddata).remove();
        deleteRecords(record_id, 'iw_tns_room', 'id', fileName);
        
    });
  }
  
});
// google address get function
function branchAddressget() {
     var inputbranch = document.getElementById('branch_address');
     if(inputbranch)
     {
        console.log(inputbranch);
        
        var autocomplete = new google.maps.places.Autocomplete(inputbranch);
        console.log(autocomplete);
         google.maps.event.addListener(autocomplete, 'place_changed', function () {
          var place = autocomplete.getPlace();
            if(autocomplete.getPlace() !== undefined) {
                $("#branch_address").val(autocomplete.getPlace().formatted_address);
                console.log("googlesearchtesting");
                console.log(autocomplete.getPlace().formatted_address);
                  for (var i = 0; i < place.address_components.length; i++) {
                       //console.log(place.address_components[i]);
                    for (var j = 0; j < place.address_components[i].types.length; j++) {
                      //console.log(place.address_components[i].types[j]);
                          if (place.address_components[i].types[j] == "postal_code") {
                            console.log(place.address_components[i].long_name);
                            if(place.address_components[i].long_name !== undefined)
                            {
                              $("#postcode").val(place.address_components[i].long_name);
                            }
                            
                          }
                          if (place.address_components[i].types[j] == "locality") {
                            //document.getElementById('postal_code').innerHTML = place.address_components[i].long_name;
                              console.log(place.address_components[i].long_name);
                              if(place.address_components[i].long_name !== undefined)
                              {
                                  $("#place").val(place.address_components[i].long_name);
                              }
                          }
                          if (place.address_components[i].types[j] == "administrative_area_level_1") {
                              if(place.address_components[i].long_name !== undefined)
                              {  
                                  console.log(place.address_components[i].long_name);
                                  $("#canton").val(place.address_components[i].long_name);
                              }
                          }
                    }
                  }
                  $('#addressDetails').show();
                //googlesearchdata();
                //console.log(autocomplete.getPlace().formatted_address);
            }
          });
     }
  }
google.maps.event.addDomListener(window, 'load', branchAddressget);
// colorpicker
$('#button_color').colorpicker().on('changeColor',function(e) {
  var color=e.color.toHex();
  //$('#api_andern').css('background',color);
});
// Branch listiing
$(document).on('click', '.branchlistfun', function() {

    var vctitle = $('#vctitle').val();
    var vccategory = $('#vccategory').val();
    var vcplace = $('#vcplace').val();
    var vcroom = $('#vcroom').val();
    var vcleader = $('#vcleader').val();
    var st=0;
    var et=10;
     var limit = st+ ',' + et;
        $.ajax({
            type: 'POST',
             url: 'ajax-actions_branchList',
            data: {
                limit: limit,
                vctitle: vctitle,
                vccategory: vccategory,
                vcplace: vcplace,
                vcroom: vcroom,
                vcleader: vcleader,
            },
            dataType: 'JSON',
            success: function(res) {
              console.log(res);

                $('.total-branch').html(res.result.html);
            }
        });
    
    return false;

});
$(document).on('click', '.removebranchFilter', function() {
    console.log("sdfbdsf");
    $('#vctitle').val('all');
    $('#vccategory').val('all');
    $('#vcplace').val('all');
    $('#vcroom').val("");
    $('#vcleader').val('all');
    $("#show-filter").removeClass("show");
    $(".branchlistfun").trigger('click');
    
});
// window scroll function
$(window).scroll(function() {
    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
        var div_count = $('.blist').length;
        //alert(div_count);
        if (div_count >= 10) {
            var start = div_count;
                var end = div_count + 10;
                var limit = start + ',' + end;
                var vctitle = $('#vctitle').val();
                var vccategory = $('#vccategory').val();
                var vcplace = $('#vcplace').val();
                var vcroom = $('#vcroom').val();
                var vcleader = $('#vcleader').val();
                console.log("vctitle"+vctitle);
                $.ajax({
                    type: 'POST',
                     url: 'ajax-actions_branchList',
                    data: {
                        limit: limit,
                        vctitle: vctitle,
                        vccategory: vccategory,
                        vcplace: vcplace,
                        vcroom: vcroom,
                        vcleader: vcleader,
                    },
                    dataType: 'JSON',
                    success: function(res) {
                      console.log(res);

                        $('.total-branch').append(res.result.html);
                    }
                });
                return false;
         }
    }
});