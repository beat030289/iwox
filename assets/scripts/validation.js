
//dotnumber
$(document).on('keypress','input.dotnumber',function(e){            
	if(e.which!=8 && e.which!=0 && e.which!=46  && (e.which<48 || e.which>57)) return false; 
}); 

$(document).on('keypress','input.no-entry',function(e){            
	return false;
});  

//alphanumber
$(document).on('keypress','input.alphanumber',function(e){
	var specialKeys = new Array();
	specialKeys.push(8); 
	specialKeys.push(9); 
	specialKeys.push(46); 
	specialKeys.push(36); 
	specialKeys.push(35); 
	specialKeys.push(37); 
	specialKeys.push(39);
	var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
	var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
	if(!ret) return false; 
}); 

//numbersonly
$(document).on('keypress','input.numbersOnly',function(e){ 
	var $this = $(this);
	attr = $this.attr('maxlength');
	if(e.which!=8 && e.which!=0 && (e.which<48 || e.which>57)) return false;
});

/* Validate email address */
function ValidateEmail(email) {
var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
return expr.test(email);
};
/* Validate mobile numbers */
function validateMobile(mobile) {
    var pattern = /^[\+\d]+(?:[\d-.\s()]*)$/;
	//var pattern = /^(0[1-9]|[12][0-9]|3[01])[.](0[1-9]|1[012])[.](19|20)\d\d$/;
	return pattern.test(mobile);
}
/* datecheck */
function validateDate(date) {
    var pattern =  /^\d{2}([.])\d{2}\1\d{4}$/;
    //var pattern = /^[0-9-+]+$/;
    return pattern.test(date);
}
// date format function
function data_change_format(str)
{
    console.log(str);
    var fDate = "";
    if(str!="" && str!=undefined && str!=null)
     {
        var res = str.split(".");
        fDate = res[2]+'-'+res[1]+'-'+res[0];
     }
     
    return fDate;
}
// date format function
function data_change_format_reverse(str)
{
    console.log(str);
    var fDate = "";
    if(str!="" && str!=undefined && str!=null)
     {
        var res = str.split("-");
        fDate = res[2]+'.'+res[1]+'.'+res[0];
     }
     
    return fDate;
}
/* Validate recaptcha */
function captchaValidate()
{
	if(grecaptcha.getResponse() == "") {
	 return false;
	}else{
	  return true;
	}
}

//statedate enddate checking
var date = new Date();
var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
var end = new Date(date.getFullYear(), date.getMonth(), date.getDate());

// $("#start_date").datepicker({
// 	format: 'dd-mm-yyyy',
// 	todayBtn:  1,
// 	todayHighlight: true,
// 	startDate: today,
// 	autoclose: true,
// }).on('changeDate', function (selected) {
// 	var minDate = new Date(selected.date.valueOf());
// 	$('#end_date').datepicker('setStartDate', minDate);
// 	$('#end_date').val('');

// });

// $("#end_date").datepicker({
// 	format: 'dd-mm-yyyy',
// 	todayHighlight: true,
// 	startDate: '+0d',
// }).on('changeDate', function (selected) {
// 	var minDate = new Date(selected.date.valueOf());
// 	$('#start_date').datepicker('setEndDate', minDate);
// });

/* timepicker */
// $('.time-picker1').timepicker({'timeFormat':'H:i'});

// $('.time-picker2').timepicker({
// 	'minTime': '12:00am',
// 	'timeFormat':'H:i'
// });

// $('.time-picker1').on('changeTime', function() {
// 	$('.time-picker2').timepicker('option', 'minTime', $(this).val());
// });

/* Validate url */
function validateURL(textval) {
  var urlregex = new RegExp( "^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
  return urlregex.test(textval);
}

/* Validate password Strength */
function checkStrength(password){
    //initial strength
	var strength = 0
	//if the password length is less than 6, return message.
	if (password.length < 5) { 
		return 'Too short' 
	}
	
	//length is ok, lets continue.
	//if length is 8 characters or more, increase strength value
	if (password.length > 5) strength += 1
	//if password contains both lower and uppercase characters, increase strength value
	if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1
	//if it has numbers and characters, increase strength value
	if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))  strength += 1 
	//if it has one special character, increase strength value
	if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1
	//if it has two special characters, increase strength value
	if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
	//now we have calculated strength value, we can return messages
	//if value is less than 2
	if (strength < 2 )
	{
		return 'Weak'			
	}
	else if (strength == 2 )
	{
		return 'Good'		
	}
	else
	{
		return 'Strong'
	}
}


/* Print functionality */
function printBillEntry(contant){ 
	var mywindow = window.open('', 'PRINT', 'height=500,width=700');   
    mywindow.document.write('<html><head><title> </title><link rel="stylesheet" type="text/css" href="css/print-style.css">');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1> </h1>');
    mywindow.document.write(contant);
    mywindow.document.write('</body></html>'); 
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10 
    mywindow.print();
    mywindow.close();
}

/* Add comma before/after digit */
function addCommas(num) {
    var str = num.toString().split('.');
    if (str[0].length >= 4) {
        //add comma every 3 digits befor decimal
        str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
    }
    /* Optional formating for decimal places
    if (str[1] && str[1].length >= 4) {
        //add space every 3 digits after decimal
        str[1] = str[1].replace(/(\d{3})/g, '$1 ');
    }*/
    return str.join('.');
}

/* Password eye icon click */
$(document).on('click','.pass-eye',function(){  
	var thiz = $(this);
	var pwd_field = thiz.closest('div').find('input');									
	if(thiz.hasClass('fa-eye')){
		pwd_field.attr('type','password');
		thiz.removeClass('fa-eye').addClass('fa-eye-slash');
	} else {
		pwd_field.attr('type','text');
		thiz.removeClass('fa-eye-slash').addClass('fa-eye');
	}
});

/*Calculate days*/
function dayCount(start_date,end_date){
	var start = new Date(start_date.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
	var end = new Date(end_date.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
	//alert(datediff(parseDate(start), parseDate(end)));
	var oneDay = 24*60*60*1000;
	var diff = 0;
	if(start_date==end_date){
		  diff = 0;
	} else {
		if (start && end) {
			diff = Math.round(Math.abs((end.getTime() - start.getTime())/(oneDay)));
		} else {
			diff = 0;
		}
	}
	
	return diff*24;
	
}

function checkEmpty(value){ 
	return value=='' || value === undefined ?false:value;
}
function checkNaN(value){
	return value=='' || value === undefined || isNaN(value)?0:value;
}
function toDecimal(value,decimal=2){
	return value.toFixed(decimal);
}

function confirmation(msg,callback_function) {
	alertify.confirm(msg,
		function(){
			var func = new callback_function();
		},
		function(){
	});
}
$(document).on('click','.go-back',function(){
    goBack();
});
function goBack() {
    window.history.go(-1);
}

/* filetype validation */
// var file = this.files[0];
// var fileType = file["type"];
// var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
// if ($.inArray(fileType, validImageTypes) < 0) {
//      // invalid file type code goes here.
// }

/* Ckeditor validation */
function CheckCkeditor() 
{
    textbox_data = CKEDITOR.instances['description'].getData();
    if (textbox_data==='')
    {
        return false;
    } else {
		return true;
	}
}


/* base64to blob image */
function base64ToBlob(base64, mime) 
{
    mime = mime || '';
    var sliceSize = 1024;
    var byteChars = window.atob(base64);
    var byteArrays = [];

    for (var offset = 0, len = byteChars.length; offset < len; offset += sliceSize) {
        var slice = byteChars.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    return new Blob(byteArrays, {type: mime});
}

/* FB share */
// FB.init({
// 	appId      : '1697659693866119',
// 	status     : true,
// 	xfbml      : true
// });

$(document).on('click','.facebook_share',function(){
	var title=$(this).data('title');
	var link=$(this).data('link');
	var picture=$(this).data('picture');
	var caption=$(this).data('caption');
	var description=$(this).data('description');
	FB.ui({
		method: 'feed',
		name: title,
		link: link,
		picture: picture,
		caption: caption,
		description: description
	});
});

/*contact form */
$(document).on('click','.contact_submit',function(){

var contact_name = $('#contact_name').val();
var contact_email = $('#contact_email').val();
var contact_message = $('#contact_message').val();
var product_id = $('#product_id').val();


if(contact_name==''){
$(".err_name").text("Enter Your Name");
return false;
} else {
$('.err_name').css("display","none");
}

if(!ValidateEmail(contact_email)){
$(".err_email").text("Enter Your Valid Email");
return false;
} else {
$('.err_email').css("display","none");
}

if(contact_message==''){
$(".err_mssg").text("Enter Your Description");
return false;
} else {
$('.err_mssg').css("display","none");
}

if(contact_name!='' && contact_email!='' && contact_message!='') {
//ajax function;

}


});


/* Get Date format */
function getDateFormat(str) {
	var date=new Date(str * 1000);
	var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var month=monthNames[date.getMonth()];
	format_date=(date.getDate() + ' ' + (month) + ' ' + date.getFullYear());
	return format_date;
}


/*milisecond to time */
function msToTime(s) {
  var ms = s % 1000;
  s = (s - ms) / 1000;
  var secs = s % 60;
  s = (s - secs) / 60;
  var mins = s % 60;
  //var hrs = (s - mins) / 60;
result = offsetify(mins) + ':' + offsetify(secs) + '.' + offsetify(Math.round(ms));
  return result;
}

/*Checkbox validation */
function CheckboxValidate()
{
valid = true;
if($('input[type=checkbox]:checked').length == 0)
{
    valid = false;
}
return valid;
}


/* one video play remaining all pause */
jQuery("video").on("play", function() {
	jQuery("video").not(this).each(function(index, video) {
	video.pause();
	});
});

/* Validate youtube url */
function validateYoutubeURL(url) {
    if (url != undefined || url != '') {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = url.match(regExp);
        return (match && match[2].length == 11)?true:false;
    }
}

/* Get randomnumber */
function getRandomNumber() {
    var minNumber = 1000000;
    var maxNumber = 999999999; // le maximum
    return Math.floor(Math.random() * (maxNumber + 1) + minNumber);
}

/* Limit  for content */
function getLimitText(content,showChar) {
    if(content.length > showChar) {
        var c = content.substr(0, showChar);
        //var h = content.substr(showChar-1, content.length - showChar);
        return c;
    }
    return content;
}

/*My reference drag and drop and fileupload*/

var upload_file = [];
$(document).ready(function(){
    window.addEventListener("dragover",function(e){
        e = e || event;
        e.preventDefault();
    },false);
    window.addEventListener("drop",function(e){
        e = e || event;
        e.preventDefault();
    },false);
    $('.drag-container').on('dragenter',function (e) {
        /* e.stopPropagation(); e.preventDefault(); */
        //$(".drag-container .drag-title").text("Drop");
    });

    // Drag over
    $('.drag-container').on('dragover', function (e) {
        /* e.stopPropagation(); e.preventDefault(); */
        //$(".drag-container .drag-title").text("Drop");
        $(".drag-container").addClass('drag-over');
    });
    
    $('.drag-container').on('dragleave', function (e) {
        /* e.stopPropagation(); e.preventDefault(); */
        //$(".drag-container .drag-title").text("Drop");
        $(".drag-container").removeClass('drag-over');
    });
    
    // Drop
    /* $('.drag-container').on('drop', function (e) { 
        e.stopPropagation();
        e.preventDefault();
        $(".drag-container").removeClass('drag-over');
        var files = e.originalEvent.dataTransfer.files;
        fileUpload(files,0);
    });    
    $("#uploadFile").change(function(){
        $('.multipleupload-section,.multipleupload-section-submit').hide();
        var files = document.getElementById("uploadFile").files;
        fileUpload(files,0);
    }); */
    // Open file selector on div click
    //$("div#uploadfile").click(function(){ 
    $(document).on('click touchstart touchend','.drag-container .upload-area',function(e){
        e.preventDefault();
        //$("input#uploadFile").click();
        //$('form#mediaUpload #mediaFile').trigger('click');
        $('form.upload-media #mediaFile').click();
        //$('.multipleupload-section,.multipleupload-section-submit').hide();
    });
});

var auto=0;
var array=[];
var files = '';
function fileUpload(uploadFiles,current) {
    if(uploadFiles!='') {
        files = uploadFiles;
    }
    var totlcnt = $('#upload-preview li.media-preview').length;
    console.log('total count'+totlcnt+' '+(totlcnt<=9 && files[current]!=undefined));
    if(totlcnt<=9 && files[current]!=undefined){
        getFileInfo(files[current],current);
        upload_file.push(files[current]);
        setTimeout(function(){ enableUpload(); },1500);
    }
    else
        return false;
	
	/* $.each(files, function(key, value) { 
		auto=auto+1;
		
		var validmedia = upload_file.length; 
		var delete_cnt = $(this).attr('data-delete');
		//var divcnt=$(".data-cnt").length;
		console.log(delete_cnt);
		if(delete_cnt){
			totlcnt=totlcnt-delete_cnt;
		}
		setTimeout(function(){
			var totlcnt = $('#upload-preview .drag-content').length;
			console.log(totlcnt);
			if(totlcnt<=9){
			 
				getFileInfo(value,auto);
				totlcnt++;
				var total_div=$(".drag-content").length;
				var invalid=$(".drag-content-invalid").length; 
				if(total_div==invalid){
					$('.multipleupload-section-submit').hide();
					$('.multipleupload-section').hide();
				}
			}
		},1500);
	});  */
}
function getFileInfo(input,array_value) {
    clearErrorMessage();
    upload_content = '';
    //var inputFile = input.files;
    var inputFile = input;
    if (inputFile) {
        current_index = parseInt($('#upload-preview input[name="removeFile"]').length);
        filesize = parseInt(inputFile.size);
        if(checkExtension(inputFile.name)) { 
            if(filesize <= 20000000) {
                $('.file-info,.uploaded-section').fadeIn();
                $('.uploaded-section #file_name').append(inputFile.name);
                $('.uploaded-section #file_size').append(bytesToSize(filesize));
                var reader = new FileReader();
                var remove_id=parseInt(current_index)-parseInt(1);
                //if(inputFile.type.search('video.*')==-1) {
                if(!inputFile.type.match('video.*')) {
                    reader.onload = function(e){ 
                    appendInfo(current_index,e.target.result,'image',1);
                    
                    /* $('#upload-preview').append('<div class="drag-content drag-content-valid" data-index="'+current_index+'"><div class="drag-icon"><i></i><i aria-hidden="true" class="fa fa-check pull-right check_'+current_index+'" style="color:green;display:none;"></i><i data-id="'+current_index+'" class="fa fa-refresh rotate-image rotate_'+current_index+'" aria-hidden="true"></i><i data-remove="'+remove_id+'" data-delete="1" data-id="'+current_index+'" class="fa fa-trash-o fa-trash-o remove-image remove_'+current_index+'" style="color:red"></i><div class="progress progressdiv_'+current_index+'" style="display:none;"><div class="progress-bar progress-bar-striped progress-bar-animated bg-success progress_'+current_index+'" style="width:50%"></div></div></div><div class="image-container"><img class="img-drag thumbnail rotate-normal" src="'+e.target.result+'" /></div><input type="hidden" name="imagerotate['+current_index+']" class="imagerotate" id="imagerotate_'+current_index+'" value="0"><div class="caption-content caption_content_'+current_index+'" data-editable="true"><span class="caption-sapn"><p class="capture-by">Added by :'+name+'</p></span><span class="confirm" style="display:none;">Error message for upload</span><input type="hidden" maxLength="25" name="captiondrag['+current_index+']" class="captiondrag" id="captiondrag_'+current_index+'" value="Added by :'+name+'" maxLength></div><input type="hidden" name="multifiles[]" id="rem_'+current_index+'" value="'+e.target.result+'"><input type="hidden" name="invalid_file"  class="invalid0" id="invalid_file_'+current_index+'" value="0"></div><input type="hidden" name="removeFile" id="removeFile_'+current_index+'" value="0">').fadeIn(); */
                    }
                    reader.readAsDataURL(inputFile);
                }
                else {
                    if(getExtension(inputFile.name)=='flv' || getExtension(inputFile.name)=='mov' || (isIE() && getExtension(inputFile.name)=='webm')) {
                        appendInfo(current_index,inputFile,getExtension(inputFile.name),'valid');
                        
                    upload_content += '<div class="drag-content drag-content-valid"><div class="drag-icon"><i></i><i aria-hidden="true" class="fa fa-check pull-right check_'+current_index+'" style="color:green;display:none;"></i><i data-remove="'+remove_id+'" data-id="'+current_index+'" data-delete="1" class="fa fa-trash-o remove-image remove_'+current_index+'" style="color:red"></i><div class="progress progressdiv_'+current_index+'" style="display:none;"><div class="progress-bar progress-bar-striped progress-bar-animated bg-success progress_'+current_index+'" style="width:50%"></div></div></div><div class="image-container">Preview not available. You can view it only after upload</div><div class="caption-content caption_content_'+current_index+'" data-editable="true"><span class="caption-sapn"><p class="capture-by">Added by :'+name+'</p></span><span class="confirm" style="display:none;">Error message for upload</span><input type="hidden" maxLength="25" name="captiondrag['+current_index+']" class="captiondrag" id="captiondrag_'+current_index+'" value="Added by :'+name+'" maxlength><input type="hidden" class="invalid0" name="invalid_file" id="invalid_file_'+current_index+'" value="0"></div></div><input type="hidden" name="removeFile" id="removeFile_'+current_index+'" value="0">';
                }
                else {
                        appendInfo(current_index,inputFile,getExtension(inputFile.name),'valid');
                    upload_content += '<div class="drag-content drag-content-valid "><div class="drag-icon"><i></i><i aria-hidden="true" class="fa fa-check pull-right check_'+current_index+'" style="color:green;display:none;"></i><i data-remove="'+remove_id+'" data-id="'+current_index+'" data-delete="1" class="fa fa-trash-o remove-image remove_'+current_index+'" style="color:red"></i><div class="progress progressdiv_'+current_index+'" style="display:none;"><div class="progress-bar progress-bar-striped progress-bar-animated bg-success progress_'+current_index+'" style="width:50%"></div></div></div><div class="image-container"><video width="100%" controls id="video_preview_'+current_index+'" class="up-video" data="'+current_index+'"><source src="'+ URL.createObjectURL(inputFile)+'" />Your browser does not support HTML5 video.</video></div><div class="caption-content caption_content_'+current_index+'" data-editable="true"><span class="caption-sapn"><p class="capture-by">Added by :'+name+'</p></span><span class="confirm" style="display:none;">Error message for upload</span><input type="hidden" maxLength="25" name="captiondrag['+current_index+']" class="captiondrag" id="captiondrag_'+current_index+'" value="Added by :'+name+'" maxlength><input type="hidden" class="invalid0" name="invalid_file" id="invalid_file_'+current_index+'" value="0"></div></div><input type="hidden" name="removeFile" id="removeFile_'+current_index+'" value="0">';
					}
					//$('#upload-preview').append(upload_content);
					
					/*var $source = $('#video_preview_'+array_value+' source');
					$source[0].src = URL.createObjectURL(inputFile);
					$source.parent()[0].load();*/
					
				}
			}
			else {
				var invalid_error=getMessage(59);
				//$('.image-upload').after('<div class="panel panel-default validation-error"><div class="panel-body">'+size_error+'</div></div>');
				//showHideUpdateForm(2);
				//$('#upload-preview').html('');
                appendInfo(array_value,inputFile.name,'media',invalid_error);
                
				upload_content += '<div class="drag-content drag-content-invalid"><div class="drag-icon"><i></i><i aria-hidden="true" class="fa fa-check pull-right check_'+array_value+'" style="color:green;display:none;"></i><i data-remove="'+remove_id+'" data-id="'+array_value+'" class="fa fa-trash-o remove-image remove_'+array_value+'" style="color:red"></i><div class="progress progressdiv_'+array_value+'" style="display:none;"></div></div><div class="image-container">'+inputFile.name+'</div><div class="caption-content caption_content_'+array_value+'" data-editable="false"><span class="confirm" style="">'+invalid_error+'</span><input type="hidden" maxLength="25" name="captiondrag['+array_value+']" class="captiondrag" id="captiondrag_'+array_value+'" value="Added by :'+name+'" maxlength><input type="hidden" class="invalid1" name="invalid_file" id="invalid_file_'+array_value+'" value="1"></div></div><input type="hidden" name="removeFile" id="removeFile_'+array_value+'" value="0">';
				//$('#upload-preview').append(upload_content);
			}
		}
		else { console.log('invalid format');
            var invalid_error=getMessage(58);
            appendInfo(array_value,inputFile.name,'media',invalid_error);
			upload_content += '<div class="drag-content drag-content-invalid"><div class="drag-icon"><i></i><i aria-hidden="true" class="fa fa-check pull-right check_'+array_value+'" style="color:green;display:none;"></i><i data-remove="'+remove_id+'" data-id="'+array_value+'" class="fa fa-trash-o remove-image remove_'+array_value+'" style="color:red"></i><div class="progress progressdiv_'+array_value+'" style="display:none;"></div></div><div class="image-container">'+inputFile.name+'</div><div class="caption-content caption_content_'+array_value+'" data-editable="false"><span class="confirm" style="">'+invalid_error+'</span><input type="hidden" maxLength="25" name="captiondrag['+array_value+']" class="captiondrag" id="captiondrag_'+array_value+'" value="Added by :'+name+'" maxlength><input type="hidden" class="invalid1" name="invalid_file" id="invalid_file_'+array_value+'" value="1"></div></div><input type="hidden" name="removeFile" id="removeFile_'+array_value+'" value="0">';
			//$('#upload-preview').append(upload_content);
			//showHideUpdateForm(2);
			//$('#upload-preview').html('');
		}
		getPlatform();
        $('.multipleupload-section').show();
        $('.multipleupload-section-submit').show();
        
    }
	array_value = array_value+1;
	setTimeout(function() {
        fileUpload('',array_value);
	},1000);
}

/*Image t0 base64 */
function convertImgToBase64(url, outputFormat, callback){
	var data_img_URL='';
	var img = new Image();
	var canvas = document.createElement('CANVAS');
		var ctx = canvas.getContext('2d');
	img.onload = function(){
		
		canvas.height = this.height;
		canvas.width = this.width;
		ctx.drawImage(this,0,0);
		var dataURL = canvas.toDataURL('image/jpg' || 'image/png');
		console.log(dataURL);
		data_img_URL+=dataURL;
		canvas = null; 
	};
    img.src = url;
    callback(data_img_URL);

}

/*alphabetsonly*/
$(document).on('keypress','.alphabetsonly',function(event){
	var inputValue = event.which;
	// allow letters and whitespaces only.
	if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) { 
		event.preventDefault(); 
	}
});



/* overall form inline validation*/

var formInlineValidation=function(formID,fields,callback){
	$('span.error-message').remove();
	var ret=true;
	for (var key in fields) {
		if (fields.hasOwnProperty(key)) {
			var fieldElement=$('#'+formID+' [name="'+key+'"]');
			var tagName=(fieldElement.prop('tagName')).toLowerCase();
			var inputType=tagName=='input'?fieldElement.attr('type'):'';
			var inputName=tagName=='input'?fieldElement.attr('name'):'';
			var appendElement=fieldElement;
			console.log(appendElement);
			var inputValue=fieldElement.val();
			if(inputName=='mobile')
				inputValue=inputValue.replace('+1','');
			if((inputType=='radio' || inputType=='checkbox') && $('#'+formID+' [name="'+key+'"]:checked').length==0)
			{
				appendElement=appendElement.closest('.radio-inline');
				ret=false;
				$('<span class="error-message">'+fields[key]+'</span>').insertBefore(appendElement);
				$('html, body').animate({scrollTop: fieldElement.offset().top-100}, 1000);
				break;
			}
			else if(inputValue=='')
			{
				ret=false;
				$('<span class="error-message">'+fields[key]+'</span>').insertBefore(appendElement);
				$('html, body').animate({scrollTop: fieldElement.offset().top-100}, 1000);
				break;
			}
			else if(inputType=='email' && !validateEmail(inputValue))
			{
				ret=false;
				$('<span class="error-message">Invalid email</span>').insertBefore(appendElement);
				$('html, body').animate({scrollTop: fieldElement.offset().top-100}, 1000);
				break;
			}
			else if(inputName=='mobile' && (inputValue).length!=10)
			{
				ret=false;
				$('<span class="error-message">Invalid mobile no.</span>').insertBefore(appendElement);
				$('html, body').animate({scrollTop: fieldElement.offset().top-100}, 1000);
				break;
			}
			
		}
	}
	callback(ret);
};


$(document).on('keyup', '.copy-txt-validate', function() {
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});
$(window).load(function() {
 //Ldata();
});
 function Ldata()
 {
      var languages=appLanguage.en;
 console.log("loadLanguage");
 console.log(languages);
 for(classs in languages)
        //console.log(classs);
        //console.log(languages[classs]);
     $('.'+classs).html(languages[classs]);
 }

/* Language changes */
// setTimeout(function(){
// 	loadLanguage();
// },500);

// function loadLanguage()
// {
// 	var languages=appLanguage.de;
// 	console.log("loadLanguage");
// 	//console.log(languages);
// 	for(classs in languages)
//         //console.log(classs);
//         //console.log(languages[classs]);
// 		$('.'+classs).html(languages[classs]);
// }
// $(document).ready(function() {
//   // The default language is English
//   console.log("testdata");
//   var lang = "en";
//   console.log(lang);
//   $(".lang").each(function(index, element) {
//     $(this).text(appLanguage[lang][$(this).attr("key")]);
//     console.log(appLanguage[lang][$(this).attr("key")]);
//   });
// });
/*$(window).load(function() {
     console.log("testdata");
  var lang = "de";
  //console.log(lang);
  // $(".lang").each(function(index, element) {
  //   //$(this).text(appLanguage[lang][$(this).attr("key")]);
  //   $(this).text(appLanguage[lang][$(this).attr("key")]);
  //   console.log(appLanguage[lang][$(this).attr("key")]);
  // });

    var languages=appLanguage.en;
    console.log("loadLanguage");
    console.log(languages);
    for(classs in languages)
        //console.log(classs);
        console.log(languages[classs]);
    $('.'+classs).html(languages[classs]);
});*/