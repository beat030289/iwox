// Create Category
// Arguments : category
$(document).on('click', '.categorysubmit', function() {
    var categoryc = $('#categoryc').val();
    var editid = $('#editid').val();
   if(categoryc=="")
    {
        $('#categoryc').closest('.form-group').find('.mandatory').html('Kategorienamen eingeben');
        $('#categoryc').focus();
        return false; 
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_category',
            method: 'POST',
            data: {
                category: categoryc,
                editid: editid,
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#categoryc').val("");
                    $('#editid').val("");
                    $(".app-drawer-wrapper").removeClass("drawer-open");
                    $(".app-drawer-overlay").addClass("d-none");
                    Load_DataTables("category");
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('Der Kategoriename wird bereits beendet');
                    return false;
                 }
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});
// value remove function
$(document).on('click', '.ctypeclear', function() {
    console.log("testdata val");
    rightPopupclose();
    $('#categoryc').val("");
});
//edit Category 
$(document).on('click', '.edit_category', function() {
  /*  alert($(this).data("id"));*/
    var eid = $(this).data("id");
    if(eid=="")
    {
        alertify.alert('Ungültiger Prozess');
        
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_category_edit',
            method: 'POST',
            data: {
                id: eid
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                /*alert(res.status);
                alert(res.result.status);*/
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#categoryc').val(res.result.details[0].category);
                    $('#editid').val(res.result.details[0].id);
                    $(".app-drawer-wrapper").addClass("drawer-open");
                    $(".app-drawer-overlay").removeClass("d-none");
                    //Load_DataTables("extratype");
                 } 
                 
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});