var appLanguage= { 
  "en" : {
  	"lang_Search" : "Search",
    "lang_Dealer" : "Dealer",
    "lang_Advertise" : "Advertise",
	"lang_Contact" : "Contact",
    "lang_Signout" : "Signout",
    "lang_Myaccount" : "My account",
    "lang_Productprice" : "Products and prices",
    "lang_Advertising" : "Advertising",
    "lang_Aboutus" : "About us",
    "lang_Sitemap" : "Sitemap",
    "lang_Vehicles" : "Vehicles",
    "lang_Garages_dealers" : "Find garages & dealers",
    "lang_Newcars" : "New cars",
    "lang_Bookmarks" : "Bookmarks",
    "lang_Changepassword" : "Change Password",
    "lang_Subscription" : "Subscription",
	"lang_Evaluation" : "Evaluation",
	"lang_Advancedsearch" : "Advanced Search",
	"lang_Imprint" : "Imprint",
	"lang_Signintoreg" : "Sign in to register",
    "lang_Username" : "Username",
    "lang_Password" : "Password",
    "lang_Rememberpassword" : "Remember Password",
    "lang_Forgotpassword" : "Forgot Password",
    "lang_Company" : "Company",
    "lang_Branch" : "Branch",
    "lang_Branchadmin" : "Branch Admin",
    "lang_Date" : "Date",
    "lang_Employee" : "Employee",
    "lang_Add" : "Add",
    "lang_Edit" : "Edit",
    "lang_Delete" : "Delete",
    "lang_update" : "update",
    "lang_Address" : "Address",
    "lang_Telephone" : "TelePhone",
    "lang_Mobile" : "Mobile",
    "lang_Save" : "Save",
    "lang_Contactperson" : "Contact Person",
    "lang_FromDate" : "From Date",
    "lang_Todate" : "To Date",
    "lang_Firstname" : "First Name",
	"lang_Surname" : "Surname",
	"lang_Postcode" : "Postcode",
	"lang_Number" : "Number",
	"lang_Street" : "Street, Nr",
	"lang_Place" : "Place",
	"lang_Country" : "Country",
	"lang_Mr" : "Mr",
	"lang_Mrs" : "Mrs",
	"lang_Back" : "Back",
	"lang_Monthly" : "Monthly",
	"lang_Yearly" : "Yearly",
	"lang_Discount" : "Discount",
	"lang_Popularbrands" : "Popular Brands",
	"lang_More" : "More",
	"lang_New" : "New",
	"lang_Compare" : "Compare",
    "lang_Register" : "Register",
    "lang_Extras" : "Extras",
    "lang_Create" : "Create",
    "lang_Base" : "Base",
    "lang_Email" : "Email",
    "lang_Function" : "Function",
    "lang_Currentpassword" : "Current Password",
    "lang_Newpassword" : "New Password",
    "lang_confirmNewpassword" : "Confirm New Password",
    "lang_User" : "User",
    "lang_Name" : "Name",
    "lang_Status" : "Status",
    "lang_Send" : "Send",
    "lang_ClearFilter" : "Clear Filter",
    "lang_Abort" : "Abort",
    "lang_View" : "View",
    "lang_submit" : "Submit",
    "lang_ExtraName" : "Extra Name",
    "lang_Price" : "Price",
    "lang_Time" : "Time",
    "lang_Category" : "Category",
    "lang_Action" : "Action",
    "lang_Createdon" : "Created on",
    "lang_pages" : "Pages",
    "lang_Selectall" : "Select All",
    "lang_Authorization" : "Authorization",
    "lang_Dob" : "DOB",
    "lang_pass" : "faithful pass",
    "lang_Turnover" : "Turnover",
    "lang_Vouchers" : "Vouchers",
    "lang_Badge" : "Badge",
    "lang_Confirmpassword" : "Confirm Password",
    "lang_Customers" : "Customers",
    "lang_Course" : "Course",
    "lang_Debts" : "Debts",
    "lang_Canton" : "Canton",
    "lang_Rooms" : "Rooms",
    "lang_Rooms" : "Rooms",
    "lang_Leader" : "Leader",
    "lang_Girls" : "Girls",
    "lang_Title" : "Title",
    "lang_Website" : "Website",
    "lang_Telleader" : "Tel. Leader",
    "lang_Emailleader" : "E-Mail leader",
    "lang_Localapproval" : "Local Approval",
    "lang_Authapproval" : "Authorization responsible",
    "lang_Approvalof" : "Approval of",
    "lang_Approvaluntil" : "Approval until",
    "lang_Uploadfile" : "Upload file",
    "lang_Responsible" : "Responsible",
    "lang_Service" : "Service",
    "lang_VoucherName" : "Voucher Name",
    "lang_Chf" : "CHF",
    "lang_Dateofexpiry" : "Date of Expiry",
    "lang_Redeemedon" : "Redeemed on",
    "lang_Percentage" : "Percentage",
    "lang_Loyaltypoints" : "Loyalty points",
    "lang_Passporttitle" : "Passport Title",
    "lang_Totalpoint" : "Total Point",
    "lang_Longdescription" : "Brief description",
    "lang_Description" : "Description",
    "lang_Uploadpicture" : "Upload Picture",
    "lang_Scorerange" : "Score range",
    "lang_Passs" : "Passport",
    "lang_Url" : "URL",
    "lang_Duration" : "Duration",
    "lang_Datefrom" : "Date from",
    "lang_Dateto" : "Date to",
    "lang_Notification" : "Notification",
    "lang_Allbranches" : "All Branches",
    "lang_Custombranch" : "Custom Branch",
    "lang_Totalnumber" : "Total Number",
    "lang_Buttontext" : "Button Text",
    "lang_Facebook" : "Facebook",
    "lang_Instagram" : "Instagram",
    "lang_Sendto" : "Send to",
    "lang_Datetime" : "Date / Time",
    "lang_Rubble" : "Rubble",
    "lang_Rubblename" : "Rubble Name",
    "lang_Totallyrubbed" : "Totally rubbed",

  },
   "de" : {
  	"lang_Navigations" : "Suchen",
    "lang_Dealer" : "Händler",
    "lang_Advertise" : "Inserieren",
	"lang_Contact" : "Kontakt",
    "lang_Signout" : "Abmelden",
    "lang_Myaccount" : "Mein Konto",
    "lang_Productprice" : "Produkte und Preise",
    "lang_Advertising" : "Werbung",
    "lang_Aboutus" : "Über uns",
    "lang_Sitemap" : "Sitemap",
    "lang_Vehicles" : "Fahrzeuge",
    "lang_Garages_dealers" : "Garagen & Händler suchen",
    "lang_Brandoverview" : "Markenübersicht",
    "lang_Newcars" : "Neuwagen",
    "lang_Bookmarks" : "Merkliste",
	"lang_Changepassword" : "Passwort ändern",
	"lang_Subscription" : "Abonnement",
	"lang_Evaluation" : "Auswertung",
	"lang_Advancedsearch" : "Erweiterte Suche",
	"lang_Imprint" : "Impressum",
	"lang_Signintoreg" : "Anmelden | Registrieren",
    "lang_Username" : "Nutzername",
    "lang_Password" : "Passwort",
    "lang_Rememberpassword" : "passwort merken",
    "lang_Company" : "Firma",
    "lang_Branch" : "Filiale",
    "lang_Branchadmin" : "Filialen Admin",
    "lang_Date" : "Datum",
    "lang_Employee" : "Mitarbeiter",
    "lang_Add" : "Hinzufügen",
    "lang_Edit" : "Bearbeiten",
    "lang_Delete" : "Löschen",
    "lang_Update" : "Aktualisieren",
    "lang_Address" : "Adresse",
    "lang_Telephone" : "Telefon",
    "lang_Mobile" : "Mobil",
    "lang_Save" : "Speichern",
    "lang_Contactperson" : "Kontaktperson",
    "lang_FromDate" : "von",
    "lang_Todate" : "bis",
	"lang_Firstname" : "Vorname",
	"lang_Surname" : "Nachname",
	"lang_Postcode" : "PLZ",
	"lang_Number" : "Nummer",
	"lang_Street" : "Strasse/Nr",
	"lang_Place" : "Ort",
	"lang_Country" : "Land",
	"lang_Mr" : "Herr",
	"lang_Mrs" : "Frau",
	"lang_Back" : "Zurück",
	"lang_Monthly" : "monatlich",
	"lang_Yearly" : "jährlich",
	"lang_Discount" : "Rabatt",
	"lang_Popularbrands" : "Beliebte Marken",
	"lang_More" : "mehr",
	"lang_New" : "Neu",
	"lang_Compare" : "Vergleichen",
    "lang_Register" : "Anmelden",
    "lang_Extras" : "Extras",
    "lang_Create" : "Erstellen",
    "lang_Base" : "Basis",
    "lang_Email" : "E-Mail",
    "lang_Function" : "Funktion",
    "lang_Currentpassword" : "Aktuelles Passwort",
    "lang_Newpassword" : "Neues Passwort",
    "lang_confirmNewpassword" : "Neues Passwort bestätigen",
    "lang_User" : "Benutzer",
    "lang_Name" : "Name",
    "lang_Status" : "Status",
    "lang_Send" : "Senden",
    "lang_ClearFilter" : "Filter Löschen",
    "lang_Abort" : "Abbrechen",
    "lang_View" : "Aussicht",
    "lang_submit" : "Einreichen",
    "lang_ExtraName" : "Extra Name",
    "lang_Price" : "Preis (CHF)",
    "lang_Time" : "Zeit",
    "lang_Category" : "Kategorie",
    "lang_Action" : "Aktion",
    "lang_Createdon" : "Erstellt am",
    "lang_pages" : "Seiten",
    "lang_Selectall" : "alles auswählen",
    "lang_Authorization" : "Berechtigung",
    "lang_Dob" : "Geburtsdatum",
    "lang_pass" : "Treupass",
    "lang_Turnover" : "Umsatz (CHF)",
    "lang_Vouchers" : "Gutscheine",
    "lang_Badge" : "Badge",
    "lang_Confirmpassword" : "Passwort bestätigen",
    "lang_Customers" : "Kunden",
    "lang_Course" : "Verlauf",
    "lang_Debts" : "Schulden",
    "lang_Canton" : "Kanton",
    "lang_Rooms" : "Zimmer",
    "lang_Leader" : "Leiter",
    "lang_Girls" : "Girls",
    "lang_Title" : "Titel",
    "lang_Website" : "Webseite",
    "lang_Telleader" : "Tel. Leiter",
    "lang_Emailleader" : "E-Mail Leiter",
    "lang_Localapproval" : "Lokal Bewilligung",
    "lang_Authapproval" : "Bewilligung Verantwortliche",
    "lang_Approvalof" : "Bewilligung von",
    "lang_Approvaluntil" : "Bewilligung bis",
    "lang_Uploadfile" : "Datei hochladen",
    "lang_Responsible" : "Verantwortliche",
    "lang_Service" : "Dienstleistung",
    "lang_VoucherName" : "Gutscheinname",
    "lang_Chf" : "CHF",
    "lang_Dateofexpiry" : "Gültig bis",
    "lang_Redeemedon" : "Eingelöst am",
    "lang_Percentage" : "Prozentsatz",
    "lang_Loyaltypoints" : "Treuepunkte",
    "lang_Passporttitle" : "Pass Titel",
    "lang_Totalpoint" : "Total Punkt",
    "lang_Longdescription" : "Kurz Beschreibung",
    "lang_Description" : "Beschreibung",
    "lang_Uploadpicture" : "Bilder hochladen",
    "lang_Passs" : "Pass",
    "lang_Url" : "URL",
    "lang_Duration" : "Dauer",
    "lang_Datefrom" : "Datum von",
    "lang_Dateto" : "Datum bis",
    "lang_Notification" : "Benachrichtigungen",
    "lang_Allbranches" : "Alle Filialen",
    "lang_Custombranch" : "Brauch Filialen",
    "lang_Totalnumber" : "Gesamtzahl",
    "lang_Buttontext" : "Schaltflächentext",
    "lang_Facebook" : "Facebook",
    "lang_Instagram" : "Instagram",
    "lang_Sendto" : "Sesendet an",
    "lang_Datetime" : "Datum & Uhrzeit",
    "lang_Rubble" : "Rubble",
    "lang_Rubblename" : "Rubble Name",
    "lang_Totallyrubbed" : "Total gerubbelt",

    
    
 }
};
