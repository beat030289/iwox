/****** User creation Submit ******/
/* Arguments -  Mobilnumber, price,percentage,title,expirydate*/
$(document).on('click', '.vouchersSubmit', function(e) {
    var mobile = $('#mobile').val();
    var formss = $('form#vouchersForm');
    var form_data = new FormData(formss[0]);
    var title = $('#title').val();
    var expiry = $('#expiry').val();
    var mobile = $('#mobile').val();
    var type = $('#type').val();
    var price = $('#price').val();
    var percentage = $('#percentage').val();
    var ccv="";
    if(type==1)
    {
        ccv = $('#price').val();
    }
    else
    {
        ccv = $('#percentage').val();
    }
    
   if (title == '') {
        $('#title').closest('.form-group').find('.mandatory').html('Titel eingeben');
        $('#title').focus();
        return false;
    }
    else if (expiry == '') {
        $('#expiry').closest('.form-group').find('.mandatory').html('Geben Sie das Ablaufdatum ein');
        $('#expiry').focus();
        return false;
    } 
    else if (!validateMobile(mobile) || mobile == '') {
        $('#mobile').closest('.form-group').find('.mandatory').html('Geben Sie die richtige Handynummer ein');
        $('#mobile').focus();
        return false;
    } 
    else if(ccv=="")
    {
        if(type==1)
        {
            $('#price').closest('.form-group').find('.mandatory').html('Menge eingeben');
            $('#price').focus();
            return false;
        }
        else
        {
            $('#percentage').closest('.form-group').find('.mandatory').html('Geben Sie den Prozentsatz ein');
            $('#percentage').focus();
            return false;
        }
    }
      else {
        console.log("testdata");
       $.ajax({
            url: 'ajax-actions_vouchers',
            method: 'POST',
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,   
            success: function(res) {
                console.log(res);
                if (res.status == 'success' && res.result.status == 'success') {
                    location.href='vouchers'; 
                    //alertify.alert('Erfolgreich erstellt');
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('Handynummer existiert bereits');
                    return false;
                 }
                 else {
                    console.log("errror page");
                    alertify.alert('ungültiger Prozess erneut versuchen');
                    return false;
                }
            }
        });
    }
    return false;

});

//customer search option
$(document).on('click', '.vouchersSearchsubmit', function() {
    var vname = $('#vname').val();
    var eprice = $('#eprice').val();
    var eexpiry = $('#eexpiry').val();
    var emobile = $('#emobile').val();
    var estatus = $('#estatus').val();
    console.log(vname);
    console.log(eprice);
    console.log(emobile);
    console.log(emobile);
    console.log(estatus);
    var dataParams = [{
                        "name": "vname",
                        "value": vname
                    },
                    {
                        "name": "eprice",
                        "value": eprice
                    },
                    {
                        "name": "eexpiry",
                        "value": eexpiry
                    },
                    {
                        "name": "emobile",
                        "value": emobile
                    },
                    {
                        "name": "estatus",
                        "value": estatus
                    }];
console.log(dataParams);
                    Load_DataTables("vouchers", dataParams);
});
//Vouchers search option
$(document).on('click', '.removeVouchersFilter', function() {
    console.log("sdfbdsf");
    $('#vname').val("All");
    $('#eprice').val("All");
    $('#eexpiry').val("All");
    $('#emobile').val(" ");
    $('#estatus').val("All");
    $("#show-filter").removeClass("show");
    Load_DataTables("vouchers");
    
});

//user search option
$(document).on('click', '.srate', function() {
    var type = $($(this)).attr("data-type");
    var price = $($(this)).attr("data-price");
    console.log(type);
    console.log(price);
    $(".select-voucher").removeClass("active");
    $(".p"+price).addClass("active");
    if(type=="pcdata")
    {
        $("#percentageView").show();
        $("#priceView").hide();
        $("#price").val();
        $("#type").val("2");
    }
    else{
        $("#priceView").show();
        $("#percentageView").hide();
        $("#price").val(price);
        $("#percentage").val();
        $("#type").val("1");
    }
    
    
});
// Vouchers userdetails image 
// Arguments : userid
$(document).on('click', '.vouchersdetilsshow', function() {
    //$(".open-right-drawer").addClass("is-active");
    var thiz = $(this);
    var id = $(thiz).attr("data-vouchersid");
    console.log(id);
    $.ajax({
            url: 'ajax-actions_vouchersseparate',
            method: 'POST',
            data: {
                vouchersid: id
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                console.log(res.result.vouchersdetails);
                var voucherdetails = res.result.vouchersdetails[0];
                if (res.status == 'success' && res.result.status == 'success') {
                    var st = voucherdetails.status=='1'?'Aktiv':'inaktiv';
                    if(voucherdetails.type==1)
                    {
                        var pv='<td><span class="">CHF</span></td><td><strong>'+voucherdetails.price+'</strong></td>';
                    }
                    else
                    {
                        var pv='<td><span class="">Prozentsatz</span></td><td><strong>'+voucherdetails.percentage+'</strong></td>';
                    }
                    var vouchersDetailsData='<tr><td><span class="">Titel</span></td> <td><strong>'+voucherdetails.title+'</strong></td></tr><tr>'+pv+'</tr><tr><td><span class="">Gültig bis</span></td><td><strong>'+voucherdetails.expiry+'</strong></td> </tr><tr><td><span class="">Erstellt am</span></td><td><strong>'+voucherdetails.created_date+'</strong></td></tr> <tr><td><span class="">Eingelöst am</span></td><td><strong>'+voucherdetails.created_date+'</strong></td></tr> <tr> <td><span class="">Mobilnummer</span></td><td><strong>'+voucherdetails.mobile+'</strong></td></tr>'; 
                    $("#vouchersSeparateDetails").html(vouchersDetailsData); 
                    $('.voucherEdit').attr('href', 'editvouchers?vouchersid='+id);
                   generatecode(res.result.qrid);
                    rightPopupopen(); 
                 } else {
                    
                }
            }
        });
    return false;
    
});

/*QR code show on admin*/
function generatecode(a) {
    $('#bqrcode').html('');
    var qrContainer = document.getElementById('bqrcode');
    var qrCode = new QRCode(qrContainer, a);

    function generateQRCode() {
        qrCode.clear();
        qrCode.makeCode(a);
    }
    return generateQRCode;
     
}