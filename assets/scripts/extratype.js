// Create Extra type
// Arguments : extra type, price
$(document).on('click', '.extratypesubmit', function() {
    console.log(extratype);
    var extratype = $('#extratypec').val();
    var nickname = $('#nickname').val();
    var price = $('#price').val();
    var eid = $('#eid').val();
    if(extratype=="")
    {
        $('#extratypec').closest('.form-group').find('.mandatory').html('Extra eingeben');
        $('#extratypec').focus();
        return false; 
    }
    else if(nickname=="")
    {
        $('#nickname').closest('.form-group').find('.mandatory').html('Kurzname eingeben');
        $('#nickname').focus();
        return false; 
    }
    else if(price=="" || price<=0)
    {
        $('#price').closest('.form-group').find('.mandatory').html('Geben Sie einen gültigen Preis ein');
        $('#price').focus();
        return false; 
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_extratype',
            method: 'POST',
            data: {
                extratype: extratype,
                price: price,
                eid: eid,
                nickname: nickname,
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#extratypec').val("");
                    $('#nickname').val("");
                    $('#price').val("");
                    $('#eid').val("");
                    $(".app-drawer-wrapper").removeClass("drawer-open");
                    $(".app-drawer-overlay").addClass("d-none");
                    Load_DataTables("extratype");
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('extra name existiert bereits');
                    return false;
                 }
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});
// value remove function
$(document).on('click', '.etypeclear', function() {
    console.log("testdata val");
    rightPopupclose();
    $('#extratypec').val("");
    $('#nickname').val("");
    $('#price').val("");
    
});
$(document).on('click', '.ztypeclear', function() {
      console.log("ztestdata val");
    rightPopupclose();
    $('#zeitc').val("00:00");
    
});
// Create Zeit
// Arguments : zeit
$(document).on('click', '.zeitsubmit', function() {
    var zeit = $('#zeitc').val();
    var editid = $('#editid').val();
    console.log(zeit);
    if(zeit=="" || zeit==="undefined")
    {
        $('#zeitc').closest('.form-group').find('.mandatory').html('Geben Sie die richtige Zeit ein');
        $('#zeitc').focus();
        return false; 
    }
    else
    {
      var timedata = timeconvertion(zeit);
      console.log("timedata"+timedata);
      if(timedata =="0")
      {
        $('#zeitc').closest('.form-group').find('.mandatory').html('Geben Sie eine gültige Zeitdauer ein');
        $('#zeitc').focus();
        return false;
      }
      else
      {
        $.ajax({
              url: 'ajax-actions_zeit',
              method: 'POST',
              data: {
                  zeit: timedata,
                  timedata: zeit,
                  editid: editid
                  
              },
              dataType: 'JSON',
              success: function(res) {
                  console.log(res);
                 if (res.status == 'success' && res.result.status == 'success') {
                      $('#zeitc').val("00:00");
                      $('#editid').val("");
                      $(".app-drawer-wrapper").removeClass("drawer-open");
                      $(".app-drawer-overlay").addClass("d-none");
                      Load_DataTables("zeit");
                   } 
                   else if(res.status == 'success' && res.result.status == 'already')
                   {
                      alertify.alert('Die Zeit vergeht bereits');
                      return false;
                   }
                  else {
                      alertify.alert('Ungültiger Vorgang erneut versuchen');
                      return false;
                  }
              }
          });
      }
     }
    return false;
});
// timeconvertion function
function timeconvertion(timeval)
{
  var a = timeval.split(':'); // split it at the colons
  var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60;
  var miliseconds=seconds*1000;
  mydate=new Date(miliseconds);
  humandate1=mydate.getUTCHours();
  humandate2=mydate.getUTCMinutes();
  var humandate="";
  // console.log("humandate1"+humandate1);
  // console.log("humandate2"+humandate2);
  // console.log(typeof(humandate1));
      if(humandate1 != "0" && humandate2 != "0")
      {
        console.log("valuedata point");
        humandate=mydate.getUTCHours()+" Stunde "+mydate.getUTCMinutes()+" min ";
      }
      else if(humandate1 == "0" && humandate2 !="0")
      {
        humandate=mydate.getUTCMinutes()+" min ";
      }
      else if(humandate1 != "0" && humandate2 == "0")
      {
        humandate=mydate.getUTCHours()+" Stunde ";
      }
      else
      {
        humandate=0;
      }
      //console.log(humandate);
      return humandate;
}
// right side popup open function
$(document).on('click', '.rdataClick', function() {
    console.log("checkdata");
    $(".app-drawer-wrapper").addClass("drawer-open");
    $(".app-drawer-overlay").removeClass("d-none");
});
// Create permit perstion
// Arguments : name, Idcard
$(document).on('click', '.permitsubmit', function() {
    var pname = $('#pname').val();
    var idcard = $('#idcard').val();
    var editid = $('#editid').val();
    if(pname=="")
    {
        $('#pname').closest('.form-group').find('.mandatory').html('Name eingeben');
        $('#pname').focus();
        return false; 
    }
    else if(idcard=="" )
    {
        $('#idcard').closest('.form-group').find('.mandatory').html('ID-Karte eingeben');
        $('#idcard').focus();
        return false; 
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_permitperson',
            method: 'POST',
            data: {
                pname: pname,
                idcard: idcard,
                editid: editid,
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#pname').val("");
                    $('#idcard').val("");
                    $('#editid').val("");
                    $(".app-drawer-wrapper").removeClass("drawer-open");
                    $(".app-drawer-overlay").addClass("d-none");
                    Load_DataTables("permit");
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('name existiert bereits');
                    return false;
                 }
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});
// Create Hair type
// Arguments : hairtype
$(document).on('click', '.hairsubmit', function() {
    var hair = $('#hairtype').val();
    var editid = $('#editid').val();
    //console.log(hair);
    if(hair=="")
    {
        $('#hairtype').closest('.form-group').find('.mandatory').html('Haartyp eingeben');
        $('#hairtype').focus();
        return false; 
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_hair',
            method: 'POST',
            data: {
                hair: hair,
                editid: editid,
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#hairtype').val("");
                    $('#editid').val("");
                    $(".app-drawer-wrapper").removeClass("drawer-open");
                    $(".app-drawer-overlay").addClass("d-none");
                    Load_DataTables("hair");
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('Haartyp tritt bereits aus');
                    return false;
                 }
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});

// value remove function
$(document).on('click', '.ehairclear', function() {
   rightPopupclose();
    $('#hairtype').val("");
});

// Create Eyes
// Arguments : eyestype
$(document).on('click', '.eyessubmit', function() {
  console.log("ihviogg");
    var eyestype = $('#eyestype').val();
    var editid = $('#editid').val();
    console.log(eyestype);
    if(eyestype=="")
    {
        $('#eyestype').closest('.form-group').find('.mandatory').html('Geben Sie den Augentyp ein');
        $('#eyestype').focus();
        return false; 
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_eyes',
            method: 'POST',
            data: {
                eyestype: eyestype,
                editid: editid,
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#eyestype').val("");
                    $('#editid').val("");
                    $(".app-drawer-wrapper").removeClass("drawer-open");
                    $(".app-drawer-overlay").addClass("d-none");
                    Load_DataTables("eyes");
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('Der Augentyp tritt bereits aus');
                    return false;
                 }
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});

// value remove function
$(document).on('click', '.eeyesclear', function() {
   rightPopupclose();
    $('#eyestype').val("");
});

// Create Shoes
// Arguments : shoestype
$(document).on('click', '.shoessubmit', function() {
  console.log("ihviogg");
    var shoestype = $('#shoestype').val();
    var editid = $('#editid').val();
    console.log(shoestype);
    if(shoestype=="")
    {
        $('#shoestype').closest('.form-group').find('.mandatory').html('Schuhe eingeben');
        $('#shoestype').focus();
        return false; 
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_shoes',
            method: 'POST',
            data: {
                shoestype: shoestype,
                editid: editid,
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#shoestype').val("");
                    $('#editid').val("");
                    $(".app-drawer-wrapper").removeClass("drawer-open");
                    $(".app-drawer-overlay").addClass("d-none");
                    Load_DataTables("shoes");
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('Schuhe gibt es schon');
                    return false;
                 }
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});

// value remove function
$(document).on('click', '.eshoesclear', function() {
   rightPopupclose();
    $('#shoestype').val("");
});
// Create Cloth
// Arguments : clothtype
$(document).on('click', '.clothsubmit', function() {
  console.log("ihviogg");
    var clothtype = $('#clothtype').val();
    var editid = $('#editid').val();
    console.log(clothtype);
    if(clothtype=="")
    {
        $('#clothtype').closest('.form-group').find('.mandatory').html('Konfektion eingeben');
        $('#clothtype').focus();
        return false; 
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_cloth',
            method: 'POST',
            data: {
                clothtype: clothtype,
                editid: editid,
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#clothtype').val("");
                    $('#editid').val("");
                    $(".app-drawer-wrapper").removeClass("drawer-open");
                    $(".app-drawer-overlay").addClass("d-none");
                    Load_DataTables("cloth");
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('fertige Kleidung gibt es bereits');
                    return false;
                 }
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});

// value remove function
$(document).on('click', '.eclothclear', function() {
   rightPopupclose();
    $('#clothtype').val("");
});

// Create Civil status
// Arguments : civilstatustype
$(document).on('click', '.civilstatussubmit', function() {
  console.log("ihviogg");
    var civilstatustype = $('#civilstatustype').val();
    var editid = $('#editid').val();
    console.log(civilstatustype);
    if(civilstatustype=="")
    {
        $('#civilstatustype').closest('.form-group').find('.mandatory').html('Zivilstatus eingeben');
        $('#civilstatustype').focus();
        return false; 
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_civilstatus',
            method: 'POST',
            data: {
                civilstatustype: civilstatustype,
                editid: editid,
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#civilstatustype').val("");
                    $('#editid').val("");
                    $(".app-drawer-wrapper").removeClass("drawer-open");
                    $(".app-drawer-overlay").addClass("d-none");
                    Load_DataTables("civilstatus");
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('Zivilstatus bereits verlassen');
                    return false;
                 }
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});

// value remove function
$(document).on('click', '.ecivilstatusclear', function() {
   rightPopupclose();
    $('#civilstatustype').val("");
});

// Create private part
// Arguments : privateparttype
$(document).on('click', '.privatepartsubmit', function() {
  console.log("ihviogg");
    var privateparttype = $('#privateparttype').val();
    var editid = $('#editid').val();
    console.log(privateparttype);
    if(privateparttype=="")
    {
        $('#privateparttype').closest('.form-group').find('.mandatory').html('Feld sollte nicht leer sein');
        $('#privateparttype').focus();
        return false; 
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_privatepart',
            method: 'POST',
            data: {
                privateparttype: privateparttype,
                editid: editid,
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#privateparttype').val("");
                    $('#editid').val("");
                    $(".app-drawer-wrapper").removeClass("drawer-open");
                    $(".app-drawer-overlay").addClass("d-none");
                    Load_DataTables("privatepart");
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('Intimbereich existiert bereits');
                    return false;
                 }
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});

// value remove function
$(document).on('click', '.eprivatepartclear', function() {
   rightPopupclose();
    $('#privateparttype').val("");
});

// Create Bust
// Arguments : busttype
$(document).on('click', '.bustsubmit', function() {
  console.log("ihviogg");
    var busttype = $('#busttype').val();
    var editid = $('#editid').val();
    console.log(busttype);
    if(busttype=="")
    {
        $('#busttype').closest('.form-group').find('.mandatory').html('Feld sollte nicht leer sein');
        $('#busttype').focus();
        return false; 
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_busttype',
            method: 'POST',
            data: {
                busttype: busttype,
                editid: editid,
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#busttype').val("");
                    $('#editid').val("");
                    $(".app-drawer-wrapper").removeClass("drawer-open");
                    $(".app-drawer-overlay").addClass("d-none");
                    Load_DataTables("bust");
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('Intimbereich existiert bereits');
                    return false;
                 }
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});

// value remove function
$(document).on('click', '.ebustclear', function() {
   rightPopupclose();
    $('#busttype').val("");
});
// Create Nationality
// Arguments : nationalitytype
$(document).on('click', '.nationalitysubmit', function() {
  console.log("ihviogg");
    var nationalitytype = $('#nationalitytype').val();
    var editid = $('#editid').val();
    console.log(nationalitytype);
    if(nationalitytype=="")
    {
        $('#nationalitytype').closest('.form-group').find('.mandatory').html('Geben Sie die Nationalität ein');
        $('#nationalitytype').focus();
        return false; 
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_nationality',
            method: 'POST',
            data: {
                nationalitytype: nationalitytype,
                editid: editid,
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#nationalitytype').val("");
                    $('#editid').val("");
                    $(".app-drawer-wrapper").removeClass("drawer-open");
                    $(".app-drawer-overlay").addClass("d-none");
                    Load_DataTables("nationality");
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('Nationalität existiert bereits');
                    return false;
                 }
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});

// value remove function
$(document).on('click', '.enationalityclear', function() {
   rightPopupclose();
    $('#nationalitytype').val("");
});
//edit extra type
$(document).on('click', '.edit_extratype', function() {
    //alert($(this).data("id"));
    var eid = $(this).data("id");
    if(eid=="")
    {
        $('#extratypec').closest('.form-group').find('.mandatory').html('Ungültiger Prozess');
        $('#extratypec').focus();
        return false; 
    }
    
    else
    {
        $.ajax({
            url: 'ajax-actions_extratype_edit',
            method: 'POST',
            data: {
                id: eid
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                /*alert(res.status);
                alert(res.result.status);*/
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#extratypec').val(res.result.details[0].extra);
                    $('#price').val(res.result.details[0].price);
                    $('#eid').val(res.result.details[0].id);
                    $('#nickname').val(res.result.details[0].nickname);
                    $(".app-drawer-wrapper").addClass("drawer-open");
                    $(".app-drawer-overlay").removeClass("d-none");
                    //Load_DataTables("extratype");
                 } 
                 
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});

//edit Zeit type
$(document).on('click', '.edit_zeit', function() {
  /*  alert($(this).data("id"));*/
    var eid = $(this).data("id");
    if(eid=="")
    {
        $('#zeitc').closest('.form-group').find('.mandatory').html('Ungültiger Prozess');
        $('#zeitc').focus();
        return false; 
    }
    
    else
    {
        $.ajax({
            url: 'ajax-actions_zeit_edit',
            method: 'POST',
            data: {
                id: eid
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                /*alert(res.status);
                alert(res.result.status);*/
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#zeitc').val(res.result.details[0].timedata);
                    $('#editid').val(res.result.details[0].id);
                    $(".app-drawer-wrapper").addClass("drawer-open");
                    $(".app-drawer-overlay").removeClass("d-none");
                    //Load_DataTables("extratype");
                 } 
                 
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});
//edit Permit type
$(document).on('click', '.edit_permit', function() {
  /*  alert($(this).data("id"));*/
    var eid = $(this).data("id");
    if(eid=="")
    {
        $('#pname').closest('.form-group').find('.mandatory').html('Ungültiger Prozess');
        $('#pname').focus();
        return false; 
    }
    
    else
    {
        $.ajax({
            url: 'ajax-actions_permitperson_edit',
            method: 'POST',
            data: {
                id: eid
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                /*alert(res.status);
                alert(res.result.status);*/
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#pname').val(res.result.details[0].name);
                    $('#idcard').val(res.result.details[0].idcard);
                    $('#editid').val(res.result.details[0].id);
                    $(".app-drawer-wrapper").addClass("drawer-open");
                    $(".app-drawer-overlay").removeClass("d-none");
                    //Load_DataTables("extratype");
                 } 
                 
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});
//edit hair type
$(document).on('click', '.edit_hair', function() {
  /*  alert($(this).data("id"));*/
    var eid = $(this).data("id");
    if(eid=="")
    {
        alertify.alert('Ungültiger Prozess');
        
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_hair_edit',
            method: 'POST',
            data: {
                id: eid
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                /*alert(res.status);
                alert(res.result.status);*/
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#hairtype').val(res.result.details[0].hair);
                    $('#editid').val(res.result.details[0].id);
                    $(".app-drawer-wrapper").addClass("drawer-open");
                    $(".app-drawer-overlay").removeClass("d-none");
                    //Load_DataTables("extratype");
                 } 
                 
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});
//edit eyes type
$(document).on('click', '.edit_eyes', function() {
  /*  alert($(this).data("id"));*/
    var eid = $(this).data("id");
    if(eid=="")
    {
        alertify.alert('Ungültiger Prozess');
        
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_eyes_edit',
            method: 'POST',
            data: {
                id: eid
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                /*alert(res.status);
                alert(res.result.status);*/
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#eyestype').val(res.result.details[0].eyes);
                    $('#editid').val(res.result.details[0].id);
                    $(".app-drawer-wrapper").addClass("drawer-open");
                    $(".app-drawer-overlay").removeClass("d-none");
                    //Load_DataTables("extratype");
                 } 
                 
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});
//edit shoes type
$(document).on('click', '.edit_shoes', function() {
  /*  alert($(this).data("id"));*/
    var eid = $(this).data("id");
    if(eid=="")
    {
        alertify.alert('Ungültiger Prozess');
        
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_shoes_edit',
            method: 'POST',
            data: {
                id: eid
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                /*alert(res.status);
                alert(res.result.status);*/
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#shoestype').val(res.result.details[0].shoes);
                    $('#editid').val(res.result.details[0].id);
                    $(".app-drawer-wrapper").addClass("drawer-open");
                    $(".app-drawer-overlay").removeClass("d-none");
                    //Load_DataTables("extratype");
                 } 
                 
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});

//edit Cloth type
$(document).on('click', '.edit_cloth', function() {
  /*  alert($(this).data("id"));*/
    var eid = $(this).data("id");
    if(eid=="")
    {
        alertify.alert('Ungültiger Prozess');
        
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_cloth_edit',
            method: 'POST',
            data: {
                id: eid
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                /*alert(res.status);
                alert(res.result.status);*/
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#clothtype').val(res.result.details[0].cloth);
                    $('#editid').val(res.result.details[0].id);
                    $(".app-drawer-wrapper").addClass("drawer-open");
                    $(".app-drawer-overlay").removeClass("d-none");
                    //Load_DataTables("extratype");
                 } 
                 
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});

//edit civilstatus type
$(document).on('click', '.edit_civilstatus', function() {
  /*  alert($(this).data("id"));*/
    var eid = $(this).data("id");
    if(eid=="")
    {
        alertify.alert('Ungültiger Prozess');
        
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_civilstatus_edit',
            method: 'POST',
            data: {
                id: eid
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                /*alert(res.status);
                alert(res.result.status);*/
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#civilstatustype').val(res.result.details[0].civilstatus);
                    $('#editid').val(res.result.details[0].id);
                    $(".app-drawer-wrapper").addClass("drawer-open");
                    $(".app-drawer-overlay").removeClass("d-none");
                    //Load_DataTables("extratype");
                 } 
                 
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});
//edit privatepart 
$(document).on('click', '.edit_privatepart', function() {
  /*  alert($(this).data("id"));*/
    var eid = $(this).data("id");
    if(eid=="")
    {
        alertify.alert('Ungültiger Prozess');
        
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_privatepart_edit',
            method: 'POST',
            data: {
                id: eid
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                /*alert(res.status);
                alert(res.result.status);*/
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#privateparttype').val(res.result.details[0].privatepart);
                    $('#editid').val(res.result.details[0].id);
                    $(".app-drawer-wrapper").addClass("drawer-open");
                    $(".app-drawer-overlay").removeClass("d-none");
                    //Load_DataTables("extratype");
                 } 
                 
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});
//edit Bust 
$(document).on('click', '.edit_bust', function() {
  /*  alert($(this).data("id"));*/
    var eid = $(this).data("id");
    if(eid=="")
    {
        alertify.alert('Ungültiger Prozess');
        
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_bust_edit',
            method: 'POST',
            data: {
                id: eid
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                /*alert(res.status);
                alert(res.result.status);*/
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#busttype').val(res.result.details[0].bust);
                    $('#editid').val(res.result.details[0].id);
                    $(".app-drawer-wrapper").addClass("drawer-open");
                    $(".app-drawer-overlay").removeClass("d-none");
                    //Load_DataTables("extratype");
                 } 
                 
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});
//edit nationality 
$(document).on('click', '.edit_nationality', function() {
  /*  alert($(this).data("id"));*/
    var eid = $(this).data("id");
    if(eid=="")
    {
        alertify.alert('Ungültiger Prozess');
        
    }
    else
    {
        $.ajax({
            url: 'ajax-actions_nationality_edit',
            method: 'POST',
            data: {
                id: eid
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                /*alert(res.status);
                alert(res.result.status);*/
               if (res.status == 'success' && res.result.status == 'success') {
                    $('#nationalitytype').val(res.result.details[0].nationality);
                    $('#editid').val(res.result.details[0].id);
                    $(".app-drawer-wrapper").addClass("drawer-open");
                    $(".app-drawer-overlay").removeClass("d-none");
                    //Load_DataTables("extratype");
                 } 
                 
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});