
var aws_viewlink_gallery = "images/gallery/";
var aws_viewlink_point = "images/point/";
var aws_viewlink_advertise = "images/advertise/";
var aws_viewlink_notification = "images/notification/";
var aws_viewlink_rubble = "images/rubble/";
var pathArray = window.location.pathname.split('/');
var secondLevelLocation = pathArray[0];
/*console.log("first"+pathArray);
console.log("secondLevelLocation"+secondLevelLocation);
// nav bar active function
*/$(function() {
    var hdata = location.pathname.split("/")[1];
    console.log("secondLevelLocation"+hdata);
    $('.vertical-nav-menu li').removeClass('mm-active');
    $('a[href^="' + hdata + '"]').closest('li').addClass('mm-active');
 });

/* Drag and drop functionality */
$(function() {
    var filetypec = $("#filetype").val();
    console.log(filetypec);
    var drop_area = $('.drop-area').length;
    if (drop_area) {
        var fileDiv = document.getElementById("upload");
        var fileInput = document.getElementById("file");

        fileInput.addEventListener("change", function(e) {
            var files = this.files
            console.log(files);
            if (filetypec == "girls") {
                showThumbnail(files);
            } else if (filetypec == "point") {
                showThumbnailpoint(files);
            }
            else if (filetypec == "note") {
                showThumbnailnote(files);
            }


        }, false)

        fileDiv.addEventListener("click", function(e) {
            $(fileInput).show().focus().click().hide();
            e.preventDefault();
        }, false)

    }

    // Drag enter
    $('#upload').on('dragenter', function(e) {
        e.stopPropagation();
        e.preventDefault();

    });

    // Drag over
    $('#upload').on('dragover', function(e) {
        e.stopPropagation();
        e.preventDefault();

    });

    // Drop
    $('#upload').on('drop', function(e) {
        e.stopPropagation();
        e.preventDefault();
        var file = e.originalEvent.dataTransfer.files;
        var img_cnt = file.length;
        // for (var i = 0; i < file.length; i++) 
        for (var i in file) 
        {
           var fileName, fileExtension, fileSize, fileType, dateModified;
            fileName = file[0].name;
            fileExtension = fileName.replace(/^.*\./, '');
            if (fileExtension == 'png' || fileExtension == 'jpg' || fileExtension == 'jpeg') {
                readImageFile(file[i]); // GET IMAGE INFO USING fileReader().
            } else {
                // IF THE FILE IS NOT AN IMAGE.

                fileSize = files[i].size; // FILE SIZE.
                fileType = files[i].type; // FILE TYPE.
                dateModified = files[i].lastModifiedDate; // FILE LAST MODIFIED.
                alertify.alert('Falscher Dateityp-' + fileName + 'nur png, jpg, jpeg erlaubt');
            }

            function readImageFile(filec) {
                var reader = new FileReader(); // CREATE AN NEW INSTANCE.

                reader.onload = function(e) {
                    var img = new Image();
                    img.src = e.target.result;

                    img.onload = function() {
                        var w = this.width;
                        var h = this.height;
                        //if (w == 800 && h == 800) {

                         var fd = new FormData();
                        var uid = $("#u_id").val();
                        var editid = $("#editid").val();
                        var typefile = $("#filetype").val();
                        fd.append('uid', uid);
                        fd.append('editid', editid);
                        fd.append('file', filec);
                        if (typefile == "girls") {
                            uploadData(fd);
                           
                        } else if (typefile == "point") {
                            uploadDatapoint(fd);
                        }
                        else if (typefile == "note") {
                            uploadDatanote(fd);
                        }


                        /* } else {
                             alertify.alert('Ungültige Dateigröße-' + fileName + ' Nur 800x800 zulässig');
                         }*/
                    }
                };
                reader.readAsDataURL(filec);
            }
            //fd.append('filewws', file[0]);
            
        }
    });
});

