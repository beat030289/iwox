
/****** Rubble creation Submit ******/
/* Arguments -  title,date expiry,description,image*/
/*$(document).ready(function(){
	$("#cdate").datepicker({
		isRTL: false,
		format: 'dd.mm.yyyy hh:ii',
		autoclose:true,
		language: 'de'
	});
});*/
$(document).on('click', '.rubbleSubmit', function(e) {
    var form_data = new FormData($('#rubbleForm')[0]);
    var title = $('#title').val();
    var cdate = $('#cdate').val();
    var description = $('#description').val();
    var rubbleimage = $('#rubble_1').val();
    var oldfile = $('#oldfile').val();
    if (title == '') {
        $('#title').closest('.form-group').find('.mandatory').html('Titel eingeben');
        $('#title').focus();
        return false;
    }
    else if (cdate == '') {
        $('#cdate').closest('.form-group').find('.mandatory').html('Wählen Gültig bis');
        $('#cdate').focus();
        return false;
    } 
    else if (description == '') {
        $('#description').closest('.form-group').find('.mandatory').html('Beschreibung eingeben');
        $('#description').focus();
        return false;
    }
    else if (rubbleimage == '' && oldfile=='') {
        $('#rubble_1').closest('.form-group').find('.mandatory').html('Bild hochladen');
        $('#rubble_1').focus();
         return false;
    } 
    else {
       $.ajax({
            url: 'ajax-actions_rubble',
             method: 'POST',
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,     
            success: function(res) {
                console.log(res);
                if (res.status == 'success' && res.result.status == 'success') {
                    location.href='rubble'; 
                    //alertify.alert('Erfolgreich erstellt');
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('eingeben existiert bereits');
                    return false;
                 }
                 else {
                    console.log("errror page");
                    alertify.alert('ungültiger Prozess erneut versuchen');
                    return false;
                }
            }
        });
    }
    return false;

});
/* Image loader Function */
var imageLoaderRubble = document.getElementById('rubble_1');
if(imageLoaderRubble)
{
    console.log("imageLoaderRubble");
    imageLoaderRubble.addEventListener('change', handleImageRubble, false);
}

function handleImageRubble(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
         console.log("rubbleup_1");
        $('.rubbleup_1 img').attr('src',event.target.result);
    }
    reader.readAsDataURL(e.target.files[0]);
}
//rubble search option
$(document).on('click', '.rubbleSearchsubmit', function() {
    var cName = $('#cName').val();
    var mobile = $('#cdate').val();
    var cstatus = $('#cstatus').val();
    console.log(cName);
    console.log(cdate);
    console.log(cstatus);
    var dataParams = [{
                        "name": "cname",
                        "value": cName
                    },
                    {
                        "name": "cdate",
                        "value": cdate
                    },
                    {
                        "name": "cstatus",
                        "value": cstatus
                    }
                    ];
console.log(dataParams);
Load_DataTables("rubbleview", dataParams);
});
//user search option
$(document).on('click', '.removeRubbleFilter', function() {
    console.log("sdfbdsf");
    var cName = $('#cName').val();
    var mobile = $('#cdate').val();
    var cstatus = $('#cstatus').val("all");
    $("#show-filter").removeClass("show");
    Load_DataTables("rubbleview");
    
});
// Separate Rubble image 
// Arguments : Rubbleid
$(document).on('click', '.rubbleSeparate', function() {
    //$(".open-right-drawer").addClass("is-active");
    var thiz = $(this);
    var id = $(thiz).attr("data-rubbleid");
    console.log(id);
    $.ajax({
            url: 'ajax-actions_rubbleseparate',
            method: 'POST',
            data: {
                rubbleid: id
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
                console.log(res.result.rubbledetails[0].title);
                var rubbledetails = res.result.rubbledetails[0];
                if (res.status == 'success' && res.result.status == 'success') {
                    var st = rubbledetails.status=='1'?'Aktiv':'inaktiv';
                    var rubbleDetailsData='<tr><td>Titel</td><td><strong>'+rubbledetails.title+'</strong></td></tr><tr><td>Gültig bis</td><td><strong>'+rubbledetails.cdate+'</strong></td></tr><tr><td>Erstellt am</td><td><strong>'+rubbledetails.created_date+'</strong></td></tr><tr><td>Total gerubbelt</td><td><strong>-</strong></td></tr><tr><td>Status</td><td><strong>'+st+'</strong></td></tr>'; 
                   
                    console.log(rubbleDetailsData);
                    $("#rubbleSeparateDetails").html(rubbleDetailsData); 
                    $('.rubbleEdit').attr('href', 'editrubble?rubbleid='+id);
                    $('.voucher-img').html('<img src="'+aws_viewlink_rubble+''+rubbledetails.image+'" class="img-fluid" alt="">');
                    rightPopupopen(); 
                 } else {
                    
                }
            }
        });
    return false;
    
});

//data function
/*$(document).on('click', '#cdate', function(event) {
    $(this).datepicker().datepicker("show");    
    $(this).datepicker({
    format: 'dd-mm-yyyy',
    numberOfMonths: 1,
    autoclose: true,
    startDate: truncateDate(new Date())
});
    $(this).datepicker('cdate', truncateDate(new Date())); 
});

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}*/
/*
$(function () {
 $('.datepicker').datepicker({
    language: 'de'
});
 console.log("ytrufy");
});
$(document).on('click', '.datepicker', function(e)
{
    console.log("datatest");
});*/
/* $('.datepicker').datepicker({

    language: 'de'
}

);*/

/*$(document).ready(function(){
     $('#cdate').datepicker();
});*/
