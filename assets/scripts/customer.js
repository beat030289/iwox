
/****** User creation Submit ******/
/* Arguments -  Mobilnumber, email,firstname,lastname,dob,status,password*/
$(document).on('click', '.customerSubmit', function(e) {
    var badge = $('#badge').val();
    var firstName = $('#firstName').val();
    var lastName = $('#lastName').val();
    var mobile = $('#mobile').val();
    var password = $('#password').val();
    var confirmpassword = $('#confirmpassword').val();
    var email = $('#email').val();
    var dob = $('#dob').val();
    var userstatus = $('#status').val();
    console.log("dob"+dob);
    var formData=$('#customerForm').serialize();
    console.log(formData);
    if (badge == '') {
        $('#badge').closest('.form-group').find('.mandatory').html('Wählen Badge');
        $('#badge').focus();
        return false;
    }
    else if (firstName == '') {
        $('#firstName').closest('.form-group').find('.mandatory').html('Bitte Vornamen eingeben');
        $('#firstName').focus();
        return false;
    } 
    else if (lastName == '') {
        $('#lastName').closest('.form-group').find('.mandatory').html('Nachnamen eingeben');
        $('#lastName').focus();
        return false;
    }
    else if (!ValidateEmail(email) || email == '') {
        $('#email').closest('.form-group').find('.mandatory').html('Geben Sie die richtige Handynummer ein');
        $('#email').focus();
        return false;
    } 
    else if (!validateMobile(mobile) || mobile == '') {
        $('#mobile').closest('.form-group').find('.mandatory').html('Geben Sie die richtige Handynummer ein');
        $('#mobile').focus();
        return false;
    } else if (password == '') {
        $('#password').closest('.form-group').find('.mandatory').html('Passwort eingeben');
        $('#password').focus();
        return false;
    } 
     else if (confirmpassword == '') {
        $('#confirmpassword').closest('.form-group').find('.mandatory').html('Passwort bestätigen eingeben');
        $('#confirmpassword').focus();
        return false;
    }
     else if (password != confirmpassword) {
        $('#confirmpassword').closest('.form-group').find('.mandatory').html('Kennwort und Bestätigungskennwort stimmen nicht überein');
        $('#confirmpassword').focus();
        return false;
    }
    else if (dob == '') {
        $('#dob').closest('.form-group').find('.mandatory').html('Wählen Sie Geburtsdaten');
        $('#dob').focus();
        return false;
    } 
    else if (userstatus == '') {
        $('#status').closest('.form-group').find('.mandatory').html('Status auswählen');
        $('#status').focus();
        return false;
    }  else {
       $.ajax({
            url: 'ajax-actions_customercreate',
            method: 'POST',
            dataType: 'JSON',
            data: formData,   
            success: function(res) {
                console.log(res);
                if (res.status == 'success' && res.result.status == 'success') {
                    location.href='customers'; 
                    //alertify.alert('Erfolgreich erstellt');
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('Handynummer existiert bereits');
                    return false;
                 }
                 else {
                    console.log("errror page");
                    alertify.alert('ungültiger Prozess erneut versuchen');
                    return false;
                }
            }
        });
    }
    return false;

});

//customer search option
$(document).on('click', '.customerSearchsubmit', function() {
    var branch = $('#cbranch').val();
    var badge = $('#cbadge').val();
    var cName = $('#cName').val();
    var mobile = $('#cmobile').val();
    var cstatus = $('#cstatus').val();
    console.log(branch);
    console.log(badge);
    console.log(cName);
    console.log(mobile);
    console.log(cstatus);
    var dataParams = [{
                        "name": "cname",
                        "value": cName
                    },
                    {
                        "name": "branch",
                        "value": branch
                    },
                    {
                        "name": "badge",
                        "value": badge
                    },
                    {
                        "name": "mobile",
                        "value": mobile
                    },
                    {
                        "name": "cstatus",
                        "value": cstatus
                    }];
console.log(dataParams);
                    Load_DataTables("customer", dataParams);
});
//user search option
$(document).on('click', '.removeCustomerFilter', function() {
    console.log("sdfbdsf");
    $('#cbranch').val("all");
    $('#cbadge').val("all");
    $('#cName').val("");
    $('#cmobile').val("");
    $('#cstatus').val("1");
    $("#show-filter").removeClass("show");
    Load_DataTables("customer");
    
});