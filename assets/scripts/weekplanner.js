// check tag click function
/*$("*").live('click',function(evt) {
    evt.stopPropagation();
    alert(evt.target.tagName);      
})*/
$(document).on('click', '.rchpoint', function(evt) {
    //evt.stopPropagation();
   // alert(evt.target.tagName); 
    $('input:checkbox').removeAttr('checked');
    });
// Create Weeekly planner
// Arguments : date,branch,girls
$(document).on('click', '.workSubmit', function() {
    var branch = $('#sbranch').val();
    var cdate = $('#cdate').val();
    var edittype = $('#edittype').val();
    console.log(branch);
    console.log(cdate);
    var i = 0;
    var arr = [];
       $('.ads_Checkbox:checked').each(function () {
           arr[i++] = $(this).val();
       });
       console.log(arr);
    //alert("branch"+branch);
   if(!validateDate(cdate) || cdate == '')
    {
        $('#cdate').closest('.form-group').find('.mandatory').html('Datum auswählen');
        $('#cdate').focus();
        return false; 
    }
    else if(branch=="" || branch==null || branch==undefined)
    {
        $('#sbranch').closest('.form-group').find('.mandatory').html('Zweig auswählen');
        $('#sbranch').focus();
        return false; 
    }
    /*else if(arr.length<=0)
    {
        alertify.alert('Mädchen auswählen');
        return false;
    }*/
    else
    {
        $.ajax({
            url: 'ajax-actions_weeklyplanner',
            method: 'POST',
            data: {
                branch: branch,
                edittype: edittype,
                cdate: data_change_format(cdate),
                girls: arr,
            },
            dataType: 'JSON',
            success: function(res) {
                console.log(res);
               if (res.status == 'success' && res.result.status == 'success') {
                    rightPopupclose();
                    $('input:checkbox').removeAttr('checked');
                    $("#cdate").val("");
                    $("#sbranch").val("");
                    //location.href='weeklyplanner';
                    workplanner_cc();
                 } 
                 else if(res.status == 'success' && res.result.status == 'already')
                 {
                    alertify.alert('Dienstname wird bereits beendet');
                    return false;
                 }
                else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
     }
    return false;
});



// ajax seach option all girls

$(document).on('keyup', '#all_girls', function() {
    var filter = $('#all_girls').val();
    var val = filter.toLowerCase();

    $("#s_all li").hide();

    $("#s_all li").each(function() {

        var text = $(this).text().toLowerCase();

        if (text.indexOf(val) != -1) {
            $(this).show();
        }
    });

});
// ajax seach option present girls

$(document).on('keyup', '#present_girls', function() {
    var filter = $('#present_girls').val();
    var val = filter.toLowerCase();

    $("#s_present li").hide();

    $("#s_present li").each(function() {

        var text = $(this).text().toLowerCase();

        if (text.indexOf(val) != -1) {
            $(this).show();
        }
    });

});
// ajax seach option Active girls

$(document).on('keyup', '#active_girls', function() {
    var filter = $('#active_girls').val();
    var val = filter.toLowerCase();

    $("#s_active li").hide();

    $("#s_active li").each(function() {

        var text = $(this).text().toLowerCase();

        if (text.indexOf(val) != -1) {
            $(this).show();
        }
    });

});
// ajax seach option Inactive girls

$(document).on('keyup', '#inactive_girls', function() {
    var filter = $('#inactive_girls').val();
    var val = filter.toLowerCase();

    $("#s_inactive li").hide();

    $("#s_inactive li").each(function() {

        var text = $(this).text().toLowerCase();

        if (text.indexOf(val) != -1) {
            $(this).show();
        }
    });

});
// leftpad
function leftPad(number, targetLength) {
    var output = number + '';
    while (output.length < targetLength) {
        output = '0' + output;
    }
    return output;
}
// ordinal suffix
const toOrdinalSuffix = num => {
  const int = parseInt(num),
    digits = [int % 10, int % 100],
    ordinals = ['st', 'nd', 'rd', 'th'],
    oPattern = [1, 2, 3, 4],
    tPattern = [11, 12, 13, 14, 15, 16, 17, 18, 19];
  return oPattern.includes(digits[0]) && !tPattern.includes(digits[1])
    ? int + ordinals[digits[0] - 1]
    : int + ordinals[3];
};
// time function
function endFirstWeek(firstDate, firstDay) {
    if (! firstDay) {
        return 7 - firstDate.getDay();
    }
    if (firstDate.getDay() < firstDay) {
        return firstDay - firstDate.getDay();
    } else {
        return 7 - firstDate.getDay() + firstDay;
    }
}

function getWeeksStartAndEndInMonth(month, year, start) {
    let weeks = [];
    var weeklist = "";
    var daylist="";
    firstDate = new Date(year, month, 1),
    lastDate = new Date(year, month + 1, 0),
    numDays = lastDate.getDate();
    /**/console.log("numDays"+numDays);
    console.log("month"+month);
    console.log("year"+year);
    var start = 1;
    var num=0;
    today = new Date();
    var dd = today.getDate();
    var currentday = today.getDate();
    var currentmonth = today.getMonth();
    let end = endFirstWeek(firstDate, 1);
    var monthnum=parseInt(month)+parseInt(1);
    var view_month =leftPad(monthnum, 2);
    var view_sday ="";
    var view_eday ="";
    var st ="";
    var et ="";
    monthName = new Date(year+'-'+monthnum+'-1').toLocaleDateString('de', { month: 'short' });
    console.log(monthName);
    var startdate="";
    var enddate=""
     //console.log(end);
    while (start <= numDays) {
        weeks.push({start: start, end: end});
        num = num+parseInt(1);
        var ordata = toOrdinalSuffix(num);
        //console.log(ordata);
        var scval="";
        if(currentday>=start && currentday<=end && currentmonth==month)
        {
            scval="selected";
            /*console.log(currentday);
            console.log("current week");
            console.log(start+"to"+end);*/
            if(start==1)
            {
                
                var cc=num+1;
                daylist='<li class="page-item"><a href="javascript:void(0);"data-w="0" class="page-link"><i class="lnr-chevron-left "></i></a></li><li class="page-item"><a href="javascript:void(0);" class="page-link"><i class="lnr-calendar-full"></i> '+start+' '+monthName+'  - '+end+'  '+monthName+'</a></li><li class="page-item"><a href="javascript:void(0);" data-w="'+cc+'"  class="page-link timeBasedata"><i class="lnr-chevron-right "></i></a></li>';
                    view_sday =leftPad(start, 2);
                    view_eday =leftPad(end, 2);
                    startdate=year+'-'+view_month+'-'+view_sday;
                    enddate=year+'-'+view_month+'-'+view_eday;
                    st=start;
                    et=end;
                    
            }
            if(numDays == end)
            {
                var cc=num-1;
                daylist='<li class="page-item"><a href="javascript:void(0);" data-w="'+cc+'" class="page-link timeBasedata"><i class="lnr-chevron-left "></i></a></li><li class="page-item"><a href="javascript:void(0);" class="page-link"><i class="lnr-calendar-full"></i> '+start+' '+monthName+'  - '+end+'  '+monthName+'</a></li><li class="page-item"><a href="javascript:void(0);" data-w="0"  class="page-link"><i class="lnr-chevron-right "></i></a></li>';
                    view_sday =leftPad(start, 2);
                    view_eday =leftPad(end, 2);
                    startdate=year+'-'+view_month+'-'+view_sday;
                    enddate=year+'-'+view_month+'-'+view_eday;
                    st=start;
                    et=end;
                    
            }
            if(start!=1 && numDays != end)
            {
                var cc=num+1;
                var ccc=num-1;
                daylist='<li class="page-item"><a href="javascript:void(0);" data-w="'+ccc+'" class="page-link timeBasedata"><i class="lnr-chevron-left "></i></a></li><li class="page-item"><a href="javascript:void(0);" class="page-link"><i class="lnr-calendar-full"></i> '+start+' '+monthName+'  - '+end+'  '+monthName+'</a></li><li class="page-item"><a href="javascript:void(0);" data-w="'+cc+'"  class="page-link timeBasedata"><i class="lnr-chevron-right "></i></a></li>';
                    view_sday =leftPad(start, 2);
                    view_eday =leftPad(end, 2);
                    startdate=year+'-'+view_month+'-'+view_sday;
                    enddate=year+'-'+view_month+'-'+view_eday;
                    st=start;
                    et=end;
                    
            }
        }
        if(start==1)
            {
                var cc=num+1;
                daylist='<li class="page-item"><a href="javascript:void(0);"data-w="0" class="page-link"><i class="lnr-chevron-left "></i></a></li><li class="page-item"><a href="javascript:void(0);" class="page-link"><i class="lnr-calendar-full"></i> '+start+' '+monthName+'  - '+end+'  '+monthName+'</a></li><li class="page-item"><a href="javascript:void(0);" data-w="'+cc+'"  class="page-link timeBasedata"><i class="lnr-chevron-right "></i></a></li>';
                    view_sday =leftPad(start, 2);
                    view_eday =leftPad(end, 2);
                    startdate=year+'-'+view_month+'-'+view_sday;
                    enddate=year+'-'+view_month+'-'+view_eday;
                    st=start;
                    et=end;
                    
            }
        weeklist+='<option data-y="'+year+'" data-m="'+month+'" data-ws="'+start+'" data-we="'+end+'" value="'+num+'" '+scval+'>'+ordata+' Week</option>';
       
        start = end + 1;
        //console.log("start"+start);
        end = end + 7;
        //console.log("end"+end);
        end = start === 1 && end === 8 ? 1 : end;
        if (end > numDays) {
            end = numDays;
        }
    }
    res = {weeklist: weeklist,daylist: daylist,startdate: startdate,enddate: enddate,st: st,et: et};
    return res;
}  
// separte week
function getWeeksStartAndEndInMonth2(month, year, start,st,et) {
    console.log("selectedmonth"+month);
    console.log("selectedyear"+year);
    pmonth = parseInt(month);
    let weeks = [];
    var weeklist = "";
    var daylist="";
    firstDate = new Date(year, pmonth, 1),
    lastDate = new Date(year, pmonth + 1, 0),
    numDays = lastDate.getDate();
    /*console.log("numDays"+numDays);
    console.log("month"+month);
    console.log("year"+year);*/
    var start = 1;
    var num=0;
    today = new Date();
    var dd = today.getDate();
    var currentday = today.getDate();
    var currentmonth = today.getMonth();
    let end = endFirstWeek(firstDate, 1);
    var monthnum=parseInt(pmonth)+parseInt(1);
    monthName = new Date(year+'-'+monthnum+'-1').toLocaleDateString('de', { month: 'short' });
    console.log(monthName);
    var view_month =leftPad(monthnum, 2);
    var view_sday =leftPad(st, 2);
    var view_eday =leftPad(et, 2);
    var startdate=year+'-'+view_month+'-'+view_sday;
    var enddate=year+'-'+view_month+'-'+view_eday;

    //console.log(end);
    while (start <= numDays) {
      //console.log(new Date().toLocaleDateString('de', { weekday: 'short' }));
        weeks.push({start: start, end: end});
        num = num+parseInt(1);
        var ordata = toOrdinalSuffix(num);
        //console.log(ordata);
        var scval="";
        var tt = st-parseInt(1);
        if(st == start )
        {
            console.log("test"+end);
            console.log("test2"+numDays);
            if (end > numDays) {
                end = numDays;
                 console.log("test"+end);
            }
            if(end<numDays)
            {
                var cc=num+1;
                var cadd2="timeBasedata";
            }
            else
            {
                var cc=0;
                var cadd2="";
            }
            if(tt == 0)
            {
                var ccc=0;
                var cadd="";
            }
            else
            {
                var ccc=num-1;
                var cadd="timeBasedata";
            }
            daylist='<li class="page-item"><a href="javascript:void(0);" data-w="'+ccc+'" class="page-link '+cadd+'"><i class="lnr-chevron-left "></i></a></li><li class="page-item"><a href="javascript:void(0);" class="page-link"><i class="lnr-calendar-full"></i> '+start+' '+monthName+'  - '+end+'  '+monthName+'</a></li><li class="page-item"><a href="javascript:void(0);" data-w="'+cc+'"  class="page-link '+cadd2+'"><i class="lnr-chevron-right "></i></a></li>';
        }
        start = end + 1;
        //console.log("start"+start);
        end = end + 7;
        //console.log("end"+end);
        end = start === 1 && end === 8 ? 1 : end;
        if (end > numDays) {
            end = numDays;
        }
    }
    res = {weeklist: weeklist,daylist: daylist,startdate: startdate,enddate: enddate,st: st,et: et};
    console.log(weeks);
    return res;
}  
// defalt call function
function workplanner_cc()
{
   //alert("data");
    var d = new Date();
    var month = d.getMonth();
    var year = d.getFullYear();
    $( "#month" ).val(month);
    var gweek=getWeeksStartAndEndInMonth(month, year,"monday");
    //console.log(gweek);
    /*$( "#week" ).html(gweek.weeklist);
    $( "#pl" ).html(gweek.daylist);*/
    getdata(gweek,"current");
}
function monthChange()
{
    //alert("hugfuggh");
    var d = new Date();
    var year = d.getFullYear();
    var monthdata= $( "#month" ).val();
    var vmonth = parseInt(monthdata);
    var gweek=getWeeksStartAndEndInMonth(vmonth, year,"monday");
   /* console.log(gweek);
    $( "#week" ).html(gweek.weeklist);
    $( "#pl" ).html(gweek.daylist);*/
    getdata(gweek,"month");
}
function weekChange()
{
    //alert("hugfuggh");
    var d = new Date();
    var year = d.getFullYear();
    var syear = $("#week option:selected").attr("data-y")
    var smonth = $("#week option:selected").attr("data-m")
    var st = $("#week option:selected").attr("data-ws")
    var et = $("#week option:selected").attr("data-we")

    console.log(st);
    console.log(et);
    var gweek=getWeeksStartAndEndInMonth2(smonth, syear,"monday",st,et);
    /*console.log(gweek);
    $( "#pl" ).html(gweek.daylist);
*/    getdata(gweek,"week");
    
}
$(document).on('click', '.timeBasedata', function(evt) {
    var weekid = $($(this)).attr("data-w");
    console.log(weekid);
    $( "#week" ).val(weekid);
    var d = new Date();
    var year = d.getFullYear();
    var syear = $("#week option:selected").attr("data-y")
    var smonth = $("#week option:selected").attr("data-m")
    var st = $("#week option:selected").attr("data-ws")
    var et = $("#week option:selected").attr("data-we")
    var gweek=getWeeksStartAndEndInMonth2(smonth, syear,"monday",st,et);
    //console.log(gweek);
    //$( "#pl" ).html(gweek.daylist);
    getdata(gweek,"timeweek");
});

function getdata(datedata,type)
{
   //alert(datedata);
    //alert(type);
    if(type=="current")
    {
        $( "#week" ).html(datedata.weeklist);
        $( "#pl" ).html(datedata.daylist)
    }
    else if(type=="month")
    {
        $( "#week" ).html(datedata.weeklist);
        $( "#pl" ).html(datedata.daylist);
    }
    else if(type=="week")
    {
        $( "#pl" ).html(datedata.daylist);
    }
    else if(type=="timeweek")
    {
        $( "#pl" ).html(datedata.daylist);
    }
    res = datedata.startdate.split("-");
    var dummydateth="";
    var dummydatetd="";
    var smonth =parseInt(res[1])-parseInt(1);
     var syear = parseInt(res[0]);
    var totaldays = (parseInt(datedata.et) - parseInt(datedata.st)) + parseInt(1);
    lastDate = new Date(parseInt(res[0]), parseInt(smonth) + 1, 0),
        numDays = lastDate.getDate();
    var sdateFirst = "";
    var sdateSecond = "";
    if (totaldays < 7) {
        var ft = parseInt(7) - parseInt(totaldays);
        for (v = 0; v < ft; v++) {
            // dummydateth+='<th></th>';
            dummydatetd += '<td></td>';
        }
        if (datedata.st != 1) {
            var gedate = datacorrectformat(syear, res[1], ft);
            sdateFirst = datedata.startdate;
            sdateSecond = gedate;
            console.log("sdateFirst-->" + sdateFirst + "sdateSecond-->" + sdateSecond);
        } else {
            fft = parseInt(ft) - parseInt(1)
            signvar = -Math.abs(fft);
            var gedate = datacorrectformat(syear, smonth, signvar);
            sdateFirst = gedate;
            sdateSecond = datedata.enddate;
            console.log("sdateFirst-->" + sdateFirst + "sdateSecond-->" + sdateSecond);
        }

    } else {
        sdateFirst = datedata.startdate;
        sdateSecond = datedata.enddate;
    }
    DateVariableFun = generateDateList(sdateFirst, sdateSecond);
    var ddl = DateVariableFun.daylist;
    var dml = DateVariableFun.monthlist;
    var dyl = DateVariableFun.yearlist;
    
   /*alert("sdateFirst--->"+sdateFirst);
    alert("sdateSecond--->"+sdateSecond);*/
    $.ajax({
            url: 'ajax-actions_weeklyplannergetdata',
            method: 'POST',
            data: {
                sdate: sdateFirst,
                edate: sdateSecond,
             },
            dataType: 'JSON',
            success: function(res) {
                //alert(res);
                //console.log(res);
                branch = res.result.branch;
                workdata = res.result.workdata;
                finaldata = res.result.rdata;
                girlsdata = res.result.gdata;
                backdate = res.result.datagetdate;
                fullday = res.result.fullday;
                todaynoedate = currentdate();
                /*console.log(res.result.rdata);
                console.log(res.result.backdate);*/
                var blist="";
                var tblist="";
                var bid="";
               if (res.status == 'success' && res.result.status == 'success') {
                    var hviewdata ="";
                    var i;
                    if(totaldays<7 && datedata.st==1)
                    {
                       // hviewdata+=dummydateth;
                    }
                    var daystart = parseInt(datedata.st);
                    var dayend = parseInt(datedata.et);
                    res2 = datedata.startdate.split("-");
                     for (i = 0; i < (ddl.length); i++) {
                    daysval = new Date(dyl[i] + '-' + dml[i] + '-' + ddl[i]).toLocaleDateString('de', {
                        weekday: 'short'
                    });
                    monthval = new Date(dyl[i] + '-' + dml[i] + '-' + ddl[i]).toLocaleDateString('de', {
                        month: 'short'
                    });
                    var work_count = "";
                    var tty = ddl[i];
                    //alert("ddl[i]"+ddl[i]);
                      var fval = finaldata[1][tty];
                      if(fval!=undefined)
                      {
                        var branch_count = fval;
                      }
                      else
                      {
                        var branch_count ="";
                      }
                      hviewdata += '<th><div class="single-date"><a href="javascript:;" class="date">' + daysval + '. ' + ddl[i] + ' ' + monthval + '</a><a href="javascript:void(0)" class="girls-sheduled branchDetailsshow" data-placement="bottom" rel="popover-focus" popover-id="0"><span class="text-primary"><i class="fa fa-circle"></i></span>'+branch_count+' Filialen</a><div id="popover-content-0" class="d-none"><div class="popver-title"> <h5>Tagesübersicht</h5></div><div class="popover-sides left"><ul class="girls-li"><li><span class="text-primary"><i class="fa fa-circle"></i></span> Filiale Name 01  <strong>1</strong></li><li><span class="text-secondary"><i class="fa fa-circle"></i></span> Filiale Name 02 <strong>1</strong></li><li class="total">Total Girls <strong>2</strong></li></ul></div><div class="popover-sides right"> <ul class="girls-li"><li><span class="text-primary"><i class="fa fa-check-circle"></i></span> Working today  <strong>2</strong></li><li><span class="text-success"><i class="fa fa-thumbs-up"></i></span> Available <strong>0</strong></li><li><span class="text-danger"><i class="fa fa-thumbs-down"></i></span> Unavailable <strong>0</strong></li></ul></div></div></div></th>';
                    }

                    if(totaldays<7 && datedata.st!=1)
                    {
                       // hviewdata+=dummydateth;
                    }
                    for(c=0;c<branch.length;c++)
                    {

                        tblist+='<tr>';
                        bid=branch[c].id;
                        //finaldata[2][];
                       var colorv =finaldata[2]["c"+bid];
                       //alert(colorv);
                       //console.log("branchid"+bid);
                       
                            blist+='<li class="list-group-item wplan-h"><div class="widget-content p-0"><div class="widget-content-wrapper"><div class="widget-content-left mr-2"><span class="weekFilialenCircl" style="color:'+branch[c].color+'"><i class="fa fa-circle"></i></span></div><div class="widget-content-left"><div class="widget-heading">'+branch[c].title+'</div><div class="widget-subheading">'+branch[c].place+'</div></div></div></div></li>';
                            /*if(totaldays<7 && datedata.st==1)
                                {
                                    tblist+=dummydatetd;
                                }*/
                            //var backno=0;
                           for (var key in fullday) {
                            bdate = backdate[key];
                            //backno =parseInt(backno)+parseInt(1);
                            /*console.log("---bdate"+bdate);
                            console.log("---k"+k);*/
                           // alert(bdate);
                            var ty=fullday[key];
                           /* if(k<10)
                            {
                                ty ="0"+k; 
                            }
                            else
                            {
                                ty =k;
                            }*/
                                var tval =  ""+bid+""+ty+"";
                                if(finaldata[0][tval])
                                {
                                   var multigirlsid = finaldata[3]["g"+tval];
                                  var girlsList="";
                                   if(multigirlsid!="" || multigirlsid!=null || multigirlsid!=null) 
                                   {
                                    var gres = multigirlsid.split(",");
                                    if(gres.length>0)
                                    {
                                        gres.forEach(function(girlsidval,key){
                                            
                                           var sp_girls = girlsdata[girlsidval];
                                           girlsList+='<span class="girlsView">'+sp_girls+'</span>';
                                        });
                                    }

                                   }
                                        var addeditbutton='';
                                        if(todaynoedate>bdate)
                                        {
                                            addeditbutton = '';
                                        }
                                        else
                                        {
                                            addeditbutton = '<ul class="act"><li><a href="javascript:" data-toggle="tooltip" data-original-title="Erstellen" class="text-success add open-editWeekly" data-date="'+bdate+'" data-branchid="'+bid+'" data-type="edit" ><i class="fa fa-edit"></i></a></li></ul>';
                                        }
                                        
                                    tblist+='<td class="" style="padding:0px 1px 0px 0px;vertical-align: top;"><div style="background:'+branch[c].color+';color:#e9ecef; height:100px"> <div class="scroll-area-td"><div class="wplan scrollbar-container ps">'+girlsList+'</div>'+addeditbutton+'</div></div></td>';
                                }
                                else
                                {
                                    var addeditbuttonnew='';
                                    if(todaynoedate>bdate)
                                        {
                                            addeditbuttonnew = '';
                                        }
                                        else
                                        {
                                            addeditbuttonnew = '<a href="javascript:" data-toggle="tooltip" data-original-title="Erstellen" class="add-shedule open-editWeekly" data-date="'+bdate+'" data-branchid="'+bid+'" data-type="new" ><i class="ion-android-add-circle"></i></a>';
                                        }
                                    tblist+='<td><div class="scroll-area-td"><div class="wplan scrollbar-container ps">'+addeditbuttonnew+'</div></div></td>';
                                    //rightPopupopen
                                }
                          }
                         /* if(totaldays<7 && datedata.st!=1)
                            {
                                tblist+=dummydatetd;
                            }*/
                           tblist+='</tr>';
                            //alert(tblist);
                    }
                    /*console.log(blist);
                   console.log(tblist);*/
                   $("#hdata").html(hviewdata);
                   $("#blistdata").html(blist);
                   $("#tblistdata").html(tblist);
                   /*alert(hviewdata);
                   alert(blist);*/
                   //alert(tblist);
                 } else {
                    alertify.alert('Ungültiger Vorgang erneut versuchen');
                    return false;
                }
            }
        });
    return false;
}
$(document).on('click', '.branchDetailsshow', function(evt) {
    console.log("testdataval");
    $(".popover").toggleClass("show");
});


$(document).on('click', '.add-open', function(evt) {
    //alert("formtest");
    $("#cdate").val("");
    rightPopupopen();
});
//Two date different days

function generateDateList(from, to) {
    var daylist = [];
    var listDate = [];
    var monthlist = [];
    var yearlist = [];
    var startDate = from;
    var endDate = to;
    var dateMove = new Date(startDate);
    var strDate = startDate;

    while (strDate < endDate) {
        var strDate = dateMove.toISOString().slice(0, 10);
        listDate.push(strDate);
        var sst = strDate.split('-');
        daylist.push(sst[2]);
        monthlist.push(sst[1]);
        yearlist.push(sst[0]);
        dateMove.setDate(dateMove.getDate() + 1);
    };

    res = {
        datelist: listDate,
        daylist: daylist,
        monthlist: monthlist,
        yearlist: yearlist
    };
    return res;
}
// Get Correct dateformat
function datacorrectformat(ydata, mdata, ddata) {
    //alert(ydata);alert(mdata);alert(ddata);
    var today = new Date(ydata, mdata, ddata);
    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }
    today = yyyy + '-' + mm + '-' + dd;
    //alert(today);
    return today;
}

$(document).on('click', '.open-editWeekly', function(evt) {
    var type =$(this).attr("data-type"); 
    var cdate =$(this).attr("data-date"); 
    var branchid =$(this).attr("data-branchid");
    /*alert("type"+type);
    alert("cdate"+cdate);
    alert(branchid);*/
    if(type=="new") 
    {
        $("#cdate").val(data_change_format2(cdate));
        $("#sbranch").val(branchid); 
        $("#edittype").val("");
        //rightPopupopen();
    }
    else if(type=="edit")
    {
        $("#cdate").val(data_change_format2(cdate));
        $("#sbranch").val(branchid); 
        $("#edittype").val("olddata");
        //rightPopupopen();
    }
    else
    {
        $("#cdate").val("");
        $("#sbranch").val(""); 
        $("#edittype").val("");
    }
    $.ajax({
            url: 'ajax-actions_workgirls',
            method: 'POST',
            data: {
                cdate: cdate,
                type: type,
                branch: branchid,
             },
            dataType: 'JSON',
            success: function(res) {
                //alert(res);
                console.log(res);
                var glistdata="";
                var activeglistdata="";
                var inactiveglistdata="";
                var presentglistdata="";
                if(res.status =="success")
                {
                    var girlsList = res.result.girls;
                    if(girlsList.length>0)
                    {
                        girlsList.forEach(function(girls){
                            if(girls.branch_id==null || girls.branch_id==branchid)
                            {
                               var ch ="";
                                var classdata="";
                                var classdata_overlay="";
                                //alert("gcount"+girls.gcount);
                                //alert("bgcount"+girls.bgcount);
                                if(girls.gcount>0)
                                {
                                    ch="checked";
                                }
                                if(girls.bgcount>0)
                                {
                                    classdata="bookedgirls";
                                    classdata_overlay="bookedGirlsOverlay";
                                }
                                glistdata+='<li class="list-group-item '+classdata_overlay+'"><div class="widget-content"><div class="widget-content-wrapper"><div class="widget-content-left mr-2"><div class="custom-checkbox"><input type="checkbox" id="" class="ads_Checkbox '+classdata+'" '+ch+' value="'+girls.id+'"></div></div><div class="widget-content-left mr-2"><div class="widget-content-left erstellen-eirls"><span style="background: url('+aws_viewlink_gallery+girls.pimage+');"></span></div></div><div class="widget-content-left"><div class="widget-heading">'+girls.profilename+'</div><div class="widget-subheading"></div></div></div></div></li>';
                                if(girls.status==1)
                                {
                                    activeglistdata+='<li class="list-group-item"><div class="widget-content"><div class="widget-content-wrapper"><div class="widget-content-left mr-2"><div class="custom-checkbox"><input type="checkbox" id="" class="ads_Checkbox" value="'+girls.id+'"></div></div><div class="widget-content-left mr-2"><div class="widget-content-left erstellen-eirls"><span style="background: url('+aws_viewlink_gallery+girls.pimage+');"></span></div></div><div class="widget-content-left"><div class="widget-heading">'+girls.profilename+'</div><div class="widget-subheading"></div></div></div></div></li>';
                                }
                                if(girls.status!=1)
                                {
                                    inactiveglistdata+='<li class="list-group-item"><div class="widget-content"><div class="widget-content-wrapper"><div class="widget-content-left mr-2"><div class="custom-checkbox"><input type="checkbox" id="" class="ads_Checkbox"  value="'+girls.id+'" ></div></div><div class="widget-content-left mr-2"><div class="widget-content-left erstellen-eirls"><span style="background: url('+aws_viewlink_gallery+girls.pimage+');"></span></div></div><div class="widget-content-left"><div class="widget-heading">'+girls.profilename+'</div><div class="widget-subheading"></div></div></div></div></li>';
                                }
                                presentglistdata+='<li class="list-group-item"><div class="widget-content"><div class="widget-content-wrapper"><div class="widget-content-left mr-2"><div class="custom-checkbox"><input type="checkbox" id="" class="ads_Checkbox" value="'+girls.id+'"></div></div><div class="widget-content-left mr-2"><div class="widget-content-left erstellen-eirls"><span style="background: url('+aws_viewlink_gallery+girls.pimage+');"></span></div></div><div class="widget-content-left"><div class="widget-heading">'+girls.profilename+'</div><div class="widget-subheading"></div></div></div></div></li>';
                           }
                        });
                       
                    }
                    //console.log(glistdata);
                    $("#s_all").html(glistdata);
                    $("#s_active").html(activeglistdata);
                    $("#s_inactive").html(inactiveglistdata);
                    $("#s_present").html(presentglistdata);
                    //$(".bookedgirls").prop("readonly", true);
                     //$(".bookedgirls").prop('checked', true).attr("readonly")
                    rightPopupopen();
                }
                
            }
        });
    
   
});

// date format function
function data_change_format2(str)
{
  console.log(str);
  var fDate = "";
  if(str!="" && str!=undefined && str!=null)
   {
    var res = str.split("-");
    fDate = res[2]+'.'+res[1]+'.'+res[0];
   }
   
  return fDate;
}
//currentdate
function currentdate()
{
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    return today = yyyy + '-' + mm + '-' + dd;
    //console.log(today);
}
// Hide checkbox function
$(document).on('click', '.bookedgirls', function(evt) {
  return false;
});

