 // dateconfiguration

var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
 $('.swissSdate').daterangepicker({
     singleDatePicker: true,
     startDate: moment(),
     autoUpdateInput: false,
     locale: {
         "format": "DD.MM.YYYY",
         "daysOfWeek": [
             "So",
             "Mo",
             "Di",
             "Mi",
             "Do",
             "Fr",
             "Sa"

         ],
         "monthNames": [
             "Jan",
             "Feb",
             "Mär",
             "Apr",
             "Mai",
             "Jun",
             "Jul",
             "Aug",
             "Sep",
             "Okt",
             "Nov",
             "Dez"
         ],
         "firstDay": 1
     }

 });
 $('.swissSdate').on('apply.daterangepicker', function(ev, picker) {
     $(this).val(picker.startDate.format('DD.MM.YYYY'));
 });
 $('.swissEdate').daterangepicker({
     singleDatePicker: true,
     startDate: moment(),
     autoUpdateInput: false,
     locale: {
         "format": "DD.MM.YYYY",
         "daysOfWeek": [
             "So",
             "Mo",
             "Di",
             "Mi",
             "Do",
             "Fr",
             "Sa"

         ],
         "monthNames": [
             "Jan",
             "Feb",
             "Mär",
             "Apr",
             "Mai",
             "Jun",
             "Jul",
             "Aug",
             "Sep",
             "Okt",
             "Nov",
             "Dez"
         ],
         "firstDay": 1
     }

 });
 $('.swissEdate').on('apply.daterangepicker', function(ev, picker) {
     $(this).val(picker.startDate.format('DD.MM.YYYY'));
 });

 function dateAuto_startdate() {

     $('.swissSdate').daterangepicker({
         singleDatePicker: true,
         startDate: moment(),
         autoUpdateInput: false,
         locale: {
             "format": "DD.MM.YYYY",
             "daysOfWeek": [
                 "So",
                 "Mo",
                 "Di",
                 "Mi",
                 "Do",
                 "Fr",
                 "Sa"

             ],
             "monthNames": [
                 "Jan",
                 "Feb",
                 "Mär",
                 "Apr",
                 "Mai",
                 "Jun",
                 "Jul",
                 "Aug",
                 "Sep",
                 "Okt",
                 "Nov",
                 "Dez"
             ],
             "firstDay": 1
         }

     });
     $('.swissSdate').on('apply.daterangepicker', function(ev, picker) {
         $(this).val(picker.startDate.format('DD.MM.YYYY'));
     });
 }

 function dateAuto_enddate() {

     $('.swissSdate').daterangepicker({
         singleDatePicker: true,
         startDate: moment(),
         autoUpdateInput: false,
         locale: {
             "format": "DD.MM.YYYY",
             "daysOfWeek": [
                 "So",
                 "Mo",
                 "Di",
                 "Mi",
                 "Do",
                 "Fr",
                 "Sa"

             ],
             "monthNames": [
                 "Jan",
                 "Feb",
                 "Mär",
                 "Apr",
                 "Mai",
                 "Jun",
                 "Jul",
                 "Aug",
                 "Sep",
                 "Okt",
                 "Nov",
                 "Dez"
             ],
             "firstDay": 1
         }

     });
     $('.swissSdate').on('apply.daterangepicker', function(ev, picker) {
         $(this).val(picker.startDate.format('DD.MM.YYYY'));
     });
 }