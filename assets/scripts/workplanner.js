var timet = "00:00";
var timeOptionc = [];
secs = '00';
mnts = '00';
for (i = 0; i < 95; i++) {
    secs = parseInt(secs) + 15;
    if (secs >= 60) {
        secs = "00";
        mnts = parseInt(mnts) + 1;
    }
    timet = mnts + ':' + secs;
    timeOptionc.push(timet);
}
$(document).on('click', '.add-work', function(evt) {
   // alert("uhghih");
    var cdate = $($(this)).attr("data-cdate");
    var branch = $($(this)).attr("data-branchid");
    var employeename = $($(this)).attr("data-employee");
    var editid = $($(this)).attr("data-id");
    console.log(cdate);
    console.log(branch);
    console.log(employeename);
    console.log(editid);
    $.ajax({
        url: 'ajax-actions_getworkplanner',
        method: 'POST',
        dataType: 'JSON',
        data: {
            cdate: cdate,
            branchid: branch,
            employeename: employeename,
            editid: editid
        },
        success: function(res) {
            console.log(res);
            branch = res.result.branch;
            workplanner = res.result.workplanner;
            worktime = res.result.worktime;
            workgirls = res.result.girls;
            $("#addtime").html("");
            $("#editid").val("");
            $("#rowcount").val(1);
            var edata = "";
            stime = "";
            etime = "";
            if (res.status == 'success' && res.result.status == 'success') {
                branch_select = "";
                girls_select = "";

                if (workplanner.length > 0) {
                    var correctdate = data_change_format_reverse(workplanner[0].cdate);
                    $("#cdate").val(correctdate);
                    $("#editid").val(workplanner[0].id);
                    $("#rowcount").val(worktime.length);
                    branch.forEach(function(bdata) {
                        var scc = "";
                        if (bdata.id == workplanner[0].branch_id) {
                            scc = "selected";
                        }
                        branch_select += ' <option value="' + bdata.id + '" ' + scc + '>' + bdata.title + '</option>';
                    });
                    workgirls.forEach(function(gdata) {
                        var scc = "";
                        if (gdata.id == workplanner[0].lead) {
                            scc = "selected";
                        }
                        girls_select += ' <option value="' + gdata.id + '" ' + scc + '>' + gdata.gname + '</option>';
                    });
                    $("#branchid").html(branch_select);
                    $("#employeename").html(girls_select);
                    //$("#employeename").val(workplanner[0].lead);
                    var tval = 0;
                    for (var k = 0; k < worktime.length; k++) {
                        timeOptionc.forEach(function(times) {
                            time1 = times != "" ? times.split(":") : [];
                            if (time1[0].length > 1) {
                                opentime1 = times;
                            } else {
                                opentime1 = '0' + times;
                            }
                            if (opentime1 == worktime[k].starttime) {
                                var sc = "selected";
                                console.log("opentime1" + worktime[k].starttime);
                            } else {
                                var sc = "";
                            }
                            if (opentime1 == worktime[k].endtime) {
                                var scc = "selected";
                                console.log("opentime1" + worktime[k].endtime);
                            } else {
                                var scc = "";
                            }
                            console.log("sc" + sc)
                            stime += '<option value="' + opentime1 + '" ' + sc + '>' + opentime1 + '</option>';
                            etime += '<option value="' + opentime1 + '" ' + scc + '>' + opentime1 + '</option>';

                        });
                        tval = parseInt(tval) + parseInt(1);
                        if (tval == 1) {
                            edata += '<div id="t' + tval + '"><div class="form-row mb-3" ><div class="col-md-5"><select type="select" name="starttime" class="custom-select">' + stime + '</select></div><div class="col-md-5"><select type="select" name="endtime" class="custom-select">' + etime + '</select></div><div class="col-md-2"><button class="btn btn-block btn-primary btn-lg addTimeDate "><i class="lnr-plus-circle"></i></button></div></div></div>';
                        } else {
                            edata += '<div id="t' + tval + '"><div class="form-row mb-3" ><div class="col-md-5"><select type="select" name="starttime" class="custom-select">' + stime + '</select></div><div class="col-md-5"><select type="select" name="endtime" class="custom-select">' + etime + '</select></div><div class="col-md-2"><button data-rid="t' + tval + '" class="btn btn-block btn-danger btn-lg removeTimeDate "><i class="lnr-circle-minus"></i></button></div></div></div>';
                        }

                    }
                    $("#addtime").append(edata);

                } else {
                    branch.forEach(function(bdata) {

                        branch_select += ' <option value="' + bdata.id + '">' + bdata.title + '</option>';
                    });
                    workgirls.forEach(function(gdata) {
                        var scc = "";
                        girls_select += ' <option value="' + gdata.id + '" ' + scc + '>' + gdata.gname + '</option>';
                    });
                    timeOptionc.forEach(function(times) {
                        time1 = times != "" ? times.split(":") : [];
                        if (time1[0].length > 1) {
                            opentime1 = times;
                        } else {
                            opentime1 = '0' + times;
                        }

                        stime += '<option value="' + opentime1 + '" >' + opentime1 + '</option>';
                        etime += '<option value="' + opentime1 + '" >' + opentime1 + '</option>';

                    });
                    $("#branchid").html(branch_select);
                    $("#employeename").html(girls_select);
                    $("#addtime").append('<div id="t1"><div class="form-row mb-3" ><div class="col-md-5"><select type="select" name="starttime" class="custom-select">' + stime + '</select></div><div class="col-md-5"><select type="select" name="endtime" class="custom-select">' + etime + '</select></div><div class="col-md-2"><button class="btn btn-block btn-primary btn-lg addTimeDate "><i class="lnr-plus-circle"></i></button></div></div></div>');
                }
                rightPopupopen();
            } else {
                console.log("errror page");
                alertify.alert('ungültiger Prozess erneut versuchen');
                return false;
            }
        }
    });

    return false;
});
// add multipile time

$(document).on('click', '.addTimeDate', function(evt) {
    var tbt = '';
    var tc = "",
        stime = "",
        etime = "";
    tc = $("#rowcount").val();
    tc = parseInt(tc) + parseInt(1);
    $("#rowcount").val(tc);
    //console.log(tc);
    timeOptionc.forEach(function(times) {
        time1 = times != "" ? times.split(":") : [];
        if (time1[0].length > 1) {
            opentime1 = times;
        } else {
            opentime1 = '0' + times;
        }
        var sc = "";
        //console.log("opentime1"+opentime1)
        /*if (opentime1 == content[j].start_time) {
            var sc = "selected";
            console.log("opentime1" + content[j].start_time);
        } else {
            var sc = "";
        }
        console.log("sc" + sc)*/
        stime += '<option value="' + opentime1 + '" ' + sc + '>' + opentime1 + '</option>';
        etime += '<option value="' + opentime1 + '" ' + sc + '>' + opentime1 + '</option>';

    });
    var timeselect = '';
    if (tc < 1) {
        tbt = '<button class="btn btn-block btn-primary btn-lg addTimeDate "><i class="lnr-plus-circle"></i></button>';
    } else {
        tbt = '<button class="btn btn-block btn-danger btn-lg removeTimeDate " data-rid="t' + tc + '" ><i class="lnr-circle-minus"></i></button>';
    }
    timeselect = '<div id="t' + tc + '"><div class="form-row mb-3" ><div class="col-md-5"><select type="select" name="starttime" class="custom-select">' + stime + '</select></div><div class="col-md-5"><select type="select" name="endtime" class="custom-select">' + etime + '</select></div><div class="col-md-2">' + tbt + '</div></div></div>';

    $("#addtime").append(timeselect);
});
//remove options
$(document).on('click', '.removeTimeDate', function(evt) {
    var rid = $($(this)).attr("data-rid");
    $("#" + rid).html("");
});
$(document).on('click', '.removeOldData', function(evt) {

    $("#cdate").html("");
    $("#brnachid").val("");
    $("#employeename").val("");
    $("#addTime").html("");
    $("#editid").val("");
    $("#rowcount").val(1);
});
//create workplanner
$(document).on('click', '.workplanesubmit', function(e) {
    var starttime = [];
    var endtime = [];
    var branchid = $('#branchid').val();
    var cdate = $('#cdate').val();
    var employeename = $('#employeename').val();
    var editid = $('#editid').val();
    var stime = document.getElementsByName('starttime');
    var etime = document.getElementsByName('endtime');
    for (var i = 0; i < stime.length; i++) {
        starttime.push(stime[i].value);
        endtime.push(etime[i].value);
    }
    console.log(starttime);
    console.log(endtime);
    if (!validateDate(cdate) || cdate == '') {
        $('#cdate').closest('.form-group').find('.mandatory').html('Datum auswählen (DD.MM.YYYY)');
        $('#cdate').focus();
        return false;
    } else if (branchid == '') {
        $('#branchid').closest('.form-group').find('.mandatory').html('Wählen Filiale');
        $('#branchid').focus();
        return false;
    } else if (employeename == '') {
        $('#employeename').closest('.form-group').find('.mandatory').html('Wählen Sie Mitarbeiter');
        $('#employeename').focus();
        return false;
    } else {
        $.ajax({
            url: 'ajax-actions_createworkplanner',
            method: 'POST',
            dataType: 'JSON',
            data: {
                cdate: data_change_format(cdate),
                branchid: branchid,
                employeename: employeename,
                editid: editid,
                starttime: starttime,
                endtime: endtime
            },
            success: function(res) {
                console.log(res);
                if (res.status == 'success' && res.result.status == 'success') {
                    location.href = 'arbeitsplan';
                    //alertify.alert('Erfolgreich erstellt');
                } else {
                    console.log("errror page");
                    alertify.alert('ungültiger Prozess erneut versuchen');
                    return false;
                }
            }
        });
    }
    return false;

});
//Remove workplanner
$(document).on('click', '.rm-work', function(e) {
    var id = $($(this)).attr("data-id");

    if (id == '') {

        return false;
    } else {
        $.ajax({
            url: 'ajax-actions_removeworkplanner',
            method: 'POST',
            dataType: 'JSON',
            data: {
                id: id
            },
            success: function(res) {
                console.log(res);
                if (res.status == 'success' && res.result.status == 'success') {
                    location.href = 'arbeitsplan';
                    //alertify.alert('Erfolgreich erstellt');
                } else {
                    console.log("errror page");
                    alertify.alert('ungültiger Prozess erneut versuchen');
                    return false;
                }
            }
        });
    }
    return false;

});
// workplanner call function
// leftpad
function leftPad(number, targetLength) {
    var output = number + '';
    while (output.length < targetLength) {
        output = '0' + output;
    }
    return output;
}
// time function
function endFirstWeek(firstDate, firstDay) {
    if (!firstDay) {
        return 7 - firstDate.getDay();
    }
    if (firstDate.getDay() < firstDay) {
        return firstDay - firstDate.getDay();
    } else {
        return 7 - firstDate.getDay() + firstDay;
    }
}

function getWeeksStartAndEndInMonth_girls(month, year, start) {
    let weeks = [];
    var weeklist = "";
    var daylist = "";
    firstDate = new Date(year, month, 1),
        lastDate = new Date(year, month + 1, 0),
        numDays = lastDate.getDate();
    /**/
    console.log("numDays" + numDays);
    console.log("month" + month);
    console.log("year" + year);
    var start = 1;
    var num = 0;
    today = new Date();
    var dd = today.getDate();
    var currentday = today.getDate();
    var currentmonth = today.getMonth();
    let end = endFirstWeek(firstDate, 1);
    var monthnum = parseInt(month) + parseInt(1);
    var view_month = leftPad(monthnum, 2);
    var view_sday = "";
    var view_eday = "";
    var st = "";
    var et = "";
    monthName = new Date(year + '-' + monthnum + '-1').toLocaleDateString('de', {
        month: 'short'
    });
    console.log(monthName);
    var startdate = "";
    var enddate = ""
        //console.log(end);
    while (start <= numDays) {
        weeks.push({
            start: start,
            end: end
        });
        num = num + parseInt(1);
        var ordata = toOrdinalSuffix(num);
        //console.log(ordata);
        var scval = "";
        if (currentday >= start && currentday <= end && currentmonth == month) {
            scval = "selected";
            /*console.log(currentday);
            console.log("current week");
            console.log(start+"to"+end);*/
            if (start == 1) {

                var cc = num + 1;
                daylist = '<li class="page-item"><a href="javascript:void(0);"data-w="0" class="page-link"><i class="lnr-chevron-left "></i></a></li><li class="page-item"><a href="javascript:void(0);" class="page-link"><i class="lnr-calendar-full"></i> ' + start + ' ' + monthName + '  - ' + end + '  ' + monthName + '</a></li><li class="page-item"><a href="javascript:void(0);" data-w="' + cc + '"  class="page-link timeBasedata_girls"><i class="lnr-chevron-right "></i></a></li>';
                view_sday = leftPad(start, 2);
                view_eday = leftPad(end, 2);
                startdate = year + '-' + view_month + '-' + view_sday;
                enddate = year + '-' + view_month + '-' + view_eday;
                st = start;
                et = end;

            }
            if (numDays == end) {
                var cc = num - 1;
                daylist = '<li class="page-item"><a href="javascript:void(0);" data-w="' + cc + '" class="page-link timeBasedata_girls"><i class="lnr-chevron-left "></i></a></li><li class="page-item"><a href="javascript:void(0);" class="page-link"><i class="lnr-calendar-full"></i> ' + start + ' ' + monthName + '  - ' + end + '  ' + monthName + '</a></li><li class="page-item"><a href="javascript:void(0);" data-w="0"  class="page-link"><i class="lnr-chevron-right "></i></a></li>';
                view_sday = leftPad(start, 2);
                view_eday = leftPad(end, 2);
                startdate = year + '-' + view_month + '-' + view_sday;
                enddate = year + '-' + view_month + '-' + view_eday;
                st = start;
                et = end;

            }
            if (start != 1 && numDays != end) {
                var cc = num + 1;
                var ccc = num - 1;
                daylist = '<li class="page-item"><a href="javascript:void(0);" data-w="' + ccc + '" class="page-link timeBasedata_girls"><i class="lnr-chevron-left "></i></a></li><li class="page-item"><a href="javascript:void(0);" class="page-link"><i class="lnr-calendar-full"></i> ' + start + ' ' + monthName + '  - ' + end + '  ' + monthName + '</a></li><li class="page-item"><a href="javascript:void(0);" data-w="' + cc + '"  class="page-link timeBasedata_girls"><i class="lnr-chevron-right "></i></a></li>';
                view_sday = leftPad(start, 2);
                view_eday = leftPad(end, 2);
                startdate = year + '-' + view_month + '-' + view_sday;
                enddate = year + '-' + view_month + '-' + view_eday;
                st = start;
                et = end;

            }
        }
        if (start == 1) {
            var cc = num + 1;
            daylist = '<li class="page-item"><a href="javascript:void(0);"data-w="0" class="page-link"><i class="lnr-chevron-left "></i></a></li><li class="page-item"><a href="javascript:void(0);" class="page-link"><i class="lnr-calendar-full"></i> ' + start + ' ' + monthName + '  - ' + end + '  ' + monthName + '</a></li><li class="page-item"><a href="javascript:void(0);" data-w="' + cc + '"  class="page-link timeBasedata_girls"><i class="lnr-chevron-right "></i></a></li>';
            view_sday = leftPad(start, 2);
            view_eday = leftPad(end, 2);
            startdate = year + '-' + view_month + '-' + view_sday;
            enddate = year + '-' + view_month + '-' + view_eday;
            st = start;
            et = end;

        }
        weeklist += '<option data-y="' + year + '" data-m="' + month + '" data-ws="' + start + '" data-we="' + end + '" value="' + num + '" ' + scval + '>' + ordata + ' Week</option>';

        start = end + 1;
        //console.log("start"+start);
        end = end + 7;
        //console.log("end"+end);
        end = start === 1 && end === 8 ? 1 : end;
        if (end > numDays) {
            end = numDays;
        }
    }
    res = {
        weeklist: weeklist,
        daylist: daylist,
        startdate: startdate,
        enddate: enddate,
        st: st,
        et: et
    };
    return res;
}
// separate week
function getWeeksStartAndEndInMonth2_girls(month, year, start, st, et) {
    console.log("selectedmonth" + month);
    console.log("selectedyear" + year);
    pmonth = parseInt(month);
    let weeks = [];
    var weeklist = "";
    var daylist = "";
    firstDate = new Date(year, pmonth, 1),
        lastDate = new Date(year, pmonth + 1, 0),
        numDays = lastDate.getDate();
    /*console.log("numDays"+numDays);
    console.log("month"+month);
    console.log("year"+year);*/
    var start = 1;
    var num = 0;
    today = new Date();
    var dd = today.getDate();
    var currentday = today.getDate();
    var currentmonth = today.getMonth();
    let end = endFirstWeek(firstDate, 1);
    var monthnum = parseInt(pmonth) + parseInt(1);
    monthName = new Date(year + '-' + monthnum + '-1').toLocaleDateString('de', {
        month: 'short'
    });
    console.log(monthName);
    var view_month = leftPad(monthnum, 2);
    var view_sday = leftPad(st, 2);
    var view_eday = leftPad(et, 2);
    var startdate = year + '-' + view_month + '-' + view_sday;
    var enddate = year + '-' + view_month + '-' + view_eday;

    //console.log(end);
    while (start <= numDays) {
        //console.log(new Date().toLocaleDateString('de', { weekday: 'short' }));
        weeks.push({
            start: start,
            end: end
        });
        num = num + parseInt(1);
        var ordata = toOrdinalSuffix(num);
        //console.log(ordata);
        var scval = "";
        var tt = st - parseInt(1);
        if (st == start) {
            //console.log("test"+end);
            // console.log("test2"+numDays);
            if (end > numDays) {
                end = numDays;
                console.log("test" + end);
            }
            if (end < numDays) {
                var cc = num + 1;
                var cadd2 = "timeBasedata_girls";
            } else {
                var cc = 0;
                var cadd2 = "";
            }
            if (tt == 0) {
                var ccc = 0;
                var cadd = "";
            } else {
                var ccc = num - 1;
                var cadd = "timeBasedata_girls";
            }
            daylist = '<li class="page-item"><a href="javascript:void(0);" data-w="' + ccc + '"  class="page-link ' + cadd + '"><i class="lnr-chevron-left "></i></a></li><li class="page-item"><a href="javascript:void(0);" class="page-link"><i class="lnr-calendar-full"></i> ' + start + ' ' + monthName + '  - ' + end + '  ' + monthName + '</a></li><li class="page-item"><a href="javascript:void(0);" data-w="' + cc + '"  class="page-link ' + cadd2 + '"><i class="lnr-chevron-right "></i></a></li>';
        }
        start = end + 1;
        //console.log("start"+start);
        end = end + 7;
        //console.log("end"+end);
        end = start === 1 && end === 8 ? 1 : end;
        if (end > numDays) {
            end = numDays;
        }
    }
    res = {
        weeklist: weeklist,
        daylist: daylist,
        startdate: startdate,
        enddate: enddate,
        st: st,
        et: et
    };
    //console.log(weeks);
    return res;
}
// defalt call function
function workplanner_girls() {
    //alert("data");
    var d = new Date();
    var month = d.getMonth();
    var year = d.getFullYear();
    $("#month").val(month);
    var gweek = getWeeksStartAndEndInMonth_girls(month, year, "monday");
    //console.log(gweek);
    /*$( "#week" ).html(gweek.weeklist);
    $( "#pl" ).html(gweek.daylist);*/
    getdata_girls(gweek, "current");
}

function weekChange_girls() {
    //alert("hugfuggh");
    var d = new Date();
    var year = d.getFullYear();
    var syear = $("#week option:selected").attr("data-y")
    var smonth = $("#week option:selected").attr("data-m")
    var st = $("#week option:selected").attr("data-ws")
    var et = $("#week option:selected").attr("data-we")

    console.log(st);
    console.log(et);
    var gweek = getWeeksStartAndEndInMonth2_girls(smonth, syear, "monday", st, et);
    /*console.log(gweek);
    $( "#pl" ).html(gweek.daylist);
*/
    getdata_girls(gweek, "week");

}

function branchChange_girls() {
    var d = new Date();
    var year = d.getFullYear();
    var syear = $("#week option:selected").attr("data-y")
    var smonth = $("#week option:selected").attr("data-m")
    var st = $("#week option:selected").attr("data-ws")
    var et = $("#week option:selected").attr("data-we")

    console.log(st);
    console.log(et);
    var gweek = getWeeksStartAndEndInMonth2_girls(smonth, syear, "monday", st, et);
    /*console.log(gweek);
    $( "#pl" ).html(gweek.daylist);
*/
    getdata_girls(gweek, "week");
}
$(document).on('click', '.timeBasedata_girls', function(evt) {
    var weekid = $($(this)).attr("data-w");
    //console.log(weekid);
    $("#week").val(weekid);
    var d = new Date();
    var year = d.getFullYear();
    var syear = $("#week option:selected").attr("data-y")
    var smonth = $("#week option:selected").attr("data-m")
    var st = $("#week option:selected").attr("data-ws")
    var et = $("#week option:selected").attr("data-we")
    var gweek = getWeeksStartAndEndInMonth2_girls(smonth, syear, "monday", st, et);
    //console.log(gweek);
    //$( "#pl" ).html(gweek.daylist);
    getdata_girls(gweek, "timeweek");
});

function getdata_girls(datedata, type) {
    console.log(datedata);
    console.log(type);
    var branchtype = $("#branchtype").val();
    if (type == "current") {
        $("#week").html(datedata.weeklist);
        $("#pl").html(datedata.daylist)
    } else if (type == "month") {
        $("#week").html(datedata.weeklist);
        $("#pl").html(datedata.daylist);
    } else if (type == "week") {
        $("#pl").html(datedata.daylist);
    } else if (type == "timeweek") {
        $("#pl").html(datedata.daylist);
    }
    res = datedata.startdate.split("-");
    var dummydateth = "";
    var dummydatetd = "";
    var smonth = parseInt(res[1]) - parseInt(1);
    var syear = parseInt(res[0]);
    var totaldays = (parseInt(datedata.et) - parseInt(datedata.st)) + parseInt(1);
    lastDate = new Date(parseInt(res[0]), parseInt(smonth) + 1, 0),
        numDays = lastDate.getDate();
    var sdateFirst = "";
    var sdateSecond = "";
    if (totaldays < 7) {
        var ft = parseInt(7) - parseInt(totaldays);
        for (v = 0; v < ft; v++) {
            // dummydateth+='<th></th>';
            dummydatetd += '<td></td>';
        }
        if (datedata.st != 1) {
            var gedate = datacorrectformat(syear, res[1], ft);
            sdateFirst = datedata.startdate;
            sdateSecond = gedate;
            console.log("sdateFirst-->" + sdateFirst + "sdateSecond-->" + sdateSecond);
        } else {
            fft = parseInt(ft) - parseInt(1)
            signvar = -Math.abs(fft);
            var gedate = datacorrectformat(syear, smonth, signvar);
            sdateFirst = gedate;
            sdateSecond = datedata.enddate;
            console.log("sdateFirst-->" + sdateFirst + "sdateSecond-->" + sdateSecond);
        }

    } else {
        sdateFirst = datedata.startdate;
        sdateSecond = datedata.enddate;
    }
    DateVariableFun = generateDateList(sdateFirst, sdateSecond);
    var ddl = DateVariableFun.daylist;
    var dml = DateVariableFun.monthlist;
    var dyl = DateVariableFun.yearlist;
    $.ajax({
        url: 'ajax-actions_workplannergetdata',
        method: 'POST',
        data: {
            sdate: sdateFirst,
            edate: sdateSecond,
            branchtype: branchtype,
        },
        dataType: 'JSON',
        success: function(res) {
            console.log(res);
            branch = res.result.branch;
            workdata = res.result.workdata;
            finaldata = res.result.rdata;
            ccdata = res.result.ccount[0];
            //console.log("ccdata"+ccdata);
            var ccount = "";
            if (ccdata != undefined && ccdata != null && ccdata != "") {
                ccount = res.result.ccount[0].mdata;
            }
            //console.log("ccount"+ccount);
            colcount = res.result.colcount;
            fdata = res.result.fdata;
            timecount = res.result.timecount;
            ChkData = [];
            //console.log(timecount);
            var blist = "";
            var tblist = "";
            var bid = "";
            var totalpoint = [];
            if (res.status == 'success' && res.result.status == 'success') {
                var hviewdata = "";
                var i;
                if (totaldays < 7 && datedata.st == 1) {
                    //hviewdata+=dummydateth;
                }
                var daystart = parseInt(datedata.st);
                var dayend = parseInt(datedata.et);
                res2 = datedata.startdate.split("-");
                for (i = 0; i < (ddl.length); i++) {
                    daysval = new Date(dyl[i] + '-' + dml[i] + '-' + ddl[i]).toLocaleDateString('de', {
                        weekday: 'short'
                    });
                    monthval = new Date(dyl[i] + '-' + dml[i] + '-' + ddl[i]).toLocaleDateString('de', {
                        month: 'short'
                    });
                    var work_count = "";
                    var tty = ddl[i];
                    var ccv = colcount[0][tty];
                    var count_girls = (ccv != undefined && ccv != null && ccv != "") ? ccv : '0';
                    hviewdata += '<th><div class="single-date"><a href="javascript:;" class="date">' + daysval + '. ' + ddl[i] + ' ' + monthval + '</a><ul class="control"><li><a href="javascript:;" data-toggle="tooltip" data-original-title="Ansicht"><i class="fa fa-eye"></i></a></li><li><a href="javascript:;" data-toggle="tooltip" data-original-title="Bearbeiten"><i class="fa fa-edit"></i></a></li></ul><a href="javascript:void(0)" class="girls-sheduled branchDetailsshow" data-placement="bottom" rel="popover-focus" popover-id="0"><span class="text-primary"><i class="fa fa-circle"></i></span> ' + count_girls + ' Benutzer</a><div id="popover-content-0" class="d-none"><div class="popver-title"> <h5>Tagesübersicht</h5></div><div class="popover-sides left"><ul class="girls-li"><li><span class="text-primary"><i class="fa fa-circle"></i></span> Filiale Name 01  <strong>1</strong></li><li><span class="text-secondary"><i class="fa fa-circle"></i></span> Filiale Name 02 <strong>1</strong></li><li class="total">Total Girls <strong>2</strong></li></ul></div><div class="popover-sides right"> <ul class="girls-li"><li><span class="text-primary"><i class="fa fa-check-circle"></i></span> Working today  <strong>2</strong></li><li><span class="text-success"><i class="fa fa-thumbs-up"></i></span> Available <strong>0</strong></li><li><span class="text-danger"><i class="fa fa-thumbs-down"></i></span> Unavailable <strong>0</strong></li></ul></div></div></div></th>';
                }

                if (totaldays < 7 && datedata.st != 1) {
                    //hviewdata+=dummydateth;
                }
                if (ccount != undefined && ccount != null && ccount != "") {
                    for (c = 0; c < ccount; c++) {
                        /*console.log("c"+c);
                        console.log(colcount);
                        console.log("sdate"+datedata.st);
                        console.log("edate"+datedata.et);*/
                        sval = parseInt(datedata.st);
                        edval = parseInt(datedata.et);
                        tblist += '<tr>';
                        if (totaldays < 7 && sval == 1) {
                            //tblist+=dummydatetd;
                        }
                        //for (k = sval; k <= edval; k++) {
                        for (k = 0; k < (ddl.length); k++) {
                            var kk = "" + ddl[k] + "";
                            //console.log("colcountsp---"+kk);
                            var ytt = ddl[k];
                            var cls = colcount[0][ytt];
                            //console.log("colcountsp"+cls);
                            if (c == 0 && cls > 1) {

                                tblist += '<td rowspan="' + cls + '">';
                                for (tl = 0; tl < cls; tl++) {
                                    //console.log("tl"+tl);
                                    var spid = "" + ddl[k] + "" + tl + "";
                                    var girlsname = fdata[spid]['girlsname'];
                                    var branchname = fdata[spid]['branchname'];
                                    var branchcolor = fdata[spid]['branchcolor'];
                                    var eid = fdata[spid]['id'];
                                    var vtime = timecount[eid];
                                    //console.log(vtime);
                                    tblist += '<div class="scroll-area-td"><div class="wplan scrollbar-container ps--active-y" style="background-color:' + branchcolor + '";><p>' + girlsname + ' <span>' + branchname + '</span></p><p class="working-slots">' + vtime + '</p><ul class="act"><li><a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Bearbeiten" data-cdate="" data-branchid="" data-id="' + eid + '" data-employee="" class="text-success add-work"><i class="ion-android-create"></i></a></li><li><a href="#" data-toggle="tooltip" data-original-title="Löschen" class="text-danger remove rm-work" data-id="' + eid + '"><i class="ion-android-close"></i></a></li></ul></div></div>';
                                }
                                tblist += '</td>';
                            } else {
                                if (cls > 1) {

                                } else {
                                    var sspid = "" + ddl[k] + "0";
                                    //console.log("sspid"+sspid);
                                    var chid = fdata[sspid];
                                    if (chid != undefined && chid != null && chid != "") {
                                        var eid = fdata[sspid]['id'];
                                        var girlsname = fdata[sspid]['girlsname'];
                                        var branchname = fdata[sspid]['branchname'];
                                        var branchcolor = fdata[sspid]['branchcolor'];
                                        var vtime = timecount[eid];
                                        if (jQuery.inArray(eid, ChkData) === -1) {
                                            tblist += '<td >';
                                            tblist += '<div class="scroll-area-td"><div class="wplan scrollbar-container ps--active-y" style="background-color:' + branchcolor + '";><p>' + girlsname + ' <span>' + branchname + '</span></p><p class="working-slots">' + vtime + '</p><ul class="act"><li><a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Bearbeiten" data-cdate="" data-branchid="" data-id="' + eid + '" data-employee="" class="text-success add-work"><i class="ion-android-create"></i></a></li><li><a href="#" data-toggle="tooltip" data-original-title="Löschen" class="text-danger remove rm-work" data-id="' + eid + '"><i class="ion-android-close"></i></a></li></ul></div></div>';
                                            tblist += '</td>';
                                            ChkData.push(eid);
                                        } else {
                                            tblist += '<td >';
                                            tblist += '<div class="wplan"><a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Erstellen" data-cdate="" data-branchid="" data-id="" data-employee=""class="add-shedule add-work"><i class="ion-android-add-circle"></i></a>';
                                            tblist += '</td></div>';
                                        }

                                    } else {
                                        tblist += '<td >';
                                        tblist += '<div class="wplan"><a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Erstellen" data-cdate="" data-branchid="" data-id="" data-employee="" class="add-shedule add-work"><i class="ion-android-add-circle"></i></a></div>';
                                        tblist += '</td>';
                                    }

                                }

                            }

                        }
                        if (totaldays < 7 && datedata.st != 1) {
                            //tblist+=dummydatetd;
                        }
                        tblist += '</tr>';
                    }
                } else {
                    tblist += '<tr>';
                    //for (k = datedata.st; k <= datedata.et; k++) {
                    for (k = 0; k < (ddl.length); k++) {
                        tblist += '<td >';
                        tblist += '<div class="wplan"><a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Erstellen" data-cdate="" data-branchid="" data-id="" data-employee="" class="add-shedule add-work"><i class="ion-android-add-circle"></i></a></div>';
                        tblist += '</td>';
                    }
                    tblist += '</tr>';
                }
                //console.log(totalpoint);

                // console.log(hviewdata);
                //console.log(tblist);
                $("#hdata").html(hviewdata);
                //$("#blistdata").html(blist);
                $("#tblistdata").html(tblist);
            } else {
                alertify.alert('Ungültiger Vorgang erneut versuchen');
                return false;
            }
        }
    });
    return false;
}
//Two date different days

function generateDateList(from, to) {
    var daylist = [];
    var listDate = [];
    var monthlist = [];
    var yearlist = [];
    var startDate = from;
    var endDate = to;
    var dateMove = new Date(startDate);
    var strDate = startDate;

    while (strDate < endDate) {
        var strDate = dateMove.toISOString().slice(0, 10);
        listDate.push(strDate);
        var sst = strDate.split('-');
        daylist.push(sst[2]);
        monthlist.push(sst[1]);
        yearlist.push(sst[0]);
        dateMove.setDate(dateMove.getDate() + 1);
    };

    res = {
        datelist: listDate,
        daylist: daylist,
        monthlist: monthlist,
        yearlist: yearlist
    };
    return res;
}
// Get Correct dateformat
function datacorrectformat(ydata, mdata, ddata) {
    //alert(ydata);alert(mdata);alert(ddata);
    var today = new Date(ydata, mdata, ddata);
    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }
    today = yyyy + '-' + mm + '-' + dd;
    //alert(today);
    return today;
}

//datacorrectformat(ydata,mdata,ddata)