var mRubble = require("../models/rubble");

module.exports = {
    //Rubble listing functionality
    rubblepage(req, res) {
        mRubble.getRubbleNamelist(req,function(response){
            console.log(response);
            var data = {'page_js':'Load_DataTables("rubbleview")',rubblelist:(response.rubblelist)};
            res.render("pages/rubble/rubble",data);
        });
     },
     //add rubble functionality
     addrubblepage(req, res) {
        var datac =[[]];
        var data = {details: datac};
        console.log(data);
        res.render("pages/rubble/addrubble",data);
    },
     //Edit rubble functionality
     editrubblepage(req, res) {
        mRubble.editrubbledatils(req,function(response){
            var datac =[[]];
            var data = {details: response.rubbledetails};
            console.log(data);
            res.render("pages/rubble/addrubble",data);
        });
    },
   
    // Rubble create functionality
    rubbleCreatepage(req, res) {
        console.log("Rubble create");
        console.log(res.req.body);
        mRubble.rubbleSubmit(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
      // Rubble create functionality
    rubbleseparate(req, res) {
        /*console.log("Rubble create");
        console.log(res.req.body);*/
        mRubble.getSeparaterubble(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    
};