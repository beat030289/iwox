var mNotification = require("../models/notification");
module.exports = {
    //Notification listing functionality
    notificationViewPage(req, res) {
       /* mNotification.getNotificationPageList(req,function(response){
        console.log("testdata");
        console.log(response.notificationlist);*/
        var data = {
            'page_js': 'Load_DataTables("notificationview")'
        };
        res.render("pages/notification/notificationview",data);
        //});
    },
    notificationCreatePage(req, res) {
        mNotification.getNotificationDeails(req, function(response) {
            console.log(response.branch);
            var randN = uniqid();
            var data = {
                details: [
                    []
                ],
                branch: (response.branch),
                note_image: [],
                u_id: randN
            };
            res.render("pages/notification/notificationcreate", data);
        });
    },
    notificationEditPage(req, res) {
        console.log("notificationedit");
        mNotification.editNotificationDetails(req, function(response) {
            console.log(response.detalis);
           /* console.log(response.pointimage);*/
            var randN = uniqid();
            var data = {
                details: (response.detalis),
                note_image: (response.noteimage),
                branch: (response.branch),
                u_id: randN
            };
            res.render("pages/notification/notificationcreate", data);
        });
    },
    // Notification create functionality
    notificationCreationPage(req, res) {
        /*console.log("Notification create");
        console.log(res.req.body);*/
        mNotification.notificationSubmit(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // Adertise create functionality
    advertiseseparate(req, res) {
        /*console.log("Adertise create");
        console.log(res.req.body);*/
        mNotification.getSeparateadvertise(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // image upload functionality
    imageuploadpage(req, res) {
        console.log("point create");
        console.log(res.req.body);
        mNotification.imageupload(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
};