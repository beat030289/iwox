var mCategory = require("../models/category");

module.exports = {
    // Category list page
    categorypage(req, res) {
        var data = {'page_js':'Load_DataTables("category")'};
         res.render("pages/extra/category",data);
    },
    //category creation functionality
    categoryCreatepage(req, res) {
        console.log(res.req.body);
        mCategory.createCategory(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    //get Category data
    categoryGetpage(req, res) {
        console.log(res.req.body);
        mCategory.getCategory(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    
};