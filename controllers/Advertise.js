var mAdertise = require("../models/advertise");

module.exports = {
    //Adertise listing functionality
    advertiseviewpage(req, res) {
        mAdertise.getAdvertisepagelist(req,function(response){
        console.log("testdata");
        console.log(response.advertiselist);
        var data = {
            'page_js': 'Load_DataTables("advertiseview")',advertiselist:(response.advertiselist)
        };
        res.render("pages/advertise/advertiseview", data);
        });
    },
    advertisecreatepage(req, res) {
        mAdertise.getadvertisedeails(req, function(response) {
            //console.log(response.pointdetalis);
            var randN = uniqid();
            var data = {
                details: [
                    []
                ],
                branch: (response.branch),
                point_image: [],
                u_id: randN
            };
            res.render("pages/advertise/advertisecreate", data);
        });
    },
    advertiseeditpage(req, res) {
        console.log("advertiseedit");
        mAdertise.editadvertisedetails(req, function(response) {
            console.log(response.detalis);
           /* console.log(response.pointimage);*/
            var randN = uniqid();
            var data = {
                details: (response.webdetalis)
            };
            res.render("pages/advertise/advertiseedit", data);
        });
    },
    // Adertise create functionality
    advertisecreationpage(req, res) {
        /*console.log("Adertise create");
        console.log(res.req.body);*/
        mAdertise.advertiseSubmit(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // Adertise create functionality
    advertiseseparate(req, res) {
        /*console.log("Adertise create");
        console.log(res.req.body);*/
        mAdertise.getSeparateadvertise(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // image upload functionality
    imageuploadpage(req, res) {
        console.log("point create");
        console.log(res.req.body);
        mPoint.imageupload(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
};