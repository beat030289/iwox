var mUser = require("../models/user");

module.exports = {
    //user listing functionality
    userpage(req, res) {
        mUser.getUserNamelist(req,function(response){
            console.log(response.usernamelist);
            var data = {'page_js':'Load_DataTables("user")',usernamelist: (response.usernamelist)};
            res.render("pages/user/user",data);
        });
    },
      // Get user functionality
     adduserpage(req, res) {
        mUser.getUserdatils(req,function(response){
            console.log(response.branch);
            var datac =[[]];
            var data = {details: datac,module: (response.websetting),branch: (response.branch)};
            console.log(data);
            res.render("pages/user/adduser",data);
        });
    },
    // usercreate functionality
    usercreate(req, res) {
        console.log("usercreate");
         //res.send("testdata");
        console.log(res.req.body);
        mUser.userSubmit(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
     edituserpage(req, res) {
        mUser.editUserdatils(req,function(response){
            //console.log(response.branch);
            //console.log(response.branch);
            var datac =[[]];
            var data = {details: response.details,module: (response.websetting),branch: (response.branch)};
            console.log(data);
            res.render("pages/user/adduser",data);
        });
    },
    // getseparate User functionality
    getSeparateuser(req, res) {
        console.log("Separate user deatils");
        mUser.getSeparateuser(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
};