var mWeeklyplanner = require("../models/weeklyplanner");

module.exports = {
    weeklyplannerpage(req, res) {
    	 mWeeklyplanner.getdetails(req,function(response){
            console.log(response.branch);
            console.log(response.girls);
            var data = {'page_js':'workplanner_cc()',branch:(response.branch),girls:(response.girls)};
        res.render("pages/weeklyplanner/weeklyplanner",data);
    });
    },
    // work create functionality
    workcreatepage(req, res) {
        console.log("work create");
        console.log(res.req.body);
        mWeeklyplanner.workSubmit(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // work create functionality
    getdatapage(req, res) {
        console.log("work getdata");
        console.log(res.req.body);
        mWeeklyplanner.workgetdata(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            console.log(result);
            res.send(json_data);
        });

    },
     // work girls get functionality
    getgirlsdatapage(req, res) {
        console.log("work getdata");
        console.log(res.req.body);
        mWeeklyplanner.girlsData(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            console.log(result);
            res.send(json_data);
        });

    },
   
};