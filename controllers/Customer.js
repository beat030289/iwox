var mCustomer = require("../models/customer");

module.exports = {
    //customer listing functionality
    customerpage(req, res) {
        mCustomer.getCustomerNamelist(req,function(response){
            console.log(response.customernamelist);
            var data = {'page_js':'Load_DataTables("customer")',customernamelist:(response.customernamelist)};
            res.render("pages/customer/customer",data);
        });
     },
     //add customer functionality
     addcustomerpage(req, res) {
        var datac =[[]];
        var data = {details: datac};
        console.log(data);
        res.render("pages/customer/addcustomer",data);
    },
     //Edit customer functionality
     editcustomerpage(req, res) {
        mCustomer.editcustomerdatils(req,function(response){
            var datac =[[]];
            var data = {details: response.customerdetails};
            console.log(data);
            res.render("pages/customer/addcustomer",data);
        });
    },
    //customer profile functionality
     customerprofilepage(req, res) {
        mCustomer.getcustomerdatils(req,function(response){
            var datac =[[]];
            var data = {details: (response.customerProfile)};
            console.log(data);
            res.render("pages/customer/customerprofile",data);
        });
    },
    // customer create functionality
    customerCreatepage(req, res) {
        console.log("customer create");
        console.log(res.req.body);
        mCustomer.customerSubmit(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    
};