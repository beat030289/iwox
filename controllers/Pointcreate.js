var mPoint = require("../models/pointcreate");

module.exports = {
    //point listing functionality
    pointviewpage(req, res) {
        mPoint.getPointpagelist(req,function(response){
            /*console.log(response.pointlist);
            console.log(response.branch);*/
            var data = {'page_js':'Load_DataTables("pointview")',pointlist: (response.pointlist),branch: (response.branch)};
            res.render("pages/points/pointsview",data);
       });
     },
     pointcreatepage(req, res) {
     	mPoint.getpointdeails(req,function(response){
            //console.log(response.pointdetalis);
            var randN = uniqid();
            var data = {details:[[]],branch:(response.branch),point_image:[],u_id:randN};
            res.render("pages/points/pointcreate",data);
       });
     },
     pointeditpage(req, res) {
     	console.log("pointedit");
        mPoint.editpointdetails(req,function(response){
            /*console.log(response.pointdetalis);
            console.log(response.pointimage);*/
            var randN = uniqid();
            var data = {details:(response.pointdetalis),branch:(response.branch),point_image:(response.pointimage),u_id:randN};
            res.render("pages/points/pointcreate",data);
       });
     },
     // Point create functionality
    pointcreationpage(req, res) {
        /*console.log("point create");
        console.log(res.req.body);*/
        mPoint.pointSubmit(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // image upload functionality
    imageuploadpage(req, res) {
        console.log("point create");
        console.log(res.req.body);
        mPoint.imageupload(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
};