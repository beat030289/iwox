var mChart = require("../models/chart");

module.exports = {
   chartData(req, res) {
        console.log("Chart");
        console.log(res.req.body);
        mChart.chartData(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    
};