var mVouchers = require("../models/vouchers");

module.exports = {
    //Vouchers listing functionality
    voucherspage(req, res) {
        mVouchers.getvoucherslist(req,function(response){
        console.log(response.voucherlist);    
        var data = {'page_js':'Load_DataTables("vouchers")',list: (response.voucherslist)};
        res.render("pages/vouchers/vouchers",data);
        });
     },
 //add Vouchers functionality
     addvoucherspage(req, res) {
        var datac =[[]];
        var data = {details: datac};
        res.render("pages/vouchers/addvouchers",data);
    },
         //Edit Vouchers functionality
     editvoucherspage(req, res) {
        mVouchers.editvouchersdatils(req,function(response){
            var datac =[[]];
            var data =[];
            console.log(response.vouchersdetails);
           var data = {details: (response.vouchersdetails)};
            res.render("pages/vouchers/addvouchers",data);
        });
     },
    
    // Vouchers create functionality
    vouchersCreatepage(req, res) {
        console.log("Vouchers create");
        console.log(res.req.body);
        mVouchers.vouchersSubmit(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // Vouchers get vouchers Details
    vouchersDetailspage(req, res) {
        console.log("Vouchers seprate");
        console.log(res.req.body);
        mVouchers.getvouchersdatils(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    }
   
};