var mBranches = require("../models/branches");

module.exports = {
    branchviewpage(req, res) {
        mBranches.getBranch(req,function(response){
        console.log(response.details);
        //var data ={details:(response.details)details:(response.details)};
        var data ={details:(response.details),leader:(response.leader),category:(response.category)};
    	res.render("pages/branch/branch",data);
    });
    },
    // add branch details
    branchaddpage(req, res) {
        mBranches.getBranchdetails(req,function(response){
            var datav = [[]];
            var datac = [];
            //console.log(response.users);
           // console.log(response.category);
            var data ={details:datav,user:(response.users),category:(response.category),branchauth:datac,localapprove:datac,rooms:datac};
            res.render("pages/branch/addbranch",data);
        });
    },
    branchdetailspage(req, res) {
        mBranches.editbranch(req,function(response){
           console.log(response.details);
           console.log(response.localauth);
        var data = {details: (response.details),user:(response.users),category:(response.category),branchauth:(response.branchauth),localapprove:(response.localauth),rooms:(response.rooms),girls:(response.girls) };
    	   res.render("pages/branch/branchdetails",data);
         });
    },
    //Edit branch functionality
     editbranchpage(req, res) {
        console.log("editbranch");
        mBranches.editbranch(req,function(response){
            /*console.log(response.details);
            console.log(response.users);
            console.log(response.category);
            console.log(response.branchauth);
            console.log(response.localauth);*/
            console.log(response.rooms);
            var data = {details: (response.details),user:(response.users),category:(response.category),branchauth:(response.branchauth),localapprove:(response.localauth),rooms:(response.rooms) };
            res.render("pages/branch/addbranch",data);
        });
    },
    // get leader functionality
    getleader(req, res) {
        console.log("getleader");
         //res.send("testdata");
        console.log(res.req.body);
        mBranches.getleaderdetails(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };

            res.send(json_data);
        });

    },
    // Branch create functionality
    branchCreatepage(req, res) {
        console.log("branch create controller");
        console.log(res.req.body);
        mBranches.branchCreatepage(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result,
                    
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // Branch create functionality
    branchListpage(req, res) {
        console.log("branch List controller");
        console.log(res.req.body);
        mBranches.branchListpage(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
               var json_data = {
                    status: 'success',
                    result: result
                   
                };
            //console.log(json_data);
            res.send(json_data); 
            
            
        });

    },
   
};

