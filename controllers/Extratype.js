var mExtra = require("../models/extratype");

module.exports = {
    // Extratype list page
    extratypepage(req, res) {
        var data = {'page_js':'Load_DataTables("extratype")'};
         res.render("pages/extra/extratype",data);
    },
    //extra creation functionality
    extratypeCreatepage(req, res) {
        console.log(res.req.body);
        mExtra.createExtratype(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    //get extra data
    extratypeGetpage(req, res) {
        console.log(res.req.body);
        mExtra.getExtratype(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // Zeit list page
    zeitpepage(req, res) {
        var data = {'page_js':'Load_DataTables("zeit")'};
         res.render("pages/extra/zeit",data);
    },
    //Zeit creation functionality
    zeitCreatepage(req, res) {
        console.log(res.req.body);
        mExtra.createZeit(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    //get Zeit data
    zeitGetpage(req, res) {
        console.log(res.req.body);
        mExtra.getZeit(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // permit person list page
    permitpage(req, res) {
        var data = {'page_js':'Load_DataTables("permit")'};
         res.render("pages/extra/permit",data);
    },
    //permit creation functionality
    permitCreatepage(req, res) {
        console.log(res.req.body);
        mExtra.createPermitpage(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    //get permit data
    permitGetpage(req, res) {
        console.log(res.req.body);
        mExtra.getPermit(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
     // Hair list page
    hairpage(req, res) {
        var data = {'page_js':'Load_DataTables("hair")'};
         res.render("pages/extra/hair",data);
    },
    //hair type creation functionality
    hairCreatepage(req, res) {
        console.log(res.req.body);
        mExtra.createhairpage(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    //get hair data
    hairGetpage(req, res) {
        console.log(res.req.body);
        mExtra.getHair(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
     // Eyes list page
    eyespage(req, res) {
        var data = {'page_js':'Load_DataTables("eyes")'};
         res.render("pages/extra/eyes",data);
    },
    //Eyes type creation functionality
    eyesCreatepage(req, res) {
        console.log(res.req.body);
        mExtra.createeyespage(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    //get eyes data
    eyesGetpage(req, res) {
        console.log(res.req.body);
        mExtra.getEyes(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // Shoes list page
    shoespage(req, res) {
        var data = {'page_js':'Load_DataTables("shoes")'};
         res.render("pages/extra/shoes",data);
    },
    //Shoes type creation functionality
    shoesCreatepage(req, res) {
        console.log(res.req.body);
        mExtra.createshoespage(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // Shoes list page
    clothpage(req, res) {
        var data = {'page_js':'Load_DataTables("cloth")'};
         res.render("pages/extra/cloth",data);
    },
    //get shoes data
    shoesGetpage(req, res) {
        console.log(res.req.body);
        mExtra.getShoes(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    //Cloth type creation functionality
    clothCreatepage(req, res) {
        console.log(res.req.body);
        mExtra.createclothpage(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    //get cloth data
    clothGetpage(req, res) {
        console.log(res.req.body);
        mExtra.getCloth(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // civil status list page
   civilstatuspage(req, res) {
        var data = {'page_js':'Load_DataTables("civilstatus")'};
         res.render("pages/extra/civilstatus",data);
    },
    //civil status type creation functionality
    civilstatusCreatepage(req, res) {
        console.log(res.req.body);
        mExtra.createcivilstatuspage(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    //get civilstatus data
    civilstatusGetpage(req, res) {
        console.log(res.req.body);
        mExtra.getCivilstatus(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
      // Private part list page
   privatepartpage(req, res) {
        var data = {'page_js':'Load_DataTables("privatepart")'};
         res.render("pages/extra/privatepart",data);
    },
    //Private part  creation functionality
    privatepartCreatepage(req, res) {
        console.log(res.req.body);
        mExtra.createprivatepartpage(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    //get privatepart data
    privatepartGetpage(req, res) {
        console.log(res.req.body);
        mExtra.getPrivatepart(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
       // Bust list page
   bustpage(req, res) {
        var data = {'page_js':'Load_DataTables("bust")'};
         res.render("pages/extra/busttype",data);
    },
    //bust  creation functionality
    bustCreatepage(req, res) {
        console.log(res.req.body);
        mExtra.createbustpage(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    //get Bust data
    bustGetpage(req, res) {
        console.log(res.req.body);
        mExtra.getBust(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
       // Nationality list page
   nationalitypage(req, res) {
        var data = {'page_js':'Load_DataTables("nationality")'};
         res.render("pages/extra/nationality",data);
    },
    //bust  creation functionality
    nationalityCreatepage(req, res) {
        console.log(res.req.body);
        mExtra.createnationalitypage(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    //get Nationality data
    nationalityGetpage(req, res) {
        console.log(res.req.body);
        mExtra.getNationality(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
};