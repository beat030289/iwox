var mLogin = require("../models/login");

module.exports = {
    loginpage(req, res) {
        console.log("loginpage");
        res.render("pages/index");
    },
    // login functionality
    loginprocess(req, res) {
        //console.log(res.req.body);
        mLogin.loginsubmit(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // logout Functionality
    logoutprocess(req, res) {
        req.session.destroy();
        res.redirect('/');
    },
    // logout Functionality
    changepasswordpage(req, res) {
        res.render("pages/changepassword");
    },
    // Change password Functionality
    changepassword(req, res) {
        console.log("changepassword1");
        console.log(res.req.body);
        mLogin.changePassword(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    profilepage(req, res) {
     mLogin.getProfile(req,function(response){
        //console.log(response.branch);
           var data = {details: (response.details)};
           console.log(data);
        res.render("pages/user/profile",data);
     });
        
    },
    // profileupdate functionality
    profileUpdate(req, res) {
        console.log("usercreate");
         //res.send("testdata");
        console.log(res.req.body);
        mLogin.profileUpdate(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    

     forgotpage(req, res) {
     
        res.render("pages/forgotpassword");
     
        
    },
     resetpassword(req, res) {
     userid = req.query.uid;
        var data = {uid: userid};
           console.log(data);
        res.render("pages/resetpassword",data);
     
        
    },
     // forgot password mail send
    forgotpasswordmail(req, res) {
        console.log("usercreate");
         //res.send("testdata");
        console.log(res.req.body);
        mLogin.forgotMail(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
     // forgot password mail send
    resetpasswordchange(req, res) {
        console.log("usercreate");
         //res.send("testdata");
        console.log(res.req.body);
        mLogin.resetpassword(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
};