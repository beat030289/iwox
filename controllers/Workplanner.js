var mWorkplanner = require("../models/workplanner");

module.exports = {
    workplannerViewPage(req, res) {
    	mWorkplanner.getdetails(req,function(response){
    	var data = {'page_js':'workplanner_girls()',branch:(response.branch),girls:(response.girls)};
        res.render("pages/workplanner/workplanner",data);
    });
    },
    // getworkplanner functionality
    getworkplannerPage(req, res) {
        console.log("get workplanner Page");
        console.log(res.req.body);
        mWorkplanner.getworkplanner(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // createworkplanner functionality
   createworkplannerPage(req, res) {
        console.log("create workplanner Page");
        console.log(res.req.body);
        mWorkplanner.workplannerSubmit(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    removeworkplannerPage(req, res) {
        console.log("remove workplanner Page");
        console.log(res.req.body);
        mWorkplanner.workplannerRemove(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
     getdatapage(req, res) {
        console.log("get dataPage");
        console.log(res.req.body);
        mWorkplanner.workgetdata(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
   
};