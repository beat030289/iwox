var mServices = require("../models/services");

module.exports = {
	// Get all Service list
        servicepage(req, res) {
        mServices.getServiceData(req,function(response){
            //console.log(response.details);
            var data ={details:(response.details),branch:(response.branch),sname:(response.sname),stime:(response.stime)};
            res.render("pages/service/service",data);
        });
    },
    // add Service form
     serviceaddpage(req, res) {
     	mServices.getService(req,function(response){
        var datav=[[]];
        var datat=[];
        var data ={details:datav,branch:(response.branch),zeit:(response.zeit),services:(response.services),extra:(response.extra),extradata:datat,servicedata:datat};
        res.render("pages/service/addservice",data);
        });
    },
    // separate service view
     servicedetailspage(req, res) {
        mServices.getServiceSpearate(req,function(response){
            /*console.log(response.details);
            console.log(response.extradata);
            console.log(response.servicedata);*/
            var data ={details:(response.details),extradata:(response.extradata),servicedata:(response.servicedata)};
            res.render("pages/service/servicedetails",data);
        });
    },
    //get servicetype data
    servicetypeGetpage(req, res) {
        console.log(res.req.body);
        mServices.getServicetype(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // Service type view
    servicetypepage(req, res) {
        var dataParams=[{"name": "userid", "value":app.locals.encrypt(req.session.userid)}];
        var data = {'page_js':'Load_DataTables("servicetype",'+JSON.stringify(dataParams)+')'};
        res.render("pages/service/servicetype",data);
    },
    // service type creation
    servicetypeCreatepage(req, res) {
        //console.log(res.req.body);
        mServices.createServicetype(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
// Service Creation function
    serviceCreatepage(req, res) {
        console.log(res.req.body);
        mServices.createService(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    //Edit Service functionality
     serviceEditpage(req, res) {
        console.log("editbranch");
        mServices.editservice(req,function(response){
            /*console.log(response.details);
            console.log(response.branch);
            console.log(response.zeit);
            console.log(response.services);
            console.log(response.extra);
            console.log(response.rooms);
            console.log(response.extradata);
            console.log(response.servicedata);*/
            var data = {details: (response.details),branch:(response.branch),zeit:(response.zeit),services:(response.services),extra:(response.extra),extradata:(response.extradata),servicedata:(response.servicedata) };
           res.render("pages/service/addservice",data);
        });
    },
    // Service List functionality
    serviceListpage(req, res) {
        console.log("service List controller");
        console.log(res.req.body);
        mServices.serviceListpage(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
               var json_data = {
                    status: 'success',
                    result: result
                   
                };
            //console.log(json_data);
            res.send(json_data); 
            
            
        });

    },
};