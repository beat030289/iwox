var mRemove = require("../models/removerecord");

module.exports = {
    // table data remove function functionality
    removetabledata(req, res) {
        console.log("data remove");
        console.log(res.req.body);
        mRemove.removeDataRecord(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
     /*autodata(req, res) {
        console.log("data auto");
        console.log(res.req.body);
        mRemove.autoDataRecord(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            console.log(json_data);
            //res.send(json_data);
        });

    },*/
      autodata(req, res) {
        console.log("autodatacall1");
        /* mRemove.autoDataRecord(req,function(response){
             console.log("autodatacall2");
            console.log(response);
            
        });*/
        mRemove.autoDataRecord(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            console.log(json_data);
            res.send(json_data);
        });
    },
     girlsIdproof(req, res) {
        console.log("autodatacall1");
        mRemove.girlsIDProofDataRecord(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            console.log(json_data);
            res.send(json_data);
        });
    },
     
};