var mGirls = require("../models/girls");

module.exports = {
	// Get all girls list
    girlspage(req, res) {
        mGirls.getGirlsview(req,function(response){
            console.log(response.details);
            var data = {details: response.details,pindata:(response.pindata),branch:(response.branch)}
        res.render("pages/girl/girls",data);
        });
    },
    // Get details girls list
    girlsdetailspage(req, res) {
        mGirls.editgirls(req,function(response){
            var datav = [[]];
            var datac = [];
            /*console.log(response.details);
            console.log(response.users);
            console.log(response.servicetype);
            console.log(response.extratype);
            console.log(response.officedata);
            console.log(response.pindata);
            console.log(response.servicedata);
            console.log(response.extradata);
            console.log(response.girlsimage);*/
            console.log(response.sptime);
            var data = {details: (response.details),servicetype: (response.servicetype),extratype: (response.extratype),extradata:(response.extradata),servicedata:(response.servicedata),officedata:(response.officedata),user:(response.user),idproof:datav,pindata:(response.pindata),gallery_image:(response.girlsimage),sptime: (response.sptime),};
        res.render("pages/girl/girlsdetails",data);
        });
    },
    // add Girls form
     addgirlspage(req, res) {
     	mGirls.getGirlslist(req,function(response){
            /*console.log(response.servicetype);
            console.log(response.extratype);
            console.log(response.user);*/
            //var uniqid = require('uniqid');
            var randN = uniqid();
            var datav = [[]];
            var datac = [];
            console.log("ghiudfgghiufdgfghfgufdg");
            console.log(randN);
            var data = {details: datav,servicetype: (response.servicetype),extratype: (response.extratype),extra:(response.extra),extradata:datac,servicedata:datac,officedata:datac,user:(response.user),officedata:datac,idproof:datav,gallery_image:datac,'u_id':randN,pindata:datac,pin:(response.pin),hair:(response.hair),eyes:(response.eyes),shoes:(response.shoes),cloth:(response.cloth),bust:(response.bust),privatepart:(response.privatepart),nationality:(response.nationality),civilstatus:(response.civilstatus)};
        res.render("pages/girl/addgirls",data);
        });
    },
    //Edit Girls functionality
     editgirlspage(req, res) {
        console.log("editgirls");
        mGirls.editgirls(req,function(response){
            /*console.log(response.details);
            console.log(response.users);
            console.log(response.servicetype);
            console.log(response.extratype);
            console.log(response.officedata);
            console.log(response.pindata);
            console.log(response.servicedata);
            console.log(response.extradata);
            console.log(response.girlsimage);*/
            /*console.log(response.pindata);
            console.log(response.pin);*/
            var datav = [[]];
            var datac = [];
            var randN = uniqid();
            var data = {details: (response.details),servicetype: (response.servicetype),extratype: (response.extratype),extradata:(response.extradata),servicedata:(response.servicedata),user:(response.user),officedata:(response.officedata),idproof:datav,pindata:(response.pindata),gallery_image:(response.girlsimage),'u_id':randN,pin:(response.pin),hair:(response.hair),eyes:(response.eyes),shoes:(response.shoes),cloth:(response.cloth),bust:(response.bust),privatepart:(response.privatepart),nationality:(response.nationality),civilstatus:(response.civilstatus)};
            res.render("pages/girl/addgirls",data);
        });
    },
    //creat girls data
    girlsCreatepage(req, res) {
        console.log("show req data");
        console.log(res.req.body);
        mGirls.createGirls(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
     //creat girls data
    galleryCreatepage(req, res) {
        console.log("show req data");
        console.log(res.req.body);
        mGirls.createGallery(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
                var json_data = {
                    status: 'success',
                    result: result
                };
            //console.log(json_data);
            res.send(json_data);
        });

    },
    // Girls List functionality
    girlsListpage(req, res) {
        //console.log("girls List controller");
        //console.log(res.req.body);
        mGirls.girlsListpage(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
               var json_data = {
                    status: 'success',
                    result: result
                   
                };
            //console.log(json_data);
            res.send(json_data); 
            
            
        });

    },
    // remove Image option
    removeImagepage(req, res) {
       mGirls.removeImageData(req, res, function(err, result) {
            res.setHeader("Content-Type", "text/json");
            res.setHeader("Access-Control-Allow-Origin", "*");
            if (err)
                var json_data = {
                    status: 'error',
                    result: '',
                    message: err
                };
            else
               var json_data = {
                    status: 'success',
                    result: result
                   
                };
            res.send(json_data); 
        });
    },
};