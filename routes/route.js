var express = require('express');
var router = express.Router();

var Login = require('../controllers/Login');
var Dashboard = require('../controllers/Dashboard');
var Reports = require('../controllers/Reports');
var Workplanner = require('../controllers/Workplanner');
var Branches = require('../controllers/Branches');
var Services = require('../controllers/Services');
var Users = require('../controllers/Users');
var Dtable = require('../controllers/Dtable');
var Removerecord = require('../controllers/Removerecord');
var Extratype = require('../controllers/Extratype');
var Category = require('../controllers/Category');
var Customer = require('../controllers/Customer');
var Girls = require('../controllers/Girls');
var Vouchers = require('../controllers/Vouchers');
var Weeklyplanner = require('../controllers/Weeklyplanner');
var Pointcreate = require('../controllers/Pointcreate');
var Advertise = require('../controllers/Advertise');
var Notification = require('../controllers/Notification');
var Debt = require('../controllers/Debt');
var Chat = require('../controllers/Chat');
var Rubble = require('../controllers/Rubble');
var Chart = require('../controllers/Chart');
// runs on ALL requests
router.all("*", function(req, res, next) {
    sessionPermission1 = [];

    global.session_id = req.session.userid;
    global.role_id = req.session.role;
    //console.log("role_id"+role_id);
    if (global.session_id != '' && typeof(session_id) != 'undefined') {
        sql.runQuery('SELECT m.name,m.id,s.uadd,s.uedit,s.uview,s.udelete FROM ' + iw_webmodules + ' m LEFT JOIN ' + iw_setting + ' s ON m.id = s.module AND userid="' + req.session.userid + '" ORDER BY m.id ASC', function(err, permission) {
            global.sessionPermission = permission;
            
            global.sessionPermissionUser = getPermissionArray(sessionPermission);
           // console.log(sessionPermissionUser);
            /*console.log(sessionPermissionUser[1]);
            console.log(sessionPermissionUser[1][1]);
            console.log(sessionPermissionUser[1][2]);
            console.log(sessionPermissionUser[1][3]);
            console.log(sessionPermissionUser[1][4]);*/
            global.moduleAccess = getUniqueArray(arrayColumn(sessionPermission, 'name'));
            //console.log(moduleAccess);
            next();
            
        });
    } else
        next();
});
// Data table get data
router.get('/data_tables/:page', function(req, res) {
    var page_name = req.params.page;
    var params = req.query;
    console.log("test");
    //console.log(params);
    dataTable.fetchRecords(page_name, params, function(response) {
        res.json(response);
    });

});

//controllers
/*----------LOGIN LOGOUT---------------------------------------------------------------------*/
//login pages
router.get('/', Login.loginpage);
//forgotpassword page
router.get('/forgotpassword', Login.forgotpage);
//reset page
router.get('/resetpassword', Login.resetpassword);
//login process
router.post('/ajax-actions_login', Login.loginprocess);
//logout
router.get('/logout', Login.logoutprocess);
//changpassword page
router.get('/changepassword', requireLogin, Login.changepasswordpage);
//changpassword process
router.post('/ajax-actions_changePassword', Login.changepassword);
//profile page
router.get('/profile', requireLogin, Login.profilepage);
//profile update process
router.post('/ajax-actions_profileUpdate', Login.profileUpdate);
router.post('/ajax-actions_forgotpassword', Login.forgotpasswordmail);
//resetpassword emailsend
router.post('/ajax-actions_resetpassword', Login.resetpasswordchange);


/*----------DASHBOARD---------------------------------------------------------------------*/
//Dashboard pages
router.get('/dashboard', requireLogin, Dashboard.dashpage);

/*----------REPORTS---------------------------------------------------------------------*/
//reports pages
router.get('/bericht', requireLogin, Reports.reportpage);

/*----------BRANCHES---------------------------------------------------------------------*/
// list Branch Page
router.get('/filiale', requireLogin, Branches.branchviewpage);
// add branch data
router.get('/addfiliale', requireLogin, Branches.branchaddpage);
// branch view details
router.get('/filialedetails', requireLogin, Branches.branchdetailspage);
//get leaderdetails
router.post('/ajax-actions_getleader', Branches.getleader);
//Branch Create
router.post('/ajax-actions_branchcreate', Branches.branchCreatepage);
//Branch Edit
router.get('/editbranch', requireLogin, Branches.editbranchpage);
//Branch List
router.post('/ajax-actions_branchList', Branches.branchListpage);
/*----------SERVICES---------------------------------------------------------------------*/
// list Service Page
router.get('/dienstleistung', requireLogin, Services.servicepage);
// add service Page
router.get('/adddienstleistung', requireLogin, Services.serviceaddpage);
//Service Create
router.post('/ajax-actions_servicecreate', Services.serviceCreatepage);
//Service Edit
router.get('/editservice', requireLogin, Services.serviceEditpage);
// view service Page
router.get('/viewdienstleistung', requireLogin, Services.servicedetailspage);
// list service type page
router.get('/servicetype', requireLogin, Services.servicetypepage);
// edit service type page
router.post('/ajax-actions_servicetype_edit', Services.servicetypeGetpage);
//create service type
router.post('/ajax-actions_servicetype', Services.servicetypeCreatepage);
//Service List
router.post('/ajax-actions_serviceList', Services.serviceListpage);
/*----------USERS---------------------------------------------------------------------*/
// User list Page
router.get('/user', requireLogin, Users.userpage);
// User add Page
router.get('/adduser', requireLogin, Users.adduserpage);
// User create 
router.post('/ajax-actions_usercreate', Users.usercreate);
// User edit Page
router.get('/edituser', requireLogin, Users.edituserpage);
//get separate user process
router.post('/ajax-actions_separate', Users.getSeparateuser);
/*----------EXtra---------------------------------------------------------------------*/
// Extra type list Page
router.get('/extratype', requireLogin, Extratype.extratypepage);
//create extra type
router.post('/ajax-actions_extratype', Extratype.extratypeCreatepage);
//edit extra type
router.post('/ajax-actions_extratype_edit', Extratype.extratypeGetpage);
// Zeit list Page
router.get('/zeit', requireLogin, Extratype.zeitpepage);
//edit zeit type
router.post('/ajax-actions_zeit_edit', Extratype.zeitGetpage);
//create zeit
router.post('/ajax-actions_zeit', Extratype.zeitCreatepage);
// permit person list Page
router.get('/permit', requireLogin, Extratype.permitpage);
//create permit person
router.post('/ajax-actions_permitperson', Extratype.permitCreatepage);
//edit  permit person
router.post('/ajax-actions_permitperson_edit', Extratype.permitGetpage);
// Hair list Page
router.get('/hair', requireLogin, Extratype.hairpage);
//create Hair
router.post('/ajax-actions_hair', Extratype.hairCreatepage);
//edit  permit person
router.post('/ajax-actions_hair_edit', Extratype.hairGetpage);
// Eyes list Page
router.get('/eyes', requireLogin, Extratype.eyespage);
//create Eyes
router.post('/ajax-actions_eyes', Extratype.eyesCreatepage);
//edit  eyes 
router.post('/ajax-actions_eyes_edit', Extratype.eyesGetpage);
// Shoes list Page
router.get('/shoes', requireLogin, Extratype.shoespage);
//edit  shoes 
router.post('/ajax-actions_shoes_edit', Extratype.shoesGetpage);
//create Shoes
router.post('/ajax-actions_shoes', Extratype.shoesCreatepage);
// cloth list Page
router.get('/cloth', requireLogin, Extratype.clothpage);
//create Cloth
router.post('/ajax-actions_cloth', Extratype.clothCreatepage);
//edit  cloth 
router.post('/ajax-actions_cloth_edit', Extratype.clothGetpage);
// Civil status list Page
router.get('/civilstatus', requireLogin, Extratype.civilstatuspage);
//create Civil status
router.post('/ajax-actions_civilstatus', Extratype.civilstatusCreatepage);
//edit  civilstatus 
router.post('/ajax-actions_civilstatus_edit', Extratype.civilstatusGetpage);
// privatepart list Page
router.get('/privatepart', requireLogin, Extratype.privatepartpage);
//create Civil status
router.post('/ajax-actions_privatepart', Extratype.privatepartCreatepage);
//edit  private part 
router.post('/ajax-actions_privatepart_edit', Extratype.privatepartGetpage);
// Bust
router.get('/oberweite', requireLogin, Extratype.bustpage);
//create 
router.post('/ajax-actions_busttype', Extratype.bustCreatepage);
//edit  bust part 
router.post('/ajax-actions_bust_edit', Extratype.bustGetpage);
// nationality
router.get('/nationality', requireLogin, Extratype.nationalitypage);
//create nationality
router.post('/ajax-actions_nationality', Extratype.nationalityCreatepage);
//edit  nationality part 
router.post('/ajax-actions_nationality_edit', Extratype.nationalityGetpage);

/*----------CATEGORY---------------------------------------------------------------------*/
// category list Page
router.get('/category', requireLogin, Category.categorypage);
//create category
router.post('/ajax-actions_category', Category.categoryCreatepage);
//edit  category part 
router.post('/ajax-actions_category_edit', Category.categoryGetpage);
/*----------CUSTOMER---------------------------------------------------------------------*/
// Customer list Page
router.get('/customers', requireLogin, Customer.customerpage);
//create Customer
router.post('/ajax-actions_customercreate', Customer.customerCreatepage);
// Add Customer page
router.get('/addcustomer', requireLogin, Customer.addcustomerpage);
// Edit Customer page
router.get('/editcustomer', requireLogin, Customer.editcustomerpage);
// Customer profile page
router.get('/customerprofile', requireLogin, Customer.customerprofilepage);
/*----------Girls---------------------------------------------------------------------*/
// girls list Page
router.get('/girls', requireLogin, Girls.girlspage);
// girls list Page
router.get('/girlsdetails', requireLogin, Girls.girlsdetailspage);
// girls Add Page
router.get('/addgirls', requireLogin, Girls.addgirlspage);
//create create girls action
router.post('/ajax-actions_girlscreate', Girls.girlsCreatepage);
// girls Edit Page
router.get('/editgirls', requireLogin, Girls.editgirlspage);
//create  girls gallery cration
router.post('/ajax-actions_galleryupload', Girls.galleryCreatepage);
//Girls List
router.post('/ajax-actions_girlsList', Girls.girlsListpage);
//Remove image L List
router.post('/ajax-actions_removeImage', Girls.removeImagepage);
/*----------Vouchers---------------------------------------------------------------------*/
// Vouchers list Page
router.get('/vouchers', requireLogin, Vouchers.voucherspage);
// Vouchers Add Page
router.get('/addvouchers', requireLogin, Vouchers.addvoucherspage);
// Vouchers edit Page
router.get('/editvouchers', requireLogin, Vouchers.editvoucherspage);
//create  Vouchers cration
router.post('/ajax-actions_vouchers', Vouchers.vouchersCreatepage);
//create  Vouchers seprateuser
router.post('/ajax-actions_vouchersseparate', Vouchers.vouchersDetailspage);
/*----------Weekly Planer---------------------------------------------------------------------*/
// Weekly list Page
router.get('/weeklyplanner', requireLogin, Weeklyplanner.weeklyplannerpage);
//create  Work
router.post('/ajax-actions_weeklyplanner', Weeklyplanner.workcreatepage);
//get data
router.post('/ajax-actions_weeklyplannergetdata', Weeklyplanner.getdatapage);
//get girls data
router.post('/ajax-actions_workgirls', Weeklyplanner.getgirlsdatapage);
/*----------Points---------------------------------------------------------------------*/
// point list Page
router.get('/pointview', requireLogin, Pointcreate.pointviewpage);
// create point Page
router.get('/pointcreate', requireLogin, Pointcreate.pointcreatepage);
// point edit Page
router.get('/editPoint', requireLogin, Pointcreate.pointeditpage);
//create  point function
router.post('/ajax-actions_pointcreation', Pointcreate.pointcreationpage);
//imageupload  point function
router.post('/ajax-actions_galleryuploadpoint', Pointcreate.imageuploadpage);
/*----------Advertise---------------------------------------------------------------------*/
// Advertise list Page
router.get('/advertise', requireLogin, Advertise.advertiseviewpage);
// create Advertise Page
router.get('/advertisecreate', requireLogin, Advertise.advertisecreatepage);
// Advertise edit Page
router.get('/editAdvertise', requireLogin, Advertise.advertiseeditpage);
//create  Advertise function
router.post('/ajax-actions_advertise', Advertise.advertisecreationpage);
//Seprate  Advertise function
router.post('/ajax-actions_advertiseseparate', Advertise.advertiseseparate);
/*----------Notification---------------------------------------------------------------------*/
// Notification list Page
router.get('/notification', requireLogin, Notification.notificationViewPage);
// create Notification Page
router.get('/notificationcreate', requireLogin, Notification.notificationCreatePage);
// Notfication edit Page
router.get('/editNotification', requireLogin, Notification.notificationEditPage);
//create  notificatio function
router.post('/ajax-actions_notification', Notification.notificationCreationPage);
//imageupload  notification function
router.post('/ajax-actions_galleryuploadnote', Notification.imageuploadpage);
/*----------Debt---------------------------------------------------------------------*/
// Debt list Page
router.get('/customerdebt', requireLogin, Debt.debtViewPage);
//profile
router.get('/customerprofiledebt', requireLogin, Debt.customerprofiledebtpage);
/*----------Workplanner---------------------------------------------------------------------*/
// Workplanner Page
router.get('/arbeitsplan', requireLogin, Workplanner.workplannerViewPage);
//getdetail function
router.post('/ajax-actions_getworkplanner', Workplanner.getworkplannerPage);
//Createworkplanner function
router.post('/ajax-actions_createworkplanner', Workplanner.createworkplannerPage);
//Remove workplanner function
router.post('/ajax-actions_removeworkplanner', Workplanner.removeworkplannerPage);
//workplanner getdata
router.post('/ajax-actions_workplannergetdata', Workplanner.getdatapage);

/*----------Chat Mesage---------------------------------------------------------------------*/
// Chat Page
router.get('/chat', requireLogin, Chat.chatpage);
/*----------Rubble---------------------------------------------------------------------*/
// Rubble list Page
router.get('/rubble', requireLogin, Rubble.rubblepage);
// Rubble Add Page
router.get('/addrubble', requireLogin, Rubble.addrubblepage);
// Rubble edit Page
router.get('/editrubble', requireLogin, Rubble.editrubblepage);
//Rubble  Rubble cration
router.post('/ajax-actions_rubble', Rubble.rubbleCreatepage);
//Seprate  Rubble function
router.post('/ajax-actions_rubbleseparate', Rubble.rubbleseparate);
/*----------Chart---------------------------------------------------------------------*/
// Chart function
router.post('/ajax-actions_chartList', Chart.chartData);
/*----------DELETE---------------------------------------------------------------------*/
// Delete function
router.post('/ajax-actions_removeRecord', Removerecord.removetabledata);
//router.post('/ajax-actions_auto', Removerecord.autodata);
router.get('/auto_data', Removerecord.autodata);
// Delete function
router.post('/ajax-actions_removeRecord_girls', Removerecord.girlsIdproof);






module.exports = router;
/** Check Login**/
function requireLogin(req, res, next) {
    if (req.session && req.session.userid) { // Check if session exists
        app.locals.session_userid = req.session.userid;
        app.locals.session_username = req.session.username;
        // console.log(req.session.username);   
        next();
    } else {
        app.locals.session_userid = '';
        app.locals.session_username = '';
        res.redirect('/');
    }
};
global.getUniqueArray = function(array) {
    var uniqueArray = [];
   // Loop through array values
    for (i = 0; i < array.length; i++) {
        if (uniqueArray.indexOf(array[i]) === -1) {
            uniqueArray.push(array[i]);
        }
    }
    return uniqueArray;
};
global.arrayColumn = function(array, columnName) {
    return array.map(function(value, index) {
        return value[columnName];
    });
}


global.arrayColumnReassign = function(array, columnName, columnName2) {
    var arrdata = {};
    sessionPermission.forEach(function(sdata,key){
        var obj = {};
        var gval = sdata[columnName]
        obj[columnName] = sdata[columnName2];        
        arrdata[gval]=sdata[columnName2];
    });
   // console.log(arrdata);
    return arrdata;
    
}
global.getPermissionArray=function(sPermission)
{
    var arrdata = {};
    sessionPermission.forEach(function(sdata,key){
        var obj = {};
        obj['modulename'] = sdata.name;
        obj[1] = sdata.uadd;        //1- add permission
        obj[2] = sdata.uedit;       //1- edit permission
        obj[3] = sdata.uview;       //1- view permission
        obj[4] = sdata.udelete;     //1- delete permission
        arrdata[sdata.id]=obj;
        
       
    });
   // console.log(arrdata);
    return arrdata;
}
//sql callfunction

function myQueryFn(sql, args = {}) {
    return new Promise((resolve, reject) => {
        con.query(sql, args, (err, rows) => {
            if (err) {
                console.error('err', sql);
                return reject(err);
            }
            resolve(rows);
        });
    });
}

global.myQuery = async function (sql, args = {}) {
    try {
        //console.log(sql);
        let items = await myQueryFn(sql, args);
        let result = await items;
        return { "status": true, "error": null, "result": result };
    } catch (error) {
        console.log(error);
    }
}

//IN condtion form
global.condtionSql = function(arr)
{
    var cmdata = "";
    getArr = arr.split(",");
     if (Array.isArray(getArr))
     {
        vkey = 0;
        tkey = getArr.length;
         getArr.forEach(function(arrId, key) {
            vkey = parseInt(key) + parseInt(1);
            if (vkey == tkey) {
                cmdata += "'" + arrId + "'";
            } else {
                cmdata += "'" + arrId + "',";
            }

        });
     }
     else
     {
        cmdata += "'" + arr + "'";
     }
    return cmdata;
}
//IN condtion form array
global.condtionSqlArray = function(getArr)
{
   var cmdata = "";
   if (Array.isArray(getArr))
     {
        vkey = 0;
        tkey = getArr.length;
         getArr.forEach(function(arrId, key) {
            vkey = parseInt(key) + parseInt(1);
            if (vkey == tkey) {
                cmdata += "'" + arrId + "'";
            } else {
                cmdata += "'" + arrId + "',";
            }

        });
     }

     console.log(cmdata);
    return cmdata;
}
//service,extra,girls multi query
global.multiRowInSql = function(arr,id)
{
    var multidata = "";
    getArr = arr.split(",");
     if (Array.isArray(getArr))
     {
        vkey = 0;
        tkey = getArr.length;
         getArr.forEach(function(arrId, key) {
            vkey = parseInt(key) + parseInt(1);
            if (vkey == tkey) {
                multidata +='("'+id+'", "'+arrId+'")';
            } else {
                multidata += '("'+id+'", "'+arrId+'"),';
            }

        });
     }
     else
     {
        multidata +='("'+id+'", "'+arr+'")';
     }
     return multidata;
}
global.multiRowInSqlLog = function(arr,id,branch,girls,type,updown,date)
{
    var multidata = "";
    getArr = arr.split(",");
     if (Array.isArray(getArr))
     {
        vkey = 0;
        tkey = getArr.length;
         getArr.forEach(function(arrId, key) {
            vkey = parseInt(key) + parseInt(1);
            if (vkey == tkey) {
                multidata +='("'+id+'", "'+arrId+'", "'+branch+'", "'+girls+'", "'+type+'", "'+updown+'", "'+date+'")';
            } else {
                multidata += '("'+id+'", "'+arrId+'", "'+branch+'", "'+girls+'", "'+type+'", "'+updown+'", "'+date+'"),';
            }

        });
     }
     else
     {
        multidata +='("'+id+'", "'+arr+'")';
     }
     return multidata;
}