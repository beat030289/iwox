/* System Modules */
global.express = require('express');
global.session = require('express-session');
global.bodyParser = require('body-parser');
global.dateFormat = require('dateformat');
global.ejs = require('ejs');
global.fs = require('fs-extra');
global.os = require("os");
var url = require('url');
var mysql = require('mysql');
global.fileupload = require('express-fileupload');
global.pagination = require('pagination');
global.uniqid = require('uniqid');
global.voucher_codes = require('voucher-code-generator');
global.OneSignal = require('onesignal-node');
global.myClient = new OneSignal.Client({
    userAuthKey: 'YzVlOGRmZDItMjZiMS00YjE5LWI3ODMtYWViNjhlN2YwOWIx',
    app: {
        appAuthKey: 'ZTNkNWZlYjAtZGY3Ni00Y2UwLTk0OGYtOWM4NGM4ZDU3NjZi',
        appId: '2182c77d-42ce-4c6a-b14f-e2ed0bf331b8'
    }

});
/*var vcc = voucher_codes.generate({
    length: 6,
    count: 1,
    charset: voucher_codes.charset("alphabetic")
});
console.log(vcc);
console.log(vcc[0]);*/

global.uniqueRandom = require('unique-random');
/* Table Definition */
global.iw_user = 'iw_tns_user';
global.iw_setting = 'iw_tns_setting';
global.iw_webmodules = 'iw_mas_webmodules';
global.iw_servicetype = 'iw_mas_servicetype';
global.iw_extratype = 'iw_mas_extratype';
global.iw_zeit = 'iw_mas_zeit';
global.iw_category = 'iw_mas_category';
global.iw_customer = 'iw_tns_customer';
global.iw_branch = 'iw_tns_branch';
global.iw_authorization = 'iw_tns_authorization';
global.iw_localapproval = 'iw_tns_localapproval';
global.iw_room = 'iw_tns_room';
global.iw_service = 'iw_tns_service';
global.iw_serviceextra = 'iw_tns_service_extra';
global.iw_serviceservicetype = 'iw_tns_service_servicetype';
global.iw_girls = 'iw_tns_girls';
global.iw_girls_idproof = 'iw_tns_girls_idproof';
global.iw_girls_permit = 'iw_tns_girls_permit';
global.iw_girls_gallery = 'iw_tns_girls_gallery';
global.iw_girls_service = 'iw_tns_girls_service';
global.iw_girls_extra = 'iw_tns_girls_extra';
global.iw_girls_pin = 'iw_tns_girls_pin';
global.iw_permitperson = 'iw_tns_permit_person';
global.iw_hair = 'iw_tns_hair';
global.iw_eyes = 'iw_tns_eyes';
global.iw_privatepart = 'iw_tns_privatepart';
global.iw_civilstatus = 'iw_tns_civilstatus';
global.iw_nationality = 'iw_tns_nationality';
global.iw_bust = 'iw_tns_bust';
global.iw_shoes = 'iw_tns_shoes';
global.iw_cloth = 'iw_tns_cloth';
global.iw_vouchers = 'iw_tns_vouchers';
global.iw_pin = 'iw_tns_pin';
global.iw_workplanner = 'iw_tns_workplanner';
global.iw_points = 'iw_tns_points';
global.iw_point_gallery = 'iw_tns_point_gallery';
global.iw_advertise = 'iw_tns_advertise';
global.iw_notification = 'iw_tns_notification';
global.iw_notification_gallery= 'iw_tns_notification_gallery';
global.iw_notification_branch = 'iw_tns_notification_branch';
global.iw_workplannergirls = 'iw_tns_workplannergirls';
global.iw_workplanner_time = 'iw_tns_workplanner_time';
global.iw_chat = 'iw_tns_chat';
global.iw_chat_login = 'iw_tns_chat_login';
global.iw_rubble = 'iw_tns_rubble';
global.iw_booking = 'iw_tns_booking';
global.iw_booking_girls = 'iw_tns_booking_girls';
global.iw_booking_service = 'iw_tns_booking_service';
global.iw_booking_extra = 'iw_tns_booking_extra';

/*Image link Name*/
global.project_file_link = "assets/images/";
global.project_file_link_font = "images/";
global.file_link_localapproval = project_file_link + "localapproval/";
global.file_link_room = project_file_link + "rooms/";
global.file_link_branch = project_file_link + "branch/";
global.file_link_girls = project_file_link + "girls/";
global.file_link_gallery = project_file_link + "gallery/";
global.file_link_point = project_file_link + "point/";
global.file_link_advertise = project_file_link + "advertiseimage/";
global.file_link_notification = project_file_link + "notification/";
global.file_link_rubble = project_file_link + "rubble/";
/*Image link Name front*/
global.file_link_branch_fornt = project_file_link_font + "branch/";
global.file_link_rooms_fornt = project_file_link_font + "rooms/";
global.file_link_localapproval_fornt = project_file_link_font + "localapproval/";
global.file_link_girls_fornt = project_file_link_font + "girls/";
global.file_link_gallery_fornt = project_file_link_font + "gallery/";
global.file_link_point_fornt = project_file_link_font + "point/";
global.file_link_advertise_fornt = project_file_link_font + "advertiseimage/";
global.file_link_notification_fornt = project_file_link_font + "notification/";
global.file_link_rubble_fornt = project_file_link_font + "rubble/";
/* User defined Modules */
global.nodemailer = require('nodemailer');
global.sql = require('./libraries/sqlfunctions');
global.dataTable = require('./libraries/dataTable');
global.utls = require('./utils/utils');
global.chat = require('./chat/chat');
global.mobileapi_login = require('./mobileapi/mobileapi_login');
global.mobileapi_booking = require('./mobileapi/mobileapi_booking');
global.mobileapi_report = require('./mobileapi/mobileapi_report');
global.mobileapi_dailyreport = require('./mobileapi/mobileapi_dailyreport');
global.mobileapi_plus = require('./mobileapi/mobileapi_plus');
global.mobileapi_minus = require('./mobileapi/mobileapi_minus');
global.mobileapi_pendingpayment = require('./mobileapi/mobileapi_pendingpayment');

//var slashes = require('slashes');
global.rad2deg = require('rad2deg');
global.deg2rad = require('deg2rad');
global.datetime = require('node-datetime');
var Cryptr = require('cryptr');
global.cryptr = new Cryptr('lifo@123#');
global.md5 = require('md5');
global.app = express();
global.site_url = os.hostname();
global.randomstring = require("randomstring");
// global.vhost = require('vhost');
var fss = require('fs');
// var cors = require('cors')
// app.use(cors());
global.nDate = new Date().toLocaleString('en-US', {
    timeZone: 'Europe/Zurich'
});
/* Mail configration */
global.transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        user: 'iwoxch@gmail.com',
        pass: 'Swiss2020!'
    }
});
var sslPath = '/etc/letsencrypt/live/qlic.ch/';

/* Mysql connection */
console.log("site_url"+site_url);
if (site_url == 'BALA'|| site_url == 'DESKTOP-9T80SR5') {
    global.con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "",
        port: 3306,
        database: "iwox",
        charset: "utf8mb4_unicode_ci"
    });
    console.log(con);
    console.log("localserver");
} else {
	global.con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "iwox123",
        database: "iwox",
    });
    //console.log(con);
    console.log(con);
}
// price module
global.numeral = require('numeral');
numeral.register('locale', 'fr-ch', {
    delimiters: {
        thousands: '\'',
        decimal: '.'
    },
    abbreviations: {
        thousand: 'k',
        million: 'm',
        billion: 'b',
        trillion: 't'
    },
    ordinal: function(number) {
        return number === 1 ? 'er' : 'e';
    },
    currency: {
        symbol: 'CHF'
    }
});
numeral.locale('fr-ch');
numeral().format('0,0');
const app_exp = require("express")();
app_exp.set("view engine", "pug");
app_exp.use(require("body-parser").urlencoded({
    extended: false
}));
/** Initialize session **/
app.use(session({
    secret: 'iwoxApp',
    // store: sessionStore, // connect-mongo session store
    proxy: true,
    resave: true,
    saveUninitialized: true
}));
/** Initialize file upload **/
app.use(fileupload());
/** Request Header Setting **/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
// set the view engine to ejs
app.use(express.static(__dirname + '/assets'));
//app.use('/admin', express.static('/assets/admin_assets'));
app.set('view engine', 'ejs');
app.use(function(req, res, next) {
    //global.siteadmin_url=req.protocol + '://iwox.ch/';
    global.site_redirect_url = req.protocol + '://' + req.get('host') + '/';
    global.site_redirect_urlv = req.protocol + '://' + req.get('host');
    global.image_url = req.protocol + '://' + req.get('host') + '/';
    console.log(site_redirect_url);
   
    next();
});
/*Router call function */

var apiroutes = require('./routes/route');

app.use(apiroutes);
/** Check not empty **/
function checkNotEmpty(val) {
    if (val != '' && val != 0 && typeof(val) != 'undefined' && typeof(val) != 'null')
        return true;
    else return false;
}
app.locals.checkNotEmpty = function(val) {
        if (val != '' && val != 0 && typeof(val) != 'undefined' && typeof(val) != 'null')
            return true;
        else return false;
    }
    /** Encrypt **/
app.locals.encrypt = function(id) {
        const encryptedString = cryptr.encrypt(id);
        return encryptedString;
    }
    /** Decrypt **/
app.locals.decrypt = function(id) {
        const decryptedString = cryptr.decrypt(id);
        return decryptedString;
    }
    //app.listen(5005);
console.log(site_url);
if (site_url == 'BALA'|| site_url == 'DESKTOP-9T80SR5') {
    server = app.listen(5005);
    console.log(5005);
} else {
    server = app.listen(80);
    console.log(80);
   
}

global.io = require("socket.io")(server);

io.on('connection', (socket) => {
    socket.on('iwox_chat', function(data) {
        console.log(data);
        chat.chatApi(data, function(delivery, ret) {
            console.log("chatdata");
            console.log(data);
            socket.emit(delivery, ret);
        });
    });
    socket.on('iwox_api_profile', function(data) {
        console.log(data);
        mobileapi_login.mobileApi(data, function(resdata, ret) {
            //console.log(data);
            socket.emit(resdata, ret);
        });
    });
    socket.on('iwox_api_booking', function(data) {
        console.log(data);
        mobileapi_booking.mobileApi(data, function(resdata, ret) {
            /*console.log("resdata");
            console.log(ret);*/
            socket.emit(resdata, ret);
        });
    });
    socket.on('iwox_api_report', function(data) {
        console.log(data);
        mobileapi_report.mobileApi(data, function(resdata, ret) {
            console.log("resdata");
            console.log(ret);
            socket.emit(resdata, ret);
        });
    });
    socket.on('iwox_api_dailyreport', function(data) {
        console.log(data);
        mobileapi_dailyreport.mobileApi(data, function(resdata, ret) {
            console.log("resdata");
            console.log(ret);
            socket.emit(resdata, ret);
        });
    });
    socket.on('iwox_api_plus', function(data) {
        console.log(data);
        mobileapi_plus.mobileApi(data, function(resdata, ret) {
            console.log("resdata");
            console.log(ret);
            socket.emit(resdata, ret);
        });
    });
    socket.on('iwox_api_minus', function(data) {
        console.log(data);
        mobileapi_minus.mobileApi(data, function(resdata, ret) {
            console.log("resdata");
            console.log(ret);
            socket.emit(resdata, ret);
        });
    });
    socket.on('iwox_api_pendingpayment', function(data) {
        console.log(data);
        mobileapi_pendingpayment.mobileApi(data, function(resdata, ret) {
            console.log("resdata");
            console.log(ret);
            socket.emit(resdata, ret);
        });
    });
    socket.on('disconnect', function() {
        //console.log('user ' + app.locals.session_id + ' disconnected');
        //console.log(socket.id);
    });
});

/** Check not empty **/
function checkNotEmpty(val) {
    if (val != '' && val != 0 && typeof(val) != 'undefined' && typeof(val) != 'null')
        return true;
    else return false;
}
/** Encrypt **/
app.locals.encrypt = function(id) {
        const encryptedString = cryptr.encrypt(id);
        return encryptedString;
    }
    /** Decrypt **/
app.locals.decrypt = function(id) {
    try {
        const decryptedString = cryptr.decrypt(id);
        return decryptedString;
    } catch (ex) {
        return '';
    }
}
