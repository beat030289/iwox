module.exports = {
	fetchTable:function(table,fields,where,callback){
		con.query('SET GLOBAL sql_mode ="" ', function(err, result){
			
		});
		var where=typeof(where)!='undefined' && where!=''?' WHERE '+where:'';
		con.query('SELECT '+fields+' FROM '+table+where, function(err, result,fields){
			if (err) {
				callback(err,null,null);
			}
			else
				callback(null,result,fields);
		});
	},
	fetchJoinTable:function(table,fields,join,where,callback){
		con.query('SET GLOBAL sql_mode ="" ', function(err, result){
			
		});
		var where=typeof(where)!='undefined' && where!=''?' WHERE '+where:'';
		var join=typeof(join)!='undefined' && join!=''?join:'';
		con.query('SELECT '+fields+' FROM '+table+join+where, function(err, result){
			if (err) {
				callback(err,null);
			}
			else
				callback(null,result);
		});
	},
	runQuery:function(query,callback){
		con.query('SET GLOBAL sql_mode ="" ', function(err, result){
			
		});
		con.query(query, function(err, result){
			if (err) 
				callback(err,null);
			else
				callback(null,result);
		});
		
	},
	insertTable:function(table,values,callback){
		con.query('SET GLOBAL sql_mode ="" ', function(err, result){
			
		});
		con.query('INSERT INTO '+table+' SET ?', values, (err, res) => {
			if (err) 
				callback(err,null);
			else
				callback(null,res.insertId);
		});
	},
	insertTable2:function(table,columns,values,callback) {
		con.query('SET GLOBAL sql_mode ="" ', function(err, result){
			
		});
		con.query('INSERT INTO '+table+' ('+columns+') VALUES ('+values+')', (err, res) => {
			if (err) 
				callback(err,null);
			else
				callback(null,res.insertId);
		});
	},
	updateTable:function(table,values,where,callback) {
		con.query('SET GLOBAL sql_mode ="" ', function(err, result){
			
		});
		console.log(values);
		quer=con.query('UPDATE '+table+' SET ? WHERE ?', [values,where], (err, res) => {
			
		  if (err) 
			callback(err,null);
		  else
			callback(null,res.changedRows);
		});
		console.log(quer.sql);
		
	},
	insertValues:function(table,data,callback){
		var values=' ';
		for (var column in data)
			values+=column+'="'+data[column]+'",';
		values = values.substring(0, values.length-1);
		con.query('INSERT INTO '+table+' SET '+values,function(err, res){
			if (err) 
				callback(err,null);
			else
				callback(null,res.insertId);
		});
		
	},
	insertMultiple:function(table,datas,callback){
		data=datas[0];

		values=new Array();
		acloumns=new Array();
		var columns='(';
		for (var column in data)
		{
			acloumns.push(column);
			columns+=column+',';
		}
		columns = columns.substring(0, columns.length-1)+')';
		datas.forEach(function(cvalue,key){
			var value=new Array();
			acloumns.forEach(function(column,key){
				if(checkNotEmpty(cvalue[column]) && isValidDate(cvalue[column]))
					value.push((moment(new Date(cvalue[column])).format('YYYY-MM-DD')));
				else
					value.push(checkNotEmpty(cvalue[column])?addslashes(cvalue[column]):'');
			});
			values.push(value);
		});
		
		if(values.length>100)
		{
			//console.log(table);
			var exQueries=new Array();
			var executeQuery1 = "INSERT INTO "+table+" "+columns+" VALUES ";
			values.forEach(function(value,key){
				executeQuery1+='("'+(value.join('","'))+'")'+(key!=(values.length-1)?(key%100!=0?',':''):'');
				if(key%100==0)
				{
					exQueries.push(executeQuery1);
					executeQuery1 = "INSERT INTO "+table+" "+columns+" VALUES ";
				}
				else if(key==(values.length-1))
				{
					exQueries.push(executeQuery1);
				}

			});

			module.exports.saveInsertMutiple(0,exQueries,function(err,res){
				callback(err,res);
			});
			
		}
		else
		{
			var executeQuery1 = "INSERT INTO "+table+" "+columns+" VALUES ";
			values.forEach(function(value,key){
				executeQuery1+='("'+(value.join('","'))+'")'+(key!=(values.length-1)?',':'');
			});
			con.query(executeQuery1,function(err, res) {
				if(err==null)
					 callback(null,true);
				else if(err)
					callback(err,false);
			});
			
		}
		
	},
	saveInsertMutiple:function(key,queries,callback){
		executeQuery1=queries[key];
		con.query(executeQuery1,function(err,response) {
			if(err==null)
			{
				if(err==null && key==(queries.length-1))
					 callback(null,true);
				else
				{
					key=key+1;
					module.exports.saveInsertMutiple(key,queries,callback);
				}
			}
			else
				callback(err,false);
		});
	},
	updateMultiple:function(table,datas,condition,callback){

		if(datas.length>100)
		{
			//console.log(table);
			var exQueries=new Array();
			var updateString='';
			datas.forEach(function(value,key){
				var executeQuery1 = "UPDATE "+table+" SET ";
				for(column in value)
					executeQuery1+=' '+column+'="'+(checkNotEmpty(value[column]) && isValidDate(value[column])?(moment(new Date(value[column])).format('YYYY-MM-DD')):addslashes(value[column]))+'",';
				executeQuery1=executeQuery1.substring(0, executeQuery1.length - 1);
				executeQuery1+=' WHERE ';
				for(i=0;i<condition.length;i++)
					executeQuery1+=condition[i]+'="'+value[condition[i]]+'" AND';
				
				executeQuery1=executeQuery1.substring(0, executeQuery1.length - 3)+';';
				//exQueries.push(executeQuery1);
				updateString+=executeQuery1;
				if(key%100==0)
				{
					exQueries.push(updateString);
					updateString='';
				}
				else if(key==(datas.length-1))
				{
					exQueries.push(updateString);
				}

			});
			module.exports.saveUpdateMutiple(0,exQueries,function(err,res){
				callback(err,res);
			});
			
		}
		else
		{
			var exQueries=new Array();
			var updateString='';
			datas.forEach(function(value,key){
				var executeQuery1 = "UPDATE "+table+" SET ";
				for(column in value)
					executeQuery1+=' '+column+'="'+(checkNotEmpty(value[column]) && isValidDate(value[column])?(moment(new Date(value[column])).format('YYYY-MM-DD')):value[column])+'",';
				executeQuery1=executeQuery1.substring(0, executeQuery1.length - 1);
				executeQuery1+=' WHERE ';
				for(i=0;i<condition.length;i++)
					executeQuery1+=condition[i]+'="'+value[condition[i]]+'" AND';
				
				executeQuery1=executeQuery1.substring(0, executeQuery1.length - 3);
				//exQueries.push(executeQuery1);
				updateString+=executeQuery1;
				if(key%100==0)
				{
					exQueries.push(updateString);
					updateString='';
				}
				else if(key==(datas.length-1))
				{
					exQueries.push(updateString);
				}

			});
			module.exports.saveUpdateMutiple(0,exQueries,function(err,res){
				callback(err,res);
			});
			
		}
		
	},
	saveUpdateMutiple:function(key,queries,callback){
		executeQuery1=queries[key];
		con.query(executeQuery1,function(err,response) {
			if(err==null)
			{
				if(err==null && key==(queries.length-1))
					 callback(null,true);
				else
				{
					key=key+1;
					module.exports.saveUpdateMutiple(key,queries,callback);
				}
			}
			else
			{
				console.log(err);
				callback(err,false);
			}
		});
	},
	updateValues:function(table,data,where,callback){
		var values=condition=' ';
		/* Update columns and values */
		for (var column in data)
			values+=column+'="'+data[column]+'",';
		values = values.substring(0, values.length-1);
		/* Where condition columns and values */
		for (var column in where)
			condition+=column+'="'+where[column]+'",';
		condition = condition.substring(0, condition.length-1);
		
		con.query('UPDATE '+table+' SET '+values+' WHERE '+condition+'',function(err, res){
			if (err)
			{
				callback(err,null);
			}
			else
				callback(null,res.changedRows);
		});		
	},
	deleteValues:function(table,where,callback){
		var values=condition=' ';
		/* Where condition columns and values */
		for (var column in where)
			condition+=column+'="'+where[column]+'",';
		condition = condition.substring(0, condition.length-1);
		
		con.query('DELETE FROM '+table+' WHERE '+condition+'',function(err, res){
			if (err) 
				callback(err,null);
			else
				callback(null,true);
		});
		
	}
};

