var sql = require('./sqlfunctions');
var ejs = require('ejs');
var url = require('url');
var nodemailer = require('nodemailer');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var ip = require('ip');
var os = require("os");
var dateFormat = require('dateformat');
var fileupload = require('express-fileupload');
var fs = require('fs-extra');
//var slashes = require('slashes');
var Cryptr = require('cryptr');
var cryptr = new Cryptr('lifo@123#');
var ReverseMd5 = require('reverse-md5');
//var geoip = require('geoip-lite');
var thumb = require('node-thumbnail').thumb;
app.set('view engine', 'ejs');
var nDate = new Date().toLocaleString('en-US', {
    timeZone: 'Europe/Zurich'
});
var YOUR_API_KEY = "AIzaSyBW-PM1jYDUVx3HG7BDCUuudzRhyBYVx0Y";
var imageurlink_shop = "assets/images/shop/";
var imageurlink_shopthumbnail = "assets/images/thumbnail/";
var imageurlink_product = "assets/images/product/";
var imageurlink_deal = "assets/images/deal/";
var imageurlink_dealthumbnail = "assets/images/dealthumbnail/";
var imageurlink_notification = "assets/images/notification/";
//var NodeGeocoder = require('node-geocoder');
// var options = {
//   provider: 'google',

//   // Optional depending on the providers
//   httpAdapter: 'https', // Default
//   apiKey: 'AIzaSyBW-PM1jYDUVx3HG7BDCUuudzRhyBYVx0Y', // for Mapquest, OpenCage, Google Premier
//   formatter: null         // 'gpx', 'string', ...
// };
//var geocoder = NodeGeocoder(options);
var OneSignal = require('onesignal-node');
var myClient = new OneSignal.Client({
    userAuthKey: 'ZmMzYjYzMTAtNzIzNS00ZjkxLTkwNjItZjYyZDk5MTI4NzQ0',
    app: {
        appAuthKey: 'YWFlYjUxNDQtNDZkYS00YzE5LWJlYjQtMWZkZjhmZmM4NGRm',
        appId: '12b3b617-f0f1-446c-82bb-08aee8d0b1cf'
    }

});


/** Initialize file upload **/

app.use(fileupload());
/** Defining email constants **/
global.site_url = os.hostname();
var transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        user: 'qlicswiss@gmail.com',
        pass: 'Swiss2019!'
    }
});

module.exports = {
    userCheck: function(typea, typeb, typec, res, callback) {
        res = {
            t1: typea,
            t2: typeb,
            t3: typec
        };
        callback(res);
    },
    ajaxApi: function(req, res, callback) {
        var process = res.req.body.process; /** Process to be called **/
        var reqData = res.req.body; /** Posted value **/
        console.log(reqData);
        console.log(reqData.file);
        console.log("process" + process);
        switch (process) {
            case '41':
                /*** category Submit ***/
                cateName = checkNotEmpty(reqData.cate_name) ? reqData.cate_name : '';
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                where = edit_id != '' ? ' AND category_id!="' + edit_id + '"' : '';

                if (cateName == '') {
                    res = {
                        status: 'error',
                        message: 'Category Empty'
                    };
                    callback(null, res);
                } else {
                    var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                    sql.fetchTable('category', 'category_id', 'category_name="' + cateName + '"' + where, function(err1, resul) {
                        if (resul == '') {
                            if (edit_id) {
                                sql.runQuery('UPDATE category SET category_name="' + cateName + '" WHERE category_id="' + edit_id + '"', function(err_up, result_up) {
                                    if (err_up) {
                                        res = {
                                            status: 'error',
                                            message: 'error'
                                        };
                                        callback(null, res);
                                    } else {

                                        res = {
                                            status: 'success',
                                            message: 'successfully'
                                        };
                                        callback(null, res);
                                    }
                                });
                            } else {
                                sql.insertTable2('category', 'category_name,datetime', '"' + cateName + '","' + createdDate + '"', function(err, result) {
                                    if (err) {
                                        res = {
                                            status: 'error',
                                            message: 'error'
                                        };
                                        callback(null, res);
                                    } else {

                                        res = {
                                            status: 'success',
                                            message: 'successfully'
                                        };
                                        callback(null, res);
                                    }
                                });
                            }
                        } else {
                            res = {
                                status: 'already',
                                message: 'Category already exits'
                            };
                            callback(null, res);
                        }
                    });
                }
                break;
            case '42':
                /** Delete Record **/
                //var reId = "";
                record_id = checkNotEmpty(app.locals.decrypt(reqData.record_id)) ? app.locals.decrypt(reqData.record_id) : '';
                record_table = checkNotEmpty(reqData.record_table) ? reqData.record_table : '';
                record_column = checkNotEmpty(reqData.record_column) ? reqData.record_column : '';
                fileName = checkNotEmpty(reqData.fileName) ? reqData.fileName : '';
                var type = "";
                var id = "";
                if (req.session.admin_id != "" && req.session.admin_id != undefined && req.session.admin_id != null) {
                    type = "admin";
                    id = app.locals.encrypt(req.session.admin_id);
                } else if (req.session.shopdmin_id != "" && req.session.shopdmin_id != undefined && req.session.shopdmin_id != null) {
                    type = "shop";
                    id = app.locals.encrypt(req.session.shopdmin_id);
                } else if (req.session.branchadmin_id != "" && req.session.branchadmin_id != undefined && req.session.branchadmin_id != null) {
                    type = "branchadmin";
                    id = app.locals.encrypt(req.session.bbranch_id);
                }

                sql.runQuery('DELETE FROM ' + record_table + ' WHERE ' + record_column + '="' + record_id + '"', function(err, result) {
                    if (err) {
                        callback(err, null);
                    } else {
                        if (record_table == 'notification') {
                            sql.runQuery('SELECT note_image,id FROM notificationfile WHERE note_id="' + record_id + '"', function(err_img, result_img) {
                                if (err_img) {
                                    console.log();
                                } else {
                                    if (result_img != "") {
                                        console.log(result_img);
                                        result_img.forEach(function(i, idx, array) {
                                            //console.log(i.play_id);
                                            if (i.note_image != null && i.note_image != undefined && i.note_image != "") {
                                                fs.unlink(imageurlink_notification + i.note_image);
                                                sql.runQuery('DELETE FROM notificationfile WHERE id="' + i.id + '"', function(err_d, result_d) {

                                                    if (err_d) {
                                                        console.log(err_d);
                                                    }
                                                });

                                            }
                                        });
                                    }
                                }
                            });
                        }
                        if (fileName != '') {
                            if (record_table == 'shop') {
                                sql.runQuery('DELETE FROM shoptime WHERE shop_id="' + record_id + '"', function(err_d, result_d) {

                                    if (err_d) {
                                        console.log(err_d);
                                    }
                                });
                                var str = fileName.split('.');
                                var thumbnailimg = str[0] + '_thumb.' + str[1];
                                console.log(thumbnailimg);
                                fs.unlink(imageurlink_shop + fileName);
                                fs.unlink(imageurlink_shopthumbnail + thumbnailimg);
                            }
                            if (record_table == 'productfile') {
                                sql.runQuery('DELETE FROM productfile WHERE id="' + record_id + '"', function(err_d, result_d) {

                                    if (err_d) {
                                        console.log(err_d);
                                    }
                                });

                                fs.unlink(imageurlink_product + fileName);

                            }
                            if (record_table == 'notificationfile') {
                                sql.runQuery('DELETE FROM notificationfile WHERE id="' + record_id + '"', function(err_d, result_d) {

                                    if (err_d) {
                                        console.log(err_d);
                                    }
                                });

                                fs.unlink(imageurlink_notification + fileName);

                            }
                        }
                        //console.log(err);
                        res = {
                            status: 'success',
                            message: 'successfully',
                            type: type,
                            id: id
                        };
                        callback(err, res);
                    }
                });
                break;
            case '43':
                /*** Shop Details Submit ***/
                shopName = checkNotEmpty(reqData.shop_name) ? reqData.shop_name : '';
                shopUsername = checkNotEmpty(reqData.shop_uname) ? reqData.shop_uname : '';
                shopPassword = checkNotEmpty(reqData.shop_password) ? reqData.shop_password : '';
                shopmdPassword = checkNotEmpty(reqData.shop_password) ? md5(reqData.shop_password) : '';
                categoryId = checkNotEmpty(reqData.category_id) ? reqData.category_id : '';
                contactPerson = checkNotEmpty(reqData.contact_person) ? reqData.contact_person : '';
                email = checkNotEmpty(reqData.email) ? reqData.email : '';
                phone = checkNotEmpty(reqData.phone) ? reqData.phone : '';
                description = checkNotEmpty(reqData.description) ? reqData.description : '';
                facebook = checkNotEmpty(reqData.facebook) ? reqData.facebook : '';
                homepage = checkNotEmpty(reqData.homepage) ? reqData.homepage : '';
                shopPhone = checkNotEmpty(reqData.shop_phone) ? reqData.shop_phone : '';
                shopEmail = checkNotEmpty(reqData.shop_email) ? reqData.shop_email : '';
                innerbanner = checkNotEmpty(reqData.innerbanner) ? reqData.innerbanner : '';
                address = checkNotEmpty(reqData.shop_address) ? reqData.shop_address : '';
                place = checkNotEmpty(reqData.place) ? reqData.place : '';
                postcode = checkNotEmpty(reqData.postcode) ? reqData.postcode : '';
                canton = checkNotEmpty(reqData.canton) ? reqData.canton : '';
                uid = checkNotEmpty(reqData.uid) ? reqData.uid : '';
                //shop_addressline2=checkNotEmpty(reqData.shop_addressline2)?reqData.shop_addressline2:'';
                // var startweekday=checkNotEmpty(reqData.startweekday)?reqData.startweekday:'';
                // var endweekday=checkNotEmpty(reqData.endweekday)?reqData.endweekday:'';
                // var start_time=checkNotEmpty(reqData.start_time)?reqData.start_time:'';
                // var end_time=checkNotEmpty(reqData.end_time)?reqData.end_time:'';
                // var totalcount=checkNotEmpty(reqData.totalcount)?reqData.totalcount:'';
                // var timetype=checkNotEmpty(reqData.timetype)?reqData.timetype:'';
                // console.log("ttotal"+totalcount);
                // console.log(timetype);
                /*console.log("startweekday");
                console.log(reqData.startweekday[]);
                console.log("endweekday");
                console.log(reqData.endweekday[]);
                console.log("start_time"+start_time);
                console.log("end_time"+end_time);  */
                var valcate = "";
                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                var img = dateFormat(nDate, "yyyy-mm-ddHH:mm:ss");

                if (shopName) {
                    shopAlias = shopName; //shopName.toLowerCase();
                    shopAlias = shopAlias.trim();
                    shopAlias = shopAlias.replace(/[^A-Z0-9]+/ig, "_");
                }
                var strFirstThree = shopName.substring(0, 3);
                var fileNameTime = (new Date().getTime());
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                where = edit_id != '' ? ' AND shop_id!="' + edit_id + '"' : '';
                if (shopName == '') {
                    res = {
                        status: 'error',
                        message: 'Enter All field'
                    };
                    callback(null, res);
                } else {
                    sql.fetchTable('users', 'user_id', '(user_email="' + shopUsername + '" OR user_phone="' + shopUsername + '")', function(err_u, resul_u) {
                        if (resul_u == '') {
                            sql.fetchTable('shop', 'shop_id', '(shop_name="' + shopName + '" OR shop_username="' + shopUsername + '") AND shop_id!="' + edit_id + '"', function(err1, resul) {
                                if (resul == '') {
                                    if (edit_id) {
                                        sql.runQuery('UPDATE shop SET shop_name="' + shopName + '",shop_username="' + shopUsername + '",shop_password="' + shopPassword + '",category_id="' + categoryId + '",contact_person="' + contactPerson + '",shop_address="' + address + '",email="' + email + '",phone="' + phone + '",description="' + description + '" ,shop_mdpassword="' + shopmdPassword + '",facebook="' + facebook + '",homepage="' + homepage + '",shop_phone="' + shopPhone + '",shop_email="' + shopEmail + '",place="' + place + '",postcode="' + postcode + '",canton="' + canton + '" WHERE shop_id="' + edit_id + '"', function(err_up, result_up) {
                                            if (err_up) {
                                                res = {
                                                    status: 'error',
                                                    message: 'Invalid User'
                                                };
                                                callback(null, res);
                                            } else {
                                                // if(totalcount>1)
                                                //  {
                                                //  var ttotal = 0;
                                                //  for(k=0;k<(startweekday.length);k++)
                                                //      {
                                                //          var cct= parseInt(k)+1;
                                                //          if(k===((startweekday.length)-1))
                                                //          {

                                                //              valcate+='("'+edit_id+'", "'+startweekday[k]+'", "'+endweekday[k]+'", "'+start_time[k]+'","'+end_time[k]+'","'+cct+'","'+totalcount+'","'+timetype[k]+'")';
                                                //          }
                                                //          else{

                                                //          valcate+='("'+edit_id+'", "'+startweekday[k]+'", "'+endweekday[k]+'", "'+start_time[k]+'","'+end_time[k]+'","'+cct+'","'+totalcount+'","'+timetype[k]+'"),';
                                                //          }
                                                //      }

                                                //  }
                                                //  else
                                                //  {

                                                //      valcate+='("'+edit_id+'", "'+startweekday+'", "'+endweekday+'", "'+start_time+'","'+end_time+'","1","'+totalcount+'","'+timetype+'")';
                                                //  }
                                                /* else{
                                                        console.log("notarraydata");
                                                        valcate+='("'+edit_id+'", "'+startweekday+'", "'+endweekday+'", "'+start_time+'","'+end_time+'","1","1")';
                                                    } */
                                                //console.log(valcate);
                                                //      if(totalcount>0)
                                                //      {
                                                //      sql.runQuery('DELETE FROM shoptime WHERE shop_id="'+edit_id+'"',function(err_d,result_d){
                                                //      if(err_d)
                                                //          {

                                                //              callback(err_d,null);
                                                //          }
                                                //      else{
                                                //              sql.runQuery('INSERT INTO shoptime ( shop_id, start_day, end_day, start_time, end_time, numid, total, type) VALUES '+valcate,function(errcc,resultcc){
                                                //          if(errcc)
                                                //          {
                                                //              console.log(errcc);

                                                //          }
                                                //          else{

                                                //      }
                                                //  });
                                                //  }
                                                // });
                                                // }
                                                sql.fetchTable('shop', 'shop_id', 'shop_id!="' + edit_id + '"', function(err1, resull) {
                                                    if (resull != '') {
                                                        //shopAlias=shopAlias+'_'+edit_id;
                                                        //console.log('welcome'+resull);
                                                    }
                                                    if (req.files) {
                                                        /** Image upload **/
                                                        //console.log(reqData.old_shop_image);
                                                        let shopImage = req.files.shop_image;
                                                        var oldFileName = shopImage.name;
                                                        var fileExtension = oldFileName.replace(/^.*\./, '');
                                                        var newFileName = strFirstThree + fileNameTime + '.' + fileExtension;
                                                        //console.log(newFileName);
                                                        if (checkNotEmpty(shopImage)) {
                                                            /*** Use the mv() method to place the file somewhere on your server **/
                                                            shopImage.mv(imageurlink_shop + newFileName, function(err, result) {
                                                                if (err) {
                                                                    res['image_upload_status'] = 'upload failed';
                                                                } else {
                                                                    if (reqData.old_shop_image != "") {
                                                                        fs.unlink(imageurlink_shop + reqData.old_shop_image);
                                                                    }
                                                                    sql.runQuery('UPDATE shop SET shop_image="' + newFileName + '" WHERE shop_id="' + edit_id + '"', function(err_up_i, result_up) {
                                                                        if (err_up_i) {
                                                                            res['image_upload_status'] = 'upload failed';
                                                                        } else {
                                                                            thumb({
                                                                                source: imageurlink_shop + newFileName, // could be a filename: dest/path/image.jpg
                                                                                destination: 'assets/images/thumbnail',
                                                                                concurrency: 4,
                                                                                //width: 200,
                                                                                //height: 200,
                                                                            }, function(files, err, stdout, stderr) {
                                                                                console.log('All done!');
                                                                                var thumFileName = strFirstThree + fileNameTime + '_thumb.' + fileExtension;
                                                                                sql.runQuery('UPDATE shop SET innerbanner="' + thumFileName + '" WHERE shop_id="' + edit_id + '"', function(err_th_i, result_th) {
                                                                                    if (err_th_i) {
                                                                                        console.log(err_th_i);
                                                                                    } else {
                                                                                        if (innerbanner != "") {
                                                                                            console.log("testv");
                                                                                            fs.unlink(imageurlink_shopthumbnail + reqData.innerbanner);
                                                                                        }
                                                                                    }

                                                                                });

                                                                            });
                                                                            res['image_upload_status'] = 'upload success';
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        } else {
                                                            res['image_upload_status'] = 'empty';
                                                        }
                                                    }
                                                });
                                                res = {
                                                    status: 'success',
                                                    message: 'successfully'
                                                };
                                                callback(null, res);

                                            }
                                            //data end
                                        });
                                    } else {
                                        sql.insertTable2('shop', 'shop_name,shop_username,shop_password,category_id,datetime,created_by,shop_address,description,email,phone,contact_person,shop_mdpassword,facebook,homepage,shop_phone,shop_email,place,postcode,canton', '"' + shopName + '","' + shopUsername + '","' + shopPassword + '","' + categoryId + '","' + createdDate + '","' + req.session.admin_id + '","' + address + '","' + description + '","' + email + '","' + phone + '","' + contactPerson + '","' + shopmdPassword + '","' + facebook + '","' + homepage + '","' + shopPhone + '","' + shopEmail + '","' + place + '","' + postcode + '","' + canton + '"', function(err, result) {
                                            if (err) {
                                                console.log(err);
                                                res = {
                                                    status: 'error',
                                                    message: 'Invalid'
                                                };
                                                callback(null, res);
                                            } else {
                                                var cc = result;
                                                sql.runQuery('UPDATE shoptime SET shop_id="' + cc + '" WHERE uid="' + uid + '"', function(err_up, result_up) {
                                                    if (err_up) {
                                                        // res={status:'error',message:'Invalid User'};
                                                        // callback(null,res);
                                                        console.log(err_up);
                                                    }
                                                });
                                                sql.runQuery('UPDATE shoptab SET shopid="' + cc + '" WHERE uid="' + uid + '"', function(err_upc, result_upc) {
                                                    if (err_upc) {
                                                        console.log(err_upc);
                                                        // res={status:'error',message:'Invalid User'};
                                                        // callback(null,res);
                                                    }
                                                });
                                                /** Image upload **/
                                                let shopImage = req.files.shop_image;
                                                var oldFileName = shopImage.name;
                                                var fileExtension = oldFileName.replace(/^.*\./, '');
                                                var newFileName = strFirstThree + fileNameTime + '.' + fileExtension;
                                                // if(totalcount>1)
                                                // {
                                                // var ttotal = 0;
                                                // for(k=0;k<(startweekday.length);k++)
                                                //  {
                                                //      var cct= parseInt(k)+1;
                                                //      if(k===((startweekday.length)-1))
                                                //      {

                                                //          valcate+='("'+cc+'", "'+startweekday[k]+'", "'+endweekday[k]+'", "'+start_time[k]+'","'+end_time[k]+'","'+cct+'","'+totalcount+'","'+timetype[k]+'")';
                                                //      }
                                                //      else{

                                                //      valcate+='("'+cc+'", "'+startweekday[k]+'", "'+endweekday[k]+'", "'+start_time[k]+'","'+end_time[k]+'","'+cct+'","'+totalcount+'","'+timetype[k]+'"),';
                                                //      }
                                                //  }

                                                // }
                                                // else
                                                // {

                                                //  valcate+='("'+cc+'", "'+startweekday+'", "'+endweekday+'", "'+start_time+'","'+end_time+'","1","'+totalcount+'","'+timetype+'")';
                                                // }
                                                // console.log(valcate);
                                                // if(totalcount>0)
                                                // {
                                                //  sql.runQuery('INSERT INTO shoptime ( shop_id, start_day, end_day, start_time, end_time, numid, total, type) VALUES '+valcate,function(errcc,resultcc){
                                                //      if(errcc)
                                                //      {
                                                //          console.log(errcc);

                                                //      }
                                                //      else{

                                                //      }
                                                //  });
                                                // }
                                                if (checkNotEmpty(shopImage)) {
                                                    shopImage.mv(imageurlink_shop + newFileName, function(err, result) {
                                                        if (err) {
                                                            res['image_upload_status'] = 'upload failed';
                                                        } else {
                                                            if (reqData.old_shop_image != "") {
                                                                fs.unlink(imageurlink_shop + reqData.old_shop_image);
                                                            }
                                                            sql.runQuery('UPDATE shop SET shop_image="' + newFileName + '" WHERE shop_id="' + cc + '"', function(err_up_i, result_up) {
                                                                if (err_up_i) {
                                                                    res['image_upload_status'] = 'upload failed';
                                                                } else {
                                                                    thumb({
                                                                        source: imageurlink_shop + newFileName,
                                                                        destination: 'assets/images/thumbnail',
                                                                        concurrency: 4,

                                                                    }, function(files, err, stdout, stderr) {
                                                                        console.log('All done!');
                                                                        var thumFileName = strFirstThree + fileNameTime + '_thumb.' + fileExtension;
                                                                        sql.runQuery('UPDATE shop SET innerbanner="' + thumFileName + '" WHERE shop_id="' + cc + '"', function(err_th_i, result_th) {
                                                                            if (err_th_i) {
                                                                                console.log(err_th_i);
                                                                            } else {
                                                                                if (innerbanner != "") {
                                                                                    console.log("testv");
                                                                                    fs.unlink(imageurlink_shopthumbnail + reqData.innerbanner);
                                                                                }
                                                                            }

                                                                        });

                                                                    });
                                                                    res['image_upload_status'] = 'upload success';
                                                                }
                                                            });
                                                        }
                                                    });
                                                } else {
                                                    res['image_upload_status'] = 'empty';
                                                }
                                                res = {
                                                    status: 'success',
                                                    message: 'successfully',
                                                    test: 'pro'
                                                };
                                                console.log(res);
                                                callback(null, res);
                                            }
                                        });
                                    }
                                } else {
                                    res = {
                                        status: 'already',
                                        message: 'Shop Name / Username  already exits'
                                    };
                                    callback(null, res);
                                }
                            });
                        } else {
                            res = {
                                status: 'already',
                                message: 'Username  already exits'
                            };
                            callback(null, res);
                        }
                    });
                }
                break;
            case '44':
                /*** Admin Login ***/
                username = checkNotEmpty(reqData.username) ? reqData.username : '';
                password = checkNotEmpty(reqData.password) ? md5(reqData.password) : '';
                sql.fetchTable('users', 'user_id,user_firstname,user_lastname,user_email', '(user_phone="' + username + '" OR user_email="' + username + '") AND password="' + password + '"', function(err, result) {
                    if (err)
                        callback(err, null);
                    else {
                        if (result == "") {
                            var ret = {
                                status: 'error',
                                'message': 'Invalid Username/ Password '
                            };
                            callback(err, ret);
                        } else {
                            /*** Setting Session ***/
                            req.session.admin_id = result[0].user_id;
                            req.session.admin_email = result[0].user_email;
                            req.session.admin_name = result[0].user_firstname + ' ' + result[0].user_lastname;
                            var ret = {
                                status: 'success',
                                user: result[0]
                            };
                            callback(null, ret);
                        }
                    }
                });
                break;
            case '45':
                /*** change Password  Admin***/
                oldPassword = checkNotEmpty(reqData.oldPassword) ? md5(reqData.oldPassword) : '';
                newPassword = checkNotEmpty(reqData.newPassword) ? md5(reqData.newPassword) : '';
                where = req.session.admin_id != '' ? ' AND user_id!="' + req.session.admin_id + '"' : '';
                console.log(oldPassword);
                if (oldPassword == '' || newPassword == '') {
                    res = {
                        status: 'empty',
                        message: 'Enter the all field'
                    };
                    callback(null, res);
                } else {
                    sql.fetchTable('users', 'user_id', 'password="' + oldPassword + '" AND user_id="' + req.session.admin_id + '"', function(err1, resul, fields) {
                        if (resul != '') {
                            sql.runQuery('UPDATE users SET password="' + newPassword + '" WHERE user_id="' + req.session.admin_id + '"', function(err_u, result_u) {
                                if (err_u) {
                                    res = {
                                        status: 'error',
                                        message: 'Old Password Incorrect'
                                    };
                                    callback(null, res);
                                } else {
                                    res = {
                                        status: 'success',
                                        message: 'Successfully Update'
                                    };
                                    callback(null, res);
                                }
                            });
                        } else {
                            res = {
                                status: 'invalid',
                                message: 'Old Password Incorrect',
                                test: resul
                            };
                            callback(null, res);
                        }
                    });
                }
                break;
            case '46':
                /*** Setings Submit ***/
                firstName = checkNotEmpty(reqData.first_name) ? reqData.first_name : '';
                lastName = checkNotEmpty(reqData.last_name) ? reqData.last_name : '';
                email = checkNotEmpty(reqData.email) ? reqData.email : '';
                phone = checkNotEmpty(reqData.phone) ? reqData.phone : '';
                if (firstName == '') {
                    res = {
                        status: 'error',
                        message: 'Enter the all field'
                    };
                    callback(null, res);
                } else {
                    var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                    sql.fetchTable('users', 'user_id', '(user_email="' + email + '" OR user_phone="' + phone + '") AND user_id!="' + req.session.admin_id + '"', function(err1, resul) {
                        if (resul == '') {
                            sql.runQuery('UPDATE users SET user_firstname="' + firstName + '",user_lastname="' + lastName + '",user_email="' + email + '",user_phone="' + phone + '" WHERE user_id="' + req.session.admin_id + '"', function(err_u, result_u) {
                                if (err_u) {
                                    res = {
                                        status: 'error',
                                        message: 'Invalid User'
                                    };
                                    callback(null, res);
                                } else {
                                    res = {
                                        status: 'success',
                                        message: 'Successfully Update'
                                    };
                                    callback(null, res);
                                }
                            });
                        } else {
                            res = {
                                status: 'already',
                                message: ' Email or  Phone Number already exits'
                            };
                            callback(null, res);
                        }
                    });
                }
                break;
            case '47':
                /** Shop Details Record **/
                shopId = checkNotEmpty(app.locals.decrypt(reqData.shop_id)) ? app.locals.decrypt(reqData.shop_id) : '';
                sql.runQuery('SELECT shop.shop_name,shop.shop_id,shop.shop_image,shop.category_id,shop.shop_address,shop.contact_person,shop.email,shop.phone,shop.description,category.category_name FROM shop,category WHERE shop.shop_id="' + shopId + '" AND shop.category_id=category.category_id', function(err_shop, result_shop) {
                    if (err_shop) {
                        result_shop = [];
                        res = {
                            status: 'error',
                            'message': " Invalid Request"
                        };
                        callback(null, res);
                    } else {
                        var shopData = '<div class=""><h2 class="m-b-0 m-t-0">' + result_shop[0].shop_name + '</h2> <small class="text-muted db">' + result_shop[0].category_name + '</small><hr><div class="row"><div class="col-lg-3 col-md-3 col-sm-6"><div class="white-box text-center"> <img src="../images/shop/' + result_shop[0].shop_image + '" class="img-responsive" /> </div></div><div class="col-lg-9 col-md-9 col-sm-6"><h4 class="box-title m-t-40">Description</h4><p>' + result_shop[0].description + '</p></div><div class="col-lg-12 col-md-12 col-sm-12"><h3 class="box-title m-t-40">General Info</h3><div class="table-responsive"><table class="table"><tbody><tr><td width="390">Contact Person</td><td>' + result_shop[0].contact_person + '</td></tr><tr><td>Email</td><td>' + result_shop[0].email + '</td></tr><tr><td>Phone</td><td>' + result_shop[0].phone + '</td></tr><tr><td>Addres</td><td>' + result_shop[0].shop_address + '</td></tr></tbody></table></div></div></div></div>';
                        res = {
                            status: 'success',
                            shop: shopData
                        };
                        callback(null, res);
                    }
                });
                break;
            case '48':
                /*** Branch Submit ***/

                branchName = checkNotEmpty(reqData.branch_name) ? reqData.branch_name : '';
                branchAddress = checkNotEmpty(reqData.branch_address) ? reqData.branch_address : '';
                shopId = checkNotEmpty(reqData.shop_id) ? app.locals.decrypt(reqData.shop_id) : '';
                email = checkNotEmpty(reqData.email) ? reqData.email : '';
                phone = checkNotEmpty(reqData.phone) ? reqData.phone : '';
                place = checkNotEmpty(reqData.place) ? reqData.place : '';
                postcode = checkNotEmpty(reqData.postcode) ? reqData.postcode : '';
                canton = checkNotEmpty(reqData.canton) ? reqData.canton : '';
                openingtime = checkNotEmpty(reqData.openingtime) ? reqData.openingtime : '';
                url_type = checkNotEmpty(reqData.type) ? reqData.type : '';
                uid = checkNotEmpty(reqData.uid) ? reqData.uid : '';
                var branchLat = "";
                var branchLong = "";
                console.log("phone" + phone)
                    // geocoder.geocode(branchAddress, function(err, res) {
                    //  if(err)
                    //   console.log(err);
                    // else
                    // {
                    //  console.log(res);
                    //  console.log(res[0].latitude);
                    //  console.log(res[0].longitude);
                    //  branchLat=res[0].latitude;
                    //  branchLong=res[0].longitude;    
                    // }

                // });
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                where = edit_id != '' ? ' AND branch_id!="' + edit_id + '"' : '';

                if (branchName == '') {
                    res = {
                        status: 'error',
                        message: 'Branch Empty'
                    };
                    callback(null, res);
                } else {
                    geocoder.geocode(branchAddress, function(err, res) {
                        if (err)
                            console.log(err);
                        else {
                            console.log(res);
                            console.log(res[0].latitude);
                            console.log(res[0].longitude);
                            branchLat = res[0].latitude;
                            branchLong = res[0].longitude;
                        }
                        var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                        sql.fetchTable('branch', 'branch_id', 'branch_name="' + branchName + '" AND shop_id="' + shopId + '"' + where, function(err1, resul) {
                            if (resul == '') {
                                if (edit_id) {
                                    sql.runQuery('UPDATE branch SET branch_name="' + branchName + '",branch_name="' + branchName + '",branch_address="' + branchAddress + '",branch_lat="' + branchLat + '",branch_long="' + branchLong + '",phone="' + phone + '",email="' + email + '",place="' + place + '",postcode="' + postcode + '",canton="' + canton + '",openingtime="' + openingtime + '" WHERE branch_id="' + edit_id + '"', function(err_up, result_up) {
                                        if (err_up) {
                                            res = {
                                                status: 'error',
                                                message: 'error'
                                            };
                                            callback(null, res);
                                        } else {
                                            res = {
                                                status: 'success',
                                                message: 'successfully',
                                                shopId: reqData.shop_id,
                                                type: url_type
                                            };
                                            callback(null, res);
                                        }
                                    });
                                } else {
                                    sql.insertTable2('branch', 'branch_name,branch_address,branch_lat,branch_long,shop_id,created_date,created_by,email,phone,place,postcode,canton,openingtime', '"' + branchName + '","' + branchAddress + '","' + branchLat + '","' + branchLong + '","' + shopId + '","' + createdDate + '","' + shopId + '","' + email + '","' + phone + '","' + place + '","' + postcode + '","' + canton + '","' + openingtime + '"', function(err, result) {
                                        if (err) {
                                            res = {
                                                status: 'error',
                                                message: 'error'
                                            };
                                            callback(null, res);
                                        } else {
                                            var cc = result;
                                            sql.runQuery('UPDATE branchtime SET branch_id="' + cc + '" WHERE uid="' + uid + '"', function(err_up, result_up) {
                                                if (err_up) {
                                                    // res={status:'error',message:'Invalid User'};
                                                    // callback(null,res);
                                                    console.log(err_up);
                                                }
                                            });
                                            sql.runQuery('UPDATE branchtab SET branchid="' + cc + '" WHERE uid="' + uid + '"', function(err_upc, result_upc) {
                                                if (err_upc) {
                                                    console.log(err_upc);
                                                    // res={status:'error',message:'Invalid User'};
                                                    // callback(null,res);
                                                }
                                            });
                                            res = {
                                                status: 'success',
                                                message: 'successfully',
                                                shopId: reqData.shop_id,
                                                type: url_type
                                            };
                                            callback(null, res);
                                        }
                                    });
                                }
                            } else {
                                res = {
                                    status: 'already',
                                    message: 'Branch already exits'
                                };
                                callback(null, res);
                            }
                        });
                    });
                }

                break;
            case '49':
                /*** Product Details Submit ***/

                productName = checkNotEmpty(reqData.product_name) ? reqData.product_name : '';
                // productCode=checkNotEmpty(reqData.product_code)?reqData.product_code:'';
                description = checkNotEmpty(reqData.description) ? reqData.description : '';
                short_description = checkNotEmpty(reqData.short_description) ? reqData.short_description : '';
                title = checkNotEmpty(reqData.title) ? reqData.title : '';
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                shopId = checkNotEmpty(reqData.shop_id) ? app.locals.decrypt(reqData.shop_id) : '';
                productPoint = checkNotEmpty(reqData.product_point) ? reqData.product_point : '';
                console.log(productPoint);
                url_type = checkNotEmpty(reqData.type) ? reqData.type : '';
                sId = app.locals.encrypt(shopId);
                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                var img = dateFormat(nDate, "yyyy-mm-ddHH:mm:ss");
                var strFirstThree = productName.substring(0, 3);
                var fileNameTime = (new Date().getTime());
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                totalcountimage = checkNotEmpty(reqData.totalcountimage) ? reqData.totalcountimage : '';
                console.log("totalcountimage" + totalcountimage);
                if (productName == '') {
                    res = {
                        status: 'error',
                        message: 'Enter All field'
                    };
                    callback(null, res);
                } else {
                    sql.fetchTable('product', 'product_id', 'product_name="' + productName + '" AND shop_id="' + shopId + '" AND product_id!="' + edit_id + '"', function(err1, resul) {
                        if (resul == '') {
                            if (edit_id) {
                                sql.runQuery('UPDATE product SET product_name="' + productName + '",description="' + description + '",points="' + productPoint + '",title="' + title + '",short_description="' + short_description + '" WHERE product_id="' + edit_id + '"', function(err_up, result_up) {
                                    if (err_up) {
                                        res = {
                                            status: 'error',
                                            message: 'Invalid User'
                                        };
                                        callback(null, res);
                                    } else {
                                        var chk_type = "";
                                        console.log("imagetesting");
                                        if (req.files) {
                                            const parent = req.files;
                                            let carFiles = parent.file;
                                            chk_type = req.files.product_image.length;
                                            console.log(req.files.product_image.length);
                                            var chk_array = Array.isArray(carFiles);
                                            console.log(chk_array);
                                        }
                                        //if (totalcountimage>1 && totalcountimage!=0)
                                        if (chk_type != undefined) {
                                            /** Image upload **/
                                            //console.log(reqData.old_shop_image);

                                            if (req.files) {
                                                console.log("imagetesting2");
                                                console.log(req.files.product_image.length);
                                                for (let i = 0; i < req.files.product_image.length; i++) {

                                                    let productImage = req.files.product_image[i];
                                                    var oldFileName = productImage.name;
                                                    var fileExtension = oldFileName.replace(/^.*\./, '');
                                                    var newFileName = strFirstThree + i + fileNameTime + '.' + fileExtension;
                                                    console.log(newFileName);
                                                    numid = i + 1;
                                                    req.files.product_image[i].mv(imageurlink_product + newFileName, function(err) {

                                                        if (err) {
                                                            res['image_upload_status'] = 'upload failed';
                                                        }

                                                    });
                                                    sql.insertTable2('productfile', 'product_image,product_id,shop_id,numid,total', '"' + newFileName + '","' + productId + '","' + shopId + '","' + numid + '","' + totalcountimage + '"', function(errc, resultc) {
                                                        if (errc) {
                                                            console.log(errc);
                                                        }
                                                    });

                                                }
                                                //res.send('files uploaded);
                                            }
                                        } else
                                        //else if(totalcountimage<2 && totalcountimage!=0)
                                        {
                                            if (req.files) {

                                                let productImage = req.files.product_image;
                                                var oldFileName = productImage.name;
                                                var fileExtension = oldFileName.replace(/^.*\./, '');
                                                var newFileName = strFirstThree + fileNameTime + '.' + fileExtension;
                                                //console.log(newFileName);
                                                if (checkNotEmpty(productImage)) {
                                                    /*** Use the mv() method to place the file somewhere on your server **/
                                                    productImage.mv(imageurlink_product + newFileName, function(err, result) {
                                                        if (err) {
                                                            res['image_upload_status'] = 'upload failed';
                                                        } else {
                                                            sql.insertTable2('productfile', 'product_image,product_id,shop_id,numid,total', '"' + newFileName + '","' + productId + '","' + shopId + '","1","1"', function(errc, resultc) {
                                                                if (errc) {
                                                                    console.log(errc);
                                                                }
                                                            });
                                                        }
                                                    });
                                                } else {
                                                    res['image_upload_status'] = 'empty';
                                                }
                                            }

                                        }


                                        res = {
                                            status: 'success',
                                            message: 'successfully',
                                            shopId: reqData.shop_id,
                                            type: url_type
                                        };
                                        callback(null, res);
                                    }
                                });
                            } else {
                                sql.insertTable2('product', 'product_name,description,shop_id,created_by,created_date,points,title,short_description', '"' + productName + '","' + description + '","' + shopId + '","' + shopId + '","' + createdDate + '","' + productPoint + '","' + title + '","' + short_description + '"', function(err, result) {
                                    if (err) {
                                        res = {
                                            status: 'error',
                                            message: 'Invalid'
                                        };
                                        callback(null, res);
                                    } else {
                                        var productId = result;
                                        sql.runQuery('SELECT count(product_id) as pcode FROM product WHERE shop_id="' + shopId + '" ', function(err_img, result_img) {
                                            if (err_img) {
                                                console.log(err_img);
                                            } else {
                                                var productcode = parseInt(result_img[0].pcode);
                                                var pad = "0000";
                                                var pccode = (pad + productcode).slice(-pad.length);
                                                sql.runQuery('UPDATE product SET product_code="' + pccode + '" WHERE product_id="' + productId + '"', function(err_pc, result_pc) {
                                                    if (err_pc) {
                                                        console.log(err_pc);
                                                    } else {
                                                        console.log(result_pc);
                                                    }
                                                });
                                            }
                                        });

                                        /** Image upload **/
                                        var chk_type = "";
                                        console.log("imagetesting");
                                        if (req.files) {
                                            const parent = req.files;
                                            let carFiles = parent.file;
                                            chk_type = req.files.product_image.length;
                                            console.log(req.files.product_image.length);
                                            var chk_array = Array.isArray(carFiles);
                                            console.log(chk_array);
                                        }
                                        //if (totalcountimage>1 && totalcountimage!=0)
                                        if (chk_type != undefined) {
                                            /** Image upload **/
                                            //console.log(reqData.old_shop_image);
                                            if (req.files) {

                                                for (let i = 0; i < req.files.product_image.length; i++) {

                                                    let productImage = req.files.product_image[i];
                                                    var oldFileName = productImage.name;
                                                    var fileExtension = oldFileName.replace(/^.*\./, '');
                                                    var newFileName = strFirstThree + i + fileNameTime + '.' + fileExtension;
                                                    console.log(newFileName);
                                                    numid = i + 1;
                                                    req.files.product_image[i].mv(imageurlink_product + newFileName, function(err) {

                                                        if (err) {
                                                            res['image_upload_status'] = 'upload failed';
                                                        }

                                                    });
                                                    sql.insertTable2('productfile', 'product_image,product_id,shop_id,numid,total', '"' + newFileName + '","' + productId + '","' + shopId + '","' + numid + '","' + totalcountimage + '"', function(errc, resultc) {
                                                        if (err) {
                                                            console.log(errc);
                                                        }
                                                    });

                                                }
                                                //res.send('files uploaded);
                                            }
                                        }
                                        //else if(totalcountimage<2 && totalcountimage!=0)
                                        else {
                                            let productImage = req.files.product_image;
                                            var oldFileName = productImage.name;
                                            var fileExtension = oldFileName.replace(/^.*\./, '');
                                            var newFileName = strFirstThree + fileNameTime + '.' + fileExtension;
                                            //console.log(newFileName);
                                            if (checkNotEmpty(productImage)) {
                                                /*** Use the mv() method to place the file somewhere on your server **/
                                                productImage.mv(imageurlink_product + newFileName, function(err, result) {
                                                    if (err) {
                                                        res['image_upload_status'] = 'upload failed';
                                                    } else {
                                                        sql.insertTable2('productfile', 'product_image,product_id,shop_id,numid,total', '"' + newFileName + '","' + productId + '","' + shopId + '","1","1"', function(errc, resultc) {
                                                            if (err) {
                                                                console.log(errc);
                                                            }
                                                        });
                                                    }
                                                });
                                            } else {
                                                res['image_upload_status'] = 'empty';
                                            }

                                        }
                                        res = {
                                            status: 'success',
                                            message: 'successfully',
                                            shopId: reqData.shop_id,
                                            type: url_type
                                        };
                                        callback(null, res);
                                    }
                                });
                            }
                        } else {
                            res = {
                                status: 'already',
                                message: 'product Name already exits'
                            };
                            callback(null, res);
                        }
                    });
                }
                break;
            case '50':
                /** Delete Record  branch/product/branchadmin/staff**/
                if (req.session.admin_id) {
                    var url_type = "admin";
                } else if (req.session.shopadmin_id) {
                    var url_type = "shop";
                }
                record_id = checkNotEmpty(app.locals.decrypt(reqData.record_id)) ? app.locals.decrypt(reqData.record_id) : '';
                record_table = checkNotEmpty(reqData.record_table) ? reqData.record_table : '';
                record_column = checkNotEmpty(reqData.record_column) ? reqData.record_column : '';
                fileName = checkNotEmpty(reqData.fileName) ? reqData.fileName : '';
                shopId = checkNotEmpty(reqData.shopId) ? reqData.shopId : '';
                if (record_table != 'product') {
                    sql.runQuery('DELETE FROM ' + record_table + ' WHERE ' + record_column + '="' + record_id + '"', function(err, result) {
                        if (err) {
                            callback(err, null);
                        } else {
                            res = {
                                status: 'success',
                                message: 'successfully',
                                type: url_type
                            };
                            callback(err, res);
                        }
                    });
                } else {
                    sql.runQuery('SELECT id,product_image FROM productfile WHERE product_id="' + record_id + '"', function(err_img, result_img) {
                        if (err_img) {
                            console.log(err_cont);
                        } else {
                            if (result_img == "") {
                                sql.runQuery('DELETE FROM ' + record_table + ' WHERE ' + record_column + '="' + record_id + '"', function(err, result) {
                                    if (err) {
                                        callback(err, null);
                                    } else {
                                        console.log("ok");
                                        res = {
                                            status: 'success',
                                            message: 'successfully',
                                            type: url_type
                                        };
                                        callback(err, res);
                                    }
                                });
                            } else {

                                console.log(result_img);
                                console.log("result_img.length" + result_img.length);
                                var total = result_img.length;
                                var piv = 0;
                                for (i = 0; i < result_img.length; i++) {
                                    console.log(result_img[i].product_image);
                                    console.log(result_img[i].id);
                                    var id = result_img[i].id;
                                    var imname = result_img[i].product_image;
                                    fs.unlink(imageurlink_product + imname);

                                    // fs.unlink(imageurlink_product+imname, function(err,scc){
                                    // if (err)
                                    // console.log(err);
                                    // else
                                    // {
                                    sql.runQuery('DELETE FROM productfile WHERE id="' + id + '"', function(err, result) {
                                        if (err) {
                                            callback(err, null);
                                        } else {
                                            piv = parseInt(piv) + 1;
                                            console.log("piv" + piv);
                                            if (total == piv) {
                                                sql.runQuery('DELETE FROM ' + record_table + ' WHERE ' + record_column + '="' + record_id + '"', function(err, result) {
                                                    if (err) {
                                                        callback(err, null);
                                                    } else {
                                                        console.log("ok");
                                                        res = {
                                                            status: 'success',
                                                            message: 'successfully',
                                                            type: url_type
                                                        };
                                                        callback(err, res);
                                                    }
                                                });

                                            }
                                        }
                                    });

                                    // }
                                    // });

                                }
                            }


                        }
                    });
                }
                break;
            case '51':
                /*** BranchAdmin Submit ***/
                branchId = checkNotEmpty(reqData.branch_id) ? reqData.branch_id : '';
                baName = checkNotEmpty(reqData.ba_name) ? reqData.ba_name : '';
                email = checkNotEmpty(reqData.email) ? reqData.email : '';
                phone = checkNotEmpty(reqData.phone) ? reqData.phone : '';
                baPassword = checkNotEmpty(reqData.password) ? reqData.password : '';
                baUsername = checkNotEmpty(reqData.username) ? reqData.username : '';
                bamdPassword = checkNotEmpty(reqData.password) ? md5(reqData.password) : '';
                shopId = checkNotEmpty(reqData.shop_id) ? app.locals.decrypt(reqData.shop_id) : '';
                url_type = checkNotEmpty(reqData.type) ? reqData.type : '';

                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                where = edit_id != '' ? ' AND branchadmin_id!="' + edit_id + '"' : '';
                if (baName == '') {
                    res = {
                        status: 'error',
                        message: 'Branch Admin Name Empty'
                    };
                    callback(null, res);
                } else {
                    var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                    sql.fetchTable('users', 'user_id', '(user_email="' + baUsername + '" OR user_phone="' + baUsername + '")', function(err_u, resul_u) {
                        if (resul_u == '') {
                            sql.fetchTable('shop', 'shop_id', 'shop_username="' + baUsername + '"', function(err_s, resul_s) {
                                if (resul_s == '') {
                                    sql.fetchTable('branchadmin', 'branchadmin_id', 'ba_username="' + baUsername + '"' + where, function(err1, resul) {
                                        if (resul == '') {
                                            if (edit_id) {
                                                sql.runQuery('UPDATE branchadmin SET ba_name="' + baName + '",email="' + email + '",phone="' + phone + '",ba_password="' + baPassword + '",ba_mdpassword="' + bamdPassword + '",ba_username="' + baUsername + '",branch_id="' + branchId + '" WHERE branchadmin_id="' + edit_id + '"', function(err_up, result_up) {
                                                    if (err_up) {
                                                        console.log(err_up);
                                                        res = {
                                                            status: 'error',
                                                            message: 'Server Error'
                                                        };
                                                        callback(null, res);
                                                    } else {

                                                        res = {
                                                            status: 'success',
                                                            message: 'successfully',
                                                            shopId: reqData.shop_id,
                                                            type: url_type
                                                        };
                                                        callback(null, res);
                                                    }
                                                });
                                            } else {
                                                sql.insertTable2('branchadmin', 'ba_name,email,phone,ba_password,ba_mdpassword,shop_id,branch_id,created_date,created_by,ba_username', '"' + baName + '","' + email + '","' + phone + '","' + baPassword + '","' + bamdPassword + '","' + shopId + '","' + branchId + '","' + createdDate + '","' + req.session.admin_id + '","' + baUsername + '"', function(err, result) {
                                                    if (err) {
                                                        res = {
                                                            status: 'error',
                                                            message: 'Server Error'
                                                        };
                                                        callback(null, res);
                                                    } else {
                                                        //var pad = "000000";
                                                        //var rc = (pad+result).slice(-pad.length);
                                                        res = {
                                                            status: 'success',
                                                            message: 'successfully',
                                                            shopId: reqData.shop_id,
                                                            type: url_type
                                                        };
                                                        callback(null, res);
                                                    }
                                                });
                                            }
                                        } else {
                                            res = {
                                                status: 'already',
                                                message: 'Username already exits'
                                            };
                                            callback(null, res);
                                        }
                                    });
                                } else {
                                    res = {
                                        status: 'already',
                                        message: 'User Name already exits'
                                    };
                                    callback(null, res);
                                }
                            });
                        } else {
                            res = {
                                status: 'already',
                                message: 'User Name already exits'
                            };
                            callback(null, res);
                        }
                    });
                }
                break;
            case '52':
                /*** Branch Staff Submit ***/

                branchId = checkNotEmpty(reqData.branch_id) ? reqData.branch_id : '';
                staffName = checkNotEmpty(reqData.staff_name) ? reqData.staff_name : '';
                staffRole = checkNotEmpty(reqData.staff_name) ? reqData.staff_role : '';
                email = checkNotEmpty(reqData.email) ? reqData.email : '';
                phone = checkNotEmpty(reqData.phone) ? reqData.phone : '';
                staffPassword = checkNotEmpty(reqData.password) ? reqData.password : '';
                //staffUsername=checkNotEmpty(reqData.username)?reqData.username:'';
                staffmdPassword = checkNotEmpty(reqData.password) ? md5(reqData.password) : '';
                shopId = checkNotEmpty(reqData.shop_id) ? app.locals.decrypt(reqData.shop_id) : '';
                url_type = checkNotEmpty(reqData.type) ? reqData.type : '';

                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                console.log("edit_id" + edit_id);
                where = edit_id != '' ? ' AND staff_id!="' + edit_id + '"' : '';

                if (staffName == '') {
                    res = {
                        status: 'error',
                        message: ' Staff Name Empty'
                    };
                    callback(null, res);
                } else {
                    var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                    if (edit_id) {
                        console.log("editdata");
                        sql.runQuery('UPDATE staff SET staff_name="' + staffName + '",email="' + email + '",phone="' + phone + '",staff_role="' + staffRole + '",branch_id="' + branchId + '",staff_password="' + staffPassword + '",staff_mdpassword="' + staffmdPassword + '",type="' + url_type + '" WHERE staff_id="' + edit_id + '"', function(err_up, result_up) {
                            if (err_up) {
                                console.log(err_up);
                                res = {
                                    status: 'error',
                                    message: 'Invalid User'
                                };
                                callback(null, res);
                            } else {

                                res = {
                                    status: 'success',
                                    message: 'successfully',
                                    shopId: reqData.shop_id,
                                    type: url_type
                                };
                                callback(null, res);
                            }
                        });
                    } else {
                        sql.fetchTable('users', 'user_id', 'user_phone="' + phone + '"', function(err_u, resul_u) {
                            if (resul_u == '') {
                                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                                sql.fetchTable('shop', 'shop_id', 'phone="' + phone + '"', function(err_s, resul_s) {
                                    if (resul_s == '') {
                                        sql.fetchTable('branchadmin', 'branchadmin_id', 'phone="' + phone + '"', function(err_ba, resul_ba) {
                                            if (resul_ba == '') {
                                                sql.fetchTable('staff', 'staff_id', 'phone="' + phone + '"' + where, function(err1, resul) {
                                                    if (resul == '') {
                                                        console.log("newdata");
                                                        var pad = "000000";
                                                        //var uName = rc;
                                                        var rc = (pad + branchId).slice(-pad.length);
                                                        const random = uniqueRandom(100000, 900000);
                                                        var uName = random();
                                                        //console.log(uName);
                                                        var randN = randomstring.generate({
                                                            length: 5,
                                                            charset: 'numeric'
                                                        });
                                                        // staffPassword =randN;
                                                        // staffmdPassword =md5(staffPassword);
                                                        sql.insertTable2('staff', 'staff_name,email,phone,staff_password,staff_mdpassword,shop_id,branch_id,created_date,created_by,staff_username,staff_role,type', '"' + staffName + '","' + email + '","' + phone + '","' + staffPassword + '","' + staffmdPassword + '","' + shopId + '","' + branchId + '","' + createdDate + '","' + branchId + '","' + uName + '","' + staffRole + '","' + url_type + '"', function(err, result) {
                                                            if (err) {
                                                                res = {
                                                                    status: 'error',
                                                                    message: 'Invalid User'
                                                                };
                                                                callback(null, res);
                                                            } else {
                                                                // const template = 'views/email-templates/newuser.ejs';
                                                                // let templateData = {
                                                                //  email: email,
                                                                //  uname: uName,
                                                                //  password: staffPassword,
                                                                // };
                                                                // ejs.renderFile(template, templateData, (err, html) => {
                                                                //  if (err) console.log(err); 
                                                                //  var mailOptions = {
                                                                //                  from: 'testermodule@gmail.com',
                                                                //                  to: email,
                                                                //                  subject: 'Herzlich willkommen auf Qlic',
                                                                //                  generateTextFromHtml : true,
                                                                //                  html: html
                                                                //              };
                                                                //  console.log(mailOptions);
                                                                //  transporter.sendMail(mailOptions, function(error, info){
                                                                //      if (error) {
                                                                //          console.log(error);
                                                                //      } else {
                                                                //          console.log('Email sent: ' + info.response);
                                                                //          res={status:'success',message:'successfully',shopId:reqData.shop_id,type:url_type};
                                                                //          callback(null,res);
                                                                //      }
                                                                //  });
                                                                // });
                                                                res = {
                                                                    status: 'success',
                                                                    message: 'successfully',
                                                                    shopId: reqData.shop_id,
                                                                    type: url_type
                                                                };
                                                                callback(null, res);
                                                            }

                                                        });

                                                    } else {
                                                        res = {
                                                            status: 'already',
                                                            message: 'Phone Number Already Exits'
                                                        };
                                                        callback(null, res);
                                                    }
                                                });
                                            } else {
                                                res = {
                                                    status: 'already',
                                                    message: 'Phone Number Already Exits'
                                                };
                                                callback(null, res);
                                            }
                                        });
                                    } else {
                                        res = {
                                            status: 'already',
                                            message: 'Phone Number Already Exits'
                                        };
                                        callback(null, res);
                                    }
                                });
                            } else {
                                res = {
                                    status: 'already',
                                    message: 'Phone Number Already Exits'
                                };
                                callback(null, res);
                            }
                        });
                    }

                }
                break;
            case '53':
                /*** Shop Admin Login ***/
                username = checkNotEmpty(reqData.username) ? reqData.username : '';
                password = checkNotEmpty(reqData.password) ? md5(reqData.password) : '';
                sql.fetchTable('shop', 'shop_id,email,shop_name', 'shop_username="' + username + '" AND shop_mdpassword="' + password + '"', function(err, result) {
                    if (err)
                        callback(err, null);
                    else {
                        if (result == "") {
                            var ret = {
                                status: 'error',
                                'message': 'Invalid Username/ Password '
                            };
                            callback(err, ret);
                        } else {
                            /*** Setting Session ***/
                            req.session.shopadmin_id = result[0].shop_id;
                            req.session.shopadmin_email = result[0].email;
                            req.session.shop_name = result[0].shop_name;
                            var ret = {
                                status: 'success',
                                shop: result[0]
                            };
                            callback(null, ret);
                        }
                    }
                });
                break;
            case '54':
                /*** change Password  Shop***/
                oldPassword = checkNotEmpty(reqData.oldPassword) ? md5(reqData.oldPassword) : '';
                newPassword = checkNotEmpty(reqData.newPassword) ? md5(reqData.newPassword) : '';
                if (oldPassword == '' || newPassword == '') {
                    res = {
                        status: 'empty',
                        message: 'Enter the all field'
                    };
                    callback(null, res);
                } else {
                    sql.fetchTable('shop', 'shop_id', 'shop_mdpassword="' + oldPassword + '" AND shop_id="' + req.session.shopadmin_id + '"', function(err1, resul, fields) {
                        if (resul != '') {
                            sql.runQuery('UPDATE shop SET shop_mdpassword="' + newPassword + '",shop_password="' + reqData.newPassword + '" WHERE shop_id="' + req.session.shopadmin_id + '"', function(err_u, result_u) {
                                if (err_u) {
                                    res = {
                                        status: 'error',
                                        message: 'aktuelles Passwort falsch'
                                    };
                                    callback(null, res);
                                } else {
                                    res = {
                                        status: 'success',
                                        message: 'Erfolgreich aktualisieren'
                                    };
                                    callback(null, res);
                                }
                            });
                        } else {
                            res = {
                                status: 'invalid',
                                message: 'aktuelles Passwort falsch',
                                test: resul
                            };
                            callback(null, res);
                        }
                    });
                }
                break;
            case '55':
                /*** change Password  Branch Admin***/
                oldPassword = checkNotEmpty(reqData.oldPassword) ? md5(reqData.oldPassword) : '';
                newPassword = checkNotEmpty(reqData.newPassword) ? md5(reqData.newPassword) : '';
                if (oldPassword == '' || newPassword == '') {
                    res = {
                        status: 'empty',
                        message: 'Enter the all field'
                    };
                    callback(null, res);
                } else {
                    sql.fetchTable('branchadmin', 'branchadmin_id', 'ba_mdpassword="' + oldPassword + '" AND branchadmin_id="' + req.session.branchadmin_id + '"', function(err1, resul, fields) {
                        if (resul != '') {
                            sql.runQuery('UPDATE branchadmin SET ba_mdpassword="' + newPassword + '",ba_password="' + reqData.newPassword + '" WHERE branchadmin_id="' + req.session.branchadmin_id + '"', function(err_u, result_u) {
                                if (err_u) {
                                    res = {
                                        status: 'error',
                                        message: 'aktuelles Passwort falsch'
                                    };
                                    callback(null, res);
                                } else {
                                    res = {
                                        status: 'success',
                                        message: 'Erfolgreich aktualisieren'
                                    };
                                    callback(null, res);
                                }
                            });
                        } else {
                            res = {
                                status: 'invalid',
                                message: 'aktuelles Passwort falsch',
                                test: resul
                            };
                            callback(null, res);
                        }
                    });
                }
                break;
            case '56':
                /*** Edit Shop Settings ***/
                contactPerson = checkNotEmpty(reqData.contact_person) ? reqData.contact_person : '';
                shopName = checkNotEmpty(reqData.shop_name) ? reqData.shop_name : '';
                email = checkNotEmpty(reqData.email) ? reqData.email : '';
                phone = checkNotEmpty(reqData.phone) ? reqData.phone : '';
                description = checkNotEmpty(reqData.description) ? reqData.description : '';
                innerbanner = checkNotEmpty(reqData.innerbanner) ? reqData.innerbanner : '';
                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                var img = dateFormat(nDate, "yyyy-mm-ddHH:mm:ss");
                var strFirstThree = shopName.substring(0, 3);
                var fileNameTime = (new Date().getTime());
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                where = edit_id != '' ? ' AND shop_id!="' + edit_id + '"' : '';

                sql.fetchTable('shop', 'shop_id', '(email="' + email + '" OR phone="' + phone + '") AND shop_id!="' + edit_id + '"', function(err1, resul) {
                    if (resul == '') {
                        if (edit_id) {
                            sql.runQuery('UPDATE shop SET contact_person="' + contactPerson + '",email="' + email + '",phone="' + phone + '",description="' + description + '" WHERE shop_id="' + edit_id + '"', function(err_up, result_up) {
                                if (err_up) {
                                    res = {
                                        status: 'error',
                                        message: 'Invalid User'
                                    };
                                    callback(null, res);
                                } else {
                                    sql.fetchTable('shop', 'shop_id', 'shop_id!="' + edit_id + '"', function(err1, resull) {
                                        if (resull != '') {
                                            //shopAlias=shopAlias+'_'+edit_id;
                                            //console.log('welcome'+resull);
                                        }
                                        if (req.files) {
                                            /** Image upload **/
                                            //console.log(reqData.old_shop_image);
                                            let shopImage = req.files.shop_image;
                                            var oldFileName = shopImage.name;
                                            var fileExtension = oldFileName.replace(/^.*\./, '');
                                            var newFileName = strFirstThree + fileNameTime + '.' + fileExtension;
                                            //console.log(newFileName);
                                            if (checkNotEmpty(shopImage)) {
                                                /*** Use the mv() method to place the file somewhere on your server **/
                                                shopImage.mv(imageurlink_shop + newFileName, function(err, result) {
                                                    if (err) {
                                                        res['image_upload_status'] = 'upload failed';
                                                    } else {
                                                        if (reqData.old_shop_image != "") {
                                                            fs.unlink(imageurlink_shop + reqData.old_shop_image);
                                                        }
                                                        sql.runQuery('UPDATE shop SET shop_image="' + newFileName + '" WHERE shop_id="' + edit_id + '"', function(err_up_i, result_up) {
                                                            if (err_up_i) {
                                                                res['image_upload_status'] = 'upload failed';
                                                            } else {
                                                                thumb({
                                                                    source: imageurlink_shop + newFileName, // could be a filename: dest/path/image.jpg
                                                                    destination: 'assets/images/thumbnail',
                                                                    concurrency: 4,
                                                                    //width: 200,
                                                                    //height: 200,
                                                                }, function(files, err, stdout, stderr) {
                                                                    console.log('All done!');
                                                                    var thumFileName = strFirstThree + fileNameTime + '_thumb.' + fileExtension;
                                                                    sql.runQuery('UPDATE shop SET innerbanner="' + thumFileName + '" WHERE shop_id="' + edit_id + '"', function(err_th_i, result_th) {
                                                                        if (err_th_i) {
                                                                            console.log(err_th_i);
                                                                        } else {
                                                                            if (innerbanner != "") {
                                                                                console.log("testv");
                                                                                fs.unlink(imageurlink_shopthumbnail + reqData.innerbanner);
                                                                            }
                                                                        }

                                                                    });

                                                                });
                                                                res['image_upload_status'] = 'upload success';
                                                            }
                                                        });
                                                    }
                                                });
                                            } else {
                                                res['image_upload_status'] = 'empty';
                                            }
                                        }
                                    });
                                    res = {
                                        status: 'success',
                                        message: 'successfully'
                                    };
                                    callback(null, res);
                                }
                            });
                        }
                    } else {
                        res = {
                            status: 'already',
                            message: 'Email / Phone already exits'
                        };
                        callback(null, res);
                    }
                });
                break;
            case '57':
                /*** Login ***/
                var bbshopid = "";
                var sname = "";
                var simage = "";
                username = checkNotEmpty(reqData.username) ? reqData.username : '';
                password = checkNotEmpty(reqData.password) ? md5(reqData.password) : '';
                sql.fetchTable('shop', 'shop_id,email,shop_name,shop_image,innerbanner', 'shop_username="' + username + '" AND shop_mdpassword="' + password + '"', function(err_s, result_s) {
                    sql.fetchTable('users', 'user_id,user_firstname,user_lastname,user_email', '(user_phone="' + username + '" OR user_email="' + username + '") AND password="' + password + '"', function(err_u, result_u) {
                        sql.fetchTable('branchadmin', 'branchadmin_id,ba_name,email,shop_id,branch_id', '(ba_username="' + username + '") AND ba_mdpassword="' + password + '"', function(err_b, result_b) {
                            if (err_s) {
                                callback(err_s, null);
                            } else if (err_u) {
                                callback(err_u, null);
                            } else if (err_b) {
                                callback(err_b, null);
                            } else {
                                if (result_s == "" && result_u == "" && result_b == "") {
                                    var ret = {
                                        status: 'error',
                                        'message': 'Invalid Username/ Password '
                                    };
                                    callback(null, ret);
                                }
                                /*** Setting Session ***/
                                else if (result_s != "" && result_u == "" && result_b == "") {

                                    //var ret={status:'success',shop:result_s[0],'check_type':"shop"};
                                    // callback(null,ret);
                                    sql.runQuery('UPDATE shop SET status="1" WHERE shop_username="' + username + '"', function(err_u, result_u) {
                                        if (err_u) {
                                            console.log(err_u);
                                            var ret = {
                                                status: 'error',
                                                'message': 'Invalid Username/ Password '
                                            };
                                            callback(null, ret);

                                        } else {
                                            req.session.shopadmin_id = result_s[0].shop_id;
                                            req.session.shopadmin_email = result_s[0].email;
                                            req.session.shop_name = result_s[0].shop_name;
                                            req.session.shop_image = result_s[0].innerbanner;
                                            req.session.check_type = "shop";
                                            var ret = {
                                                status: 'success',
                                                shop: result_s[0],
                                                'check_type': "shop"
                                            };
                                            callback(null, ret);
                                        }
                                    });
                                } else if (result_s == "" && result_u != "" && result_b == "") {
                                    req.session.admin_id = result_u[0].user_id;
                                    req.session.admin_email = result_u[0].user_email;
                                    req.session.admin_name = result_u[0].user_firstname + ' ' + result_u[0].user_lastname;
                                    req.session.check_type = "admin";
                                    var ret = {
                                        status: 'success',
                                        user: result_u[0],
                                        'check_type': "admin"
                                    };
                                    callback(null, ret);
                                } else if (result_s == "" && result_u == "" && result_b != "") {
                                    bbshopid = result_b[0].shop_id;
                                    console.log(bbshopid);
                                    console.log("ttttttcc");

                                    sql.fetchTable('shop', 'shop_id,email,shop_name,shop_image,innerbanner', 'shop_id="' + result_b[0].shop_id + '" ', function(err_sc, result_sc) {
                                        console.log(bbshopid);
                                        console.log("tttttt");
                                        if (result_sc != "") {
                                            console.log(result_sc);
                                            // console.log(result_sc[0].shop_name);
                                            // console.log(result_sc[0].shop_image);
                                            sname = result_sc[0].shop_name;
                                            simage = result_sc[0].innerbanner;
                                            console.log(sname);
                                            console.log(simage);
                                            req.session.bshop_name = sname;
                                            req.session.bshop_image = simage;
                                            req.session.bshop_id = result_b[0].shop_id;
                                            req.session.branchadmin_id = result_b[0].branchadmin_id;
                                            req.session.branchadmin_email = result_b[0].email;
                                            req.session.branchadmin_name = result_b[0].ba_name;
                                            req.session.bshop_id = result_b[0].shop_id;
                                            req.session.bbranch_id = result_b[0].branch_id;
                                            req.session.check_type = "branch";
                                            var ret = {
                                                status: 'success',
                                                user: result_b[0],
                                                'check_type': "branch"
                                            };
                                            callback(null, ret);
                                        }


                                    });


                                }
                            }
                        });
                    });
                });
                break;
            case '58':
                /*** Branch Admin Setings Submit ***/
                baName = checkNotEmpty(reqData.ba_name) ? reqData.ba_name : '';
                email = checkNotEmpty(reqData.email) ? reqData.email : '';
                phone = checkNotEmpty(reqData.phone) ? reqData.phone : '';
                if (baName == '') {
                    res = {
                        status: 'error',
                        message: 'Enter the all field'
                    };
                    callback(null, res);
                } else {
                    var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                    sql.fetchTable('branchadmin', 'branchadmin_id', '(email="' + email + '" OR phone="' + phone + '") AND branchadmin_id!="' + req.session.branchadmin_id + '"', function(err1, resul) {
                        if (resul == '') {
                            sql.runQuery('UPDATE branchadmin SET ba_name="' + baName + '",email="' + email + '",phone="' + phone + '" WHERE branchadmin_id="' + req.session.branchadmin_id + '"', function(err_u, result_u) {
                                if (err_u) {
                                    res = {
                                        status: 'error',
                                        message: 'Invalid User'
                                    };
                                    callback(null, res);
                                } else {
                                    res = {
                                        status: 'success',
                                        message: 'Successfully Update'
                                    };
                                    callback(null, res);
                                }
                            });
                        } else {
                            res = {
                                status: 'already',
                                message: ' Email or  Phone Number already exits'
                            };
                            callback(null, res);
                        }
                    });
                }
                break;
            case '59':
                /*** Deal Details Submit ***/
                title = checkNotEmpty(reqData.title) ? reqData.title : '';
                branch_id = checkNotEmpty(reqData.branch_id) ? reqData.branch_id : '';
                price = checkNotEmpty(reqData.price) ? reqData.price : '';
                offer_price = checkNotEmpty(reqData.offer_price) ? reqData.offer_price : '';
                expirydate = checkNotEmpty(reqData.expirydate) ? reqData.expirydate : '';
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                deal_thumb = checkNotEmpty(reqData.deal_thumb) ? reqData.deal_thumb : '';
                dealcategory_id = checkNotEmpty(reqData.dealcategory_id) ? reqData.dealcategory_id : '';
                description = checkNotEmpty(reqData.description) ? reqData.description : '';
                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                var today = dateFormat(nDate, "yyyy-mm-dd");
                var img = dateFormat(nDate, "yyyy-mm-ddHH:mm:ss");
                var strFirstThree = randomstring.generate(5);;
                var fileNameTime = (new Date().getTime());
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                var res = expirydate.split("-");
                var edate = res[2] + '-' + res[1] + '-' + res[0];
                var nexpirydate = res[0] + '-' + res[1] + '-' + res[2];
                var expiryday = noofdays(today, expirydate);
                console.log("expirydate" + expirydate);
                console.log("edate" + edate);
                console.log("nexpirydate" + nexpirydate);
                expiryval = (toTimestamp(edate + ' ' + '00:00:00'));
                console.log(expiryval);
                var percentage = ((parseInt(offer_price) / parseInt(price)) * 100);
                //percentage=checkNotEmpty(reqData.percentage)?reqData.percentage:'';
                console.log("percentage" + percentage);
                console.log("percentage" + branch_id);
                if (edit_id) {
                    sql.runQuery('UPDATE deal SET expiry="' + expiryval + '",title="' + title + '",branch_id="' + branch_id + '",price="' + price + '",offer_price="' + offer_price + '",percentage="' + percentage + '",expirydate="' + edate + '",expiryday="' + expiryday + '",datetime="' + createdDate + '",shop_id="' + req.session.shopadmin_id + '",dealcategory_id="' + dealcategory_id + '" ,date="' + expirydate + '",description="' + description + '",branch_id="' + branch_id + '" WHERE deal_id="' + edit_id + '"', function(err_up, result_up) {
                        if (err_up) {
                            console.log(err_up);
                            res = {
                                status: 'error',
                                message: 'Invalid User'
                            };
                            callback(null, res);
                        } else {
                            if (req.files) {
                                /** Image upload **/
                                let dealImage = req.files.deal_image;
                                var oldFileName = dealImage.name;
                                var fileExtension = oldFileName.replace(/^.*\./, '');
                                var newFileName = strFirstThree + fileNameTime + '.' + fileExtension;
                                if (checkNotEmpty(dealImage)) {
                                    dealImage.mv(imageurlink_deal + newFileName, function(err, result) {
                                        if (err) {
                                            res['image_upload_status'] = 'upload failed';
                                        } else {
                                            if (reqData.old_deal_image != "") {
                                                fs.unlink(imageurlink_deal + reqData.old_deal_image);
                                            }
                                            sql.runQuery('UPDATE deal SET deal_image="' + newFileName + '" WHERE deal_id="' + edit_id + '"', function(err_up_i, result_up) {
                                                if (err_up_i) {
                                                    res['image_upload_status'] = 'upload failed';
                                                } else {
                                                    thumb({
                                                        source: imageurlink_deal + newFileName, // could be a filename: dest/path/image.jpg
                                                        destination: 'assets/images/dealthumbnail',
                                                        concurrency: 4,
                                                        //width: 200,
                                                        //height: 200,
                                                    }, function(files, err, stdout, stderr) {
                                                        console.log('All done!');
                                                        var thumFileName = strFirstThree + fileNameTime + '_thumb.' + fileExtension;
                                                        sql.runQuery('UPDATE deal SET deal_thumb="' + thumFileName + '" WHERE deal_id="' + edit_id + '"', function(err_th_i, result_th) {
                                                            if (err_th_i) {
                                                                console.log(err_th_i);
                                                            } else {
                                                                if (deal_thumb != "") {
                                                                    console.log("testv");
                                                                    fs.unlink(imageurlink_dealthumbnail + reqData.deal_thumb);
                                                                }
                                                            }

                                                        });

                                                    });
                                                    res['image_upload_status'] = 'upload success';
                                                }
                                            });

                                        }
                                    });
                                } else {
                                    res['image_upload_status'] = 'empty';
                                }
                            }
                            res = {
                                status: 'success',
                                message: 'successfully'
                            };
                            callback(null, res);
                        }
                    });
                } else {
                    var company_name = "";
                    sql.runQuery('SELECT shop_name FROM shop WHERE shop_id="' + req.session.shopadmin_id + '"', function(err_shop, result_shop) {
                        if (err_shop)
                            console.log(err_shop);
                        else {
                            console.log(result_shop);
                            company_name = result_shop[0].shop_name;
                        }
                    });
                    sql.insertTable2('deal', 'expiry,title,branch_id,price,offer_price,expirydate,percentage,datetime,shop_id,expiryday,dealcategory_id,date,company_name,description', '"' + expiryval + '","' + title + '","' + branch_id + '","' + price + '","' + offer_price + '","' + edate + '","' + percentage + '","' + createdDate + '","' + req.session.shopadmin_id + '","' + expiryday + '","' + dealcategory_id + '","' + expirydate + '","' + company_name + '","' + description + '"', function(err, result) {
                        if (err) {
                            console.log(err);
                            res = {
                                status: 'error',
                                message: 'Invalid'
                            };
                            callback(null, res);
                        } else {
                            var dealId = result;
                            /** Image upload **/
                            let dealImage = req.files.deal_image;
                            var oldFileName = dealImage.name;
                            var fileExtension = oldFileName.replace(/^.*\./, '');
                            var newFileName = strFirstThree + fileNameTime + '.' + fileExtension;
                            //console.log(newFileName);
                            if (checkNotEmpty(dealImage)) {
                                /*** Use the mv() method to place the file somewhere on your server **/
                                dealImage.mv(imageurlink_deal + newFileName, function(err, result) {
                                    if (err) {
                                        res['image_upload_status'] = 'upload failed';
                                    } else {
                                        if (reqData.old_deal_image != "") {
                                            fs.unlink(imageurlink_deal + reqData.old_deal_image);
                                        }
                                        sql.runQuery('UPDATE deal SET deal_image="' + newFileName + '" WHERE deal_id="' + dealId + '"', function(err_up_i, result_up) {
                                            if (err_up_i) {
                                                res['image_upload_status'] = 'upload failed';
                                            } else {
                                                thumb({
                                                    source: imageurlink_deal + newFileName, // could be a filename: dest/path/image.jpg
                                                    destination: 'assets/images/dealthumbnail',
                                                    concurrency: 4,
                                                    //width: 200,
                                                    //height: 200,
                                                }, function(files, err, stdout, stderr) {
                                                    console.log('All done!');
                                                    var thumFileName = strFirstThree + fileNameTime + '_thumb.' + fileExtension;
                                                    sql.runQuery('UPDATE deal SET deal_thumb="' + thumFileName + '" WHERE deal_id="' + dealId + '"', function(err_th_i, result_th) {
                                                        if (err_th_i) {
                                                            console.log(err_th_i);
                                                        } else {
                                                            if (deal_thumb != "") {
                                                                console.log("testv");
                                                                fs.unlink(imageurlink_dealthumbnail + reqData.deal_thumb);
                                                            }
                                                        }

                                                    });

                                                });
                                                res['image_upload_status'] = 'upload success';
                                            }
                                        });
                                    }
                                });
                            } else {
                                res['image_upload_status'] = 'empty';
                            }

                            res = {
                                status: 'success',
                                message: 'successfully'
                            };
                            callback(null, res);
                        }
                    });
                }


                break;
            case '60':
                /** Delete Record Deal**/
                //var reId = "";
                record_id = checkNotEmpty(app.locals.decrypt(reqData.record_id)) ? app.locals.decrypt(reqData.record_id) : '';
                /* shop_id=checkNotEmpty(app.locals.decrypt(reqData.shop))?app.locals.decrypt(reqData.shop):'';
                branch_id=checkNotEmpty(app.locals.decrypt(reqData.branch))?app.locals.decrypt(reqData.branch):''; */
                record_table = checkNotEmpty(reqData.record_table) ? reqData.record_table : '';
                record_column = checkNotEmpty(reqData.record_column) ? reqData.record_column : '';
                fileName = checkNotEmpty(reqData.fileName) ? reqData.fileName : '';
                filethumb = checkNotEmpty(reqData.filethumb) ? reqData.filethumb : '';
                console.log(filethumb);
                console.log(fileName);
                sql.runQuery('DELETE FROM ' + record_table + ' WHERE ' + record_column + '="' + record_id + '"', function(err, result) {
                    if (err) {
                        callback(err, null);
                    } else {
                        if (fileName != '') {
                            if (record_table == 'deal') {
                                fs.unlink(imageurlink_deal + fileName);
                                fs.unlink(imageurlink_dealthumbnail + filethumb);
                            }

                        }
                        //console.log(err);
                        res = {
                            status: 'success',
                            message: 'successfully'
                        };
                        callback(err, res);
                    }
                });
                break;
            case '61':
                /*** Deal branch Details Submit ***/
                title = checkNotEmpty(reqData.title) ? reqData.title : '';
                company_name = checkNotEmpty(reqData.company_name) ? reqData.company_name : '';
                price = checkNotEmpty(reqData.price) ? reqData.price : '';
                offer_price = checkNotEmpty(reqData.offer_price) ? reqData.offer_price : '';
                percentage = checkNotEmpty(reqData.percentage) ? reqData.percentage : '';
                expirydate = checkNotEmpty(reqData.expirydate) ? reqData.expirydate : '';
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                deal_thumb = checkNotEmpty(reqData.deal_thumb) ? reqData.deal_thumb : '';
                dealcategory_id = checkNotEmpty(reqData.dealcategory_id) ? reqData.dealcategory_id : '';
                //shop_id=checkNotEmpty(app.locals.decrypt(reqData.shop_id))?app.locals.decrypt(reqData.shop_id):'';
                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                var today = dateFormat(nDate, "yyyy-mm-dd");
                var img = dateFormat(nDate, "yyyy-mm-ddHH:mm:ss");
                var strFirstThree = company_name.substring(0, 3);
                var fileNameTime = (new Date().getTime());
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                var res = expirydate.split("-");
                var edate = res[2] + '-' + res[1] + '-' + res[0];
                var expiryday = noofdays(today, edate);
                //console.log(expiryday);
                if (company_name == '') {
                    res = {
                        status: 'error',
                        message: 'Enter All field'
                    };
                    callback(null, res);
                } else {
                    sql.runQuery('SELECT branchadmin.shop_id FROM branchadmin WHERE branchadmin.branchadmin_id="' + req.session.branchadmin_id + '"', function(err_shop, result_shop) {
                        if (err_shop) {
                            console.log(err_shop);
                            res = {
                                status: 'error',
                                message: 'Invalid User'
                            };
                            callback(null, res);
                        } else {
                            var shopId = result_shop[0].shop_id;
                            if (edit_id) {
                                sql.runQuery('UPDATE deal SET title="' + title + '",company_name="' + company_name + '",price="' + price + '",offer_price="' + offer_price + '",percentage="' + percentage + '",expirydate="' + expirydate + '",expiryday="' + expiryday + '",datetime="' + createdDate + '",branch_id="' + req.session.branchadmin_id + '",shop_id="' + shopId + '",dealcategory_id="' + dealcategory_id + '" WHERE deal_id="' + edit_id + '"', function(err_up, result_up) {
                                    if (err_up) {
                                        console.log(err_up);
                                        res = {
                                            status: 'error',
                                            message: 'Invalid User'
                                        };
                                        callback(null, res);
                                    } else {
                                        if (req.files) {
                                            /** Image upload **/
                                            let dealImage = req.files.deal_image;
                                            var oldFileName = dealImage.name;
                                            var fileExtension = oldFileName.replace(/^.*\./, '');
                                            var newFileName = strFirstThree + fileNameTime + '.' + fileExtension;
                                            if (checkNotEmpty(dealImage)) {
                                                dealImage.mv(imageurlink_deal + newFileName, function(err, result) {
                                                    if (err) {
                                                        res['image_upload_status'] = 'upload failed';
                                                    } else {
                                                        if (reqData.old_deal_image != "") {
                                                            fs.unlink(imageurlink_deal + reqData.old_deal_image);
                                                        }
                                                        sql.runQuery('UPDATE deal SET deal_image="' + newFileName + '" WHERE deal_id="' + edit_id + '"', function(err_up_i, result_up) {
                                                            if (err_up_i) {
                                                                res['image_upload_status'] = 'upload failed';
                                                            } else {
                                                                thumb({
                                                                    source: imageurlink_deal + newFileName, // could be a filename: dest/path/image.jpg
                                                                    destination: 'assets/images/dealthumbnail',
                                                                    concurrency: 4,
                                                                    //width: 200,
                                                                    //height: 200,
                                                                }, function(files, err, stdout, stderr) {
                                                                    console.log('All done!');
                                                                    var thumFileName = strFirstThree + fileNameTime + '_thumb.' + fileExtension;
                                                                    sql.runQuery('UPDATE deal SET deal_thumb="' + thumFileName + '" WHERE deal_id="' + edit_id + '"', function(err_th_i, result_th) {
                                                                        if (err_th_i) {
                                                                            console.log(err_th_i);
                                                                        } else {
                                                                            if (deal_thumb != "") {
                                                                                console.log("testv");
                                                                                fs.unlink(imageurlink_dealthumbnail + reqData.deal_thumb);
                                                                            }
                                                                        }

                                                                    });

                                                                });
                                                                res['image_upload_status'] = 'upload success';
                                                            }
                                                        });

                                                    }
                                                });
                                            } else {
                                                res['image_upload_status'] = 'empty';
                                            }
                                        }
                                        res = {
                                            status: 'success',
                                            message: 'successfully'
                                        };
                                        callback(null, res);
                                    }
                                });
                            } else {
                                sql.insertTable2('deal', 'title,company_name,price,offer_price,expirydate,percentage,datetime,branch_id,expiryday,shop_id,dealcategory_id', '"' + title + '","' + company_name + '","' + price + '","' + offer_price + '","' + expirydate + '","' + percentage + '","' + createdDate + '","' + req.session.branchadmin_id + '","' + expiryday + '","' + shopId + '","' + dealcategory_id + '"', function(err, result) {
                                    if (err) {
                                        console.log(err);
                                        res = {
                                            status: 'error',
                                            message: 'Invalid'
                                        };
                                        callback(null, res);
                                    } else {
                                        var dealId = result;
                                        /** Image upload **/
                                        let dealImage = req.files.deal_image;
                                        var oldFileName = dealImage.name;
                                        var fileExtension = oldFileName.replace(/^.*\./, '');
                                        var newFileName = strFirstThree + fileNameTime + '.' + fileExtension;
                                        //console.log(newFileName);
                                        if (checkNotEmpty(dealImage)) {
                                            /*** Use the mv() method to place the file somewhere on your server **/
                                            dealImage.mv(imageurlink_deal + newFileName, function(err, result) {
                                                if (err) {
                                                    res['image_upload_status'] = 'upload failed';
                                                } else {
                                                    if (reqData.old_deal_image != "") {
                                                        fs.unlink(imageurlink_deal + reqData.old_deal_image);
                                                    }
                                                    sql.runQuery('UPDATE deal SET deal_image="' + newFileName + '" WHERE deal_id="' + dealId + '"', function(err_up_i, result_up) {
                                                        if (err_up_i) {
                                                            res['image_upload_status'] = 'upload failed';
                                                        } else {
                                                            thumb({
                                                                source: imageurlink_deal + newFileName, // could be a filename: dest/path/image.jpg
                                                                destination: 'assets/images/dealthumbnail',
                                                                concurrency: 4,
                                                                //width: 200,
                                                                //height: 200,
                                                            }, function(files, err, stdout, stderr) {
                                                                console.log('All done!');
                                                                var thumFileName = strFirstThree + fileNameTime + '_thumb.' + fileExtension;
                                                                sql.runQuery('UPDATE deal SET deal_thumb="' + thumFileName + '" WHERE deal_id="' + dealId + '"', function(err_th_i, result_th) {
                                                                    if (err_th_i) {
                                                                        console.log(err_th_i);
                                                                    } else {
                                                                        if (deal_thumb != "") {
                                                                            console.log("testv");
                                                                            fs.unlink(imageurlink_dealthumbnail + reqData.deal_thumb);
                                                                        }
                                                                    }

                                                                });

                                                            });
                                                            res['image_upload_status'] = 'upload success';
                                                        }
                                                    });
                                                }
                                            });
                                        } else {
                                            res['image_upload_status'] = 'empty';
                                        }

                                        res = {
                                            status: 'success',
                                            message: 'successfully'
                                        };
                                        callback(null, res);
                                    }
                                });
                            }
                        }
                    });
                }
                break;
            case '62':
                /*** dealcategory Submit ***/
                cateName = checkNotEmpty(reqData.dealcate_name) ? reqData.dealcate_name : '';
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                where = edit_id != '' ? ' AND dealcategory_id!="' + edit_id + '"' : '';

                if (cateName == '') {
                    res = {
                        status: 'error',
                        message: 'Category Empty'
                    };
                    callback(null, res);
                } else {
                    var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                    sql.fetchTable('dealcategory', 'dealcategory_id', 'dealcategory_name="' + cateName + '"' + where, function(err1, resul) {
                        if (resul == '') {
                            if (edit_id) {
                                sql.runQuery('UPDATE dealcategory SET dealcategory_name="' + cateName + '" WHERE dealcategory_id="' + edit_id + '"', function(err_up, result_up) {
                                    if (err_up) {
                                        res = {
                                            status: 'error',
                                            message: 'error'
                                        };
                                        callback(null, res);
                                    } else {

                                        res = {
                                            status: 'success',
                                            message: 'successfully'
                                        };
                                        callback(null, res);
                                    }
                                });
                            } else {
                                sql.insertTable2('dealcategory', 'dealcategory_name,datetime', '"' + cateName + '","' + createdDate + '"', function(err, result) {
                                    if (err) {
                                        res = {
                                            status: 'error',
                                            message: 'error'
                                        };
                                        callback(null, res);
                                    } else {

                                        res = {
                                            status: 'success',
                                            message: 'successfully'
                                        };
                                        callback(null, res);
                                    }
                                });
                            }
                        } else {
                            res = {
                                status: 'already',
                                message: 'Category already exits'
                            };
                            callback(null, res);
                        }
                    });
                }
                break;
            case '63':
                /** Delete Record product**/
                //var reId = "";
                record_id = checkNotEmpty(reqData.id) ? reqData.id : '';
                productfilename = checkNotEmpty(reqData.pname) ? reqData.pname : '';
                console.log("productid" + record_id);
                sql.runQuery('DELETE FROM productfile WHERE id="' + record_id + '"', function(err_d, result_d) {
                    if (err_d) {
                        console.log(err_d);
                    } else {
                        fs.unlink(imageurlink_product + productfilename);
                        res = {
                            status: 'success',
                            message: 'successfully'
                        };
                        callback(err_d, res);
                    }
                });
                break;
            case '64':
                /** shop notification**/
                //var reId = "";
                //shop_id=checkNotEmpty(reqData.shop_id)?reqData.shop_id:'';
                title = checkNotEmpty(reqData.title) ? reqData.title : '';
                customer_type = checkNotEmpty(reqData.customer_type) ? reqData.customer_type : '';
                note_type = checkNotEmpty(reqData.note_type) ? reqData.note_type : '';
                //note_subject=checkNotEmpty(reqData.note_subject)?reqData.note_subject:'';
                message = checkNotEmpty(reqData.description) ? reqData.description : '';
                product_id = checkNotEmpty(reqData.product_id) ? reqData.product_id : '';
                deal_id = checkNotEmpty(reqData.deal_id) ? reqData.deal_id : '';
                product_point = checkNotEmpty(reqData.product_point) ? reqData.product_point : '';
                product_label = checkNotEmpty(reqData.product_label) ? reqData.product_label : '';
                //web_label=checkNotEmpty(reqData.web_label)?reqData.web_label:'';
                //web_link=checkNotEmpty(reqData.web_link)?reqData.web_link:'';
                website = checkNotEmpty(reqData.website) ? reqData.website : '';
                facebook = checkNotEmpty(reqData.facebook) ? reqData.facebook : '';
                linkedin = checkNotEmpty(reqData.linkedin) ? reqData.linkedin : '';
                instagram = checkNotEmpty(reqData.instagram) ? reqData.instagram : '';
                googleplus = checkNotEmpty(reqData.googleplus) ? reqData.googleplus : '';
                email = checkNotEmpty(reqData.email) ? reqData.email : '';
                phone = checkNotEmpty(reqData.email) ? reqData.phone : '';
                xing = checkNotEmpty(reqData.xing) ? reqData.xing : '';
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                uid = checkNotEmpty(reqData.u_id) ? reqData.u_id : '';
                imagecount = checkNotEmpty(reqData.imagecount) ? reqData.imagecount : '';
                where = edit_id != '' ? ' AND note_id!="' + edit_id + '"' : '';

                var newId = "";
                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                if (edit_id) {
                    sql.runQuery('UPDATE notification SET shop_id="' + shop_id + '",title="' + title + '",note_subject="' + note_subject + '",message="' + message + '",product_id="' + product_id + '",product_point="' + product_point + '",product_label="' + product_label + '",web_label="' + web_label + '",created_date="' + createdDate + '",facebook="' + facebook + '",linkedin="' + linkedin + '",instagram="' + instagram + '",googleplus="' + googleplus + '",web_link="' + web_link + '",website="' + website + '",email="' + email + '",xing="' + xing + '" WHERE note_id="' + edit_id + '"', function(err_up, result_up) {
                        if (err_up) {
                            console.log(err_up);
                            res = {
                                status: 'error',
                                message: 'error'
                            };
                            callback(null, res);
                        } else {

                            res = {
                                status: 'success',
                                message: 'successfully'
                            };
                            callback(null, res);
                        }
                    });
                } else {

                    sql.insertTable2('notification', 'shop_id,title,message,product_id,product_point,product_label,created_date,facebook,linkedin,instagram,googleplus,website,email,xing,phone,customer_type,note_type,deal_id', '"' + req.session.shopadmin_id + '","' + title + '","' + message + '","' + product_id + '","' + product_point + '","' + product_label + '","' + createdDate + '","' + facebook + '","' + linkedin + '","' + instagram + '","' + googleplus + '","' + website + '","' + email + '","' + xing + '","' + phone + '","' + customer_type + '","' + note_type + '","' + deal_id + '"', function(err, result) {
                        if (err) {
                            console.log(err);
                            res = {
                                status: 'error',
                                message: 'error'
                            };
                            callback(null, res);
                        } else {
                            console.log(result);
                            newId = result;
                            sql.runQuery('UPDATE notificationfile SET note_id="' + newId + '" WHERE uid="' + uid + '"', function(err_im, result_im) {
                                if (err_im) {
                                    res = {
                                        status: 'error',
                                        message: 'Invalid User'
                                    };
                                    callback(null, res);
                                } else {
                                    res = {
                                        status: 'success',
                                        message: 'successfully'
                                    };
                                    callback(null, res);

                                }
                            });


                        }
                        // res={status:'success',message:'successfully'};
                        // callback(null,res);
                    });
                }
                break;
            case '65':
                /** update data **/
                //var reId = "";
                var valcate = "";
                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                noteid = checkNotEmpty(app.locals.decrypt(reqData.noteid)) ? app.locals.decrypt(reqData.noteid) : '';
                console.log("noteid  " + noteid);
                var spquery = "";
                sql.runQuery('UPDATE notification SET approval="1" WHERE note_id="' + noteid + '"', function(err_up, result_up) {
                    if (err_up) {
                        console.log(err_up);
                        res = {
                            status: 'error',
                            message: 'error'
                        };
                        callback(null, res);
                    } else {
                        sql.runQuery('SELECT nd.customer_type,nd.title FROM notification nd WHERE nd.note_id="' + noteid + '"', function(err_n, result_n) {
                            if (err_n) {
                                console.log(err_n);
                            } else {
                                console.log(result_n);
                                var nodeSubject = result_n[0].title;

                                if (result_n[0].customer_type == "all") {
                                    spquery = 'SELECT c.customer_id,c.play_id,(SELECT  n.shop_id FROM notification n WHERE n.note_id="' + noteid + '") AS shop_id FROM customers c';
                                } else if (result_n[0].customer_type == "store") {
                                    spquery = 'SELECT c.shop_id,c.customer_id,(SELECT cs.play_id FROM customers cs WHERE cs.customer_id=c.customer_id) As play_id FROM customer_product c WHERE c.shop_id = (SELECT  n.shop_id FROM notification n WHERE n.note_id="' + noteid + '" )GROUP BY c.customer_id';
                                } else if (result_n[0].customer_type == "productuse") {
                                    spquery = 'SELECT cp.shop_id,cp.customer_id,(SELECT c.play_id FROM customers c WHERE c.customer_id=cp.customer_id) AS play_id FROM customer_product cp WHERE cp.product_id=(SELECT  n.product_id FROM notification n WHERE n.note_id="' + noteid + '")  GROUP BY cp.customer_id';
                                } else if (result_n[0].customer_type == "productnotuse") {
                                    spquery = 'SELECT c.customer_id,c.play_id,(SELECT  nn.shop_id FROM notification nn WHERE nn.note_id="' + noteid + '") AS shop_id FROM customers c WHERE c.customer_id NOT IN(SELECT cp.customer_id FROM customer_product cp WHERE cp.shop_id=(SELECT  ns.shop_id FROM notification ns WHERE ns.note_id="' + noteid + '") AND cp.product_id=(SELECT  n.product_id FROM notification n WHERE n.note_id="' + noteid + '")  GROUP BY cp.customer_id)';
                                }

                                sql.runQuery('' + spquery + '', function(err_s, result_s) {
                                    if (err_s)
                                        console.log(err_s);
                                    //callback(err_s,null);

                                    else {
                                        // console.log("length"+result_s.length);
                                        // console.log("length"+result_s[0].shop_id);
                                        console.log("deails data");
                                        console.log(result_s);

                                        var playdata = [];
                                        result_s.forEach(function(i, idx, array) {
                                            //console.log(i.play_id);
                                            if (i.play_id != null && i.play_id != undefined && i.play_id != "") {
                                                playdata.push(i.play_id);
                                            }
                                        });
                                        console.log(playdata);

                                        if (result_s.length > 1) {

                                            for (k = 0; k < (result_s.length); k++) {
                                                //console.log(result_s[k].shop_id);
                                                if (k === ((result_s.length) - 1)) {

                                                    valcate += '("' + result_s[k].shop_id + '", "' + result_s[k].customer_id + '", "' + createdDate + '", "' + noteid + '")';
                                                } else {

                                                    valcate += '("' + result_s[k].shop_id + '", "' + result_s[k].customer_id + '", "' + createdDate + '", "' + noteid + '"),';
                                                }
                                            }

                                        } else {

                                            valcate += '("' + result_s[k].shop_id + '", "' + result_s[k].customer_id + '", "' + createdDate + '", "' + noteid + '")';
                                        }
                                        //console.log(valcate);
                                        sql.runQuery('INSERT INTO notification_list ( shop_id, customer_id, created_date, note_id) VALUES ' + valcate, function(errcc, resultcc) {
                                            if (errcc) {
                                                console.log(errcc);

                                            } else {
                                                if (playdata.length > 0) {

                                                    var firstNotification = new OneSignal.Notification({
                                                        contents: {
                                                            en: nodeSubject,
                                                        },
                                                        data: {
                                                            "abc": "123",
                                                            "url": "http://www.lifotechnologies.com/"
                                                        },
                                                        headings: {
                                                            en: "Qlic",
                                                        },
                                                        include_player_ids: playdata
                                                            // small_icon: "QLIC",
                                                            // large_icon: "http://app.qlic.ch/store/images/user2-160x160.jpg"     
                                                    });
                                                    myClient.sendNotification(firstNotification, function(err, httpResponse, data) {
                                                        if (err) {
                                                            console.log('Something went wrong...');
                                                        } else {
                                                            console.log(data);
                                                            res = {
                                                                status: 'success',
                                                                message: 'successfully'
                                                            };
                                                            callback(null, res);
                                                        }
                                                    });
                                                } else {
                                                    res = {
                                                        status: 'success',
                                                        message: 'successfully'
                                                    };
                                                    callback(null, res);

                                                }
                                                // res={status:'success',message:'successfully'};
                                                // callback(null,res);
                                            }
                                        });

                                    }

                                });
                                // res={status:'success',message:'successfully'};
                                // callback(null,res);
                            }
                        });
                    }


                });
                break;
            case '67':
                /** forgotpassword**/

                email = checkNotEmpty(reqData.email) ? reqData.email : '';
                console.log(email);

                sql.fetchTable('users', 'user_id', 'user_email="' + email + '"', function(err_s, result_s) {
                    if (result_s == "") {
                        var ret = {
                            status: 'error'
                        };
                        callback(null, ret);
                    } else {
                        userid = app.locals.encrypt(result_s[0].user_id);
                        var rlink = site_redirect_url + 'resetpassword?uid=' + userid;
                        console.log(rlink);
                        const template = 'views/email-templates/adminforgot.ejs';
                        let templateData = {
                            email: email,
                            rlink: rlink
                        };
                        ejs.renderFile(template, templateData, (err, html) => {
                            if (err) console.log(err);
                            var mailOptions = {
                                from: 'qlicswiss@gmail.com',
                                to: email,
                                subject: 'Qlic Forgot Password',
                                generateTextFromHtml: true,
                                html: html
                            };
                            console.log(mailOptions);
                            transporter.sendMail(mailOptions, function(error, info) {
                                if (error) {
                                    console.log(error);
                                } else {
                                    console.log('Email sent: ' + info.response);
                                    res = {
                                        status: 'success'
                                    };
                                    callback(null, res);
                                }
                            });
                        });


                    }
                });
                break;
            case '68':
                /** resetpassword**/

                password = checkNotEmpty(reqData.password) ? reqData.password : '';
                cpassword = checkNotEmpty(reqData.confirmpassword) ? reqData.confirmpassword : '';
                uid = checkNotEmpty(app.locals.decrypt(reqData.uid)) ? app.locals.decrypt(reqData.uid) : '';
                mdPassword = checkNotEmpty(reqData.password) ? md5(reqData.password) : '';
                console.log(password);
                console.log(uid);
                sql.fetchTable('users', 'user_id', 'user_id="' + uid + '"', function(err1, resul, fields) {
                    if (resul != '') {
                        sql.runQuery('UPDATE users SET password="' + mdPassword + '" WHERE user_id="' + uid + '"', function(err_u, result_u) {
                            if (err_u) {
                                res = {
                                    status: 'error',
                                    message: 'Invalid '
                                };
                                callback(null, res);
                            } else {
                                res = {
                                    status: 'success',
                                    message: 'Successfully Update'
                                };
                                callback(null, res);
                            }
                        });
                    } else {
                        res = {
                            status: 'invalid',
                            message: 'invalid',
                            test: resul
                        };
                        callback(null, res);
                    }
                });

                break;
            case '69':
                /** partner request**/

                businessName = checkNotEmpty(reqData.businessName) ? reqData.businessName : '';
                contactPerson = checkNotEmpty(reqData.contactPerson) ? reqData.contactPerson : '';
                email = checkNotEmpty(reqData.email) ? reqData.email : '';
                mobile = checkNotEmpty(reqData.mobile) ? reqData.mobile : '';
                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");

                sql.insertTable2('partner_request', 'business_name,contactperson,email,mobile,created_date', '"' + businessName + '","' + contactPerson + '","' + email + '","' + mobile + '","' + createdDate + '"', function(err, result) {
                    if (err) {
                        res = {
                            status: 'error',
                            message: 'error'
                        };
                        callback(null, res);
                    } else {
                        var rlink = req.protocol + '://app.qlic.ch/';
                        const template = 'views/email-templates/request.ejs';
                        let templateData = {
                            rlink: rlink,
                        };
                        ejs.renderFile(template, templateData, (err, html) => {
                            if (err) console.log(err);
                            var mailOptions = {
                                from: 'qlicswiss@gmail.com',
                                to: 'info@qlic.ch',
                                subject: 'Partner anfrage',
                                generateTextFromHtml: true,
                                html: html
                            };
                            console.log(mailOptions);
                            transporter.sendMail(mailOptions, function(error, info) {
                                if (error) {
                                    console.log(error);
                                } else {
                                    console.log('Email sent: ' + info.response);
                                    res = {
                                        status: 'success'
                                    };
                                    callback(null, res);
                                }
                            });
                        });
                        res = {
                            status: 'success',
                            message: 'successfully'
                        };
                        callback(null, res);
                    }
                });

                break;
            case '70':
                /** partner request**/
                var product = "";
                product += '<option value=""> Select Product</option>';
                shopId = checkNotEmpty(reqData.shopId) ? reqData.shopId : '';
                productId = checkNotEmpty(reqData.productId) ? reqData.productId : '';
                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");

                sql.runQuery('SELECT p.product_name,p.product_id FROM product p WHERE p.shop_id="' + shopId + '" ', function(err_shop, result_shop) {
                    if (err_shop) {
                        result_shop = [];
                        res = {
                            status: 'error',
                            'message': " Invalid Request",
                            product: product
                        };
                        callback(null, res);
                    } else {
                        result_shop.forEach(function(i, idx, array) {
                            console.log(idx);
                            console.log(array);
                            if (i.product_id == productId) {
                                product += '<option value="' + i.product_id + '" selected>' + i.product_name + '</option>';
                            } else {
                                product += '<option value="' + i.product_id + '">' + i.product_name + '</option>';
                            }

                        });

                        res = {
                            status: 'success',
                            product: product
                        };
                        callback(null, res);
                    }
                });

                break;
            case '71':
                /*** Upload car image  ***/
                console.log(reqData);
                console.log(req.file);
                console.log(reqData.file);
                uid = checkNotEmpty(reqData.uid) ? reqData.uid : '';
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                shop_id = checkNotEmpty(reqData.shop_id) ? app.locals.decrypt(reqData.shop_id) : '';
                intype = reqData.intype ? reqData.intype : '';
                console.log(intype);
                console.log("uid" + uid);
                var fileNameTime = (new Date().getTime());
                // var randN = randomstring.generate({
                //   length: 5,
                //   charset: 'numeric'
                // }); 
                console.log("fileNameTime" + fileNameTime);
                if (!req.files) {
                    console.log("testchecking");
                    //return res.status(400).send('No files were selected.');
                } else {
                    //carData_id=req.session.icar_data!=undefined ?(req.session.icar_data!=""?req.session.icar_data:''):'';

                    const parent = req.files;

                    let imgFiles = parent.file;
                    /** Upload car image file **/
                    var chk_array = Array.isArray(imgFiles);
                    console.log(chk_array);
                    var imgdataname = [];
                    var img_id = [];
                    var img_eid = [];
                    var imgId = "";
                    var cvlenght = 0;
                    if (checkNotEmpty(imgFiles)) {
                        if (imgFiles.length > 1) {
                            console.log("multi image");
                            var checklength = imgFiles.length;

                            Array.from(imgFiles).forEach(function(child, key) {
                                sortkey = parseInt(key) + parseInt(1);
                                console.log("startchecklength" + checklength);
                                console.log("startcvlenght" + cvlenght);
                                if (key <= 15) {
                                    var randN = randomstring.generate(7);
                                    var oldFileName = child.name;
                                    console.log(child.name);
                                    console.log(child.type);
                                    console.log(child.size);
                                    var fileExtension = oldFileName.replace(/^.*\./, '');
                                    var newFileName = randN + fileNameTime + '.' + fileExtension;

                                    child.mv(imageurlink_product + newFileName, function(err_im, result_im) {
                                        if (err_im)
                                            res['img_upload_status'] = 'upload failed';
                                        else {
                                            if (edit_id) {
                                                query1 = 'product_image,uid,shop_id,product_id';
                                                query2 = '"' + newFileName + '","' + uid + '","' + shop_id + '","' + edit_id + '"';
                                            } else {
                                                query1 = 'product_image,uid';
                                                query2 = '"' + newFileName + '","' + uid + '"';
                                            }

                                            sql.insertTable2('productfile', query1, query2, function(err_i, result_i) {
                                                if (err_i) {
                                                    res['img_upload_status'] = 'upload failed';

                                                } else {
                                                    cvlenght = parseInt(cvlenght) + parseInt(1);
                                                    console.log("result_i1");
                                                    console.log(result_i);
                                                    //res['img_upload_status']='upload success';
                                                    //imgId=result_i;
                                                    img_id.push(result_i);
                                                    img_eid.push(app.locals.encrypt(result_i));
                                                    //result_i;
                                                    imgdataname.push(newFileName);
                                                    var vccid = {
                                                        [result_i]: newFileName
                                                    };
                                                    //imgdataname.push(vccid);
                                                    if (checklength == cvlenght) {
                                                        var res = {
                                                            status: 'success',
                                                            img_id: img_id,
                                                            img_eid: img_eid,
                                                            uploadfilename: imgdataname
                                                        };
                                                        console.log(res);
                                                        callback(null, res);
                                                    }

                                                }


                                            });
                                        }
                                    });
                                    //img_id.push(imgId);
                                    //imgdataname.push(newFileName);
                                }
                            });
                        } else {
                            console.log("singel image");
                            var randN = randomstring.generate(7);
                            var oldFileName = imgFiles.name;
                            var fileExtension = oldFileName.replace(/^.*\./, '');
                            var newFileName = randN + fileNameTime + '.' + fileExtension;
                            imgFiles.mv(imageurlink_product + newFileName, function(err_im, result_im) {
                                if (err_im)
                                    res['car_upload_status'] = 'upload failed';
                                else {
                                    if (edit_id) {
                                        query1 = 'product_image,uid,shop_id,product_id';
                                        query2 = '"' + newFileName + '","' + uid + '","' + shop_id + '","' + edit_id + '"';
                                    } else {
                                        query1 = 'product_image,uid';
                                        query2 = '"' + newFileName + '","' + uid + '"';
                                    }
                                    sql.insertTable2('productfile', query1, query2, function(err_i, result_i) {
                                        if (err_i) {
                                            res['img_upload_status'] = 'upload failed';
                                            res['img_name'] = '';
                                        } else {
                                            console.log("result_i2");
                                            console.log(result_i);
                                            img_id.push(result_i);
                                            img_eid.push(app.locals.encrypt(result_i));
                                            imgdataname.push(newFileName);
                                            var res = {
                                                status: 'success',
                                                img_id: img_id,
                                                img_eid: img_eid,
                                                uploadfilename: imgdataname
                                            };
                                            console.log(res);
                                            callback(null, res);


                                        }

                                    });

                                }
                            });
                            //imgdataname.push(newFileName);
                        }
                        // var res={status:'success',img_id:img_id,uploadfilename:imgdataname};
                        // console.log(res);    
                        // callback(null,res);
                    }

                }

                break;
            case '72':
                /*** Product Details Submit ***/

                productName = checkNotEmpty(reqData.product_name) ? reqData.product_name : '';
                // productCode=checkNotEmpty(reqData.product_code)?reqData.product_code:'';
                description = checkNotEmpty(reqData.description) ? reqData.description : '';
                short_description = checkNotEmpty(reqData.short_description) ? reqData.short_description : '';
                title = checkNotEmpty(reqData.title) ? reqData.title : '';
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                shopId = checkNotEmpty(reqData.shop_id) ? app.locals.decrypt(reqData.shop_id) : '';
                uid = checkNotEmpty(reqData.u_id) ? reqData.u_id : '';
                imagecount = checkNotEmpty(reqData.imagecount) ? reqData.imagecount : '';
                productPoint = checkNotEmpty(reqData.product_point) ? reqData.product_point : '';
                console.log(productPoint);
                url_type = checkNotEmpty(reqData.type) ? reqData.type : '';
                sId = app.locals.encrypt(shopId);
                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                var img = dateFormat(nDate, "yyyy-mm-ddHH:mm:ss");
                var fileNameTime = (new Date().getTime());
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                if (productName == '') {
                    res = {
                        status: 'error',
                        message: 'Enter All field'
                    };
                    callback(null, res);
                } else {
                    sql.fetchTable('product', 'product_id', 'product_name="' + productName + '" AND shop_id="' + shopId + '" AND product_id!="' + edit_id + '"', function(err1, resul) {
                        if (resul == '') {
                            if (edit_id) {
                                sql.runQuery('UPDATE product SET product_name="' + productName + '",description="' + description + '",points="' + productPoint + '",title="' + title + '",short_description="' + short_description + '" WHERE product_id="' + edit_id + '"', function(err_up, result_up) {
                                    if (err_up) {
                                        res = {
                                            status: 'error',
                                            message: 'Invalid User'
                                        };
                                        callback(null, res);
                                    } else {
                                        sql.runQuery('SELECT id FROM productfile WHERE uid="' + uid + '"', function(err_img, result_img) {
                                            if (err_img) {
                                                console.log(err_img);
                                            } else {

                                                if (result_img == "") {
                                                    res = {
                                                        status: 'success',
                                                        message: 'successfully',
                                                        shopId: reqData.shop_id,
                                                        type: url_type
                                                    };
                                                    callback(null, res);
                                                } else {
                                                    sql.runQuery('UPDATE productfile SET shop_id="' + shopId + '",product_id="' + edit_id + '",total="' + imagecount + '" WHERE uid="' + uid + '"', function(err_im, result_im) {
                                                        if (err_im) {
                                                            res = {
                                                                status: 'error',
                                                                message: 'Invalid User'
                                                            };
                                                            callback(null, res);
                                                        } else {
                                                            res = {
                                                                status: 'success',
                                                                message: 'successfully',
                                                                shopId: reqData.shop_id,
                                                                type: url_type
                                                            };
                                                            callback(null, res);

                                                        }
                                                    });

                                                }
                                            }
                                        });

                                    }
                                });
                            } else {
                                sql.insertTable2('product', 'product_name,description,shop_id,created_by,created_date,points,title,short_description', '"' + productName + '","' + description + '","' + shopId + '","' + shopId + '","' + createdDate + '","' + productPoint + '","' + title + '","' + short_description + '"', function(err, result) {
                                    if (err) {
                                        res = {
                                            status: 'error',
                                            message: 'Invalid'
                                        };
                                        callback(null, res);
                                    } else {
                                        var productId = result;
                                        sql.runQuery('SELECT count(product_id) as pcode FROM product WHERE shop_id="' + shopId + '" ', function(err_img, result_img) {
                                            if (err_img) {
                                                console.log(err_img);
                                            } else {
                                                var productcode = parseInt(result_img[0].pcode);
                                                var pad = "0000";
                                                var pccode = (pad + productcode).slice(-pad.length);
                                                sql.runQuery('UPDATE product SET product_code="' + pccode + '" WHERE product_id="' + productId + '"', function(err_pc, result_pc) {
                                                    if (err_pc) {
                                                        console.log(err_pc);
                                                    } else {
                                                        console.log(result_pc);
                                                    }
                                                });
                                            }
                                        });
                                        sql.runQuery('UPDATE productfile SET shop_id="' + shopId + '",product_id="' + productId + '",total="' + imagecount + '" WHERE uid="' + uid + '"', function(err_im, result_im) {
                                            if (err_im) {
                                                res = {
                                                    status: 'error',
                                                    message: 'Invalid User'
                                                };
                                                callback(null, res);
                                            } else {

                                                res = {
                                                    status: 'success',
                                                    message: 'successfully',
                                                    shopId: reqData.shop_id,
                                                    type: url_type
                                                };
                                                callback(null, res);

                                            }
                                        });

                                        // res={status:'success',message:'successfully',shopId:reqData.shop_id,type:url_type};
                                        // callback(null,res);
                                    }
                                });
                            }
                        } else {
                            res = {
                                status: 'already',
                                message: 'product Name already exits'
                            };
                            callback(null, res);
                        }
                    });
                }
                break;
            case '73':
                /*** Upload car image  ***/
                uid = checkNotEmpty(reqData.uid) ? reqData.uid : '';
                console.log("uid" + uid);
                var fileNameTime = (new Date().getTime());
                // var randN = randomstring.generate({
                //   length: 5,
                //   charset: 'numeric'
                // }); 
                console.log("fileNameTime" + fileNameTime);
                if (!req.files) {
                    console.log("testchecking");
                    //return res.status(400).send('No files were selected.');
                } else {
                    //carData_id=req.session.icar_data!=undefined ?(req.session.icar_data!=""?req.session.icar_data:''):'';

                    const parent = req.files;

                    let imgFiles = parent.file;
                    /** Upload car image file **/
                    var chk_array = Array.isArray(imgFiles);
                    console.log(chk_array);
                    var imgdataname = [];
                    var img_id = [];
                    var img_eid = [];
                    var imgId = "";
                    var cvlenght = 0;
                    if (checkNotEmpty(imgFiles)) {
                        if (imgFiles.length > 1) {
                            console.log("multi image");
                            var checklength = imgFiles.length;

                            Array.from(imgFiles).forEach(function(child, key) {
                                sortkey = parseInt(key) + parseInt(1);
                                console.log("startchecklength" + checklength);
                                console.log("startcvlenght" + cvlenght);
                                if (key <= 15) {
                                    var randN = randomstring.generate(7);
                                    var oldFileName = child.name;
                                    var fileExtension = oldFileName.replace(/^.*\./, '');
                                    var newFileName = randN + fileNameTime + '.' + fileExtension;

                                    child.mv(imageurlink_notification + newFileName, function(err_im, result_im) {
                                        if (err_im)
                                            res['img_upload_status'] = 'upload failed';
                                        else {


                                            sql.insertTable2('notificationfile', 'note_image,uid', '"' + newFileName + '","' + uid + '"', function(err_i, result_i) {
                                                if (err_i) {
                                                    res['img_upload_status'] = 'upload failed';

                                                } else {
                                                    cvlenght = parseInt(cvlenght) + parseInt(1);
                                                    console.log("result_i1");
                                                    console.log(result_i);
                                                    //res['img_upload_status']='upload success';
                                                    //imgId=result_i;
                                                    img_id.push(result_i);
                                                    img_eid.push(app.locals.encrypt(result_i));
                                                    //result_i;
                                                    imgdataname.push(newFileName);
                                                    var vccid = {
                                                        [result_i]: newFileName
                                                    };
                                                    //imgdataname.push(vccid);
                                                    if (checklength == cvlenght) {
                                                        var res = {
                                                            status: 'success',
                                                            img_id: img_id,
                                                            img_eid: img_eid,
                                                            uploadfilename: imgdataname
                                                        };
                                                        console.log(res);
                                                        callback(null, res);
                                                    }

                                                }


                                            });
                                        }
                                    });
                                    //img_id.push(imgId);
                                    //imgdataname.push(newFileName);
                                }
                            });
                        } else {
                            console.log("singel image");
                            var randN = randomstring.generate(7);
                            var oldFileName = imgFiles.name;
                            var fileExtension = oldFileName.replace(/^.*\./, '');
                            var newFileName = randN + fileNameTime + '.' + fileExtension;
                            imgFiles.mv(imageurlink_notification + newFileName, function(err_im, result_im) {
                                if (err_im)
                                    res['car_upload_status'] = 'upload failed';
                                else {
                                    sql.insertTable2('notificationfile', 'note_image,uid', '"' + newFileName + '","' + uid + '"', function(err_i, result_i) {
                                        if (err_i) {
                                            res['img_upload_status'] = 'upload failed';
                                            res['img_name'] = '';
                                        } else {
                                            console.log("result_i2");
                                            console.log(result_i);
                                            img_id.push(result_i);
                                            img_eid.push(app.locals.encrypt(result_i));
                                            imgdataname.push(newFileName);
                                            var res = {
                                                status: 'success',
                                                img_id: img_id,
                                                img_eid: img_eid,
                                                uploadfilename: imgdataname
                                            };
                                            console.log(res);
                                            callback(null, res);


                                        }

                                    });

                                }
                            });
                            //imgdataname.push(newFileName);
                        }
                        // var res={status:'success',img_id:img_id,uploadfilename:imgdataname};
                        // console.log(res);    
                        // callback(null,res);
                    }

                }

                break;
            case '74':
                /*** Shop Details Submit ***/
                shopName = checkNotEmpty(reqData.shop_name) ? reqData.shop_name : '';
                shopUsername = checkNotEmpty(reqData.shop_uname) ? reqData.shop_uname : '';
                shopPassword = checkNotEmpty(reqData.shop_password) ? reqData.shop_password : '';
                shopmdPassword = checkNotEmpty(reqData.shop_password) ? md5(reqData.shop_password) : '';
                categoryId = checkNotEmpty(reqData.category_id) ? reqData.category_id : '';
                description = checkNotEmpty(reqData.description) ? reqData.description : '';
                innerbanner = checkNotEmpty(reqData.innerbanner) ? reqData.innerbanner : '';
                address = checkNotEmpty(reqData.shop_address) ? reqData.shop_address : '';
                place = checkNotEmpty(reqData.place) ? reqData.place : '';
                postcode = checkNotEmpty(reqData.postcode) ? reqData.postcode : '';
                openingtime = checkNotEmpty(reqData.openingtime) ? reqData.openingtime : '';
                canton = checkNotEmpty(reqData.canton) ? reqData.canton : '';
                var valcate = "";
                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                var img = dateFormat(nDate, "yyyy-mm-ddHH:mm:ss");
                if (shopName) {
                    shopAlias = shopName; //shopName.toLowerCase();
                    shopAlias = shopAlias.trim();
                    shopAlias = shopAlias.replace(/[^A-Z0-9]+/ig, "_");
                }
                var strFirstThree = shopName.substring(0, 3);
                var fileNameTime = (new Date().getTime());
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                where = edit_id != '' ? ' AND shop_id!="' + edit_id + '"' : '';
                if (shopName == '') {
                    res = {
                        status: 'error',
                        message: 'Enter All field'
                    };
                    callback(null, res);
                } else {
                    sql.fetchTable('users', 'user_id', '(user_email="' + shopUsername + '" OR user_phone="' + shopUsername + '")', function(err_u, resul_u) {
                        if (resul_u == '') {
                            sql.fetchTable('shop', 'shop_id', '(shop_name="' + shopName + '" OR shop_username="' + shopUsername + '") AND shop_id!="' + edit_id + '"', function(err1, resul) {
                                if (resul == '') {
                                    if (edit_id) {
                                        sql.runQuery('UPDATE shop SET shop_name="' + shopName + '",shop_username="' + shopUsername + '",shop_password="' + shopPassword + '",category_id="' + categoryId + '",shop_address="' + address + '",description="' + description + '" ,shop_mdpassword="' + shopmdPassword + '",place="' + place + '",postcode="' + postcode + '",openingtime="' + openingtime + '",canton="' + canton + '" WHERE shop_id="' + edit_id + '"', function(err_up, result_up) {
                                            if (err_up) {
                                                console.log(err_up);
                                                res = {
                                                    status: 'error',
                                                    message: 'Invalid User'
                                                };
                                                callback(null, res);
                                            } else {

                                                sql.fetchTable('shop', 'shop_id', 'shop_id!="' + edit_id + '"', function(err1, resull) {
                                                    if (resull != '') {
                                                        //shopAlias=shopAlias+'_'+edit_id;
                                                        //console.log('welcome'+resull);
                                                    }
                                                    if (req.files) {
                                                        /** Image upload **/
                                                        //console.log(reqData.old_shop_image);
                                                        let shopImage = req.files.shop_image;
                                                        var oldFileName = shopImage.name;
                                                        var fileExtension = oldFileName.replace(/^.*\./, '');
                                                        var newFileName = strFirstThree + fileNameTime + '.' + fileExtension;
                                                        //console.log(newFileName);
                                                        if (checkNotEmpty(shopImage)) {
                                                            /*** Use the mv() method to place the file somewhere on your server **/
                                                            shopImage.mv(imageurlink_shop + newFileName, function(err, result) {
                                                                if (err) {
                                                                    res['image_upload_status'] = 'upload failed';
                                                                } else {
                                                                    if (reqData.old_shop_image != "") {
                                                                        fs.unlink(imageurlink_shop + reqData.old_shop_image);
                                                                    }
                                                                    sql.runQuery('UPDATE shop SET shop_image="' + newFileName + '" WHERE shop_id="' + edit_id + '"', function(err_up_i, result_up) {
                                                                        if (err_up_i) {
                                                                            res['image_upload_status'] = 'upload failed';
                                                                        } else {
                                                                            thumb({
                                                                                source: imageurlink_shop + newFileName, // could be a filename: dest/path/image.jpg
                                                                                destination: 'assets/images/thumbnail',
                                                                                concurrency: 4,
                                                                                //width: 200,
                                                                                //height: 200,
                                                                            }, function(files, err, stdout, stderr) {
                                                                                console.log('All done!');
                                                                                var thumFileName = strFirstThree + fileNameTime + '_thumb.' + fileExtension;
                                                                                sql.runQuery('UPDATE shop SET innerbanner="' + thumFileName + '" WHERE shop_id="' + edit_id + '"', function(err_th_i, result_th) {
                                                                                    if (err_th_i) {
                                                                                        console.log(err_th_i);
                                                                                    } else {
                                                                                        if (innerbanner != "") {
                                                                                            console.log("testv");
                                                                                            fs.unlink(imageurlink_shopthumbnail + reqData.innerbanner);
                                                                                        }
                                                                                    }

                                                                                });

                                                                            });
                                                                            res['image_upload_status'] = 'upload success';
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        } else {
                                                            res['image_upload_status'] = 'empty';
                                                        }
                                                    }
                                                });
                                                res = {
                                                    status: 'success',
                                                    message: 'successfully'
                                                };
                                                callback(null, res);

                                            }
                                            //data end
                                        });
                                    }
                                } else {
                                    res = {
                                        status: 'already',
                                        message: 'Shop Name / Username  already exits'
                                    };
                                    callback(null, res);
                                }
                            });
                        } else {
                            res = {
                                status: 'already',
                                message: 'Username  already exits'
                            };
                            callback(null, res);
                        }
                    });
                }
                break;
            case '75':
                /** resetpassword**/

                firstName = checkNotEmpty(reqData.firstName) ? reqData.firstName : '';
                lastName = checkNotEmpty(reqData.lastName) ? reqData.lastName : '';
                position = checkNotEmpty(reqData.position) ? reqData.position : '';
                email = checkNotEmpty(reqData.email) ? reqData.email : '';
                phone = checkNotEmpty(reqData.phone) ? reqData.phone : '';
                mobile = checkNotEmpty(reqData.mobile) ? reqData.mobile : '';
                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                sql.insertTable2('storecontactperson', 'first_name,last_name,position,email,phone,mobile,created_date,shop_id', '"' + firstName + '","' + lastName + '","' + position + '","' + email + '","' + phone + '","' + mobile + '","' + createdDate + '","' + req.session.shopadmin_id + '"', function(err, result) {
                    if (err) {
                        console.log(err);
                        res = {
                            status: 'error',
                            message: 'error'
                        };
                        callback(null, res);
                    } else {

                        res = {
                            status: 'success',
                            message: 'successfully'
                        };
                        callback(null, res);
                    }
                });
                break;
            case '76':
                /** resetpassword**/

                sql.runQuery('SELECT first_name,last_name,position,email,phone,mobile,created_date,shop_id FROM storecontactperson WHERE shop_id="' + req.session.shopadmin_id + '"', function(err_cont, result_cont) {
                    if (err_cont) {
                        console.log(err_cont);
                    } else {
                        console.log(result_cont);
                        res = {
                            status: 'success',
                            contactperson: result_cont
                        };
                        callback(null, res);

                    }
                });
                break;
            case '77':
                /** resetpassword**/

                sql.runQuery('SELECT branch_id,branch_name FROM branch WHERE shop_id="' + req.session.shopadmin_id + '"', function(err_cont, result_cont) {
                    if (err_cont) {
                        console.log(err_cont);
                    } else {
                        console.log(result_cont);
                        res = {
                            status: 'success',
                            branch: result_cont
                        };
                        callback(null, res);

                    }
                });
                break;
            case '78':
                /** resetpassword**/

                firstName = checkNotEmpty(reqData.firstName) ? reqData.firstName : '';
                lastName = checkNotEmpty(reqData.lastName) ? reqData.lastName : '';
                position = checkNotEmpty(reqData.position) ? reqData.position : '';
                email = checkNotEmpty(reqData.email) ? reqData.email : '';
                phone = checkNotEmpty(reqData.phone) ? reqData.phone : '';
                mobile = checkNotEmpty(reqData.mobile) ? reqData.mobile : '';
                branch_id = checkNotEmpty(reqData.branchId) ? reqData.branchId : '';
                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                sql.insertTable2('branch_contactperson', 'first_name,last_name,position,email,phone,mobile,created_date,shop_id,branch_id', '"' + firstName + '","' + lastName + '","' + position + '","' + email + '","' + phone + '","' + mobile + '","' + createdDate + '","' + req.session.shopadmin_id + '","' + branch_id + '"', function(err, result) {
                    if (err) {
                        console.log(err);
                        res = {
                            status: 'error',
                            message: 'error'
                        };
                        callback(null, res);
                    } else {

                        res = {
                            status: 'success',
                            message: 'successfully'
                        };
                        callback(null, res);
                    }
                });
                break;
            case '79':
                /** resetpassword**/

                sql.runQuery('SELECT bc.first_name,bc.last_name,bc.position,bc.email,bc.phone,bc.mobile,bc.created_date,bc.shop_id,(SELECT b.branch_name FROM branch b WHERE b.branch_id=bc.branch_id) AS branchname FROM branch_contactperson bc WHERE bc.shop_id="' + req.session.shopadmin_id + '"', function(err_cont, result_cont) {
                    if (err_cont) {
                        console.log(err_cont);
                    } else {
                        console.log(result_cont);
                        res = {
                            status: 'success',
                            contactperson: result_cont
                        };
                        callback(null, res);

                    }
                });
                break;
            case '80':
                /** forgotpassword**/
                console.log("forgotpasswordtest");
                email = checkNotEmpty(reqData.email) ? reqData.email : '';
                console.log(email);
                var type = "";
                sql.fetchTable('shop', 'shop_id', 'email="' + email + '"', function(err_s, result_s) {

                    sql.fetchTable('branchadmin', 'branchadmin_id', 'email="' + email + '"', function(err_b, result_b) {
                        if (err_b)
                            console.log(err_b);
                        else

                            console.log(result_b);

                        if (err_s)
                            console.log(err_s);
                        else
                            console.log(result_s);
                        shopdata = result_s
                        branchdata = result_b

                        if (shopdata != "" && branchdata == "") {
                            userid = userid = app.locals.encrypt(result_s[0].shop_id);
                            type = app.locals.encrypt(1);
                        } else if (shopdata == "" && branchdata != "") {
                            userid = app.locals.encrypt(result_b[0].branchadmin_id);
                            type = app.locals.encrypt(2);
                        } else {
                            userid = "";
                            type = "";
                        }
                        if (userid != "") {
                            //userid = app.locals.encrypt(result_s[0].shop_id);
                            var rlink = site_redirect_url + 'resetpassword?uid=' + userid + '&cid=' + type;
                            console.log(rlink);
                            const template = 'views/email-templates/shopadminforgot.ejs';
                            let templateData = {
                                email: email,
                                rlink: rlink,
                                type: type
                            };
                            ejs.renderFile(template, templateData, (err, html) => {
                                if (err) console.log(err);
                                var mailOptions = {
                                    from: 'qlicswiss@gmail.com',
                                    to: email,
                                    subject: 'Qlic Forgot Password',
                                    generateTextFromHtml: true,
                                    html: html
                                };
                                console.log(mailOptions);
                                transporter.sendMail(mailOptions, function(error, info) {
                                    if (error) {
                                        console.log(error);
                                    } else {
                                        console.log('Email sent: ' + info.response);
                                        res = {
                                            status: 'success'
                                        };
                                        callback(null, res);
                                    }
                                });
                            });


                        }
                    });
                });
                break;
            case '81':
                /** resetpassword**/

                password = checkNotEmpty(reqData.password) ? reqData.password : '';
                cpassword = checkNotEmpty(reqData.confirmpassword) ? reqData.confirmpassword : '';
                uid = checkNotEmpty(app.locals.decrypt(reqData.uid)) ? app.locals.decrypt(reqData.uid) : '';
                type = checkNotEmpty(app.locals.decrypt(reqData.type)) ? app.locals.decrypt(reqData.type) : '';
                mdPassword = checkNotEmpty(reqData.password) ? md5(reqData.password) : '';
                console.log(password);
                console.log(uid);
                if (type == 1) {
                    sql.fetchTable('shop', 'shop_id', 'shop_id="' + uid + '"', function(err1, resul, fields) {
                        if (resul != '') {
                            sql.runQuery('UPDATE shop SET shop_mdpassword="' + mdPassword + '",shop_password="' + password + '" WHERE shop_id="' + uid + '"', function(err_u, result_u) {
                                if (err_u) {
                                    console.log(err_u);
                                    res = {
                                        status: 'error',
                                        message: 'Invalid '
                                    };
                                    callback(null, res);
                                } else {
                                    console.log(result_u);
                                    res = {
                                        status: 'success',
                                        message: 'Successfully Update'
                                    };
                                    callback(null, res);
                                }
                            });
                        } else {
                            res = {
                                status: 'invalid',
                                message: 'invalid',
                                test: resul
                            };
                            callback(null, res);
                        }
                    });
                } else {
                    sql.fetchTable('branchadmin', 'branchadmin_id', 'branchadmin_id="' + uid + '"', function(err1, resul, fields) {
                        if (resul != '') {
                            sql.runQuery('UPDATE branchadmin SET ba_mdpassword="' + mdPassword + '",ba_password="' + password + '" WHERE shop_id="' + uid + '"', function(err_u, result_u) {
                                if (err_u) {
                                    console.log(err_u);
                                    res = {
                                        status: 'error',
                                        message: 'Invalid '
                                    };
                                    callback(null, res);
                                } else {
                                    console.log(result_u);
                                    res = {
                                        status: 'success',
                                        message: 'Successfully Update'
                                    };
                                    callback(null, res);
                                }
                            });
                        } else {
                            res = {
                                status: 'invalid',
                                message: 'invalid',
                                test: resul
                            };
                            callback(null, res);
                        }
                    });

                }

                break;
            case '82':
                /** chat insert**/
                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                var timestamp = Math.floor(Date.now() / 1000);
                //var chattime2 = new Date(timestamp * 1000);
                console.log("vvvv" + timestamp);
                //chattime = (toTimestamp(createdDate));
                //console.log("chattime"+chattime);
                message = checkNotEmpty(reqData.message) ? reqData.message : '';
                sendertype = checkNotEmpty(reqData.sendertype) ? reqData.sendertype : '';
                storeid = checkNotEmpty(reqData.storeid) ? reqData.storeid : '';
                if (sendertype == 'shop') {
                    var shopId = req.session.shopadmin_id;
                    var adminId = "";
                    var sender_id = req.session.shopadmin_id;
                    var receiver_id = 'admin';
                } else {
                    var shopId = storeid;
                    var adminId = 'admin';
                    var sender_id = 'admin';
                    var receiver_id = storeid;
                }

                sql.insertTable2('chat', 'message,sender_type,shop_id,admin_id,created_date,chattime,sender_id,receiver_id', '"' + message + '","' + sendertype + '","' + shopId + '","' + adminId + '","' + createdDate + '","' + timestamp + '","' + sender_id + '","' + receiver_id + '"', function(err, result) {
                    if (err) {
                        console.log(err);
                        res = {
                            status: 'error',
                            message: 'error'
                        };
                        callback(null, res);
                    } else {

                        res = {
                            status: 'success',
                            message: 'successfully'
                        };
                        callback(null, res);
                    }
                });
                break;
            case '83':
                /** get shopchat**/
                //clist=checkNotEmpty(reqData.clist)?reqData.clist:'';
                clist = reqData.clist;
                console.log("clist" + clist);
                var slimit = ' ORDER BY c.id DESC LIMIT ' + clist + ',10';
                sql.runQuery('SELECT c.sender_type,c.message,c.sender_id,c.receiver_id,c.chattime,c.created_date,(SELECT s.shop_name FROM shop s WHERE s.shop_id="' + req.session.shopadmin_id + '") AS shopname,(SELECT ss.innerbanner FROM shop ss WHERE ss.shop_id="' + req.session.shopadmin_id + '") AS image,(SELECT COUNT(ccs.id) FROM chat ccs WHERE ((ccs.sender_id="admin" AND ccs.receiver_id="' + req.session.shopadmin_id + '") OR (ccs.sender_id="' + req.session.shopadmin_id + '" AND ccs.receiver_id="admin")) ) AS totalcount FROM chat c WHERE ((c.sender_id="' + req.session.shopadmin_id + '" AND c.receiver_id="admin") OR (c.sender_id="admin" AND c.receiver_id="' + req.session.shopadmin_id + '"))' + slimit, function(err_cont, result_cont) {
                    sql.runQuery('UPDATE chat SET view_status="1" WHERE receiver_id="' + req.session.shopadmin_id + '"', function(err_u, result_u) {
                        if (err_cont) {
                            console.log(err_cont);
                        } else {
                            console.log(result_cont);
                            res = {
                                status: 'success',
                                chardata: result_cont
                            };
                            callback(null, res);

                        }
                    });
                });
                break;
            case '84':
                /** shop login status update**/
                sql.runQuery('UPDATE shop SET status="0" WHERE shop_id="' + req.session.shopadmin_id + '"', function(err_u, result_u) {
                    if (err_u) {
                        var ret = {
                            status: 'error',
                            'message': 'Server error'
                        };
                        callback(null, ret);

                    } else {
                        var ret = {
                            status: 'success'
                        };
                        callback(null, ret);
                    }
                });
                break;
            case '85':
                /** shop login status update**/
                clist = reqData.clist;
                console.log("clist" + clist);
                var slimit = ' ORDER BY c.id DESC LIMIT ' + clist + ',10';
                sql.runQuery('SELECT s.shop_name,s.shop_id,s.innerbanner,s.status FROM shop s ORDER BY s.status DESC', function(err_shoplist, result_shoplist) {
                    console.log(result_shoplist);
                    sql.runQuery('SELECT c.sender_type,c.message,c.sender_id,c.receiver_id,c.chattime,c.created_date,(SELECT s.shop_name FROM shop s WHERE s.shop_id="' + result_shoplist[0].shop_id + '") AS shopname,(SELECT ss.innerbanner FROM shop ss WHERE ss.shop_id="' + result_shoplist[0].shop_id + '") AS image,(SELECT COUNT(ccs.id) FROM chat ccs WHERE ((ccs.sender_id="admin" AND ccs.receiver_id="' + result_shoplist[0].shop_id + '") OR (ccs.sender_id="' + result_shoplist[0].shop_id + '" AND ccs.receiver_id="admin")) ) AS totalcount FROM chat c WHERE ((c.sender_id="admin" AND c.receiver_id="' + result_shoplist[0].shop_id + '") OR (c.sender_id="' + result_shoplist[0].shop_id + '" AND c.receiver_id="admin"))' + slimit, function(err_chatdetails, result_chatdetails) {
                        sql.runQuery('SELECT sd.shop_name,sd.shop_id,sd.innerbanner,sd.email,sd.phone,sd.shop_address,sd.contact_person FROM shop sd WHERE  sd.shop_id="' + result_shoplist[0].shop_id + '"', function(err_shopdetails, result_shopdetails) {
                            sql.runQuery('UPDATE chat SET view_status="1" WHERE receiver_id="admin" AND sender_id="' + result_shoplist[0].shop_id + '"', function(err_u, result_u) {
                                if (err_shopdetails) {
                                    console.log(err_shopdetails);
                                }
                                if (err_chatdetails) {
                                    console.log(err_chatdetails);
                                }
                                if (err_shoplist) {
                                    console.log(err_shoplist);
                                } else {
                                    console.log(result_shoplist);
                                    res = {
                                        status: 'success',
                                        shoplist: result_shoplist,
                                        chatdetails: result_chatdetails,
                                        shopdetails: result_shopdetails
                                    };
                                    callback(null, res);

                                }
                            });
                        });
                    });
                });
                break;
            case '86':
                /** shop login status update**/

                sql.runQuery('SELECT count(id)as chatcount FROM chat WHERE receiver_id="admin" and view_status="0"', function(err_cc, result_cc) {
                    if (err_cc) {
                        console.log(err_shoplist);
                    } else {
                        console.log(result_cc);
                        res = {
                            status: 'success',
                            chatcount: result_cc[0].chatcount
                        };
                        callback(null, res);

                    }
                });
                break;
            case '87':
                /** shop login status update**/
                clist = reqData.clist;
                console.log("clist" + clist);
                var slimit = ' ORDER BY c.id DESC LIMIT ' + clist + ',10';
                storeid = checkNotEmpty(reqData.storeid) ? reqData.storeid : '';
                sql.runQuery('SELECT c.sender_type,c.message,c.sender_id,c.receiver_id,c.chattime,c.created_date,(SELECT s.shop_name FROM shop s WHERE s.shop_id="' + storeid + '") AS shopname,(SELECT ss.innerbanner FROM shop ss WHERE ss.shop_id="' + storeid + '") AS image,(SELECT COUNT(ccs.id) FROM chat ccs WHERE ((ccs.sender_id="admin" AND ccs.receiver_id="' + storeid + '") OR (ccs.sender_id="' + storeid + '" AND ccs.receiver_id="admin")) ) AS totalcount FROM chat c WHERE ((c.sender_id="admin" AND c.receiver_id="' + storeid + '") OR (c.sender_id="' + storeid + '" AND c.receiver_id="admin"))' + slimit, function(err_chatdetails, result_chatdetails) {
                    sql.runQuery('SELECT sd.shop_name,sd.shop_id,sd.innerbanner,sd.email,sd.phone,sd.shop_address,sd.contact_person FROM shop sd WHERE  sd.shop_id="' + storeid + '"', function(err_shopdetails, result_shopdetails) {
                        sql.runQuery('UPDATE chat SET view_status="1" WHERE receiver_id="admin" AND sender_id="' + storeid + '"', function(err_u, result_u) {
                            if (err_shopdetails) {
                                console.log(err_shopdetails);
                            }
                            if (err_chatdetails) {
                                console.log(err_chatdetails);
                            } else {
                                console.log(result_chatdetails);
                                console.log(result_shopdetails);
                                res = {
                                    status: 'success',
                                    chatdetails: result_chatdetails,
                                    shopdetails: result_shopdetails
                                };
                                callback(null, res);

                            }
                        });
                    });
                });

                break;
            case '88':
                /** shop login status update**/

                sql.runQuery('SELECT count(id)as chatcount FROM chat WHERE receiver_id="' + req.session.shopadmin_id + '" and view_status="0"', function(err_cc, result_cc) {
                    if (err_cc) {
                        console.log(err_shoplist);
                    } else {
                        console.log(result_cc);
                        res = {
                            status: 'success',
                            chatcount: result_cc[0].chatcount
                        };
                        callback(null, res);

                    }
                });
                break;
            case '89':
                /** shop login status update**/
                var startweekday = checkNotEmpty(reqData.startweekday) ? reqData.startweekday : '';
                var endweekday = checkNotEmpty(reqData.endweekday) ? reqData.endweekday : '';
                var start_time = checkNotEmpty(reqData.start_time) ? reqData.start_time : '';
                var end_time = checkNotEmpty(reqData.end_time) ? reqData.end_time : '';
                var totalcount = checkNotEmpty(reqData.totalcount) ? reqData.totalcount : '';
                var type = checkNotEmpty(reqData.type) ? reqData.type : '';
                var linetype = checkNotEmpty(reqData.linetype) ? reqData.linetype : '';
                var uid = checkNotEmpty(reqData.uid) ? reqData.uid : '';
                var editid = checkNotEmpty(reqData.editid) ? reqData.editid : '';
                var viewtype = checkNotEmpty(reqData.viewtype) ? reqData.viewtype : '';
                var typetime = checkNotEmpty(reqData.typetime) ? reqData.typetime : '';
                var snumber = checkNotEmpty(reqData.snumber) ? reqData.snumber : '';
                var valcate = "";
                if (editid) {
                    if (totalcount > 1) {
                        var ttotal = 0;
                        for (k = 0; k < (startweekday.length); k++) {
                            var cct = parseInt(k) + 1;
                            if (k === ((startweekday.length) - 1)) {

                                valcate += '("' + uid + '", "' + editid + '", "' + startweekday[k] + '", "' + endweekday[k] + '", "' + start_time[k] + '","' + end_time[k] + '","' + cct + '","' + totalcount + '","' + type[k] + '","' + linetype[k] + '","' + typetime[k] + '","' + snumber[k] + '")';
                            } else {

                                valcate += '("' + uid + '", "' + editid + '", "' + startweekday[k] + '", "' + endweekday[k] + '", "' + start_time[k] + '","' + end_time[k] + '","' + cct + '","' + totalcount + '","' + type[k] + '","' + linetype[k] + '","' + typetime[k] + '","' + snumber[k] + '"),';
                            }
                        }

                    } else {

                        valcate += '("' + uid + '", "' + editid + '", "' + startweekday + '", "' + endweekday + '", "' + start_time + '","' + end_time + '","1","' + totalcount + '","' + type + '","' + linetype + '","' + typetime + '","' + snumber + '")';
                    }
                } else {
                    if (totalcount > 1) {

                        for (k = 0; k < (startweekday.length); k++) {
                            var cct = parseInt(k) + 1;
                            if (k === ((startweekday.length) - 1)) {

                                valcate += '("' + uid + '", "' + startweekday[k] + '", "' + endweekday[k] + '", "' + start_time[k] + '","' + end_time[k] + '","' + cct + '","' + totalcount + '","' + type[k] + '","' + linetype[k] + '","' + typetime[k] + '","' + snumber[k] + '")';
                            } else {

                                valcate += '("' + uid + '", "' + startweekday[k] + '", "' + endweekday[k] + '", "' + start_time[k] + '","' + end_time[k] + '","' + cct + '","' + totalcount + '","' + type[k] + '","' + linetype[k] + '","' + typetime[k] + '","' + snumber[k] + '"),';
                            }
                        }

                    } else {

                        valcate += '("' + uid + '", "' + startweekday + '", "' + endweekday + '", "' + start_time + '","' + end_time + '","1","' + totalcount + '","' + type + '","' + linetype + '","' + typetime + '","' + snumber + '")';
                    }

                }
                if (editid) {
                    console.log("editid");
                    if (totalcount > 0) {
                        console.log("ok");
                        sql.runQuery('SELECT uid FROM shoptime WHERE shop_id="' + editid + '" AND type="' + viewtype + '"', function(err_u, result_u) {
                            if (err_u) {
                                console.log(err_u);
                            } else {
                                sql.runQuery('SELECT uid FROM shoptab WHERE shopid="' + editid + '" AND type="' + viewtype + '"', function(err_t, result_t) {
                                    if (result_t == "") {
                                        sql.insertTable2('shoptab', 'uid,type,shopid', '"' + uid + '","' + viewtype + '","' + editid + '"', function(err, result) {});
                                    } else {
                                        sql.runQuery('DELETE FROM shoptab WHERE shopid="' + editid + '" AND type="' + viewtype + '"', function(err_d, result_d) {
                                            if (result_d) {
                                                sql.insertTable2('shoptab', 'uid,type,shopid', '"' + uid + '","' + viewtype + '","' + editid + '"', function(err, result) {});
                                            }
                                        });

                                    }
                                });

                                if (result_u == "") {
                                    sql.runQuery('INSERT INTO shoptime ( uid, shop_id, start_day, end_day, start_time, end_time, numid, total, type, linetype, typetime, snumber) VALUES ' + valcate, function(errcc, resultcc) {
                                        if (errcc) {
                                            console.log(errcc);

                                        } else {
                                            console.log(resultcc);
                                            res = {
                                                status: 'success'
                                            };
                                            callback(null, res);
                                        }
                                    });
                                } else {
                                    sql.runQuery('DELETE FROM shoptime WHERE shop_id="' + editid + '" AND type="' + viewtype + '"', function(err_d, result_d) {
                                        if (err_d) {

                                            callback(err_d, null);
                                        } else {
                                            sql.runQuery('INSERT INTO shoptime ( uid, shop_id, start_day, end_day, start_time, end_time, numid, total, type, linetype, typetime, snumber) VALUES ' + valcate, function(errcc, resultcc) {
                                                if (errcc) {
                                                    console.log(errcc);

                                                } else {
                                                    console.log(resultcc);
                                                    res = {
                                                        status: 'success'
                                                    };
                                                    callback(null, res);
                                                }
                                            });
                                        }
                                    });

                                }
                            }
                        });
                    }
                } else {
                    if (totalcount > 0) {
                        console.log("ok");
                        sql.runQuery('SELECT uid FROM shoptime WHERE uid="' + uid + '" AND type="' + viewtype + '"', function(err_u, result_u) {
                            if (err_u) {
                                console.log(err_shoplist);
                            } else {
                                sql.runQuery('SELECT uid FROM shoptab WHERE uid="' + uid + '" AND type="' + viewtype + '"', function(err_t, result_t) {
                                    if (result_t == "") {
                                        sql.insertTable2('shoptab', 'uid,type', '"' + uid + '","' + viewtype + '"', function(err, result) {});
                                    } else {
                                        sql.runQuery('DELETE FROM shoptab WHERE uid="' + uid + '" AND type="' + viewtype + '"', function(err_d, result_d) {
                                            if (result_d) {
                                                sql.insertTable2('shoptab', 'uid,type', '"' + uid + '","' + viewtype + '"', function(err, result) {});
                                            }
                                        });

                                    }
                                });

                                if (result_u == "") {
                                    sql.runQuery('INSERT INTO shoptime ( uid, start_day, end_day, start_time, end_time, numid, total, type, linetype, typetime, snumber) VALUES ' + valcate, function(errcc, resultcc) {
                                        if (errcc) {
                                            console.log(errcc);

                                        } else {
                                            console.log(resultcc);
                                            res = {
                                                status: 'success'
                                            };
                                            callback(null, res);
                                        }
                                    });
                                } else {
                                    sql.runQuery('DELETE FROM shoptime WHERE uid="' + uid + '" AND type="' + viewtype + '"', function(err_d, result_d) {
                                        if (err_d) {

                                            callback(err_d, null);
                                        } else {
                                            sql.runQuery('INSERT INTO shoptime ( uid, start_day, end_day, start_time, end_time, numid, total, type, linetype, typetime, snumber) VALUES ' + valcate, function(errcc, resultcc) {
                                                if (errcc) {
                                                    console.log(errcc);

                                                } else {
                                                    console.log(resultcc);
                                                    res = {
                                                        status: 'success'
                                                    };
                                                    callback(null, res);
                                                }
                                            });
                                        }
                                    });

                                }
                            }
                        });

                    }
                }

                console.log(valcate);
                break;
            case '90':
                /** shop login status update**/
                var uid = checkNotEmpty(reqData.uid) ? reqData.uid : '';
                var editid = checkNotEmpty(reqData.editid) ? reqData.editid : '';
                var type = checkNotEmpty(reqData.type) ? reqData.type : '';
                var query1 = "";
                var query2 = "";
                if (editid) {
                    query1 = 'SELECT type,start_day,end_day,start_time,end_time,numid,total,uid,linetype,typetime,snumber FROM shoptime WHERE shop_id="' + editid + '" AND type="' + type + '" ORDER BY id ASC';
                    query2 = 'SELECT DISTINCT linetype FROM shoptime WHERE shop_id="' + editid + '" AND type="' + type + '" ORDER BY id ASC';
                } else {
                    query1 = 'SELECT type,start_day,end_day,start_time,end_time,numid,total,uid,linetype,typetime,snumber FROM shoptime WHERE uid="' + uid + '" AND type="' + type + '" ORDER BY id ASC';
                    query2 = 'SELECT DISTINCT linetype FROM shoptime WHERE uid="' + uid + '" AND type="' + type + '" ORDER BY id ASC';
                }
                sql.runQuery(query1, function(err_cc, result_cc) {
                    sql.runQuery(query2, function(err_ccv, result_ccv) {
                        if (err_cc) {
                            console.log(err_cc);
                        } else {
                            console.log(result_cc);
                            console.log(result_ccv);
                            res = {
                                status: 'success',
                                details: result_cc,
                                linetype: result_ccv
                            };
                            callback(null, res);

                        }
                    });
                });
                break;
            case '91':
                /** shop login status update**/
                var startweekday = checkNotEmpty(reqData.startweekday) ? reqData.startweekday : '';
                var endweekday = checkNotEmpty(reqData.endweekday) ? reqData.endweekday : '';
                var start_time = checkNotEmpty(reqData.start_time) ? reqData.start_time : '';
                var end_time = checkNotEmpty(reqData.end_time) ? reqData.end_time : '';
                var totalcount = checkNotEmpty(reqData.totalcount) ? reqData.totalcount : '';
                var type = checkNotEmpty(reqData.type) ? reqData.type : '';
                var linetype = checkNotEmpty(reqData.linetype) ? reqData.linetype : '';
                var uid = checkNotEmpty(reqData.uid) ? reqData.uid : '';
                var editid = checkNotEmpty(reqData.editid) ? reqData.editid : '';
                var viewtype = checkNotEmpty(reqData.viewtype) ? reqData.viewtype : '';
                var typetime = checkNotEmpty(reqData.typetime) ? reqData.typetime : '';
                var snumber = checkNotEmpty(reqData.snumber) ? reqData.snumber : '';
                var valcate = "";
                if (editid) {
                    if (totalcount > 1) {
                        var ttotal = 0;
                        for (k = 0; k < (startweekday.length); k++) {
                            var cct = parseInt(k) + 1;
                            if (k === ((startweekday.length) - 1)) {

                                valcate += '("' + uid + '", "' + editid + '", "' + startweekday[k] + '", "' + endweekday[k] + '", "' + start_time[k] + '","' + end_time[k] + '","' + cct + '","' + totalcount + '","' + type[k] + '","' + linetype[k] + '","' + typetime[k] + '","' + snumber[k] + '")';
                            } else {

                                valcate += '("' + uid + '", "' + editid + '", "' + startweekday[k] + '", "' + endweekday[k] + '", "' + start_time[k] + '","' + end_time[k] + '","' + cct + '","' + totalcount + '","' + type[k] + '","' + linetype[k] + '","' + typetime[k] + '","' + snumber[k] + '"),';
                            }
                        }

                    } else {

                        valcate += '("' + uid + '", "' + editid + '", "' + startweekday + '", "' + endweekday + '", "' + start_time + '","' + end_time + '","1","' + totalcount + '","' + type + '","' + linetype + '","' + typetime + '","' + snumber + '")';
                    }
                } else {
                    if (totalcount > 1) {

                        for (k = 0; k < (startweekday.length); k++) {
                            var cct = parseInt(k) + 1;
                            if (k === ((startweekday.length) - 1)) {

                                valcate += '("' + uid + '", "' + startweekday[k] + '", "' + endweekday[k] + '", "' + start_time[k] + '","' + end_time[k] + '","' + cct + '","' + totalcount + '","' + type[k] + '","' + linetype[k] + '","' + typetime[k] + '","' + snumber[k] + '")';
                            } else {

                                valcate += '("' + uid + '", "' + startweekday[k] + '", "' + endweekday[k] + '", "' + start_time[k] + '","' + end_time[k] + '","' + cct + '","' + totalcount + '","' + type[k] + '","' + linetype[k] + '","' + typetime[k] + '","' + snumber[k] + '"),';
                            }
                        }

                    } else {

                        valcate += '("' + uid + '", "' + startweekday + '", "' + endweekday + '", "' + start_time + '","' + end_time + '","1","' + totalcount + '","' + type + '","' + linetype + '","' + typetime + '","' + snumber + '")';
                    }

                }
                if (editid) {
                    console.log("editid");
                    if (totalcount > 0) {
                        console.log("ok");
                        sql.runQuery('SELECT uid FROM branchtime WHERE branch_id="' + editid + '" AND type="' + viewtype + '"', function(err_u, result_u) {
                            if (err_u) {
                                console.log(err_u);
                            } else {
                                sql.runQuery('SELECT uid FROM branchtab WHERE branchid="' + editid + '" AND type="' + viewtype + '"', function(err_t, result_t) {
                                    if (result_u == "") {
                                        sql.insertTable2('branchtab', 'uid,type,branchid', '"' + uid + '","' + viewtype + '","' + editid + '"', function(err, result) {});
                                    } else {
                                        sql.runQuery('DELETE FROM branchtab WHERE branchid="' + editid + '" AND type="' + viewtype + '"', function(err_d, result_d) {
                                            if (result_d) {
                                                sql.insertTable2('branchtab', 'uid,type,branchid', '"' + uid + '","' + viewtype + '","' + editid + '"', function(err, result) {});
                                            }
                                        });

                                    }
                                });

                                if (result_u == "") {
                                    sql.runQuery('INSERT INTO branchtime ( uid, branch_id, start_day, end_day, start_time, end_time, numid, total, type, linetype, typetime, snumber) VALUES ' + valcate, function(errcc, resultcc) {
                                        if (errcc) {
                                            console.log(errcc);

                                        } else {
                                            console.log(resultcc);
                                            res = {
                                                status: 'success'
                                            };
                                            callback(null, res);
                                        }
                                    });
                                } else {
                                    sql.runQuery('DELETE FROM branchtime WHERE branch_id="' + editid + '" AND type="' + viewtype + '"', function(err_d, result_d) {
                                        if (err_d) {

                                            callback(err_d, null);
                                        } else {
                                            sql.runQuery('INSERT INTO branchtime ( uid, branch_id, start_day, end_day, start_time, end_time, numid, total, type, linetype, typetime, snumber) VALUES ' + valcate, function(errcc, resultcc) {
                                                if (errcc) {
                                                    console.log(errcc);

                                                } else {
                                                    console.log(resultcc);
                                                    res = {
                                                        status: 'success'
                                                    };
                                                    callback(null, res);
                                                }
                                            });
                                        }
                                    });

                                }
                            }
                        });
                    }
                } else {
                    if (totalcount > 0) {
                        console.log("ok");
                        sql.runQuery('SELECT uid FROM branchtime WHERE uid="' + uid + '" AND type="' + viewtype + '"', function(err_u, result_u) {
                            if (err_u) {
                                console.log(err_shoplist);
                            } else {
                                sql.runQuery('SELECT uid FROM branchtab WHERE uid="' + uid + '" AND type="' + viewtype + '"', function(err_t, result_t) {
                                    if (result_u == "") {
                                        sql.insertTable2('branchtab', 'uid,type', '"' + uid + '","' + viewtype + '"', function(err, result) {});
                                    } else {
                                        sql.runQuery('DELETE FROM branchtab WHERE uid="' + uid + '" AND type="' + viewtype + '"', function(err_d, result_d) {
                                            if (result_d) {
                                                sql.insertTable2('branchtab', 'uid,type', '"' + uid + '","' + viewtype + '"', function(err, result) {});
                                            }
                                        });

                                    }
                                });

                                if (result_u == "") {
                                    sql.runQuery('INSERT INTO branchtime ( uid, start_day, end_day, start_time, end_time, numid, total, type, linetype, typetime, snumber) VALUES ' + valcate, function(errcc, resultcc) {
                                        if (errcc) {
                                            console.log(errcc);

                                        } else {
                                            console.log(resultcc);
                                            res = {
                                                status: 'success'
                                            };
                                            callback(null, res);
                                        }
                                    });
                                } else {
                                    sql.runQuery('DELETE FROM branchtime WHERE uid="' + uid + '" AND type="' + viewtype + '"', function(err_d, result_d) {
                                        if (err_d) {

                                            callback(err_d, null);
                                        } else {
                                            sql.runQuery('INSERT INTO branchtime ( uid, start_day, end_day, start_time, end_time, numid, total, type, linetype, typetime, snumber) VALUES ' + valcate, function(errcc, resultcc) {
                                                if (errcc) {
                                                    console.log(errcc);

                                                } else {
                                                    console.log(resultcc);
                                                    res = {
                                                        status: 'success'
                                                    };
                                                    callback(null, res);
                                                }
                                            });
                                        }
                                    });

                                }
                            }
                        });

                    }
                }

                console.log(valcate);
                break;
            case '92':
                /** shop login status update**/
                var uid = checkNotEmpty(reqData.uid) ? reqData.uid : '';
                var editid = checkNotEmpty(reqData.editid) ? reqData.editid : '';
                var type = checkNotEmpty(reqData.type) ? reqData.type : '';
                var query1 = "";
                var query2 = "";
                if (editid) {
                    query1 = 'SELECT type,start_day,end_day,start_time,end_time,numid,total,uid,linetype,typetime,snumber FROM branchtime WHERE branch_id="' + editid + '" AND type="' + type + '" ORDER BY id ASC';
                    query2 = 'SELECT DISTINCT linetype FROM branchtime WHERE branch_id="' + editid + '" AND type="' + type + '" ORDER BY id ASC';
                } else {
                    query1 = 'SELECT type,start_day,end_day,start_time,end_time,numid,total,uid,linetype,typetime,snumber FROM branchtime WHERE uid="' + uid + '" AND type="' + type + '" ORDER BY id ASC';
                    query2 = 'SELECT DISTINCT linetype FROM branchtime WHERE uid="' + uid + '" AND type="' + type + '" ORDER BY id ASC';
                }
                sql.runQuery(query1, function(err_cc, result_cc) {
                    sql.runQuery(query2, function(err_ccv, result_ccv) {
                        if (err_cc) {
                            console.log(err_cc);
                        } else {
                            console.log(result_cc);
                            console.log(result_ccv);
                            res = {
                                status: 'success',
                                details: result_cc,
                                linetype: result_ccv
                            };
                            callback(null, res);

                        }
                    });
                });
                break;
            case '93':
                /*** change Password  Branch Admin***/
                oldPassword = checkNotEmpty(reqData.oldPassword) ? md5(reqData.oldPassword) : '';
                newPassword = checkNotEmpty(reqData.newPassword) ? md5(reqData.newPassword) : '';
                edit_id = checkNotEmpty(reqData.edit_id) ? (reqData.edit_id) : '';
                if (oldPassword == '' || newPassword == '') {
                    res = {
                        status: 'empty',
                        message: 'Enter the all field'
                    };
                    callback(null, res);
                } else {
                    sql.fetchTable('branchadmin', 'branchadmin_id', 'ba_mdpassword="' + oldPassword + '" AND branchadmin_id="' + edit_id + '"', function(err1, resul, fields) {
                        if (resul != '') {
                            sql.runQuery('UPDATE branchadmin SET ba_mdpassword="' + newPassword + '",ba_password="' + reqData.newPassword + '" WHERE branchadmin_id="' + edit_id + '"', function(err_u, result_u) {
                                if (err_u) {
                                    res = {
                                        status: 'error',
                                        message: 'aktuelles Passwort falsch'
                                    };
                                    callback(null, res);
                                } else {
                                    res = {
                                        status: 'success',
                                        message: 'Erfolgreich aktualisieren'
                                    };
                                    callback(null, res);
                                }
                            });
                        } else {
                            res = {
                                status: 'invalid',
                                message: 'aktuelles Passwort falsch',
                                test: resul
                            };
                            callback(null, res);
                        }
                    });
                }
                break;
            case '94':
                /*** edit image***/
                productId = checkNotEmpty(reqData.productId) ? reqData.productId : '';
                console.log("testproductid" + productId);
                sql.runQuery('SELECT id,product_image,(SELECT count(product_id) FROM productfile WHERE product_id="' + productId + '")AS imagecount, numid,total FROM productfile WHERE product_id="' + productId + '"', function(err_product, result_product) {
                    if (err_product) {
                        result_product = [];
                        res = {
                            status: 'error',
                            productimage: result_product
                        };
                        callback(null, res);
                        console.log(err_product);
                    } else {
                        console.log(result_product);
                        res = {
                            status: 'success',
                            productimage: result_product
                        };
                        callback(null, res);
                    }
                });

                break;
            case '95':
                /*** delete opentime***/
                uid = checkNotEmpty(reqData.uid) ? reqData.uid : '';
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                submittype = checkNotEmpty(reqData.submittype) ? reqData.submittype : '';
                console.log("submittype" + submittype);
                console.log("uid" + uid);
                console.log("edit_id" + edit_id);
                var query1 = "";
                var query2 = "";
                if (submittype == "shop") {
                    if (edit_id) {
                        query1 = 'DELETE FROM shoptime WHERE shop_id="' + edit_id + '"';
                        query2 = 'DELETE FROM shoptab WHERE shopid="' + edit_id + '"';
                    } else {
                        query1 = 'DELETE FROM shoptime WHERE uid="' + uid + '"';
                        query2 = 'DELETE FROM shoptab WHERE uid="' + uid + '"';
                    }

                } else if (submittype == "branch") {
                    if (edit_id) {
                        query1 = 'DELETE FROM branchtime WHERE branch_id="' + edit_id + '"';
                        query2 = 'DELETE FROM branchtab WHERE branchid="' + edit_id + '"';
                    } else {
                        query1 = 'DELETE FROM branchtime WHERE uid="' + uid + '"';
                        query2 = 'DELETE FROM branchtab WHERE uid="' + uid + '"';
                    }
                }

                sql.runQuery(query2, function(err, result) {
                    if (err) {
                        callback(err, null);
                        console.log(err);
                    } else {
                        console.log(result);
                        sql.runQuery(query1, function(errc, resultc) {
                            if (errc) {
                                console.log(errc);
                                res = {
                                    status: 'error',
                                    message: 'error'
                                };
                                callback(errc, res);
                            } else {
                                console.log(resultc);
                                res = {
                                    status: 'success',
                                    message: 'successfully'
                                };
                                callback(null, res);
                            }
                        });

                    }
                });
                break;
            case '96':
                /*** delete opentime***/
                getold = checkNotEmpty(reqData.getold) ? reqData.getold : '';
                oldtype = checkNotEmpty(reqData.oldtype) ? reqData.oldtype : '';
                uid = checkNotEmpty(reqData.uid) ? reqData.uid : '';
                edit_id = checkNotEmpty(reqData.edit_id) ? reqData.edit_id : '';
                submittype = checkNotEmpty(reqData.submittype) ? reqData.submittype : '';
                console.log("submittype" + submittype);
                console.log("uid" + uid);
                console.log("edit_id" + edit_id);
                console.log("getold" + getold);
                console.log("oldtype" + oldtype);
                var query1 = "";
                var query2 = "";
                if (submittype == "shop") {
                    if (edit_id) {
                        query1 = 'UPDATE shoptime SET type="' + getold + '" WHERE shop_id="' + edit_id + '" AND type="' + oldtype + '"';
                        query2 = 'UPDATE shoptab SET type="' + getold + '" WHERE shopid="' + edit_id + '" AND type="' + oldtype + '"';

                    } else {
                        query1 = 'UPDATE shoptime SET type="' + getold + '" WHERE uid="' + uid + '" AND type="' + oldtype + '"';
                        query2 = 'UPDATE shoptab SET type="' + getold + '" WHERE uid="' + uid + '" AND type="' + oldtype + '"';
                    }

                } else if (submittype == "branch") {
                    if (edit_id) {
                        query1 = 'UPDATE branchtime SET type="' + getold + '" WHERE branch_id="' + edit_id + '" AND type="' + oldtype + '"';
                        query2 = 'UPDATE branchtab SET type="' + getold + '" WHERE branchid="' + edit_id + '" AND type="' + oldtype + '"';
                    } else {
                        query1 = 'UPDATE branchtime SET type="' + getold + '" WHERE uid="' + uid + '" AND type="' + oldtype + '"';
                        query2 = 'UPDATE branchtab SET type="' + getold + '" WHERE uid="' + uid + '" AND type="' + oldtype + '"';
                    }
                }

                sql.runQuery(query2, function(err, result) {
                    if (err) {
                        callback(err, null);
                        console.log(err);
                    } else {
                        console.log(result);
                        sql.runQuery(query1, function(errc, resultc) {
                            if (errc) {
                                console.log(errc);
                                res = {
                                    status: 'error',
                                    message: 'error'
                                };
                                callback(errc, res);
                            } else {
                                console.log(resultc);
                                res = {
                                    status: 'success',
                                    message: 'successfully'
                                };
                                callback(null, res);
                            }
                        });

                    }
                });
                break;
            case '97':
                /*** Deal deactive***/
                id = checkNotEmpty(app.locals.decrypt(reqData.id)) ? app.locals.decrypt(reqData.id) : '';
                type = checkNotEmpty(reqData.type) ? reqData.type : '';
                ctype = checkNotEmpty(reqData.ctype) ? reqData.ctype : '';
                var query1 = '';
                if (ctype == "deal") {
                    if (type == "active") {
                        query1 = 'UPDATE deal SET status="0" WHERE deal_id="' + id + '"';
                    } else {
                        query1 = 'UPDATE deal SET status="1" WHERE deal_id="' + id + '"';
                    }


                } else if (ctype == "product") {
                    if (type == "active") {
                        query1 = 'UPDATE product SET status="0" WHERE product_id="' + id + '"';
                    } else {
                        query1 = 'UPDATE product SET status="1" WHERE product_id="' + id + '"';
                    }


                }
                sql.runQuery(query1, function(errc, resultc) {
                    if (errc) {
                        console.log(errc);
                        res = {
                            status: 'error',
                            message: 'error'
                        };
                        callback(err, res);
                    } else {
                        console.log(resultc);
                        res = {
                            status: 'success',
                            message: 'successfully'
                        };
                        callback(null, res);
                    }
                });

                break;
            case '98':
                /*** new store creation function***/
                console.log(reqData);
                shopName = checkNotEmpty(reqData.shop_name) ? reqData.shop_name : '';
                shopUsername = checkNotEmpty(reqData.shop_uname) ? reqData.shop_uname : '';
                shopPassword = checkNotEmpty(reqData.shop_password) ? reqData.shop_password : '';
                shopmdPassword = checkNotEmpty(reqData.shop_password) ? md5(reqData.shop_password) : '';
                categoryId = checkNotEmpty(reqData.category_id) ? reqData.category_id : '';
                description = checkNotEmpty(reqData.description) ? reqData.description : '';
                shopemail = checkNotEmpty(reqData.shop_email) ? reqData.shop_email : '';
                address = checkNotEmpty(reqData.shop_address) ? reqData.shop_address : '';
                place = checkNotEmpty(reqData.place) ? reqData.place : '';
                postcode = checkNotEmpty(reqData.postcode) ? reqData.postcode : '';
                canton = checkNotEmpty(reqData.canton) ? reqData.canton : '';
                firstName = checkNotEmpty(reqData.store_firstName) ? reqData.store_firstName : '';
                lastName = checkNotEmpty(reqData.store_lastName) ? reqData.store_lastName : '';
                position = checkNotEmpty(reqData.store_position) ? reqData.store_position : '';
                email = checkNotEmpty(reqData.store_email) ? reqData.store_email : '';
                phone = checkNotEmpty(reqData.store_phone) ? reqData.store_phone : '';
                mobile = checkNotEmpty(reqData.store_mobile) ? reqData.store_mobile : '';
                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                var img = dateFormat(nDate, "yyyy-mm-ddHH:mm:ss");
                sql.fetchTable('shop', 'shop_id', '(shop_name="' + shopName + '" OR shop_username="' + shopUsername + '")', function(err1, resul) {
                    if (resul == '') {
                        sql.insertTable2('newstore', 'shop_name,category_id,shop_username,shop_password,shop_mdpassword,shop_address,place,postcode,canton,email,phone,description', '"' + shopName + '","' + categoryId + '","' + shopUsername + '","' + shopPassword + '","' + shopmdPassword + '","' + address + '","' + place + '","' + postcode + '","' + canton + '","' + shopemail + '","","' + description + '"', function(err, result) {
                            if (err) {
                                res = {
                                    status: 'error',
                                    message: 'Retry'
                                };
                                callback(null, res);
                            } else {
                                var new_id = result;
                                sql.insertTable2('newstorecontactperson', 'first_name,last_name,position,email,phone,mobile,created_date,new_id', '"' + firstName + '","' + lastName + '","' + position + '","' + email + '","' + phone + '","' + mobile + '","' + createdDate + '","' + new_id + '"', function(errstore, resultstore) {
                                    if (errstore) {
                                        res = {
                                            status: 'error',
                                            message: 'Retry'
                                        };
                                        callback(null, res);
                                    } else {
                                        skey = app.locals.encrypt(new_id);
                                        var rlink = req.protocol + '://qlic.ch/newstoreverification/' + skey;
                                        console.log(rlink);
                                        const template = 'views/email-templates/storecreatemail.ejs';
                                        let templateData = {
                                            rlink: rlink,
                                            cname: firstName
                                        };
                                        ejs.renderFile(template, templateData, (err, html) => {
                                            if (err) console.log(err);
                                            var mailOptions = {
                                                from: 'qlicswiss@gmail.com',
                                                to: shopemail,
                                                subject: 'Willkommen auf Qlic.ch',
                                                generateTextFromHtml: true,
                                                html: html
                                            };
                                            console.log(mailOptions);
                                            transporter.sendMail(mailOptions, function(error, info) {
                                                if (error) {
                                                    console.log(error);
                                                } else {
                                                    console.log('Email sent: ' + info.response);
                                                    res = {
                                                        status: 'success'
                                                    };
                                                    callback(null, res);
                                                }
                                            });
                                        });
                                        res = {
                                            status: 'success',
                                            message: 'successfully'
                                        };
                                        callback(null, res);

                                    }
                                });
                            }
                        });
                    } else {
                        res = {
                            status: 'error',
                            message: 'Invalid User'
                        };
                        callback(null, res);

                    }
                });
                break;
            case '99':
                /*** new store creation resent mail***/
                console.log(reqData);
                email = checkNotEmpty(reqData.email) ? reqData.email : '';

                var createdDate = dateFormat(nDate, "yyyy-mm-dd HH:mm:ss");
                var img = dateFormat(nDate, "yyyy-mm-ddHH:mm:ss");
                sql.runQuery('SELECT n.id,n.status,(SELECT s.first_name FROM newstorecontactperson s WHERE n.id=s.new_id) AS cname FROM newstore n WHERE n.email="' + email + '" AND n.status=0', function(err_u, result_u) {
                    if (err_u) {
                        console.log(err_u);
                        res = {
                            status: 'error',
                            message: 'Retry'
                        };
                        callback(null, res);
                    } else {
                        skey = app.locals.encrypt(result_u[0].id);
                        var rlink = req.protocol + '://qlic.ch/newstoreverification/' + skey;
                        console.log(rlink);
                        const template = 'views/email-templates/storecreatemail.ejs';
                        let templateData = {
                            rlink: rlink,
                            cname: result_u[0].cname
                        };
                        ejs.renderFile(template, templateData, (err, html) => {
                            if (err) console.log(err);
                            var mailOptions = {
                                from: 'qlicswiss@gmail.com',
                                to: shopemail,
                                subject: 'Willkommen auf Qlic.ch',
                                generateTextFromHtml: true,
                                html: html
                            };
                            console.log(mailOptions);
                            transporter.sendMail(mailOptions, function(error, info) {
                                if (error) {
                                    console.log(error);
                                } else {
                                    console.log('Email sent: ' + info.response);
                                    res = {
                                        status: 'success'
                                    };
                                    callback(null, res);
                                }
                            });
                        });
                        res = {
                            status: 'success',
                            message: 'successfully'
                        };
                        callback(null, res);
                    }
                });
                break;

        }
    }
}

/** Check not empty **/
function checkNotEmpty(val) {
    if (val != '' && val != 0 && typeof(val) != 'undefined' && typeof(val) != 'null')
        return true;
    else return false;
}
/** Addslashes function **/
function addslashes(str) {
    if (str != '' && typeof(str) != "undefined" && typeof(str) != "null")
        return slashes.add(str);
    else return str;
}
/** Stripslashes function **/
function stripslashes(str) {
    if (str != '' && typeof(str) != "undefined" && typeof(str) != "null")
        return slashes.strip(str);
    else return str;
}

/** day date different **/
function noofdays(dtt1, dtt2) {
    dt1 = new Date(dtt1);
    dt2 = new Date(dtt2);
    var ndays = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
    return parseInt(ndays + 1);

}
/** Check value not empty **/
app.locals.checkNotEmpty = function(val) {
        if (val != '' && val != 0 && typeof(val) != 'undefined' && typeof(val) != 'null')
            return true;
        else return false;
    }
    /** Change date format **/
app.locals.changeDateFormat = function(date, format) {
        changedDate = dateFormat(date, format);
        return changedDate;
    }
    /** Change date format **/
app.locals.geoCheckIP = function(ip) {
        if (ip != '' && typeof(ip) != 'undefined') {
            var geo = geoip.lookup(ip);;
        } else geo = '';

        return geo;
    }
    /** Encrypt **/
app.locals.encrypt = function(id) {
        const encryptedString = cryptr.encrypt(id);
        return encryptedString;
    }
    /** Decrypt **/
app.locals.decrypt = function(id) {
        const decryptedString = cryptr.decrypt(id);
        return decryptedString;
    }
    /** md5 Decrypt **/
app.locals.remd = function(id) {
        const remd5String = (reverseMd5(id).str);
        return remd5String;
    }
    /** remd5 function **/
function remd(id) {
    const remdString = (reverseMd5(id).str);
    return remdString;
}

function toTimestamp(strDate) {
    //console.log(strDate);
    //var datum = Date.parse(strDate);
    var datum = Date.parse(strDate + "-0500") / 1000;
    console.log(datum);
    return datum;
}