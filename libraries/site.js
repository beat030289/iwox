var express = require('express');
var sql = require('./sqlfunctions');
var Cryptr = require('cryptr');
var cryptr = new Cryptr('lifo@123#');
module.exports = {
	shopDetailsView:function(req,callback){
		shopId=typeof req.query.shopId!="undefined"?(req.query.shopId!=""?app.locals.decrypt(req.query.shopId):''):'';
		sql.runQuery('SELECT shop.shop_name,shop.shop_id,shop.shop_image,shop.category_id,shop.shop_address,shop.contact_person,shop.email,shop.phone,shop.description,category.category_name FROM shop,category WHERE shop.shop_id="'+shopId+'" AND shop.category_id=category.category_id',function(err_shop,result_shop){
				if(err_shop)
				{
					result_shop=[];
				}
				else
				{
					callback({status:'success',shop:result_shop});
				}
		}); 
	},
	shopDetailsAdd:function(req,callback){
		sql.runQuery('SELECT category_name,category_id FROM category',function(errQry,result){
			if(errQry)
				result=[];
			else
			{
				data=[[]];
				datav=[];
				callback({status:'success',shop:data,cate:result,shoptime:datav,shoptab:datav});
				//callback({status:'success'});
			}
	}); 
		
	},
	dealDetailsAdd:function(req,callback){
		sql.runQuery('SELECT dealcategory_name,dealcategory_id FROM dealcategory',function(errQry,result){
			sql.runQuery('SELECT branch_name,branch_id FROM branch WHERE shop_id="'+req.session.shopadmin_id+'"',function(errQrybr,resultbr){
				if(errQry)
				{
					resultbr=[];
					result=[];
					callback({status:'success',shop:data,cate:result,branch:resultbr});
				}
				else
				{
					data=[[]];
					callback({status:'success',shop:data,cate:result,branch:resultbr});
					//callback({status:'success'});
				}
			});
	}); 
		
	},
	shopDetailsEdit:function(req,callback){
		shopId=typeof req.query.shopId!="undefined"?(req.query.shopId!=""?app.locals.decrypt(req.query.shopId):''):'';
			sql.runQuery('SELECT category_name,category_id FROM category',function(errQry,result_cat){
		if(errQry)
			result_cat=[];
		else
		{
			sql.runQuery('SELECT shop_name,shop_id,shop_image,shop_username,shop_password,category_id,shop_address,shop_addressline2,contact_person,email,phone,description,facebook,homepage,innerbanner,shop_phone,shop_email,place,postcode,canton FROM shop WHERE shop_id="'+shopId+'"',function(err_shop,result_shop){
				sql.runQuery('SELECT type,start_day,end_day,start_time,end_time,numid,total,uid,linetype FROM shoptime WHERE shop_id="'+shopId+'"',function(err_time,result_time){
					sql.runQuery('SELECT type,shopid FROM shoptab WHERE shopid="'+shopId+'"',function(err_tab,result_tab){
					if(err_time)
					{
						result_shoptime=[];
					}
					
					if(err_shop)
					{
						result_shop=[];
						
						callback({status:'error',shop:result_shop,cate:result_cat,shoptime:result_time,shoptab:result_tab});
						
						
					}
					else
					{
						callback({status:'success',shop:result_shop,cate:result_cat,shoptime:result_time,shoptab:result_tab});
						//callback({status:shopId});
					}
					});
				});
			});
		}
	}); 
	},
	setingsDetails:function(req,callback){
		sql.runQuery('SELECT user_firstname,user_lastname,user_id,user_email,user_phone FROM users WHERE user_id="'+req.session.admin_id+'"',function(errQry,result){
			if(errQry)
				result=[];
			else
			{
				data=[[]];
				callback({status:'success',details:result});
				//callback({status:'success'});
			}
	}); 
	},
	branchSetingsDetails:function(req,callback){
		sql.runQuery('SELECT ba_name,email,shop_id,email,phone,branchadmin_id FROM branchadmin WHERE branchadmin_id="'+req.session.branchadmin_id+'"',function(errQry,result){
			if(errQry)
				result=[];
			else
			{
				data=[[]];
				callback({status:'success',details:result});
				//callback({status:'success'});
			}
	}); 
	},
	shopSetingsDetails:function(req,callback){
		sql.runQuery('SELECT contact_person,email,shop_id,email,phone,shop_image,description,innerbanner FROM shop WHERE shop_id="'+req.session.shopadmin_id+'"',function(errQry,result){
			if(errQry)
				result=[];
			else
			{
				data=[[]];
				callback({status:'success',details:result});
				//callback({status:'success'});
			}
	}); 
	},
	storeSetingsDetails:function(req,callback){
		sql.runQuery('SELECT category_id,contact_person,email,shop_id,email,phone,shop_image,description,innerbanner,place,postcode,openingtime,canton,shop_name,shop_address,shop_username,shop_password FROM shop WHERE shop_id="'+req.session.shopadmin_id+'"',function(errQry,result){
			sql.runQuery('SELECT category_name,category_id FROM category',function(errQry_cat,result_cat){
				sql.runQuery('SELECT type,shopid FROM shoptab WHERE shopid="'+req.session.shopadmin_id+'"',function(err_tab,result_tab){
						if(errQry_cat)
							result_cat=[];
						if(errQry)
							result=[];
						else
						{
							data=[[]];
							console.log({status:'success',details:result,category:result_cat});
							callback({status:'success',details:result,category:result_cat,shoptab:result_tab});
							//callback({status:'success'});
						}
					});
			}); 
	}); 
	},
	shopList:function(req,callback){
		sql.runQuery('SELECT shop.shop_name,shop.shop_id,shop.shop_image,shop.category_id,category.category_name FROM shop,category WHERE shop.category_id=category.category_id',function(errQry,result){
		if(errQry)
			result=[];
		else
		{
			data=[[]];
			callback({status:'success',details:result});
			//callback({status:'success'});
		}
	}); 
	},
	editCategory:function(req,callback){
		cateId = req.query.cateId!="undefined"?(req.query.cateId!=""?app.locals.decrypt(req.query.cateId):''):'';
		sql.runQuery('SELECT category_name,category_id FROM category WHERE category_id="'+cateId+'"',function(errQry,result){
			if(errQry)
				result=[];
			else
			{
				data=[[]];
				callback({status:'success',details:result});
				//callback({status:'success'});
			}
		}); 
	},
	editDealCategory:function(req,callback){
		cateId = req.query.cateId!="undefined"?(req.query.cateId!=""?app.locals.decrypt(req.query.cateId):''):'';
		sql.runQuery('SELECT dealcategory_name,dealcategory_id FROM dealcategory WHERE dealcategory_id="'+cateId+'"',function(errQry,result){
			if(errQry)
				result=[];
			else
			{
				data=[[]];
				callback({status:'success',details:result});
				//callback({status:'success'});
			}
		}); 
	},
	productDetailsEdit:function(req,callback){
		productId=typeof req.query.productId!="undefined"?(req.query.productId!=""?app.locals.decrypt(req.query.productId):''):'';
		console.log("productid-"+productId);
		sql.runQuery('SELECT p.shop_id,p.title,p.short_description,p.product_name,p.product_id,p.product_code,p.description,p.points FROM product p WHERE p.product_id="'+productId+'"',function(err_p,result_p){
			sql.runQuery('SELECT id,product_image,(SELECT count(product_id) FROM productfile WHERE product_id="'+productId+'")AS imagecount, numid,total FROM productfile WHERE product_id="'+productId+'"',function(err_product,result_product){
					if(err_product)
					{
						result_product=[];
					}
				if(err_p)
				{
					result_product=[];
					callback({status:'error',product:result_product,product_image:result_product});
				}
				else
				{
					console.log(result_p);
					console.log("responseproduct_image1");
					callback({status:'success',product:result_p,product_image:result_product});
					//callback({status:shopId});
				}
			});
			});
	  
	},
	dealDetailsEdit:function(req,callback){
		dealId=typeof req.query.dealId!="undefined"?(req.query.dealId!=""?app.locals.decrypt(req.query.dealId):''):'';
		sql.runQuery('SELECT d.deal_id,d.title,d.company_name,d.price,d.percentage,d.offer_price,d.expirydate,d.deal_image,d.deal_thumb,d.dealcategory_id,d.date,d.description,d.branch_id  FROM deal d WHERE  d.deal_id="'+dealId+'"',function(err_p,result_p){
			sql.runQuery('SELECT dealcategory_name,dealcategory_id FROM dealcategory',function(errQry,result_cat){
				sql.runQuery('SELECT branch_name,branch_id FROM branch WHERE shop_id="'+req.session.shopadmin_id+'"',function(errQrybr,resultbr){
					if(errQrybr)
					{
					}
					if(errQry)
					{
						result_cat=[];
					}
					if(err_p)
					{
						console.log(err_p);
						result_deal=[];
						resultbr=[];
						callback({status:'error',deal:result_deal,category:result_cat,branch:resultbr});
					}
					else
					{
						callback({status:'success',deal:result_p,category:result_cat,branch:resultbr});
						console.log(result_p);
						//callback({status:shopId});
					}
				});
			});
		});
	},
	dealbranchDetailsEdit:function(req,callback){
		dealId=typeof req.query.dealId!="undefined"?(req.query.dealId!=""?app.locals.decrypt(req.query.dealId):''):'';
		sql.runQuery('SELECT d.deal_id,d.title,d.company_name,d.price,d.percentage,d.offer_price,d.expirydate,d.deal_image,d.deal_thumb,d.dealcategory_id   FROM deal d WHERE  d.deal_id="'+dealId+'"',function(err_p,result_p){
			sql.runQuery('SELECT dealcategory_name,dealcategory_id FROM dealcategory',function(errQry,result_cat){
				if(errQry)
				{
				result_cat=[];	
				}
				if(err_p)
				{
					console.log(err_p);
					result_deal=[];
					callback({status:'error',deal:result_deal,category:result_cat});
				}
				else
				{
					callback({status:'success',deal:result_p,category:result_cat});
					console.log(result_p);
					//callback({status:shopId});
				}
			});
	  });
	},
	branchDetailsEdit:function(req,callback){
		branchId=typeof req.query.branchId!="undefined"?(req.query.branchId!=""?app.locals.decrypt(req.query.branchId):''):'';
		sql.runQuery('SELECT branch_name,branch_id,branch_address,branch_lat,branch_long,email,phone,place,postcode,canton,openingtime from branch WHERE branch_id="'+branchId+'"',function(err_b,result_b){
			sql.runQuery('SELECT type,branchid FROM branchtab WHERE branchid="'+branchId+'"',function(err_tab,result_tab){
					if(err_b)
					{
						console.log(err_b);
						result_branch=[];
						callback({status:'error',branch:result_branch,shoptab:result_branch});
					}
					else
					{
						console.log("shoptab1");
						callback({status:'success',branch:result_b,shoptab:result_tab});
						//callback({status:shopId});
					}
				});
			}); 
	},
	branchAdminDetailsAdd:function(req,callback){
		shopId=typeof req.query.shopId!="undefined"?(req.query.shopId!=""?app.locals.decrypt(req.query.shopId):''):'';
		sql.runQuery('SELECT branch_name,branch_id from branch WHERE shop_id="'+shopId+'"',function(err_b,result_b){
				if(err_b)
				{
					console.log(err_b);
					result_b=[];
				}
				else
				{
					var data=[[]];
					callback({status:'success',badmin:data,branch:result_b});
				}
			}); 
	},
	branchAdminDetailsEdit:function(req,callback){
		branchadminId=typeof req.query.bId!="undefined"?(req.query.bId!=""?app.locals.decrypt(req.query.bId):''):'';
		shopId=typeof req.query.shopId!="undefined"?(req.query.shop!=""?app.locals.decrypt(req.query.shopId):''):'';
		sql.runQuery('SELECT branch_name,branch_id from branch WHERE shop_id="'+shopId+'"',function(err_b,result_b){
			sql.runQuery('SELECT ba_name,branchadmin_id,ba_username,ba_password,branch_id,shop_id,email,phone FROM branchadmin WHERE branchadmin_id="'+branchadminId+'"',function(err_ba,result_ba){
				if(err_b)
				{
					console.log(err_b);
					result_b=[];
				}
				if(err_ba)
				{
					console.log(err_ba);
					result_ba=[[]];
				}
				callback({status:'success',badmin:result_ba,branch:result_b});
			});
		});			
	},
	staffAdd:function(req,callback){
		shopId=typeof req.query.shopId!="undefined"?(req.query.shop!=""?app.locals.decrypt(req.query.shopId):''):'';
		sql.runQuery('SELECT branch_name,branch_id from branch WHERE shop_id="'+shopId+'"',function(err_b,result_b){
				if(err_b)
				{
					console.log(err_b);
					result_b=[];
				}
				else
				{
					var data=[[]];
					callback({status:'success',staff:data,branch:result_b});
				}
			}); 
	},
	addNotification:function(req,callback){
		//shopId = req.query.shopId!="undefined"?(req.query.cateId!=""?app.locals.decrypt(req.query.cateId):''):'';
		sql.runQuery('SELECT shop_name,shop_id FROM shop',function(errQry,result){
			if(errQry)
				result=[];
			else
			{
				data=[[]];
				callback({status:'success',details:result});
				//callback({status:'success'});
			}
		}); 
	},
	editNotification:function(req,callback){
		noteId = req.query.noteId!="undefined"?(req.query.noteId!=""?app.locals.decrypt(req.query.noteId):''):'';
		console.log("noteId"+noteId);
		sql.runQuery('SELECT shop_name,shop_id FROM shop',function(errQry,result){
			sql.runQuery('SELECT note_id,shop_id,title,note_subject,message,product_id,product_point,product_label,web_label,web_link,facebook,instagram,linkedin,xing,email,website,googleplus,created_date FROM notification WHERE note_id="'+noteId+'"',function(errQry_note,resultc){
					sql.runQuery('SELECT id,note_image FROM notificationfile WHERE note_id="'+noteId+'"',function(err_img,result_img){
						if(err_img)
							{
								console.log(err_img);
								result_img=[];
							}
						if(errQry_note)
						{
							console.log(errQry_note);
							resultc=[];
						}
						else
						{
							datac=[[]];
							// callback({status:'success',notelist:resultc});
							//callback({status:'success'});
						}
						if(errQry){
							console.log(errQry);
							result=[];
						}
						else
						{
							data=[[]];

							callback({status:'success',details:result,notelist:resultc,noteimage:result_img});
							//callback({status:'success'});
						}
				}); 
			}); 
		}); 
	},
	viewNotification:function(req,callback){
		noteId = req.query.noteId!="undefined"?(req.query.noteId!=""?app.locals.decrypt(req.query.noteId):''):'';
		console.log("noteId"+noteId);
			sql.runQuery('SELECT n.note_id,n.shop_id,n.title,n.note_subject,n.message,n.product_id,n.product_point,n.product_label,n.web_label,web_link,facebook,instagram,linkedin,xing,email,website,googleplus,n.created_date,(SELECT s.shop_name FROM shop s WHERE s.shop_id=n.shop_id) AS shop_name,(SELECT p.product_name FROM product p WHERE p.product_id=n.product_id) AS product_name FROM notification n WHERE n.note_id="'+noteId+'"',function(errQry_note,resultc){
				if(errQry_note)
				{
					console.log(errQry_note);
					resultc=[];
				}
				else
				{
					data=[[]];
					callback({status:'success',notelist:resultc});
				}
				
			}); 
		 
	},
	addshopNotification:function(req,callback){
		shopId = req.query.shopId!="undefined"?(req.query.shopId!=""?app.locals.decrypt(req.query.shopId):''):'';
		sql.runQuery('SELECT product_name,product_id FROM product WHERE shop_id="'+shopId+'"',function(errQry,result){
				sql.runQuery('SELECT title,deal_id FROM deal WHERE shop_id="'+shopId+'"',function(errQry_deal,result_deal){
				if(errQry_deal)
				{
					result_deal=[];
				}
				
				if(errQry)
				{
					result=[];
				}
				else
				{
					data=[[]];
					callback({status:'success',product:result,deal:result_deal});
					//callback({status:'success'});
				}
			});
		}); 
	},
	userlistDetails:function(req,callback){
		sql.runQuery('SELECT product_name,product_id FROM product WHERE shop_id="'+req.session.shopadmin_id+'"',function(err_product,result_product){
				if(err_product)
				{
					result_product=[];
				}
				else
				{
					callback({status:'success',product:result_product});
				}
		}); 
	},
	staffEdit:function(req,callback){
		staffId=typeof req.query.staffId!="undefined"?(req.query.staffId!=""?app.locals.decrypt(req.query.staffId):''):'';
		shopId=typeof req.query.shopId!="undefined"?(req.query.shop!=""?app.locals.decrypt(req.query.shopId):''):'';
		sql.runQuery('SELECT branch_name,branch_id from branch WHERE shop_id="'+shopId+'"',function(err_b,result_b){
			sql.runQuery('SELECT staff_name,staff_id,staff_username,staff_password,branch_id,shop_id,email,phone,staff_role FROM staff WHERE staff_id="'+staffId+'"',function(err_ba,result_ba){
				if(err_b)
				{
					result_b=[];
				}
				if(err_ba)
				{
					result_ba=[[]];
				}
				callback({status:'success',staff:result_ba,branch:result_b});
			});
		});			
	}
};
