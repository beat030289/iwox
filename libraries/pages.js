var express = require('express');
var app = express();


// index page 

app.get('/kontakt', function(req, res) {
    res.render('pages/kontakt');
});

app.get('/produkte-und-preise', function(req, res) {
    res.render('pages/produkt');
});

app.get('/uber-uns', function(req, res) {
    res.render('pages/uber-uns');
});

app.get('/sitemap', function(req, res) {
    res.render('pages/sitemap');
});


app.get('/handler', function(req, res) {
    res.render('pages/handler');
});
app.get('/auto-inserieren', function(req, res) {
    res.render('pages/auto_inserieren');
});
app.get('/merken-liste', function(req, res) {
    res.render('pages/merken_liste');
});
app.get('/autos', function(req, res) {
    res.render('pages/autos');
});
app.get('/marken', function(req, res) {
    res.render('pages/marken');
});
app.get('/agb', function(req, res) {
    res.render('pages/agb');
});
app.get('/impressum', function(req, res) {
    res.render('pages/impressum');
});

