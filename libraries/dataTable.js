var express = require('express');
var app = express();
var Cryptr = require('cryptr');
var cryptr = new Cryptr('lifo@123#');
module.exports = {
	fetchRecords:function(pageName,params,callback){ 
		
		if(typeof(pageName)!='undefined' && pageName!='')
		{
			var sLimit=sWhere=sOrderBy='';
			if(params.iDisplayStart!='' && params.iDisplayLength!= '-1' )
				sLimit = " LIMIT "+params.iDisplayStart+", "+params.iDisplayLength;
			var output={'sEcho':(typeof(params.sEcho)!='undefined'?params.sEcho:''),'aaData':''};
			switch(pageName)
			{
				//getuser details;
				case 'user':

					sWhere='';
					sPermission = "";
					sWhere+=typeof(params.username)!='undefined' && params.username!='' && params.username!='All'?' AND (concat (u.first_name , " " , u.last_name)LIKE "' + params.username + '%")':'';
					sWhere+=typeof(params.mobile)!='undefined' && params.mobile!=''?' AND u.mobile="'+params.mobile+'" ':'';
					sWhere+=typeof(params.userstatus)!='undefined' && params.userstatus!='' && params.userstatus!='All'?' AND u.status="'+params.userstatus+'" ':'';
					sWhere+=typeof(params.branch)!='undefined' && params.branch!='' && params.branch!='All'?' AND u.branch="'+params.branch+'" ':'';
					sWhere+=typeof(params.type)!='undefined' && params.type!='' && params.type!='All'?' AND u.function="'+params.type+'" ':'';
					sWhere+=typeof(session_id)!='undefined' && session_id!=''?' AND u.id!="'+session_id+'" ':'';
					sWhere+=' AND u.role!="1"';
					sPermission = ',(SELECT uedit FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="13") AS userpermission ';
					//sWhere+=' AND id!="'+req.session.userid+'"';
					console.log(sWhere);
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (u.first_name LIKE "%'+params.sSearch+'%" OR u.last_name LIKE "%'+params.sSearch+'%" OR u.id LIKE "%'+params.sSearch+'%" OR u.email LIKE "%'+params.sSearch+'%" OR u.mobile LIKE "%'+params.sSearch+'%" OR u.branch LIKE "%'+params.sSearch+'%" OR u.status LIKE "%'+params.sSearch+'%") ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(u.id) AS row_count FROM '+iw_user+' u WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY u.id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY u.id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							console.log('SELECT u.tabid,(SELECT b.title FROM '+iw_branch+' b WHERE b.id=u.branch) AS branch,u.function,u.first_name,u.last_name,u.mobile,u.email,DATE_FORMAT(u.created_date, "%d.%m.%Y (%h:%i)") AS created_date,u.status,u.id AS userid '+sPermission+'FROM '+iw_user+' u WHERE 1=1 '+sWhere+sOrderBy+sLimit);
							con.query('SELECT u.tabid,(SELECT b.title FROM '+iw_branch+' b WHERE b.id=u.branch) AS branch,u.function,u.first_name,u.last_name,u.mobile,u.email,DATE_FORMAT(u.created_date, "%d.%m.%Y (%h:%i)") AS created_date,u.status,u.id AS userid '+sPermission+'FROM '+iw_user+' u WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									console.log(err);
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
								else
								{
									console.log(res);
								}
							});
							
						}
					});
				break;
				//get servicetype details;
				
				case 'servicetype':
					sWhere='';
					sPermission = "";
					sPermission = ',(SELECT udelete FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="14") AS userpermission ';
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (service LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%") ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_servicetype+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,service,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date,status,id AS servicetypeid'+sPermission+' FROM '+iw_servicetype+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
							});
							
						}
					});
				break;
				//get extratype details;
				case 'extratype':
					sWhere='';
					sPermission = "";
					sPermission = ',(SELECT udelete FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="15") AS userpermission ';
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (extra LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" OR price LIKE "%'+params.sSearch+'%") ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_extratype+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,extra,nickname,price,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date,status,id AS extratypeid'+sPermission+' FROM '+iw_extratype+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
							});
							
						}
					});
				break;
				//get zeit details;
				case 'zeit':
					sWhere='';
					sPermission = "";
					sPermission = ',(SELECT udelete FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="16") AS userpermission ';
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (zeit LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%") ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_zeit+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,zeit,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date,status,id AS zeitid'+sPermission+' FROM '+iw_zeit+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
							});
							
						}
					});
				break;
				//get Category details;
				case 'category':
					sWhere='';
					sPermission = "";
					sPermission = ',(SELECT udelete FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="21") AS userpermission ';
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (category LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" ) ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_category+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,category,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date,status,id AS categoryid'+sPermission+' FROM '+iw_category+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
							});
							
						}
					});
				break;
				//get Customer details;
				case 'customer':
				console.log(pageName);
					sWhere='';
					sPermission = "";
					console.log("params.cname"+params.cname);
					console.log("params.mobile"+params.mobile);
					console.log("params.cstatus"+params.cstatus);
					console.log("params.branch"+params.branch);
					console.log("params.badge"+params.badge); 
					sWhere+=typeof(params.cname)!='undefined' && params.cname!='' && params.cname!='All'?' AND (concat (first_name , " " , last_name)LIKE "' + params.cname + '%")':'';
					sWhere+=typeof(params.mobile)!='undefined' && params.mobile!=''?' AND mobile="'+params.mobile+'" ':'';
					sWhere+=typeof(params.cstatus)!='undefined' && params.cstatus!='' && params.cstatus!='All'?' AND status="'+params.cstatus+'" ':'';
					sWhere+=typeof(params.branch)!='undefined' && params.branch!='' && params.branch!='All'?' AND branch="'+params.branch+'" ':'';
					sWhere+=typeof(params.badge)!='undefined' && params.badge!='' && params.badge!='All'?' AND function="'+params.badge+'" ':'';
					sPermission = ',(SELECT udelete FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="10") AS userpermission ';
					console.log(sWhere);
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (id LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" OR first_name LIKE "%'+params.sSearch+'%" OR last_name LIKE "%'+params.sSearch+'%"OR mobile LIKE "%'+params.sSearch+'%" OR email LIKE "%'+params.sSearch+'%" OR dob LIKE "%'+params.sSearch+'%" ) ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_customer+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,badge,first_name,last_name,dob,mobile,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date,status,id AS customerid'+sPermission+' FROM '+iw_customer+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
							});
							
						}
					});
				break;
				//get permitperson details;
				case 'permit':
					sWhere='';
					sPermission = "";
					//sPermission = ',(SELECT udelete FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="15") AS userpermission ';
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (name LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" OR idcard LIKE "%'+params.sSearch+'%") ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_permitperson+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,name,idcard,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date,status,id AS permitid'+sPermission+' FROM '+iw_permitperson+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
							});
							
						}
					});
				break;
				//get Hair details;
				case 'hair':
					sWhere='';
					sPermission = "";
					//sPermission = ',(SELECT udelete FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="15") AS userpermission ';
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (hair LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" ) ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_hair+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,hair,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date,status,id AS hairid'+sPermission+' FROM '+iw_hair+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
							});
							
						}
					});
				break;
				//get Eyes details;
				case 'eyes':
					sWhere='';
					sPermission = "";
					//sPermission = ',(SELECT udelete FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="15") AS userpermission ';
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (eyes LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" ) ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_eyes+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,eyes,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date,status,id AS eyesid'+sPermission+' FROM '+iw_eyes+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
							});
							
						}
					});
				break;
				//get Shoes details;
				case 'shoes':
					sWhere='';
					sPermission = "";
					//sPermission = ',(SELECT udelete FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="15") AS userpermission ';
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (shoes LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" ) ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_shoes+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,shoes,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date,status,id AS shoesid'+sPermission+' FROM '+iw_shoes+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
							});
							
						}
					});
				break;
				//get Shoes details;
				case 'cloth':
					sWhere='';
					sPermission = "";
					//sPermission = ',(SELECT udelete FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="15") AS userpermission ';
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (cloth LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" ) ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_cloth+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,cloth,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date,status,id AS clothid'+sPermission+' FROM '+iw_cloth+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
							});
							
						}
					});
				break;
				//get civilstatus details;
				case 'civilstatus':
					sWhere='';
					sPermission = "";
					//sPermission = ',(SELECT udelete FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="15") AS userpermission ';
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (civilstatus LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" ) ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_civilstatus+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,civilstatus,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date,status,id AS civilstatusid'+sPermission+' FROM '+iw_civilstatus+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
							});
							
						}
					});
				break;
				//get privatepart details;
				case 'privatepart':
					sWhere='';
					sPermission = "";
					//sPermission = ',(SELECT udelete FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="15") AS userpermission ';
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (privatepart LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" ) ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_privatepart+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,privatepart,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date,status,id AS privatepartid'+sPermission+' FROM '+iw_privatepart+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
							});
							
						}
					});
				break;
				//get bust details;
				case 'bust':
					sWhere='';
					sPermission = "";
					//sPermission = ',(SELECT udelete FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="15") AS userpermission ';
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (bust LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" ) ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_bust+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,bust,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date,status,id AS bustid'+sPermission+' FROM '+iw_bust+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
							});
							
						}
					});
				break;
				//get Nationality details;
				case 'nationality':
					sWhere='';
					sPermission = "";
					//sPermission = ',(SELECT udelete FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="15") AS userpermission ';
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (nationality LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" ) ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_nationality+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,nationality,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date,status,id AS nationalityid'+sPermission+' FROM '+iw_nationality+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
							});
							
						}
					});
				break;
				//get Vouchers details;
				case 'vouchers':
				console.log(pageName);
					sWhere='';
					sPermission = "";
					/*console.log("params.cname"+params.vname);
					console.log("params.mobile"+params.emobile);
					console.log("params.cstatus"+params.estatus);
					console.log("params.branch"+params.eexpiry);
					console.log("params.badge"+params.eprice);*/ 
					sWhere+=typeof(params.vname)!='undefined' && params.vname!='' && params.vname!='All'?' AND title "' + params.vname +'" ':'';
					sWhere+=typeof(params.emobile)!='undefined' && params.emobile!=''?' AND mobile="'+params.emobile+'" ':'';
					sWhere+=typeof(params.estatus)!='undefined' && params.estatus!='' && params.estatus!='All'?' AND status="'+params.estatus+'" ':'';
					sWhere+=typeof(params.eexpiry)!='undefined' && params.eexpiry!='' && params.eexpiry!='All'?' AND expiry="'+params.eexpiry+'" ':'';
					sWhere+=typeof(params.eprice)!='undefined' && params.eprice!='' && params.eprice!='All'?' AND price="'+params.eprice+'" ':'';
					//sPermission = ',(SELECT udelete FROM '+ iw_vouchers+' WHERE userid="'+session_id+'" AND module="7") AS userpermission ';
					console.log(sWhere);
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (id LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" OR expiry LIKE "%'+params.sSearch+'%" OR title LIKE "%'+params.sSearch+'%"OR mobile LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" OR price LIKE "%'+params.sSearch+'%" ) ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_vouchers+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,uid,title,type,price,percentage,DATE_FORMAT(expiry, "%d.%m.%Y") AS expiry,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date,mobile,status,id AS vid'+sPermission+' FROM '+iw_vouchers+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
							});
							
						}
					});
				break;
				// points view
				case 'pointview':
				console.log(pageName);
					sWhere='';
					sPermission = "";
					/*console.log("params.ctitle"+params.ctitle);
					console.log("params.cbranch"+params.cbranch);
					console.log("params.cpoint"+params.cpoint);
					console.log("params.cdate"+params.cdate);
					console.log("params.cstatus"+params.cstatus);*/ 
					sWhere+=typeof(params.ctitle)!='undefined' && params.ctitle!='' && params.ctitle!='All'?' AND title "' + params.ctitle +'" ':'';
					sWhere+=typeof(params.cbranch)!='undefined' && params.cbranch!=''?' AND branch_id="'+params.cbranch+'" ':'';
					sWhere+=typeof(params.cpoint)!='undefined' && params.cpoint!='' && params.cpoint!='All'?' AND points="'+params.cpoint+'" ':'';
					sWhere+=typeof(params.cdate)!='undefined' && params.cdate!='' && params.cdate!='All'?' AND created_date="'+params.cdate+'" ':'';
					sWhere+=typeof(params.cstatus)!='undefined' && params.cstatus!='' && params.cstatus!='All'?' AND status="'+params.cstatus+'" ':'';
					//sPermission = ',(SELECT udelete FROM '+ iw_vouchers+' WHERE userid="'+session_id+'" AND module="7") AS userpermission ';
					console.log(sWhere);
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (id LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" OR points LIKE "%'+params.sSearch+'%" OR title LIKE "%'+params.sSearch+'%" OR mobile LIKE "%'+params.sSearch+'%" OR branch_id LIKE "%'+params.sSearch+'%" ) ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_points+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT p.id,p.title,(SELECT b.title FROM '+iw_branch+' b WHERE b.id=p.branch_id) AS branchname,p.points,DATE_FORMAT(p.created_date, "%d.%m.%Y (%h:%i)") AS created_date,p.branch_id,p.status,p.id AS pid'+sPermission+' FROM '+iw_points+' p WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										
										//console.log(output);
									});
								}
								else
								{
									console.log(err);
								}
							});
							
						}
					});
				break;
				case 'advertiseview':
				console.log(pageName);
					sWhere='';
					sPermission = "";
					/*console.log("params.ctitle"+params.ctitle);
					console.log("params.cdate"+params.cdate);
					console.log("params.cstatus"+params.cstatus);*/ 
					sWhere+=typeof(params.ctitle)!='undefined' && params.ctitle!='' && params.ctitle!='All'?' AND title "' + params.ctitle +'" ':'';
					sWhere+=typeof(params.cdate)!='undefined' && params.cdate!='' && params.cdate!='All'?' AND created_date="'+params.cdate+'" ':'';
					sWhere+=typeof(params.cstatus)!='undefined' && params.cstatus!='' && params.cstatus!='All'?' AND status="'+params.cstatus+'" ':'';
					//sPermission = ',(SELECT udelete FROM '+ iw_vouchers+' WHERE userid="'+session_id+'" AND module="7") AS userpermission ';
					console.log(sWhere);
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (id LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" OR url LIKE "%'+params.sSearch+'%" OR title LIKE "%'+params.sSearch+'%" OR fromdate LIKE "%'+params.sSearch+'%" OR todate LIKE "%'+params.sSearch+'%" ) ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_advertise+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT a.id,a.image,a.title,a.url,a.fromdate,a.todate,DATE_FORMAT(a.created_date, "%d.%m.%Y (%h:%i)") AS created_date,a.status,a.id AS aid'+sPermission+' FROM '+iw_advertise+' a WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									//console.log(res);
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										
										//console.log(output);
									});
								}
								else
								{
									console.log(err);
								}
							});
							
						}
					});
				break;
				case 'notificationview':
				//console.log(pageName);
					sWhere='';
					sPermission = "";
					/*console.log("params.ctitle"+params.ctitle);
					console.log("params.cdate"+params.cdate);
					console.log("params.cstatus"+params.cstatus);
					sWhere+=typeof(params.ctitle)!='undefined' && params.ctitle!='' && params.ctitle!='All'?' AND title "' + params.ctitle +'" ':'';
					sWhere+=typeof(params.cdate)!='undefined' && params.cdate!='' && params.cdate!='All'?' AND created_date="'+params.cdate+'" ':'';
					sWhere+=typeof(params.cstatus)!='undefined' && params.cstatus!='' && params.cstatus!='All'?' AND status="'+params.cstatus+'" ':'';
					console.log(sWhere);*/
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (id LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" OR url LIKE "%'+params.sSearch+'%" OR title LIKE "%'+params.sSearch+'%" OR type LIKE "%'+params.sSearch+'%" ) ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_notification+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT n.id,n.title,n.description,n.type,n.branchtype,n.total,DATE_FORMAT(n.created_date, "%d.%m.%Y (%h:%i)") AS created_date,n.status,n.id AS nid'+sPermission+' FROM '+iw_notification+' n WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									//console.log(res);
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										
										//console.log(output);
									});
								}
								else
								{
									console.log(err);
								}
							});
							
						}
					});
				break;
				//get Debtview details;
				case 'debtview':
				console.log(pageName);
					sWhere='';
					sPermission = "";
					console.log("params.cname"+params.cname);
					console.log("params.mobile"+params.mobile);
					console.log("params.cstatus"+params.cstatus);
					console.log("params.branch"+params.branch);
					console.log("params.badge"+params.badge); 
					sWhere+=typeof(params.cname)!='undefined' && params.cname!='' && params.cname!='All'?' AND (concat (first_name , " " , last_name)LIKE "' + params.cname + '%")':'';
					sWhere+=typeof(params.mobile)!='undefined' && params.mobile!=''?' AND mobile="'+params.mobile+'" ':'';
					sWhere+=typeof(params.cstatus)!='undefined' && params.cstatus!='' && params.cstatus!='All'?' AND status="'+params.cstatus+'" ':'';
					sWhere+=typeof(params.branch)!='undefined' && params.branch!='' && params.branch!='All'?' AND branch="'+params.branch+'" ':'';
					sWhere+=typeof(params.badge)!='undefined' && params.badge!='' && params.badge!='All'?' AND function="'+params.badge+'" ':'';
					/*sPermission = ',(SELECT udelete FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="10") AS userpermission ';
					console.log(sWhere);*/
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (id LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" OR first_name LIKE "%'+params.sSearch+'%" OR last_name LIKE "%'+params.sSearch+'%"OR mobile LIKE "%'+params.sSearch+'%" OR email LIKE "%'+params.sSearch+'%" OR dob LIKE "%'+params.sSearch+'%" ) ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_customer+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,badge,first_name,last_name,DATE_FORMAT(dob, "%d.%m.%Y") AS dob,mobile,DATE_FORMAT(created_date, "%d.%m.%Y (%h:%i)") AS created_date,status,id AS customerid'+sPermission+' FROM '+iw_customer+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										//console.log(output);
									});
								}
							});
							
						}
					});
				break;
				//get Rubble details;
				case 'rubbleview':
				console.log(pageName);
					sWhere='';
					sPermission = "";
					console.log("params.cname"+params.cname);
					console.log("params.cdate"+params.cdate);
					console.log("params.cstatus"+params.cstatus);
					sWhere+=typeof(params.cname)!='undefined' && params.cname!='' && params.cname!='all'?' AND status="'+params.cstatus+'" ':'';
					sWhere+=typeof(params.cdate)!='undefined' && params.cdate!='' && params.cdate!='all'?' AND status="'+params.cstatus+'" ':'';
					sWhere+=typeof(params.cstatus)!='undefined' && params.cstatus!='' && params.cstatus!='all'?' AND status="'+params.cstatus+'" ':'';
					
					/*sPermission = ',(SELECT udelete FROM '+ iw_setting+' WHERE userid="'+session_id+'" AND module="10") AS userpermission ';
					console.log(sWhere);*/
					/* Search Fields Start */
					if(typeof(params.sSearch)!='undefined' && params.sSearch!='')
					{
						sWhere+=' AND (id LIKE "%'+params.sSearch+'%" OR created_date LIKE "%'+params.sSearch+'%" OR cdate LIKE "%'+params.sSearch+'%" OR description LIKE "%'+params.sSearch+'%"OR title LIKE "%'+params.sSearch+'%" ) ';
					}
					/* Search Fields End */
					 con.query('SELECT COUNT(id) AS row_count FROM '+iw_rubble+' WHERE 1=1 '+sWhere, function(err, res){
						if (!err) {
							output.iTotalRecords=res[0].row_count;
							output.iTotalDisplayRecords=res[0].row_count;
							
							/* Sorting Fields Start */
							if(typeof(params.iSortCol_0)!='undefined')
							{
								switch(params.iSortCol_0)
								{
									case '0':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
									case '1':
										sOrderBy=' ORDER BY id '+params.sSortDir_0;
									break;
								}
							}
							/* Sorting Fields End */
							con.query('SELECT id,title,image,description,cdate,DATE_FORMAT(created_date,"%d.%m.%Y (%h:%i)") AS created_date,status,id AS rubbleid FROM '+iw_rubble+' WHERE 1=1 '+sWhere+sOrderBy+sLimit, function(err, res){
								if (!err) {
									console.log(err);
									module.exports.organizeRecords(pageName,res,function(records){
										output.aaData=records;
										callback(output);
										
									});
								}
								else
								{
									console.log(res);
								}
							});
							
						}
					});
				break;
			}
			
		}
	},
	organizeRecords:function(pageName,result,callback){
		var output=new Array();
		switch(pageName)
		{	case 'user':
				result.forEach(function(values,key) {
					var record=new Array();
					Object.keys(values).forEach(function(key) {
						switch(key)
						{
							case 'tabid':
								var uiddata ='<a href="javascript:void(0)" data-userid="'+app.locals.encrypt(values.userid)+'"class=" userdetilsshow open-right-drawer">#'+values.tabid+'</a>';
								record.push(uiddata);
							break;
							case 'branch':
								record.push(values.branch);
							break;
							case 'function':
								record.push(values.function);
							break;
							case 'first_name':
								record.push(values.first_name);
							break;
							case 'last_name':
								record.push(values.last_name);
							break;
							case 'mobile':
								record.push(values.mobile);
							break;
							case 'email':
								record.push(values.email);
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'userid':
								var edit_button_html = "";
								if(values.userpermission == "1")
								{
									var edit_button_html='<a href="edituser?userid='+app.locals.encrypt(values.userid)+'" data-toggle="tooltip" data-original-title="Bearbeiten"><i class="lnr-pencil"></i></a>';
								}
								var button_html=edit_button_html+' <a href="javascript:void(0)" data-userid="'+app.locals.encrypt(values.userid)+'"class=" userdetilsshow open-right-drawer" data-toggle="tooltip" data-original-title="Ansicht"><i class="lnr-link"></i></a>';
								record.push(button_html);
								
							break;

						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			case 'servicetype':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(sno);
								record.push(values.id);
							break;
							case 'service':
								record.push(values.service);
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'servicetypeid':
								var button_html='<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.servicetypeid)+'"  class="edit_servicetype"><i class="lnr-pencil"></i></a> <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.servicetypeid)+'" data-column="id" data-table="'+iw_servicetype+'" data-file="" class="remove_recrod"><i class="lnr-trash"></i></a>';
									if(values.userpermission == 1)
									{
										record.push(button_html);
									}
							break;

						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			case 'extratype':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(sno);
								record.push(values.id);
							break;
							case 'extra':
								record.push(values.extra);
							break;
							case 'nickname':
								record.push(values.nickname);
							break;
							case 'price':

								record.push(numeral(values.price).format('$ 0,.00'));
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'extratypeid':
								var button_html='<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.extratypeid)+'"  class="edit_extratype"><i class="lnr-pencil"></i></a> <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.extratypeid)+'" data-column="id" data-table="'+iw_extratype+'" data-file="" class="remove_recrod"><i class="lnr-trash"></i></a> ';
									if(values.userpermission == 1)
									{
										record.push(button_html);
									}
							break;

						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			case 'zeit':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(sno);
								record.push(values.id);
							break;
							case 'zeit':
								record.push(values.zeit);
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'zeitid':
								var button_html='<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.zeitid)+'"  class="edit_zeit"><i class="lnr-pencil"></i></a> <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.zeitid)+'" data-column="id" data-table="'+iw_zeit+'" data-file="" class="remove_recrod"><i class="lnr-trash"></i></a>';
									if(values.userpermission == 1)
									{
										record.push(button_html);
									}
							break;

						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			case 'category':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(sno);
								record.push(values.id);
							break;
							case 'category':
								record.push(values.category);
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'categoryid':
								var button_html='<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.categoryid)+'"  class="edit_category"><i class="lnr-pencil"></i></a>  <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.categoryid)+'" data-column="id" data-table="'+iw_category+'" data-file="" class="remove_recrod"><i class="lnr-trash"></i></a>';
									if(values.userpermission == 1)
									{
										record.push(button_html);
									}
							break;

						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			case 'customer':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(values.id);
							break;
							case 'badge':
								var img ="";
								if(values.badge == "VIP")
								{
									img='<a href="" data-toggle="tooltip"data-original-title="VIP"><img src="images/VIP.png" class="badge-img"></a>';
								}
								if(values.badge == "Premium")
								{
									img='<a href="" data-toggle="tooltip"data-original-title="VIP"><img src="images/Premium.png" class="badge-img"></a>';
								}
								if(values.badge == "Standard")
								{
									img='<a href="" data-toggle="tooltip"data-original-title="VIP"><img src="images/Standard.png" class="badge-img"></a>';
								}
								record.push(img);
								
							break;
							case 'first_name':
								var viewf_button_html='<a href="customerprofile?custid='+app.locals.encrypt(values.id)+'" data-toggle="tooltip" data-original-title="Bearbeiten">'+values.first_name+'</a>';
								record.push(viewf_button_html);
							break;
							case 'last_name':
								var viewl_button_html='<a href="customerprofile?custid='+app.locals.encrypt(values.id)+'" data-toggle="tooltip" data-original-title="Bearbeiten">'+values.last_name+'</a>';
								record.push(viewl_button_html);
								record.push("");
							break;
							case 'dob':
								record.push(values.dob);
							break;
							case 'mobile':
								record.push(values.mobile);
								record.push("");
								record.push("");
								record.push("");
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							// case 'customerid':
							// 	var edit_button_html = "";
							// 	if(values.userpermission == "1")
							// 	{
							// 		var edit_button_html='<a href="editcustomer?custid='+app.locals.encrypt(values.customerid)+'" data-toggle="tooltip" data-original-title="Bearbeiten"><i class="lnr-pencil"></i></a>';
							// 	}
							// 	//var button_html=edit_button_html+' <a href="javascript:void(0)" data-userid="'+app.locals.encrypt(values.custid)+'"class=" userdetilsshow open-right-drawer" data-toggle="tooltip" data-original-title="Ansicht"><i class="lnr-link"></i></a>';
							// 	record.push(edit_button_html);
								
							// break;
						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			case 'permit':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(sno);
								record.push(values.id);
							break;
							case 'name':
								record.push(values.name);
							break;
							case 'idcard':

								record.push(values.idcard);
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'permitid':
								var button_html='<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.permitid)+'"  class="edit_permit"><i class="lnr-pencil"></i></a> <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.permitid)+'" data-column="id" data-table="'+iw_permitperson+'" data-file="" class="remove_recrod"><i class="lnr-trash"></i></a>';
									/*if(values.userpermission == 1)
									{
										record.push(button_html);
									}*/
									record.push(button_html);
							break;

						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			case 'hair':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(sno);
								record.push(values.id);
							break;
							case 'hair':
								record.push(values.hair);
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'hairid':
								var button_html='<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.hairid)+'"  class="edit_hair"><i class="lnr-pencil"></i></a> <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.hairid)+'" data-column="id" data-table="'+iw_hair+'" data-file="" class="remove_recrod"><i class="lnr-trash"></i></a>';
									/*if(values.userpermission == 1)
									{
										record.push(button_html);
									}*/
									record.push(button_html);
							break;

						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			case 'eyes':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(sno);
								record.push(values.id);
							break;
							case 'eyes':
								record.push(values.eyes);
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'eyesid':
								var button_html='<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.eyesid)+'"  class="edit_eyes"><i class="lnr-pencil"></i></a> <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.eyesid)+'" data-column="id" data-table="'+iw_eyes+'" data-file="" class="remove_recrod"><i class="lnr-trash"></i></a>';
									/*if(values.userpermission == 1)
									{
										record.push(button_html);
									}*/
									record.push(button_html);
							break;

						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			case 'shoes':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(sno);
								record.push(values.id);
							break;
							case 'shoes':
								record.push(values.shoes);
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'shoesid':
								var button_html='<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.shoesid)+'"  class="edit_shoes"><i class="lnr-pencil"></i></a> <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.shoesid)+'" data-column="id" data-table="'+iw_shoes+'" data-file="" class="remove_recrod"><i class="lnr-trash"></i></a>';
									/*if(values.userpermission == 1)
									{
										record.push(button_html);
									}*/
									record.push(button_html);
							break;

						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			case 'cloth':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(sno);
								record.push(values.id);
							break;
							case 'cloth':
								record.push(values.cloth);
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'clothid':
								var button_html='<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.clothid)+'"  class="edit_cloth"><i class="lnr-pencil"></i></a> <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.clothid)+'" data-column="id" data-table="'+iw_cloth+'" data-file="" class="remove_recrod"><i class="lnr-trash"></i></a>';
									/*if(values.userpermission == 1)
									{
										record.push(button_html);
									}*/
									record.push(button_html);
							break;

						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			case 'civilstatus':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(sno);
								record.push(values.id);
							break;
							case 'civilstatus':
								record.push(values.civilstatus);
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'civilstatusid':
								var button_html='<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.civilstatusid)+'"  class="edit_civilstatus"><i class="lnr-pencil"></i></a> <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.civilstatusid)+'" data-column="id" data-table="'+iw_civilstatus+'" data-file="" class="remove_recrod"><i class="lnr-trash"></i></a>';
									/*if(values.userpermission == 1)
									{
										record.push(button_html);
									}*/
									record.push(button_html);
							break;

						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			case 'privatepart':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(sno);
								record.push(values.id);
							break;
							case 'privatepart':
								record.push(values.privatepart);
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'privatepartid':
								var button_html='<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.privatepartid)+'"  class="edit_privatepart"><i class="lnr-pencil"></i></a> <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.privatepartid)+'" data-column="id" data-table="'+iw_privatepart+'" data-file="" class="remove_recrod"><i class="lnr-trash"></i></a>';
									/*if(values.userpermission == 1)
									{
										record.push(button_html);
									}*/
									record.push(button_html);
							break;

						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			case 'bust':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(sno);
								record.push(values.id);
							break;
							case 'bust':
								record.push(values.bust);
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'bustid':
								var button_html='<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.bustid)+'"  class="edit_bust"><i class="lnr-pencil"></i></a> <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.bustid)+'" data-column="id" data-table="'+iw_bust+'" data-file="" class="remove_recrod"><i class="lnr-trash"></i></a>';
									/*if(values.userpermission == 1)
									{
										record.push(button_html);
									}*/
									record.push(button_html);
							break;

						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			case 'nationality':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(sno);
								record.push(values.id);
							break;
							case 'nationality':
								record.push(values.nationality);
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'nationalityid':
								var button_html='<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.nationalityid)+'"  class="edit_nationality"><i class="lnr-pencil"></i></a> <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.nationalityid)+'" data-column="id" data-table="'+iw_nationality+'" data-file="" class="remove_recrod"><i class="lnr-trash"></i></a>';
									/*if(values.userpermission == 1)
									{
										record.push(button_html);
									}*/
									record.push(button_html);
							break;

						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			case 'vouchers':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(values.id);
							break;
							case 'uid':
								record.push(values.uid);
							break;
							case 'title':
							var uiddata ='<a href="javascript:void(0)" data-vouchersid="'+app.locals.encrypt(values.id)+'"class=" vouchersdetilsshow open-right-drawer">'+values.title+'</a>';
								record.push(uiddata);
							break;
							case 'type':
								if(values.type==1)
								{
									record.push("CHF"+values.price);
								}
								else
								{
									record.push(values.percentage+"%");	
								}
								
							break;
							case 'expiry':
								record.push(values.expiry);	
							break;
							case 'created_date':
								record.push(values.created_date);
								record.push(values.created_date);
							break;
							case 'mobile':
								record.push(values.mobile);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'vid':
								/*var edit_button_html = "";
								if(values.userpermission == "1")
								{*/
									var edit_button_html='<a href="editvouchers?vouchersid='+app.locals.encrypt(values.vid)+'" data-toggle="tooltip" data-original-title="Bearbeiten"><i class="lnr-pencil"></i></a>';

							/*	}*/
								//var button_html=edit_button_html+' <a href="javascript:void(0)" data-userid="'+app.locals.encrypt(values.custid)+'"class=" userdetilsshow open-right-drawer" data-toggle="tooltip" data-original-title="Ansicht"><i class="lnr-link"></i></a>';
								record.push(edit_button_html);
								
							break;
						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			case 'pointview':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(values.id);
							break;
							case 'title':
								record.push(values.title);
							break;
							case 'branchname':
								record.push(values.branchname);	
							break;
							case 'points':
								record.push(values.points);	
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'pid':
								/*var edit_button_html = "";
								if(values.userpermission == "1")
								{*/
									var edit_button_html='<a href="editPoint?pointid='+app.locals.encrypt(values.pid)+'" data-toggle="tooltip" data-original-title="Bearbeiten"><i class="lnr-pencil"></i></a> ';

							/*	}*/
								//var button_html=edit_button_html+' <a href="javascript:void(0)" data-userid="'+app.locals.encrypt(values.custid)+'"class=" userdetilsshow open-right-drawer" data-toggle="tooltip" data-original-title="Ansicht"><i class="lnr-link"></i></a>';
								record.push(edit_button_html);
								
							break;
						}
					});
					output.push(record);
					
				});
			break;
			case 'advertiseview':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(values.id);
							break;
							case 'image':
							img='<a href="'+values.url+'" class=""><img src="'+file_link_advertise_fornt+values.image+'" alt="" width="200" height="100"></a>';
								
								record.push(img);	
								
							break;
							case 'title':
								record.push(values.title);	
							break;
							case 'url':
								record.push('<a href="'+values.url+'" target="_blank" data-toggle="tooltip" data-original-title="Url"><i class="lnr-link"></i></a>');	
							break;
							case 'fromdate':
								record.push(values.fromdate+'-'+values.todate);	
							break;
							case 'created_date':
								record.push(values.created_date);
							break;
							
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'aid':
								/*var edit_button_html = "";
								if(values.userpermission == "1")
								{*/
									var edit_button_html='<a href="editAdvertise?addid='+app.locals.encrypt(values.aid)+'" data-toggle="tooltip" data-original-title="Bearbeiten"><i class="lnr-pencil"></i></a><a href="javascript:void(0)" class="advertisedetilsshow" data-toggle="tooltip" data-original-title="Löschen" data-advertiseid="'+app.locals.encrypt(values.aid)+'"><i class="lnr-trash"></i></a>';
									

									/*<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.aid)+'" data-column="id" data-table="'+iw_advertise+'" data-file="" class="remove_recrod"><i class="lnr-trash"></i></a>*/

							/*	}*/
								//var button_html=edit_button_html+' <a href="javascript:void(0)" data-userid="'+app.locals.encrypt(values.custid)+'"class=" userdetilsshow open-right-drawer" data-toggle="tooltip" data-original-title="Ansicht"><i class="lnr-link"></i></a>';
								record.push(edit_button_html);
								
							break;
						}
					});
					output.push(record);
					
				});
			break;
			case 'notificationview':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(values.id);
							break;
							case 'title':
								record.push('<strong>Benachrichtigung '+sno+'</strong><br>'+values.title);
							break;
							case 'title':
								record.push(values.title);	
							break;
							case 'type':
							var tct ="";
							if(values.type=="branch" && values.branchtype=="all")
							{

								tct ="Alle Filialen";
							}
							if(values.type=="branch" && values.branchtype=="cust")
							{

								tct ="Brauch Filialen";
							}
							if(values.type=="customer" && values.branchtype=="all")
							{

								tct ="Alle Kunden";
							}
							if(values.type=="customer" && values.branchtype=="cust")
							{

								tct ="Brauch Kunden";
							}
							record.push(tct);	
							break;
							case 'total':
								record.push(values.total);	
							break;
							case 'created_date':
								record.push(values.created_date);
								
							break;
							/*case 'nid':*/
								/*var edit_button_html = "";
								if(values.userpermission == "1")
								{*/
									/*var edit_button_html='<a href="editNotification?noteid='+app.locals.encrypt(values.nid)+'" data-toggle="tooltip" data-original-title="Bearbeiten"><i class="lnr-pencil"></i></a>';*/
									

									/*<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Löschen" data-id="'+app.locals.encrypt(values.aid)+'" data-column="id" data-table="'+iw_advertise+'" data-file="" class="remove_recrod"><i class="lnr-trash"></i></a>*/

							/*	}*/
								//var button_html=edit_button_html+' <a href="javascript:void(0)" data-userid="'+app.locals.encrypt(values.custid)+'"class=" userdetilsshow open-right-drawer" data-toggle="tooltip" data-original-title="Ansicht"><i class="lnr-link"></i></a>';
								/*record.push(edit_button_html);
								
							break;*/
						}
					});
					output.push(record);
					
				});
			break;
			case 'debtview':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(values.id);
							break;
							case 'badge':
								var img ="";
								if(values.badge == "VIP")
								{
									img='<a href="" data-toggle="tooltip"data-original-title="VIP"><img src="images/VIP.png" class="badge-img"></a>';
								}
								if(values.badge == "Premium")
								{
									img='<a href="" data-toggle="tooltip"data-original-title="VIP"><img src="images/Premium.png" class="badge-img"></a>';
								}
								if(values.badge == "Standard")
								{
									img='<a href="" data-toggle="tooltip"data-original-title="VIP"><img src="images/Standard.png" class="badge-img"></a>';
								}
								record.push(img);
								
							break;
							case 'first_name':
								var viewf_button_html='<a href="customerprofiledebt?custid='+app.locals.encrypt(values.id)+'" data-toggle="tooltip" data-original-title="Bearbeiten">'+values.first_name+'</a>';
								record.push(viewf_button_html);
							break;
							case 'last_name':
								var viewl_button_html='<a href="customerprofiledebt?custid='+app.locals.encrypt(values.id)+'" data-toggle="tooltip" data-original-title="Bearbeiten">'+values.last_name+'</a>';
								record.push(viewl_button_html);
								record.push("-");
							break;
							
							case 'dob':
								record.push(values.dob);
								
							break;
							case 'mobile':
								record.push(values.mobile);
								record.push("-");
								record.push("-");
								record.push("-");
								record.push("-");
							break;
							
							case 'created_date':
								record.push(values.created_date);
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							// case 'customerid':
							// 	var edit_button_html = "";
							// 	if(values.userpermission == "1")
							// 	{
							// 		var edit_button_html='<a href="editcustomer?custid='+app.locals.encrypt(values.customerid)+'" data-toggle="tooltip" data-original-title="Bearbeiten"><i class="lnr-pencil"></i></a>';
							// 	}
							// 	//var button_html=edit_button_html+' <a href="javascript:void(0)" data-userid="'+app.locals.encrypt(values.custid)+'"class=" userdetilsshow open-right-drawer" data-toggle="tooltip" data-original-title="Ansicht"><i class="lnr-link"></i></a>';
							// 	record.push(edit_button_html);
								
							// break;
						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			//rublle data
			case 'rubbleview':
				//console.log(result);
				var sno = 0;
				result.forEach(function(values,key) {
					var record=new Array();
					sno = sno+1;
					Object.keys(values).forEach(function(key) {
						
						switch(key)
						{
							case 'id':
								record.push(values.id);
							break;
							case 'title':
							var vhtml ='<div class="widget-content p-0"><div class="widget-content-wrapper"><div class="widget-content-left mr-3"><div class="widget-content-left"><img src="'+file_link_rubble_fornt+values.image+'" alt=""></div></div><div class="widget-content-left flex2"><div class="widget-heading"><a href="javascript:void(0)" class="rubbleSeparate" data-rubbleid="'+app.locals.encrypt(values.rubbleid)+'">'+values.title+'</a></div></div></div></div>';
								record.push(vhtml);
							break;
							case 'cdate':
								record.push(values.cdate);
							break;
							case 'created_date':
								record.push(values.created_date);
								record.push("");
							break;
							case 'status':
								var actdata="";
								if(values.status=="1")
								{
									actdata='<div class="badge badge-success">aktiv</div>';	
								}
								if(values.status=="0")
								{
									actdata='<div class="badge badge-danger">inaktiv</div>';
								}
								record.push(actdata);
							break;
							case 'rubbleid':
								var edit_button_html='<a href="editrubble?rubbleid='+app.locals.encrypt(values.rubbleid)+'" data-toggle="tooltip" data-original-title="Bearbeiten"><i class="lnr-pencil"></i></a>';
								record.push(edit_button_html);
							break;

							// case 'customerid':
							// 	var edit_button_html = "";
							// 	if(values.userpermission == "1")
							// 	{
							// 		var edit_button_html='<a href="editcustomer?custid='+app.locals.encrypt(values.customerid)+'" data-toggle="tooltip" data-original-title="Bearbeiten"><i class="lnr-pencil"></i></a>';
							// 	}
							// 	//var button_html=edit_button_html+' <a href="javascript:void(0)" data-userid="'+app.locals.encrypt(values.custid)+'"class=" userdetilsshow open-right-drawer" data-toggle="tooltip" data-original-title="Ansicht"><i class="lnr-link"></i></a>';
							// 	record.push(edit_button_html);
								
							// break;
						}
					});
					output.push(record);
					//console.log(output);
				});
			break;
			
		}

		callback(output);
	}
};
/** Change date format **/
function changeDateFormat(date){ 
	if(typeof(date)!='undefined' && date!=''){
		var d = date;
		var month = d.getMonth()+1;
		var day = d.getDate();

		var output = (day<10 ? '0' : '') + day + "-" 
              + (month<10 ? '0' : '') + month + '-'
              + d.getFullYear();

		return output;
	}
	else return false;
}
/** Encrypt **/
app.locals.encrypt = function(id){
	const encryptedString = cryptr.encrypt(id);
	return encryptedString;	
}
/** Decrypt **/
app.locals.decrypt = function(id){
	const decryptedString = cryptr.decrypt(id);
	return decryptedString;	
}