/* System Modules */
global.express = require('express');
global.app = express();
global.ejs = require('ejs');
global.nodemailer = require('nodemailer');
global.bodyParser = require('body-parser');
global.os = require("os");
var mysql = require('mysql');
global.site_url = os.hostname();
global.sitePath=__dirname; 
global.Cryptr = require('cryptr');
global.md5 = require('md5');
global.fs = require('fs');
global.moment = require('moment');
var cors = require('cors')
app.use(cors());
global.http = require('http').createServer(app);


global.OneSignal = require('onesignal-node');  
var socketmobileapi = require('./libraries/socketmobileapi'); 
global.rad2deg = require('rad2deg');
global.deg2rad = require('deg2rad');
global.datetime = require('node-datetime');
global.randomstring = require("randomstring");
global.fileGetContents = require('file-get-contents');
var fss = require('fs');
const cert = fss.readFileSync('./ssl/certificate.crt');
const ca = fss.readFileSync('./ssl/ca_bundle.crt');
const key = fss.readFileSync('./ssl/private.key');
//global.http = require('http').createServer(app);
global.https = require('https').createServer({cert, ca, key},app);
if(site_url=='lifo-server' || site_url=='lifo10')
{
	global.io= require('socket.io')(http);
}
else
{
	global.io= require('socket.io')(https);
}


// Twilio Credentials

const accountSid = 'ACf2cfe31e874676f6634ff8441d1f02be';
const authToken = 'f0b2f8e2f87f7e32803e71e74851e462';

// require the Twilio module and create a REST client
global.client = require('twilio')(accountSid, authToken); 
global.OneSignalClient = new OneSignal.Client({      
   userAuthKey: 'YWEwMmM3ZDktYWFkZC00MjM1LWE4NDMtMGVhM2I5Mzk2ZGUz',      
   // note that "app" must have "appAuthKey" and "appId" keys      
   app: { appAuthKey: 'ZjlhZDFlMjItZDRiYS00ZDMwLTlhZGYtYzdkMmYyNTJlY2Fm', appId: 'd3c9ed32-5f7d-423c-940a-32f69a68e321' }      
});


global.current_timestamp=new Date().getTime();

/* Mail Configurations */

if(site_url=='lifo-server'){
	global.fromEmail='testermodule@gmail.com';
	/** Live **/
	global.transporter = nodemailer.createTransport({
		host: 'smtp.gmail.com',
		port: 465,
		secure: true, // true for 465, false for other ports
		auth: {
			user: fromEmail,
			pass: 'l!fo@123'
		}
	}); 
} else{
	global.fromEmail='testermodule@gmail.com';
	/** Live **/
	global.transporter = nodemailer.createTransport({
		host: 'smtp.gmail.com',
		port: 587,
		secure: false, // true for 465, false for other ports
		auth: {
			user: fromEmail,
			pass: 'l!fo@123'
		}
	}); 
}

/* User defined Modules */
global.sql = require('./libraries/sqlfunctions');

const app_exp = require("express")();
app_exp.set("view engine", "pug");
app_exp.use(bodyParser.urlencoded({extended: false}));

/* Mysql connection */
//console.log(site_url);
if(site_url=='ip-172-31-22-11' || site_url=='Lifo-10' || site_url=='lifo10'){
	global.image_url = "http://localhost/images/";	
	global.con = mysql.createConnection({
	  host: "192.168.1.99",
	  user: "root",
	  password: "",
	  port:3306,
	  database: "qlic",
	  charset: "utf8_general_ci"
	});
	//console.log(con);
} else{
	// global.con = mysql.createConnection({
	  // host: "localhost",
	  // user: "root",
	  // password: "1iCar@2019",
	  // port:3306,
	  // database: "icar"
	// });
	global.image_url = "http://qlic.ch/images/";
	global.con = mysql.createConnection({
	  host: "localhost",
	  user: "root",
	  password: "qlic@2019",
	  port:3306,
	  database: "qlic"
	});
}

global.cryptr = new Cryptr('lifo@123#');

/** Request Header Setting **/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// set the view engine to ejs
app.use(express.static(__dirname + '/assets'));

//app.use('/admin', express.static('/assets/admin_assets'));
app.set('view engine', 'ejs');

/*app.all("*", function (req, res, next) {  
	
	global.site_url=req.protocol + '://' + req.get('host')+'/';
	if(site_url=='http://192.168.1.134:2020/')
		global.socket_url='http://192.168.1.134:2020/';
	else
		global.socket_url='http://digitally.online/';
	global.admin_url=req.protocol + '://' + req.get('host')+'/admin/';
    global.fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	global.currentURL=req.originalUrl;
        next()
});

app.get('/',function(req, res, next){next();}, function(req, res){
	res.redirect('http://digitally.online');
});*/
/** Encrypt **/
app.locals.encrypt = function(id){
	const encryptedString = cryptr.encrypt(id);
	return encryptedString;	
}
/** Decrypt **/
app.locals.decrypt = function(id){
	try{
	const decryptedString = cryptr.decrypt(id);
	return decryptedString;	
	}
	catch(ex){
		return '';
	}
}

app.use(function(req, res, next){
	var currentURL=req.protocol+'://'+req.get('host');
	if(currentURL=='https://qlic.ch')
		res.redirect('http://qlic.ch/');	
  res.status(404);
  // respond with html page
  if (req.accepts('html')) {
    res.render('pages/404', { url: req.url });
    return;
  }
  // respond with json
  if (req.accepts('json')) {
    res.send({ error: 'Not found' });
    return;
  }
  // default to plain-text. send()
  res.type('txt').send('Not found');
});
app.use(function(req, res, next){
  res.status(500);
  // respond with html page
  if (req.accepts('html')) {
    res.render('pages/500', { url: req.url });
    return;
  }
  // respond with json
  if (req.accepts('json')) {
    res.send({ error: 'Internal Server Error' });
    return;
  }
  // default to plain-text. send()
  res.type('txt').send('Internal Server Error');
});
global.nl2br= function(str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
};
global.addslashes=function(string) {
    return string.replace(/\\/g, '\\\\').
        replace(/\u0008/g, '\\b').
        replace(/\t/g, '\\t').
        replace(/\n/g, '\\n').
        replace(/\f/g, '\\f').
        replace(/\r/g, '\\r').
        replace(/'/g, '\\\'').
        replace(/"/g, '\\"');
};
global.mysql_real_escape_string=function (str) {
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
        switch (char) {
            case "\0":
                return "\\0";
            case "\x08":
                return "\\b";
            case "\x09":
                return "\\t";
            case "\x1a":
                return "\\z";
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\"":
            case "'":
            case "\\":
            case "%":
                return "\\"+char; // prepends a backslash to backslash, percent,
                                  // and double/single quotes
        }
    });
}
global.arrayColumn = function(array, columnName) {
    return array.map(function(value,index) {
        return value[columnName];
    });
}

global.encrypt = function(id){
	const encryptedString = cryptr.encrypt(id);
	return encryptedString;	
}
/** Decrypt **/
global.decrypt = function(id){
	try{
	const decryptedString = cryptr.decrypt(id);
	return decryptedString;	
	}
	catch(ex){
		return '';
	}
};
global.inArray=function(needle, haystack) {
		var length = haystack.length;
		for(var i = 0; i < length; i++) {
			if(haystack[i] == needle) return true;
		}
		return false;
};
global.arrayColumn = function(array, columnName) {
    return array.map(function(value,index) {
        return value[columnName];
    });
};
/** Check not empty **/
global.checkNotEmpty=function(val){
	if(val!='' && val!=0 && typeof(val)!='undefined' && typeof(val)!='null' && val!=null)
		return true;
	else return false;
};
global.returnEmpty = function(val) {
	if(val!='' && typeof(val)!='undefined' && typeof(val)!='null' && val!=null)
		return val;
	else return '';
}



/* Socket Connection */
io.on('connection', function(socket){
	
	socket.on('online_status', function(data){
		userID=(data.user_id);
		online_status=(data.online_status);
		console.log(userID+ ' is '+(online_status==1?'Online':'Offline'));
		sql.updateValues(digi_user,{online_status:online_status},{id:userID},function(){
		});
	});
	/* On Message arrival */
	socket.on('typing', function(data){
		data.request_id=app.locals.decrypt(data.request_id);
		data.user_id=app.locals.decrypt(data.user_id);
		data.user_name=app.locals.decrypt(data.user_name);
		io.emit('typing', data);
	});
	socket.on('typing_mobile', function(data){
		data.request_id=(data.request_id);
		data.user_id=(data.user_id);
		data.user_name=(data.user_name);
		io.emit('typing', data);
	});
	socket.on('qlic_api', function(data){
		console.log(data);
		socketmobileapi.ajaxMobileApi(data,function(delivery,ret){
			socket.emit(delivery, ret);
		});
	});
});


//console.log(site_url);
//passEncode('password');
if(site_url=='lifo-server' || site_url=='lifo10')
{
	http.listen(3001, function(){console.log('listening on *:3001');});
}
else
{
	
	//global.io= require('socket.io')(http);
	console.log('Live Server');
	https.listen(443);
	//app.listen(80);
	//http.listen(3001, function(){console.log('listening on *:443');});
}
	


