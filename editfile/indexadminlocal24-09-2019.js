/* System Modules */
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var dateFormat = require('dateformat');
var os = require("os");
var url = require('url');
var mysql = require('mysql');
var fileupload = require('express-fileupload');
/* User defined Modules */
global.sql = require('./libraries/sqlfunctions');
var api = require('./libraries/api');
var mobileapi = require('./libraries/mobileapi');
//var socketmobileapi = require('./libraries/socketmobileapi');
var page = require('./libraries/pages');
global.site = require('./libraries/site');
global.dataTable = require('./libraries/dataTable');
//var slashes = require('slashes');
global.rad2deg = require('rad2deg');
global.deg2rad = require('deg2rad');
global.datetime = require('node-datetime');
var Cryptr = require('cryptr');
global.cryptr = new Cryptr('lifo@123#');
global.md5 = require('md5');
global.randomstring = require("randomstring");
//global.fileGetContents = require('file-get-contents');
// Twilio Credentials
const accountSid = 'ACf2cfe31e874676f6634ff8441d1f02be';
const authToken = 'f0b2f8e2f87f7e32803e71e74851e462';

// require the Twilio module and create a REST client
global.client = require('twilio')(accountSid, authToken);
//console.log(reverseMd5('49f68a5c8493ec2c0bf489821c21fc3b').str); //returns 'hi'
//var ip = require("ip");
global.app = express();
global.site_url = os.hostname();
global.http = require('http').createServer(app);
global.numeral = require('numeral');
numeral.register('locale', 'fr-ch', {
	delimiters: {
		thousands: '\'',
		decimal: '.'
	},
	abbreviations: {
		thousand: 'k',
		million: 'm',
		billion: 'b',
		trillion: 't'
	},
	ordinal : function (number) {
		return number === 1 ? 'er' : 'e';
	},
	currency: {
		symbol: 'CHF'
	}
});
numeral.locale('fr-ch');
numeral().format('0,0');
/** Encrypt **/
app.locals.encrypt = function(id){
	const encryptedString = cryptr.encrypt(id);
	return encryptedString;	
}
/** Decrypt **/
app.locals.decrypt = function(id){
	try{
	const decryptedString = cryptr.decrypt(id);
	return decryptedString;	
	}
	catch(ex){
		return '';
	}
}

/* Mysql connection */
//console.log(site_url);
if(site_url=='ip-172-31-22-11' || site_url=='Lifo-10' || site_url=='lifo10' || site_url=='lifo-server'){
	global.image_url = "http://localhost/images/";	
	global.con = mysql.createConnection({
	  host: "192.168.1.134",
	  user: "root",
	  password: "lifo@123",
	  port:3306,
	  database: "qlic",
	  charset: "utf8_general_ci"
	});
	//console.log(con);
} else{
	// global.con = mysql.createConnection({
	  // host: "localhost",
	  // user: "root",
	  // password: "1iCar@2019",
	  // port:3306,
	  // database: "icar"
	// });
	global.image_url = "http://18.185.9.203/images/";
	global.con = mysql.createConnection({
	  host: "localhost",
	  user: "root",
	  password: "qlic@2019",
	  port:3306,
	  database: "qlic"
	});
}
//console.log(con);
/* var ccon = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "qlic"
}); */

const app_exp = require("express")();
app_exp.set("view engine", "pug");
app_exp.use(require("body-parser").urlencoded({extended: false}));
/** Initialize session **/
app.use(session({
    secret: 'qlicApp',
	// store: sessionStore, // connect-mongo session store
    proxy: true,
    resave: true,
    saveUninitialized: true
}));
/** Initialize file upload **/
app.use(fileupload());
/** Request Header Setting **/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// set the view engine to ejs
app.use(express.static(__dirname + '/assets'));

//app.use('/admin', express.static('/assets/admin_assets'));
app.set('view engine', 'ejs');

//app.all("*", function (req, res, next) { 
// app.get('/',function(req, res, next){next();}, function(req, res){
// 	global.domain = req.headers.host;
// 	console.log(req.originalUrl);

// 	if(domain == "app.qlic.ch" && req.originalUrl=='/')
// 	{
// 		console.log("admin...1");
// 		//res.redirect('/admin')
// 		res.render('pages/index_1');
		
// 	}
// 	else if(domain == "store.qlic.ch" && req.originalUrl=='/')
// 	{
// 		console.log("store...1");
// 		res.redirect('/shopadmin')
// 		//location('http://store.qlic.ch/shopadmin');
		
// 	}
// 	else if(domain == "qlic.ch" && req.originalUrl=='/')
// 	{
// 		console.log("qlic..1");
// 		res.render('pages/index');
// 		//res.redirect('http://www.lifotechnologies.com');
		
// 	}
// 	//next();
// });
/**login page **/

app.use(function(req, res, next){
global.siteadmin_url=req.protocol + '://qlic.ch/';

global.site_redirect_url=req.protocol + '://' + req.get('host')+'/';

next();
});
app.get('/',function(req, res) {
	res.render('pages/index');
});
app.get('/about',function(req, res) {
	res.render('pages/about');
});
app.get('/agb',function(req, res) {
	res.render('pages/agb');
});
app.get('/anb',function(req, res) {
	res.render('pages/anb');
});
app.get('/dsgvo',function(req, res) {
	res.render('pages/dsgvo');
});
app.get('/forusers',function(req, res) {
	res.render('pages/forusers');
});
app.get('/impressum',function(req, res) {
	res.render('pages/impressum');
});
app.get('/kontakt',function(req, res) {
	res.render('pages/kontakt');
});
app.get('/partners',function(req, res) {
	res.render('pages/partners');
});
/**login page Shop Admin **/
app.get('/admin',function(req, res) {
	adminLoginId=req.session.admin_id!=undefined?(req.session.admin_id!=''?req.session.admin_id:''):'';
	//console.log(adminLoginId);
	if(adminLoginId!=''){
		res.render('admin/pages/dashboard');		
	} else {
		res.render('admin/pages/index');
	}
});
app.get('/shopadmin',function(req, res) {
	shopLoginId=req.session.shopadmin_id!=undefined?(req.session.shopadmin_id!=''?req.session.shopadmin_id:''):'';
	if(shopLoginId!=''){
		res.render('shopadmin/pages/dashboard');		
	} else {
		res.render('shopadmin/pages/index');
	}
});
/**login page Branch Admin **/
app.get('/branchadmin',function(req, res) {
	branchLoginId=req.session.branchadmin_id!=undefined?(req.session.branchadmin_id!=''?req.session.branchadmin_id:''):'';
	if(branchLoginId!=''){
		res.render('branchadmin/pages/dashboard');		
	} else {
		res.render('branchadmin/pages/index');
	}
});
// Dashboard page 
app.get('/admin/dashboard',requireAdminLogin, function(req, res){
	/** Getting marke**/
	res.render('admin/pages/dashboard');
});
app.get('/admin/forgotpassword', function(req, res){
	/** Getting marke**/
	res.render('admin/pages/forgotpassword');
});
app.get('/admin/resetpassword', function(req, res){
	/** Getting marke**/
	var data = {'uid':req.query.uid};
	res.render('admin/pages/resetpassword',data);
});
/*partners*/
app.get('/admin/partnerrequest',requireAdminLogin, function(req, res){
	var data = {'page_js':'Load_DataTables("partnerrequest")'};
	res.render('admin/pages/partnerrequest',data);
});
// Shop Dashboard page 
app.get('/shopadmin/dashboard',requireShopAdminLogin, function(req, res){
	/** Getting marke**/
	res.render('shopadmin/pages/dashboard');
});
// Branch Admin Dashboard page
app.get('/branchadmin/dashboard',requireBranchAdminLogin, function(req, res){
	/** Getting marke**/
	res.render('branchadmin/pages/dashboard');
});
app.get('/admin/category',requireAdminLogin, function(req, res){
	var data = {'page_js':'Load_DataTables("category")'};
	res.render('admin/pages/category',data);
});
app.get('/admin/addcategory',requireAdminLogin, function(req, res){
	/** createing  Category**/
	result=[[]];
	var data={details:result};
	res.render('admin/pages/addcategory',data);
});
app.get('/admin/shop',requireAdminLogin, function(req, res){
	/** Getting shop**/
	site.shopList(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.details)};
			res.render('admin/pages/shop',data); 
		}
		else
			res.redirect('admin/pages/shop');
	});	
});
/** Branch Admin View Staff**/
app.get('/branchadmin/staff',requireBranchAdminLogin, function(req, res){
	var dataParams=[{"name": "branch_id", "value":app.locals.encrypt(req.session.bbranch_id)}];
	var data = {'page_js':'Load_DataTables("bstaff",'+JSON.stringify(dataParams)+')','shop_id':app.locals.encrypt(req.session.bshop_id),'branchId':app.locals.encrypt(req.session.bbranch_id)};
	res.render('branchadmin/pages/staff',data);
}); 
/** Branch Admin Add Branch Staff**/
app.get('/branchadmin/addStaff',requireBranchAdminLogin, function(req, res) {
	site.staffAdd(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.staff),branch:(response.branch),'shop_id':app.locals.encrypt(req.session.bshop_id),'branchId':app.locals.encrypt(req.session.bbranch_id)};
			res.render('branchadmin/pages/addStaff',data); 
		}
		else
			res.redirect('branchadmin/pages/staff');
	});
});
/** Branch Admin Edit Branch Staff**/
app.get('/branchadmin/editStaff',requireBranchAdminLogin, function(req, res) {
	site.staffEdit(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.staff),branch:(response.branch),'shop_id':app.locals.encrypt(req.session.bshop_id),'branchId':app.locals.encrypt(req.session.bbranch_id)};
			res.render('branchadmin/pages/addStaff',data); 
		}
		else
			res.redirect('branchadmin/pages/staff');
	});
});
/** Branch Admin Edit setings**/
app.get('/branchadmin/editsettings',requireBranchAdminLogin, function(req, res) {
	site.branchSetingsDetails(req,function(response){
		//console.log(response);
		if(response.status=='success')
		{ 
			var data = {details:(response.details)};
			res.render('branchadmin/pages/settings',data); 
		}
		else
			res.redirect('branchadmin/pages/dashboard');
	});
});
/** view shop**/
app.get("/shopDetails-view",requireAdminLogin, function(req, res) {
	site.shopDetailsView(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.shop)};
			res.render('pages/shopDetails-view',data); 
		}
		else
			res.redirect('pages/shop');
	});
});
/** Creating shop**/
app.get("/admin/addshop",requireAdminLogin, function(req, res) {
	site.shopDetailsAdd(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.shop),category:(response.cate),shoptime:(response.shoptime)};
			res.render('admin/pages/addshop',data); 
		}
		else
			res.redirect('admin/pages/shop');
	});
});
/** Editing shop**/
app.get('/admin/editShop',requireAdminLogin, function(req, res) {
	site.shopDetailsEdit(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.shop),category:(response.cate),shoptime:(response.shoptime)};
			res.render('admin/pages/addshop',data); 
		}
		else
			res.redirect('admin/pages/shop');
	});
});
/*Add Product*/
app.get("/admin/addProduct",requireAdminLogin, function(req, res) {
		result=[[]];
		resultp=[];
		var data={details:result,product_image:resultp,'shop_id':req.query.shopId};
		res.render('admin/pages/addproduct',data);
});

/** Editing product**/
app.get('/admin/editProduct',requireAdminLogin, function(req, res) {
	site.productDetailsEdit(req,function(response){

		if(response.status=='success')
		{ 
			var data = {details:(response.product),product_image:(response.product_image),'shop_id':req.query.shopId};
			res.render('admin/pages/addproduct',data); 
			console.log(response);
		}
		else
			res.redirect('admin/pages/shop');
	});
});
/*Add Branch*/
app.get("/admin/addbranch",requireAdminLogin, function(req, res) {
		result=[[]];
		var data={details:result,'shop_id':req.query.shopId};
		res.render('admin/pages/addbranch',data);
});
/** Editing Branch**/
app.get('/admin/editBranch',requireAdminLogin, function(req, res) {
	site.branchDetailsEdit(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.branch),'shop_id':req.query.shopId};
			res.render('admin/pages/addbranch',data); 
		}
		else
			res.redirect('admin/pages/shop');
	});
});
/** Deal category**/
app.get('/admin/dealCategory',requireAdminLogin, function(req, res){
	var data = {'page_js':'Load_DataTables("dealcategory")'};
	res.render('admin/pages/dealcategory',data);
});
/** Deal Add category**/
app.get('/admin/addDealcategory',requireAdminLogin, function(req, res){
	/** createing  Category**/
	result=[[]];
	var data={details:result};
	res.render('admin/pages/adddealcategory',data);
});
/** deal Category Edit **/
app.get('/admin/editDealCategory',requireAdminLogin, function(req, res) {
	site.editDealCategory(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.details)};
			res.render('admin/pages/adddealcategory',data); 
		}
		else
			res.redirect('admin/pages/category');
	});
});
/** shop View Branch**/
app.get('/shopadmin/branch',requireShopAdminLogin, function(req, res){
	var dataParams=[{"name": "shop_id", "value":app.locals.encrypt(req.session.shopadmin_id)}];
	var data = {'page_js':'Load_DataTables("branch",'+JSON.stringify(dataParams)+')','shopid':app.locals.encrypt(req.session.shopadmin_id)};
	res.render('shopadmin/pages/branch',data);
});
/*Shop - Add Branch*/
app.get("/shopadmin/addbranch",requireShopAdminLogin, function(req, res) {
		result=[[]];
		var data={details:result,'shop_id':req.query.shopId};
		res.render('shopadmin/pages/addbranch',data);
});
/** Editing Branch**/
app.get('/shopadmin/editBranch',requireShopAdminLogin, function(req, res) {
	site.branchDetailsEdit(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.branch),'shop_id':req.query.shopId};
			res.render('shopadmin/pages/addbranch',data); 
		}
		else
			res.redirect('shopadmin/pages/branch');
	});
});
/** shop View Product**/
app.get('/shopadmin/product',requireShopAdminLogin, function(req, res){
	var dataParams=[{"name": "shop_id", "value":app.locals.encrypt(req.session.shopadmin_id)}];
	var data = {'page_js':'Load_DataTables("product",'+JSON.stringify(dataParams)+')','shopid':app.locals.encrypt(req.session.shopadmin_id)};
	res.render('shopadmin/pages/product',data);
});
/* Shop Add Product*/
app.get("/shopadmin/addProduct",requireShopAdminLogin, function(req, res) {
		result=[[]];
		resultp=[];
		var data={details:result,product_image:resultp,'shop_id':req.query.shopId};
		res.render('shopadmin/pages/addproduct',data);
});

/** Shop Editing product**/
app.get('/shopadmin/editProduct',requireShopAdminLogin, function(req, res) {
	site.productDetailsEdit(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.product),product_image:(response.product_image),'shop_id':req.query.shopId};
			res.render('shopadmin/pages/addproduct',data); 
		}
		else
			res.redirect('shopadmin/pages/product');
	});
});
/*Shop - View deal*/
app.get("/shopadmin/deal",requireShopAdminLogin, function(req, res) {
		result=[[]];
		var dataParams=[{"name": "shop_id", "value":app.locals.encrypt(req.session.shopadmin_id)}];
		var data = {'page_js':'Load_DataTables("deal",'+JSON.stringify(dataParams)+')','shop_id':app.locals.encrypt(req.session.shopadmin_id)};
		res.render('shopadmin/pages/deal',data);
});
/** Deal Editing shop**/
app.get('/shopadmin/editdeal',requireShopAdminLogin, function(req, res) {
	site.dealDetailsEdit(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.deal),'shop_id':req.query.shopId,category:(response.category)};
			res.render('shopadmin/pages/adddeal',data); 
		}
		else
			res.redirect('shopadmin/pages/deal');
	});
});
/*shop - Add deal*/
app.get("/shopadmin/adddeal",requireShopAdminLogin, function(req, res) {
		site.dealDetailsAdd(req,function(response){
		if(response.status=='success')
		{ 	
			result=[[]];
			var dataParams=[{"name": "shop_id", "value":app.locals.encrypt(req.session.shopadmin_id)}];
			var data = {'page_js':'Load_DataTables("deal",'+JSON.stringify(dataParams)+')','shop_id':app.locals.encrypt(req.session.shopadmin_id),details:result,category:(response.cate)};
			res.render('shopadmin/pages/adddeal',data);
		}
		else{
			res.redirect('shopadmin/pages/deal');
		}
	});
});
/*branch - Add deal*/
app.get("/branchadmin/adddeal",requireBranchAdminLogin, function(req, res) {
		result=[[]];
		//var data={details:result,'shop_id':req.query.shopId};
		
		site.dealDetailsAdd(req,function(response){
		if(response.status=='success')
		{ 	
			result=[[]];
			var data={details:result,'branch_id':req.query.branchId,category:(response.cate)};;
			res.render('branchadmin/pages/adddeal',data);
		}
		else{
			res.redirect('branchadmin/pages/deal');
		}
	});
		
});


/*Branch - View deal*/
app.get("/branchadmin/deal",requireBranchAdminLogin, function(req, res) {
		result=[[]];
		var dataParams=[{"name": "branch_id", "value":app.locals.encrypt(req.session.branchadmin_id)}];
		var data = {'page_js':'Load_DataTables("branchdeal",'+JSON.stringify(dataParams)+')','branch_id':app.locals.encrypt(req.session.branchadmin_id)};
		res.render('branchadmin/pages/deal',data);
});

/* Deal Editing branch */
app.get('/branchadmin/editdeal',requireBranchAdminLogin, function(req, res) {
	site.dealbranchDetailsEdit(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.deal),'branch_id':req.query.branchId,category:(response.category)};
			res.render('branchadmin/pages/adddeal',data); 
		}
		else
			res.redirect('branchadmin/pages/deal');
	});
});
app.get('/shopadmin/change-password',requireShopAdminLogin, function(req, res){
	/** change Password**/
	res.render('shopadmin/pages/change-password');
});
app.get('/branchadmin/change-password',requireBranchAdminLogin, function(req, res){
	/** change Password**/
	res.render('branchadmin/pages/change-password');
});
/** setings**/
app.get('/shopadmin/editsettings',requireShopAdminLogin, function(req, res) {
	site.shopSetingsDetails(req,function(response){
		//console.log(response);
		if(response.status=='success')
		{ 
			var data = {details:(response.details)};
			res.render('shopadmin/pages/settings',data); 
		}
		else
			res.redirect('shopadmin/pages/dashboard');
	});
});
/** Add Branch Admin**/
app.get('/admin/addbranchAdmin',requireAdminLogin, function(req, res) {
	site.branchAdminDetailsAdd(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.badmin),branch:(response.branch),'shop_id':req.query.shopId};
			res.render('admin/pages/addbranchAdmin',data); 
		}
		else
			res.redirect('admin/pages/shop');
	});
});
/** Edit Branch Admin**/
app.get('/admin/editbranchAdmin',requireAdminLogin, function(req, res) {
	site.branchAdminDetailsEdit(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.badmin),branch:(response.branch),'shop_id':req.query.shopId};
			res.render('admin/pages/addbranchAdmin',data); 
		}
		else
			res.redirect('admin/pages/shop');
	});
});
/** shop Branch Admin View**/
app.get('/shopadmin/branchadmin',requireShopAdminLogin, function(req, res){
	var dataParams=[{"name": "shop_id", "value":app.locals.encrypt(req.session.shopadmin_id)}];
	var data = {'page_js':'Load_DataTables("branchadmin",'+JSON.stringify(dataParams)+')','shopid':app.locals.encrypt(req.session.shopadmin_id)};
	res.render('shopadmin/pages/branchadmin',data);
});
/** Shop Add Branch Admin**/
app.get('/shopadmin/addbranchAdmin',requireShopAdminLogin, function(req, res) {
	site.branchAdminDetailsAdd(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.badmin),branch:(response.branch),'shop_id':req.query.shopId};
			res.render('shopadmin/pages/addbranchAdmin',data); 
		}
		else
			res.redirect('shopadmin/pages/branchaddmin');
	});
});
/** Edit Branch Admin**/
app.get('/shopadmin/editbranchAdmin',requireShopAdminLogin, function(req, res) {
	site.branchAdminDetailsEdit(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.badmin),branch:(response.branch),'shop_id':req.query.shopId};
			res.render('shopadmin/pages/addbranchAdmin',data); 
		}
		else
			res.redirect('shopadmin/pages/branchadmin');
	});
});
/** Add Branch Staff**/
app.get('/admin/addStaff',requireAdminLogin, function(req, res) {
	site.staffAdd(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.staff),branch:(response.branch),'shop_id':req.query.shopId};
			res.render('admin/pages/addStaff',data); 
		}
		else
			res.redirect('admin/pages/shop');
	});
});
/** Edit Branch Staff**/
app.get('/admin/editStaff',requireAdminLogin, function(req, res) {
	site.staffEdit(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.staff),branch:(response.branch),'shop_id':req.query.shopId};
			res.render('admin/pages/addStaff',data); 
		}
		else
			res.redirect('admin/pages/shop');
	});
});
app.get('/admin/shop-details',requireAdminLogin, function(req, res) {
	//res.render('pages/shop-details'); 
	if(req.query.type=="product")
	{
		var data = {'page_js':'Load_multiTab("product","'+req.query.shopId+'")','shopid':req.query.shopId};
		res.render('admin/pages/shop-details',data);
	}
	 else if(req.query.type=="branch")
	{
		var data = {'page_js':'Load_multiTab("branch","'+req.query.shopId+'")','shopid':req.query.shopId};
		res.render('admin/pages/shop-details',data);
		
	}
	 else if(req.query.type=="branchadmin")
	{
		var data = {'page_js':'Load_multiTab("branchadmin","'+req.query.shopId+'")','shopid':req.query.shopId};
		res.render('admin/pages/shop-details',data);
	}
	else if(req.query.type=="staff")
	{
		var data = {'page_js':'Load_multiTab("staff","'+req.query.shopId+'")','shopid':req.query.shopId};
		res.render('admin/pages/shop-details',data);
	}
	else{
		var data = {'page_js':'Load_shopView()','shopid':req.query.shopId};
		res.render('admin/pages/shop-details',data);
	}
});
/** setings**/
app.get('/admin/editsettings',requireAdminLogin, function(req, res) {
	site.setingsDetails(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.details)};
			res.render('admin/pages/settings',data); 
		}
		else
			res.redirect('admin/pages/dashboard');
	});
});
/** Category Edit **/
app.get('/admin/editCategory',requireAdminLogin, function(req, res) {
	site.editCategory(req,function(response){
		if(response.status=='success')
		{ 
			var data = {details:(response.details)};
			res.render('admin/pages/addcategory',data); 
		}
		else
			res.redirect('admin/pages/category');
	});
});
app.get('/admin/change-password',requireAdminLogin, function(req, res){
	/** change Password**/
	res.render('admin/pages/change-password');
});
/** Ajax actions : Front-End **/
app.post('/ajax-actions', function(req, res) {
	if(typeof(res.req.body.process)!='undefined'){
		console.log(res.req.body.process);
		api.ajaxApi(req,res,function(err,result){
			res.setHeader("Content-Type", "text/json");
			res.setHeader("Access-Control-Allow-Origin", "*");
			console.log(result);
			//console.log(err);
			if (err)
				var json_data={status:'error',result:'',message:err};
			else
				var json_data={status:'success',result:result};
			
			res.send(json_data); 
		}); 
	}
});
app.post('/admin/ajax-actions', function(req, res) {
	if(typeof(res.req.body.process)!='undefined'){
		console.log(res.req.body.process);
		api.ajaxApi(req,res,function(err,result){
			res.setHeader("Content-Type", "text/json");
			res.setHeader("Access-Control-Allow-Origin", "*");
			console.log(result);
			//console.log(err);
			if (err)
				var json_data={status:'error',result:'',message:err};
			else
				var json_data={status:'success',result:result};
			
			res.send(json_data); 
		}); 
	}
});
/** Ajax actions : Mobile **/
app.post('/ajax-actionsMobile', function(req, res) {
	if(typeof(res.req.body.process)!='undefined'){
		mobileapi.ajaxMobileApi(req,res,function(err,result){
			res.setHeader("Content-Type", "text/json");
			res.setHeader("Access-Control-Allow-Origin", "*");
			if (err)
				var json_data={status:'error',result:'',message:err};
			else
				var json_data={status:'success',result:result};
			
			res.send(json_data); 
		});
	}
});
/** shop Ajax actions : Front-End **/
app.post('/shopadmin/ajax-actions', function(req, res) {
	if(typeof(res.req.body.process)!='undefined'){
		api.ajaxApi(req,res,function(err,result){
			res.setHeader("Content-Type", "text/json");
			res.setHeader("Access-Control-Allow-Origin", "*");
			if (err)
				var json_data={status:'error',result:'',message:err};
			else
				var json_data={status:'success',result:result};
			
			res.send(json_data); 
		});
	}
});
/** Branchadmin Ajax actions : Front-End **/
app.post('/branchadmin/ajax-actions', function(req, res) {
	if(typeof(res.req.body.process)!='undefined'){
		api.ajaxApi(req,res,function(err,result){
			res.setHeader("Content-Type", "text/json");
			res.setHeader("Access-Control-Allow-Origin", "*");
			if (err)
				var json_data={status:'error',result:'',message:err};
			else
				var json_data={status:'success',result:result};
			
			res.send(json_data); 
		});
	}
});
/** adminLogout **/
app.get('/admin/logout-admin',requireAdminLogin, function (req, res) {
	req.session.destroy() ;
	res.redirect('/');
	//return res.redirect('/admin');
}); 
/** Logout **/
app.get('/logout-admin',requireAdminLogin, function (req, res) {
	req.session.destroy() ;
	res.redirect('/');
}); 
/** Shop Logout **/
app.get('/shopadmin/logout-admin',requireShopAdminLogin, function (req, res) {
	req.session.destroy() ;
	//res.redirect('/shopadmin');
	console.log("test");
	res.redirect('/');
	//return res.redirect('/shopadmin');
}); 
/** Branch Logout **/
app.get('/branchadmin/logout-admin',requireBranchAdminLogin, function (req, res) {
	req.session.destroy() ;
	//res.redirect('/branchadmin');
	//res.redirect('/');
	return res.redirect('/branchadmin');
});
/** Datatable listing **/
app.get('/admin/data_tables/:page', function(req, res) {
	var page_name = req.params.page;
	var params=req.query;
	dataTable.fetchRecords(page_name,params,function(response){
		res.json(response);
	});	 
});
/** Branch Datatable listing **/
app.get('/branchadmin/data_tables/:page', function(req, res) {
	var page_name = req.params.page;
	var params=req.query;
	dataTable.fetchRecords(page_name,params,function(response){
		res.json(response);
	});	 
});
/** Shop Datatable listing **/
app.get('/shopadmin/data_tables/:page', function(req, res) {
	var page_name = req.params.page;
	var params=req.query;
	dataTable.fetchRecords(page_name,params,function(response){
		res.json(response);
	});	 
});
/*notification*/
app.get('/admin/notification',requireAdminLogin, function(req, res){
	var data = {'page_js':'Load_DataTables("notification")'};
	res.render('admin/pages/notification',data);
});
/*add notification*/
app.get('/admin/addnotification',requireAdminLogin, function(req, res){
	site.addNotification(req,function(response){
		if(response.status=='success')
		{ 
			result=[[]];
			var data = {details:result,shop:(response.details)};
			res.render('admin/pages/addnotification',data);
		}
		else
			res.redirect('admin/pages/notification');
	});
});
/*Edit notification*/
app.get('/admin/editnotification',requireAdminLogin, function(req, res){
	console.log(req.params);
	site.editNotification(req,function(response){

		if(response.status=='success')
		{ 
			console.log(response);
			console.log(response.notelist[0].shop_id);
			console.log(response.notelist[0].product_id);
			var pid=response.notelist[0].product_id;
			var sid=response.notelist[0].shop_id;
			//var data = {'page_js':'Load_DataTables("notification")'};
			var data = {details:(response.notelist),shop:(response.details),'page_js':'get_product('+sid+','+pid+')'};
			res.render('admin/pages/addnotification',data);
		}
		else
			res.redirect('admin/pages/notification');
	});
});
app.get('/admin/viewnotification',requireAdminLogin, function(req, res){

	console.log(req.params);
	site.viewNotification(req,function(response){

		if(response.status=='success')
		{ 
			
			var data = {details:(response.notelist)};
			res.render('admin/pages/viewnotification',data);
		}
		else
			res.redirect('admin/pages/notification');
	});
});
/* notification Approcal*/
app.get('/admin/notificationapprove',requireAdminLogin, function(req, res){
	var data = {'page_js':'Load_DataTables("notificationapprove")'};
	res.render('admin/pages/notificationapprove',data);
});
/** Check Shop Admin Login**/
function requireShopAdminLogin (req, res, next) {
	if (req.session && req.session.shopadmin_id) { // Check if session exists
		app.locals.session_shopadmin_id= req.session.shopadmin_id;   
		app.locals.session_shop_name = req.session.shop_name; 
		next();		
	} else{
		app.locals.session_shopadmin_id = '';   
		app.locals.session_shop_name = '';   
		//res.redirect('/shopadmin');
		res.redirect('/');
	}
};
/** Check Branch Admin Login**/
function requireBranchAdminLogin (req, res, next) {
	if (req.session && req.session.branchadmin_id) { // Check if session exists
		app.locals.session_branchadmin_id= req.session.branchadmin_id;   
		app.locals.session_branch_name = req.session.branch_name; 
		next();		
	} else{
		app.locals.session_branchadmin_id = '';   
		app.locals.session_branch_name = '';   
		//res.redirect('/branchadmin');
		res.redirect('/');
	}
};
/** Check Admin Login**/
function requireAdminLogin (req, res, next) {
	if (req.session && req.session.admin_id) { // Check if session exists
		app.locals.session_admin_id = req.session.admin_id;   
		app.locals.session_admin_name = req.session.admin_name; 
		next();		
	} else{
		app.locals.session_admin_id = '';   
		app.locals.session_admin_name = '';   
		res.redirect('/');
	}
};
function checkAdminLogin (req, res, next) {
	if (app.locals.session_admin_id ) { // Check if session exists
		res.redirect('/dashboard');	
	} 
};
function setSession (req, res, next){
	if (req.session && req.session.user_id) { // Check if session exists
		app.locals.session_user_id = req.session.user_id;   
		app.locals.session_user_email = req.session.user_email;
		next();		
	} else {
		app.locals.session_user_id = '';   
		app.locals.session_user_email = '';   
		next();		
	}
	req.session.merkens=typeof req.session.merkens!="undefined"?req.session.merkens:[];
	app.locals.merkenChecked =typeof req.session.merkens!="undefined"?req.session.merkens:[];
	req.session.advanceSearchParams =typeof req.session.advanceSearchParams!="undefined"?req.session.advanceSearchParams:'';
	app.locals.advanceSearchParams =typeof req.session.advanceSearchParams!="undefined"?req.session.advanceSearchParams:'';
};
app.listen(1989);
//console.log('8080 is the magic p
/*if(site_url=='ip-172-31-22-11' || site_url=='Lifo-10' || site_url=='lifo10' || site_url=='lifo-server'){
	app.listen(1989);
	console.log(1989);
} else{
	app.listen(80);
}*/